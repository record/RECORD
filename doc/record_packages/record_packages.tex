\documentclass{article}

\usepackage{graphicx}
\usepackage{listings}
\lstset{language=C++}
\usepackage{hyperref}
\usepackage{amsthm}


\theoremstyle{remark}
\newtheorem{tutorial}{Tutorial }


\title{Utils packages for RECORD projects.}

%%%%%%%%%%%%
\begin{document}

\maketitle

\tableofcontents

%%%%%%%%%%%%
\section{Introduction}

RECORD \cite{bergez.12} is a platform for modeling and computer simulation
dedicated to the study of agro-ecosystems. It is based on the VLE software
\cite{quesnel.09} which is a multi-modeling and simulation platform.
This document quicly presents the vle packages used by RECORD development team
which are common features and utility functions shared between projects.

\section{record.eigen package}

The package {\tt record.eigen} is an overlay package for the c++ linear algebra 
library \href{http://eigen.tuxfamily.org/index.php?title=Main_Page}{Eigen}.
Two hpp files (UtilsEigen.hpp and UtilsRand.hpp) provide :
\begin{itemize}
  \item variance, covariance matrices computation
  \item translation between vle values and Eigen matrices
  \item generation of random matrices
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{record.optim package}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The package {\tt CallForOptim} provides optimization methods and generic
atomic models for calling optimization processes. Two types of optimizations
are available in this package : function optimization and reinforcement
learning agents.
\begin{itemize}
\item Given a function $f : I_d \to R$, where ${I_d}$ is the domain of input
of $f$, the function optimization problem consists in finding 
$x_{min} \in I_d$ such that: $x_{min} = argmin_{x \in I_d}(f(x))$. In this
type of optimization, the simulation model is actually seen as a
black-box function (eg. model ExBohachevsky.vpz, section \ref{exBoha}). 
\item Reinforcement learning \cite{sutton.98} consists in learning a policy from
observations and rewards provided by the model (the model is also called 
environment).  A policy is a function that gives the next action to perform,
during simulation, in order to optimize a function that aggregates the 
observed rewards over the simulation. This type of optimization 
requires intimate relations between the agent that learns the policy and the
model. Thus, the architecture is little different from the architecture for 
function optimization (see e.g. model ExMinesEnvironment.vpz, section
\ref{exMines}).
\end{itemize}

The use-cases rely on atomic models and interfaces that are described in the
next section. The available function optimization methods in this package are
listed in the section \ref{FOmethods} and RL methods are listed in 
section \ref{RLmethods}.



%%%%%%%%%%%%
\subsection{Global architecture}
\label{architecture}
%%%%%%%%%%%%

\begin{figure}[!h]
\begin{center} 
\includegraphics[scale=0.60]{figures/vle_models.pdf}
\caption{\label{vle_models} In use-case $a$, the user performs a 
simulation optimization by mean of function optimization. 
In use-case $b$, the user performs a simulation optimization by mean of
reinforcement learning.}
\end{center}
\end{figure}

The first objective of this package is to provide methods for optimization.
For this, atomic models ({\tt FOexpe} and {\tt RLexpe}) 
are provided that model the behaviour of optimization processes, mainly the
succession of calls of the model to optimize. Configuration of these processes 
is given directly in the conditions of these models, eg. the user model which
is used for the optimization, the parameters for optimization, etc..

The second objective of this package is to help in developping
optimization methods. Thus, the role of the developper is to implement one
of the two interfaces : {\tt FOmethod} for a function optimization method 
or {\tt RLmethod} for a reinforcement learning agent.


The {\tt FOmethod} interface contains, among others, the function :
\begin{lstlisting}
virtual bool optimizeStep(unsigned int current_step);
\end{lstlisting}

The {\tt RLmethod} interface, which is the one proposed by RL-glue
\cite{tanner.JMLR09} contains, among others, the functions :
\begin{lstlisting}
virtual const vv::Value& agent_start(
                    const vv::Value& observation);
virtual const vv::Value& agent_step(double reward,
                    const vv::Value& observation);
virtual void agent_episode_ends(double reward);
virtual bool is_last_episode() const;
\end{lstlisting}
Note that one simulation of the user model represents one episode. Note also
that RL methods require to be embedded in the user model, this is done thanks 
to the templace atomic model {\tt RLagent}.

%%%%%%%%%%%%
\subsection{Function optimization methods}
\label{FOmethods}
%%%%%%%%%%%%

%%%%%%
\subsubsection{Random optimizer}
%%%%%%

The random optimizer strategy consists in generating $n$ random vectors
into the input space and to take the best input vector according to the 
output.  

%%%%%%
\subsubsection{P2 algorithm}
%%%%%%

P2 \cite{crespo.08} is a family of methods for simulation based 
continuous optimization for stochastic problems. They allow to optimise expected
value or a quantile value of simulation\footnote{Multi objective optimization 
is not available yet.}. These algorithms
are based on hierarchical decomposition procedure. 
They aim at partitionning the decision space into smaller ones, and continuing 
the research into the potential optimal ones. 
The main issues are to evaluate regions of continuous decision variables,
divide one region into smaller ones, and select one region 
among all as the one that the algorithm will investigate further. 

%%%%%%
\subsubsection{Covariance Matrix Adaptation Evolution Srategy}
%%%%%%

CMA-ES algorithms \cite{shark08} are methods for continuous optimization
problems. They rely on a variance-covariance matrix over the real
inputs which is used to mutate candidates. This matrix globally determines the
best ``direction'' for candidate generation into the search domain. 
(1+1) CMA-ES is an hill climbing method since only one new 
candidate is generated from the best promising candidate generated so far.

Note that this optimizer requires the use of the Shark library. 

%%%%%%%%%%%%
\subsection{Reinforcement learning agents}
\label{RLmethods}
%%%%%%%%%%%%
%%%%%%
\subsubsection{SARSA (State-Action-Reward-State-Action)}
%%%%%%
SARSA maintains a q-matrix, ie a structure that gives the expected 
reward value by performing an action $a$ in a state $s$.
The q-matrix is updated with the following rule :
$Q(s_1,a_1) \leftarrow Q(s_1,a_1) + \alpha [ r + \gamma Q(s_2,a_2) - Q(s_1,a_1)
]$.
This update is done each time the environement is in state $s_1$, when
the agent has performed action $a_1$ and that this action has lead the
environment to be in state $s_2$ . Thus, the expected reward for pair
$(s_1,a_1)$ depends on the immediate reward $r$, the current 
expected reward $Q(s_1,a_1)$ and the expected reward of
performing action $a_2$ from state $s_2$. In order to explore the state-action
space, an $\epsilon$-greedy policy is implemented. This exploring policy 
is used to select the next action to perform $a_2$. $\alpha$ and $\gamma$ are
paremeters of the method SARSA.

This reinforcement learning agent implementation is the one proposed in the
book of Sutton and Barto \cite{sutton.98}.

%%%%%%
\subsubsection{Xitek}
%%%%%%

Xitek \cite{garcia.MODSIM99} is an implementation of the reinforcement learning
method R-learning \cite{sutton.98} adapted to simulation optimization. 
With Xitek, the problem of policy learning can moreover be broken down.
Indeed, the policy can be split into multiple state-action spaces, each one is
called ``stage'' and represent a particular problem.

%%%%%%%%%%%%
\subsection{Examples}
%%%%%%%%%%%%

Examples can be run by launching the simulation of the model  
{\tt Experiments.vpz} provided into the package.
For each example, appropriate conditions and dynamic have to be attached to the
atomic model {\tt Experiments}.

%%%%%%
\subsubsection{Using P2 for optimizing the Bohachevsky function}
\label{exBoha}
%%%%%%

\begin{figure}[!h]
\begin{center} 
\includegraphics[scale=0.50]{figures/boha.jpg}
\caption{\label{boha} Bohachevsky function value in $R^{2}$.}
\end{center}
\end{figure}

The bohachevsky function is defined by $[-100;100]^2 \to R^{+}$, the minimal
value of this function is 0 and it is taken for $x_{min} = (0,0)$.

The model {\tt ExBohachevsky.vpz} is a simulation atomic model, with no 
dynamic behaviour, that computes the value of the function for a given
$x=(x_1,x_2)$. $x$ values are given in the condition list of the model, 
and the function value is observed on port $y$.

%%%%%%
\subsubsection{Using SARSA for learning a policy on mines environment}
\label{exMines}
%%%%%%

This example is proposed by the developpers of the library RL-Glue 
\cite{tanner.JMLR09}. The goal is to learn a policy for moving an agent into a
2D grid where cell can contain mines. 

The simulation of the model {\tt ExMinesEnvironment.vpz} stops if either the
agent arrives into a goal state (the agent obtains a reward of +1000) or if the 
agent arrives into a state whith a mine (the agent obtains a reward of -100), 
see figure \ref{mines}.

A policy defines the direction to take (East, West, North or South) for each
cell in order to maximise the reward. 

\begin{figure}[!h]
\begin{center} 
\includegraphics[scale=0.60]{figures/mines.pdf}
\caption{\label{mines} Mines Environment 2D grid. Red cells represent mines and
the green cell represent the goal to reach.}
\end{center}
\end{figure} 
 
\subsection{Implementation notes}

When implementing an optimization method (see section \ref{architecture}),
sharing common tools is an efficient practice. This section is intended to
describe tools that are already available.

\begin{itemize}
  \item \underline{Accumulators} are tools for incremental computation of 
  statistics (mean, standard deviation, quantiles, etc..). See e.g. Boost 
  accumulators library. For example :
\begin{lstlisting}
using namespace cfo::tools;
Accu<MonoDim, MEAN_VAR> acc;
acc.insert(1); acc.insert(5);
acc.insert(4); acc.insert(3.6);
cout << acc.mean() << `` equals `` << 3.4 << endl;
cout << acc.moment2() << `` equals `` <<  13.74 << endl;
\end{lstlisting}   
  
\end{itemize}
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dependencies between packages}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[!h]
\begin{center} 
\includegraphics[scale=0.40]{figures/pkgDependencies.pdf}
\caption{\label{pkgDependencies} Package dependencies of utils record pkgs and 
some other record pkgs.}
\end{center}
\end{figure} 

\bibliographystyle{plain}
\bibliography{bib}


\end{document}
