#!/bin/bash

#set -e # Abort script at first error, when a command exits with non-zero status (except in until or while loops, if-tests, list constructs) #pose soucis en cas d'echec des test des pkg vle
set -u # Attempt to use undefined variable outputs error message, and forces an exit
set -o pipefail # Causes a pipeline to return the exit status of the last command in the pipe that returned a non-zero return value.
set -o posix # Change the behavior of Bash, or invoked script, to conform to POSIX standard.
#set -x # Similar to -v, but expands commands


show_help() {
  cat <<EOM
  Usage: $(basename "$0") [ -r REGEX ] [ -o FILENAME ] [-c] [-v] [-h]
  OPTIONAL
    -r REGEX     REGEX used to find files to be merged
    -o FILENAME  Output filename
    -c           Activate header checking
    -v           Activate verbose message for debug
    -h           Display help page
    
  EXEMPLE
    ./$(basename "$0") -cv -r "*.csv" -o newfile.csv
EOM
}

print_options () {
    printf '\e[34m\nUsing options:\n'
    printf "\tfile_regex : %s\n" "${file_regex}"
    printf "\tnew_file : %s\n" "${new_file}"
    printf "\tdo_check_headers : %s\n" "${do_check_headers}"
    printf "\tdebuginfo : %s\n" "${debuginfo}"
#    printf "\tprintenv : %s\n" "$(printenv)"
    printf "\n\e[39m"
}

set_script_options() {

    do_check_headers=false
    debuginfo=false

    local OPTIND
    while getopts "hcvr:o:" opt
    do
        case "$opt" in
          h) show_help
             exit 0 ;;
          v) debuginfo=true ;;
          c) do_check_headers=true ;;
          r) file_regex="${OPTARG}" ;;
          o) new_file="${OPTARG}" ;;
          \?) show_help
              exit 0 ;;
        esac
    done

    if ${debuginfo} ; then 
      print_options
    fi
}

check_all_headers() {
    local previous_header
    local firstline
    for singlefile in $1
    do
        if ${debuginfo} ; then 
            printf 'checking header of file: %s\n' "${singlefile}"
        fi
        firstline=$(head -1 "${singlefile}")
        if ${debuginfo} ; then 
            printf 'current file header line: %s\n' "${firstline}"
        fi
        : "${previous_header:=$firstline}"
        if [ ! "$firstline" == "$previous_header" ]; then
            printf "Warning!!! different header line for file %s\n" "$singlefile"
            exit 1
        fi
        previous_header=${firstline}
    done
}

merge_all_csv () {
    if $3 ; then 
        printf '\nWritting new output file: %s\n' "$2"
    fi
    awk 'FNR==1 && NR!=1{next;}{print}' $1 >& "$2"
}


mergecsv_main_script() {
    set_script_options "$@"
    local allfiles
    allfiles=$(ls -1 $file_regex)
    if ${debuginfo} ; then 
        printf 'all files:\n%s\n\n' "$allfiles"
    fi
    if ${do_check_headers} ; then 
        check_all_headers "$allfiles" || exit 1
    fi
    merge_all_csv "$allfiles" "$new_file" ${debuginfo} || exit 1
}

mergecsv_main_script "$@"
