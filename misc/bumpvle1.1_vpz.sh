#!/bin/bash 
#REM This is some UNIX command line to perform automatic bump into vle-1.1 for vle 1.0.x vpz files

################################################################################
#REM vpz replicas item removed
find . -name '*.vpz' |xargs sed -i "/replicas seed=/d"


################################################################################
#REM  vpz view now depends on vle.output pkg
#REM  handle any plugin of vle.output pkg (as plugin name remain unchanged)
find . -name '*.vpz' |xargs perl -pi -e 's/format="local" plugin=/format="local" package="vle.output"  plugin=/g'


################################################################################
#REM update dynamics 
find . -name '*.vpz' |xargs perl -pi -e 's/ model="" / /g'
find . -name '*.vpz' |xargs perl -pi -e 's/ type="local"  /  /g'
#REM Warning! only use this line if you don't have mandatory info in dynamics model field (as it will be removed)
#REM Meaning that it is not suited in case you use several dynamics in a single library 
#REM you might want to check before use what are your vpz are using with the command : 
# grep -ri --include="*.vpz" package= .
find . -name '*.vpz' |xargs perl -pi -e 's/ model="([^"]*)" package=/ package=/g'



################################################################################
#REM update vle version reference in Version field of project 
find . -name '*.vpz' |xargs perl -pi -e 's/vle_project version="([^"]*)"/vle_project version="1.1.x"/g'


################################################################################
#REM update dtd reference (auto when opening vpz under gvle-1.1)
find . -name '*.vpz' |xargs perl -pi -e 's/vle-(\d).(\d).0.dtd/vle-1.1.0.dtd/g'

