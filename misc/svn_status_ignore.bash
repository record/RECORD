#!bash
#####
## Script for ignoring not versioned files on commande
## svn status. Ignored files are listed into .svnignore file 
## into current directory.
## USING : bash svn_status_ignore.bash
#####

fileIgnore='.svnignore'
command='svn status '  

for line in $(cat $fileIgnore); do 
	command="$command | grep -v $line ";
done

#echo "Command that will be executed : $command"

eval $command

