#=====================================================================
# Recette docker pour vle-2.0 et les paquets d'extensions et de record
#=====================================================================

# exemple de commande build de l'image :
## docker build -t record -f Dockerfile_vle-2.0-record.i386.txt .
## docker build -t registry.forgemia.inra.fr/record/record.i386:ub2004_vle20 -f Dockerfile_vle-2.0-record.i386.txt .

# exemple de commande run d'un containeur :
## docker run -v "$(pwd):/work" record.i386:latest
## docker run -v "$(pwd):/work" record.i386:latest bash -c "vle -P 2CV 2CV-parcelle.vpz"
## docker run -v "$(pwd):/work" record.i386:latest R --vanilla -e 'library(rvle);mm = new("Rvle", pkg="2CV", file="2CV-parcelle.vpz");show(mm);rvle.listConditions(mm@sim);getDefault(mm, "condDecFSA.SowingDay");typeof(getDefault(mm, "condDecFSA.SowingDay"));names(getDefault(mm, "outputplugin"));mm = run(mm, outputplugin = c(vueLAI = "storage"));head(results(mm)[[1]]);'
## docker run -v "$(pwd):/work" record.i386:latest bash -c "mpirun --allow-run-as-root -d -v -np 4 cvle --withoutspawn -i planExp.csv -P 2CV 2CV-parcelle.vpz"


# exemple de lancement d'un bash interactif
## docker run -it -v "$(pwd):/work" record.i386:latest bash


# exemple pour publication sur forgemia
## docker push registry.forgemia.inra.fr/record/record.i386:ub2004_vle20


##############
## Build stage
##############
FROM i386/ubuntu:18.04 as build

WORKDIR /data

ENV DEBIAN_FRONTEND=noninteractive
ENV VLE_HOME=/data
ENV VLE_INSTALL_DIR=/vle_install
ENV PATH=${VLE_INSTALL_DIR}/bin:$PATH
ENV LD_LIBRARY_PATH=${VLE_INSTALL_DIR}/lib:${LD_LIBRARY_PATH:-}
ENV PKG_CONFIG_PATH=${VLE_INSTALL_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH:-}
ENV VLE_BASEPATH=${VLE_INSTALL_DIR}

ENV vle_version="f8964c9d46b33265aaa7969941e8536d6d956e62"
ENV rvle_version="tags/v2.0.2 -b v2.0.2"
ENV packages_version="82ee9dfbae1fb24cdbdb39ea9ff7eee19eb062fe"
ENV recordb_version="59608cf39ce147048aaafc4c516d9bc04fcd22aa"

RUN mkdir /data/pkgs-2.0

# install build dependencies (for vle, rvle, packages, recordb)
RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y -V \
            libmagick++-dev \
            git \
            ca-certificates \
            pkg-config \
            make \
            cmake \
            gcc \
            g++ \
            gfortran \
            python-dev \
            libxml2-dev \
            curl \
            libboost-dev \
            libboost-mpi-dev \
            libboost-test-dev \
            libboost-date-time-dev \
            libfontconfig1-dev \
            libcurl4-openssl-dev \
            libpng-dev \
            libssl-dev \
            libblas-dev \
            liblapack-dev \
            libfreetype6-dev \
            libtiff5-dev \
            libjpeg-dev \
            libv8-dev \
            libcairo2-dev \
            libxt-dev \
            r-base \
            r-base-core \
            r-recommended \
            r-cran-runit \
            pandoc \
   && rm -rf /var/lib/apt/lists/*


RUN R -e "install.packages(c('shiny', 'shinyjs', 'chron', 'readr', 'dygraphs', 'xts', 'DT', 'pander', 'animation', 'fields', 'knitr', 'RColorBrewer', 'timevis', 'rmarkdown', 'shinyscreenshot', 'webshot'), repos='http://cran.rstudio.com/')" 



# install vle
RUN git clone https://github.com/vle-forge/vle.git \
    && cd vle \
    && git checkout ${vle_version} \
    && mkdir build \
    && cd build \
    && cmake -DCMAKE_INSTALL_PREFIX=${VLE_INSTALL_DIR} -DWITH_CVLE=ON -DWITH_GVLE=OFF -DCMAKE_BUILD_TYPE=MinSizeRel ..\
    && make -j4\
    && make install \
    && make test || : \
    && vle --restart \
    && cd ../.. \
    && rm -fr vle


# install rvle
RUN git clone https://github.com/vle-forge/rvle.git \
    && cd rvle \
    && git checkout ${rvle_version} \
    && cd .. \
    && R CMD build rvle \
    && R CMD INSTALL --build rvle \
    && rm -rf rvle rvle_2.0.2-7.tar.gz rvle_2.0.2-7_R_x86_64-pc-linux-gnu.tar.gz

# install VLE extension packages
RUN git clone https://github.com/vle-forge/packages.git \
  && cd packages \
  && git checkout ${packages_version} \
  && vle -P vle.reader configure build test \
  && vle -P vle.tester configure build test \
  && vle -P vle.discrete-time configure build test \
  && vle -P vle.extension.decision configure build test \
  && vle -P vle.extension.dsdevs configure build test \
  && vle -P vle.extension.fsa configure build test \
  && vle -P vle.ode configure build test \
  && vle -P vle.discrete-time.decision configure build test \
  && cd .. \
  && rm -fr packages

# install VLE recordb packages
RUN git clone https://forgemia.inra.fr/record/RECORD.git \
  && cd RECORD \
  && git checkout ${recordb_version} \
  && cd pkgs \
  && vle -P record.meteo configure build test \
  && vle -P 2CV configure build test \
  && vle -P ext.Eigen configure build test \
  && vle -P record.eigen configure build test \
  && vle -P vle.discrete-time.generic configure build test \
  && vle -P GenCSVcan configure build test \
  && vle -P gluePhysic configure build test \
  && cd ../.. \
  && rm -fr RECORD




################
## Release stage
################
FROM i386/ubuntu:18.04 as release

ENV DEBIAN_FRONTEND=noninteractive

COPY --from=build /vle_install /vle_install
COPY --from=build /data /data
COPY --from=build /usr/local/lib/R/site-library /usr/local/lib/R/site-library

ENV VLE_HOME=/data
ENV VLE_INSTALL_DIR=/vle_install
ENV PATH=${VLE_INSTALL_DIR}/bin:$PATH
ENV LD_LIBRARY_PATH=${VLE_INSTALL_DIR}/lib:${LD_LIBRARY_PATH:-}
ENV PKG_CONFIG_PATH=${VLE_INSTALL_DIR}/lib/pkgconfig:${PKG_CONFIG_PATH:-}
ENV VLE_BASEPATH=${VLE_INSTALL_DIR}

WORKDIR /work

# install what is required for the bash job that will run there
RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y -V \
          libmagick++-dev \
          dos2unix \
          libxml2-dev \
          openmpi-bin \
          r-base \
          r-base-core \
          r-recommended \
          r-cran-runit \
          pandoc \
    && rm -rf /var/lib/apt/lists/*

RUN chmod -R a+rw /data

HEALTHCHECK --interval=30s --timeout=30s --retries=3 CMD \
	R --vanilla -e 'library(rvle);mm = new("Rvle", pkg="2CV", file="2CV-parcelle.vpz");mm = run(mm, outputplugin = c(vueLAI = "storage"));head(results(mm)[[1]]);' || exit 1

CMD ["vle","--version"] 

