#!/bin/sh

cd $VLE_HOME/pkg*
ls -R $1/exp | grep .vpz$ | xargs -I %  sh -c '{ echo "\n\n\nRunning "%" from pkg "'$1'":\n";vle -P '$1' %; }'
