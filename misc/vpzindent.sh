#!/bin/bash
#Apply xmllint --format command (xml indentation) over all *.vpz files of a folder

if [ "$#" -eq 1 ]; 
  then
    echo "Using directory $1";
    cd $1;
  else
   echo "Using directory "`pwd`;
fi
echo "";

mkdir tmp_vpzindent;

for F in $( find . -name \*.vpz -print ); 
do (
  echo "Indenting file: $F";
  FName="${F##*/}";
  cp $F tmp_vpzindent/$FName;
  echo "Backup file: ./tmp_vpzindent/$FName";
  xmllint --format $F >& tmp_vpzindent/tmp_vpzindent.vpz;
  mv tmp_vpzindent/tmp_vpzindent.vpz $F; 
  echo "Formatting done for $F";
  echo "";
);done

read -n1 -p 'Remove tmp_vpzindent temporary backup folder? [y | n]' DOCLEAN
case $DOCLEAN in  
  y|Y) rm -rf tmp_vpzindent;; 
esac

exit 0;

