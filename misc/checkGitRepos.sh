#!/bin/bash

if [ $# -ne 1 ]
then
 echo ""
 echo " Wrong number of arguments"
 echo ""
 echo "------------------------------------------------------"
 echo " Usage :"
 echo ""
 echo "   bash checkGitRepos.sh <git_repositories> "
 echo ""
 echo "    - <git_repositories> is a file containing the local git repositories"
 echo ""
 echo " This script will scheck the differences between the local repository"
 echo " and a distant repositories. A git fetch is performed. It requires in "
 echo " this order for each repository (= a line into the file) the name of "
 echo " the local branch, the name of the remote, and the the name of the"
 echo " distant branch "
 echo ""
 echo " Examples :"
 echo ""
 echo "   bash checkGitRepos.sh git_repositories_ronan.txt"
 echo ""
 echo " Notes:"
 echo ""
 echo "   An example of a git repositories file :  "
 echo "     /pub/src/vle/vle master vle-forge master "
 echo "     /pub/src/vle/rvle master vle-forge master "
 echo "------------------------------------------------------"
 exit 0;
fi

TMP_DIR=/tmp
LOG_FILE="log.txt"
GIT_REPOS_FILE=${1}


#### Read repos files and check
function gitReposCheck()
{
  while read line
  do

    #split line with into : gitRepos local distant
    OLDIFS=$IFS    
    IFS=" "
    set -- $line
    array=( $@ )
    localDir=${array[0]}
    brLocal=${array[1]}
    depotDistant=${array[2]}
    brDistant=${array[3]}

    cd $localDir
    git fetch ${depotDistant}
    baseToLocal=$( git rev-list ${brLocal}...`git merge-base ${brLocal} ${depotDistant}/${brDistant}` | wc -l ) 
    baseToDistant=$( git rev-list ${depotDistant}/${brDistant}...`git merge-base ${brLocal} ${depotDistant}/${brDistant}` | wc -l ) 
    currentbranch=$( git rev-parse --abbrev-ref HEAD )
    nbfilesModifiedInStatus=$(git status --porcelain | grep -v '??' | wc -l)
    echo "directory:${localDir}, compare branch ${brLocal} to ${depotDistant}/${brDistant} : ${baseToLocal} new commits, ${baseToDistant} missing commits (${currentbranch} is the current branch,  ${nbfilesModifiedInStatus} unstaged files)"
    IFS=$OLDIFS
  done < $GIT_REPOS_FILE
}

### main

gitReposCheck

