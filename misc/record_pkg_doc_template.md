
# Package XX for vle-2.0.0

<img src="../formation/figures/record_logo.png" alt="Record Team" style="width: 200px;"/>

Package short description.

for example :
The [**XX**](http://recordb.toulouse.inra.fr/distributions/2.0/XX.tar.bz2) package provide two models called **YY** and **ZZ** and their corresponding configurations in order to facilitate the use of the model.  
Sample input files are also provided in the */data* folder.

Tests and a sample simulator can be found in the [**XX_test**](http://recordb.toulouse.inra.fr/distributions/2.0/XX_test.tar.bz2) companion package 

# Table of Contents
1. [Package dependencies](#p1)
2. [Atomic model YY](#p2)
    1. [Configuring a YY Model](#p2.1)
        1. [Dynamics settings](#p2.1.1)
        2. [Parameters settings](#p2.1.2)
        3. [Input settings](#p2.1.3)
        4. [Output settings](#p2.1.4)
        5. [Observation settings](#p2.1.5)
        6. [Available configurations](#p2.1.6)
    2. [Details](#p2.2)
2. [Atomic model ZZ](#p3)
    1. [Configuring a YY Model](#p3.1)
        1. [Dynamics settings](#p3.1.1)
        2. [Parameters settings](#p3.1.2)
        3. [Input settings](#p3.1.3)
        4. [Output settings](#p3.1.4)
        5. [Observation settings](#p3.1.5)
        6. [Available configurations](#p3.1.6)
    2. [Details](#p3.2)

---

## Package dependencies <a name="p1"></a>

List of required external packages (with link to distribution when available).

for example :  

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)
* [record.meteo](http://recordb.toulouse.inra.fr/distributions/2.0/record.meteo_test.tar.bz2)

--- 

## Atomic model YY <a name="p2"></a>

Atomic model short description.
must include the class it inherit from (DEVS or extension)

for example :

The **YY** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate ... based on... 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#YY_panel'>Show/Hide Model Details</button>
<div id="YY_panel" class="collapse">

### Configuring a YY Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : XX
* **name** : YY

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</div>



--- 

## Atomic model ZZ <a name="p3"></a>

Atomic model short description.
must include the class it inherit from (DEVS or extension)

for example :

The **ZZ** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate ... based on... 


<details close  >
<summary>Show/Hide Model Details</summary>

### Configuring a ZZ Model <a name="p3.1"></a>

#### Dynamics settings <a name="p3.1.1"></a>

To define the dynamic:

* **library** : XX
* **name** : ZZ

#### Parameters settings <a name="p3.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p3.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p3.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p3.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p3.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p3.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</details>
