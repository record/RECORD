#!/bin/sh

cd $VLE_HOME/pkg*
ls -r | xargs -I % sh -c '{ echo "\n\n\nRunning "'$1' '$2' '$3' '$4'" on pkg "%":\n";vle -P % '$1' '$2' '$3' '$4'; }'

