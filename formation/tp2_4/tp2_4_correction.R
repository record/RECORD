#####################################
# Exercice 1 : loading and simulating
#####################################
library("rvle")

exercice1 = function()
{
  f = new("Rvle", file = "2CV-exploration.vpz", pkg = "2CV");
  show(f)
  res = results(run(f)); 
  #res$viewEco$"2CV_exploration:EconomicAssesment.Yield" = 1257.468
  f = setDefault(f, outputplugin=c(viewCropLAI="vle.output/storage"));
  res = results(run(f));
  LAI150 = res$viewCropLAI[151,"2CV_exploration,2CV,CropFull:CropLAI.LAI"]
  ## 0,1421705
}

###############################################
# Exercice 2 : simulating with different values
###############################################
exercice2 = function()
{
  f = new("Rvle", file = "2CV-exploration.vpz", pkg = "2CV")
  f = setDefault(f, outputplugin=c(viewCropLAI="vle.output/storage"));

  res = results(run(f,
    condLAI.p2logi = 0.005, 
    condMeteo.meteo_file = "meteo2.txt"))
  LAI150 = res$viewCropLAI[151,"2CV_exploration,2CV,CropFull:CropLAI.LAI"]
  ## 0.05046791
}

###############################################
# Exercice 3 : simulating a simple experiment plan 
###############################################
exercice3 = function()
{
 f = new("Rvle", file = "2CV-exploration.vpz", pkg = "2CV")
 f = setDefault(f, outputplugin=c(viewCropLAI="vle.output/storage"));
 #f = setDefault(f, proc = "thread", thread = 2)
 
 res = results(run(f, plan = "linear", 
   condMeteo.meteo_file  = c("meteo1.txt","meteo2.txt", "meteo3.txt", "meteo4.txt",
                     "meteo5.txt", "meteo6.txt", "meteo7.txt", "meteo8.txt")))
 LAI150_meteo5 = res[[1,5]]$viewCropLAI[151,"2CV_exploration,2CV,CropFull:CropLAI.LAI"]
 ##   0,2098149
 LAI = do.call(cbind, lapply(res, function(x) 
         x$viewCropLAI$"2CV_exploration,2CV,CropFull:CropLAI.LAI"))
 matplot(LAI)            
}


###############################################
# Exercice 4 : simulating a second experiment plan
###############################################
exercice4 = function()
{
 f = new("Rvle", file = "2CV-exploration.vpz", pkg = "2CV")
 f = setDefault(f, outputplugin=c(viewCropLAI="vle.output/storage"));
 #f = setDefault(f, proc = "thread", thread = 2)
 
 res = results(run(f, plan = "linear", 
   condLAI.p2logi = seq(from=0.008, to=0.050, length.out=8), 
   condMeteo.meteo_file  = c("meteo1.txt","meteo2.txt", "meteo3.txt", "meteo4.txt",
                     "meteo5.txt", "meteo6.txt", "meteo7.txt", "meteo8.txt")))
 LAI = do.call(cbind, lapply(res, function(x) 
         x$viewCropLAI$"2CV_exploration,2CV,CropFull:CropLAI.LAI"))
 matplot(LAI)            
}
