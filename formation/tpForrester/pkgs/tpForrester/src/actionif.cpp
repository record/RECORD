/*
 * @file actionif.cpp
 *
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/devs/DynamicsDbg.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace tpForrester {

class ActionIf : public vd::Dynamics
{
public:
    ActionIf(const vd::DynamicsInit& model,
             const vd::InitEventList& events) :
        vd::Dynamics(model, events),
        mAction(false), C3(0), C4(0)
    {
    }

    virtual ~ActionIf()
    {
    }

    virtual vd::Time init(const vd::Time& /* time */)
    {
        return vd::infinity;
    }

    void internalTransition(const vd::Time& /* event */)
    {
        mAction = false;
    }

    void externalTransition(const vd::ExternalEventList& events,
                            const vd::Time& /* time */)
    {
        for (vd::ExternalEventList::const_iterator it = events.begin();
             it != events.end(); ++it) {
            if ((*it)->getStringAttributeValue("name") == "C4")
                C4 = (*it)->getDoubleAttributeValue("value");
             if ((*it)->getStringAttributeValue("name") == "C3")
                 C3 = (*it)->getDoubleAttributeValue("value");
        }

        if (C4 > C3)
            mAction = true;
    }

    virtual vd::Time timeAdvance() const
    {
        if (mAction)
            return 0;
        else
            return vd::infinity;;
    }

    virtual void output(const vd::Time& /* time */,
                        vd::ExternalEventList& output) const
    {
        vd::ExternalEvent* e = new vd::ExternalEvent("C4");
        e->putAttribute("name", new vv::String("C4"));
        e->putAttribute("value", new vv::Double(0));

        output.push_back(e);
    }

private:

    bool mAction;
    double C3;
    double C4;
};

} // namespace tpForrester

DECLARE_DYNAMICS_DBG(tpForrester::ActionIf)
