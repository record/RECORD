/**
  * @file seqst.cpp
  * @author ...
  * ...
  * @@tag Statechart@vle.extension.fsa @@namespace:tpForrester;class:seqst;400|400;s:Single,182,160,50,50,1,,,,|;t:Single,Single,,,,Freq,,Reset,1,215/210!107/104!|;T:@@end tag@@
  */

#include <vle/extension/fsa/Statechart.hpp>

//@@begin:include@@
namespace vd = vle::devs;
namespace vv = vle::value;//@@end:include@@
namespace vd = vle::devs;
namespace vf = vle::extension::fsa;
namespace vv = vle::value;

namespace tpForrester {

class seqst : public vf::Statechart
{
    enum state_t { Single };

public:
    seqst(
       const vd::DynamicsInit& init,
       const vd::InitEventList& evts)
        : vf::Statechart(init, evts)
    {
//@@begin:constructorUser@@
if (evts.exist("Value"))
   mValue = evts.getDouble("Value");
else
   throw vle::utils::ModellingError("Parameter Value not set");

if (evts.exist("Frequency"))
    mFrequency = evts.getDouble("Frequency");
else
    throw vle::utils::ModellingError("Parameter Frequency not set");

if (evts.exist("Name"))
    mName = evts.getString("Name");
else
    throw vle::utils::ModellingError("Parameter Name not set");





//@@end:constructorUser@@
      // structure
      states(this) << Single;

      transition(this, Single, Single) << after(&seqst::Freq) << send(&seqst::Reset);


      initialState(Single);
    }

    virtual ~seqst()
    {}

private:

//@@begin:after(Freq)@@
vd::Time Freq(const vd::Time& /*time*/)
{
   return mFrequency;
}//@@end:after@@

//@@begin:send(Reset)@@
void Reset(const vd::Time& /*time*/,
           vd::ExternalEventList& output) const
{
   vd::ExternalEvent* e = new vd::ExternalEvent("Reset");
   e->putAttribute("name", new vv::String(mName));
   e->putAttribute("value", new vv::Double(mValue));

   output.push_back(e);
}//@@end:send@@
//@@begin:definitionUser@@
double mValue;
double mFrequency;

std::string mName;





//@@end:definitionUser@@
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::seqst)

