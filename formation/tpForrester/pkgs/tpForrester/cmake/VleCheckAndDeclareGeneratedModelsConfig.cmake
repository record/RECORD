# VleCheckAndDeclareGeneratedModelsConfig.cmake
# =============================================
#
# Check the dependencies of VLE's generated models
# Also Declare and Configure
#
# Copyright (c) 2012 INRA
# Patrick Chabrier <patrick.chabrier@toulouse.inra.fr>
#
# Distributed under the OS-approved BSD License (the "License");
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#
# ===========================================================================
#
# Changelog
# ---------
#
# 1 Initial version.
#
# ===========================================================================
#
# Usage
# -----
#
# CheckAndDeclareGeneratedModels()
#
#
# Example (inside a src/CMakeLists.txt)
# -------------------------------------
#
#SET(INCLUDIR ${CMAKE_SOURCE_DIR}/src ${VLE_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
#
#SET(LINKDIR ${VLE_LIBRARY_DIRS})
#
#link_directories(${LINKDIR})
#
#CheckAndDeclareGeneratedModels()
#
# ===========================================================================

##
## Define a macro to check if an item is present inside a list
##

MACRO(LIST_CONTAINS var value)
  SET(${var})
  FOREACH (value2 ${ARGN})
    IF (${value} STREQUAL ${value2})
      SET(${var} TRUE)
    ENDIF (${value} STREQUAL ${value2})
  ENDFOREACH (value2)
ENDMACRO(LIST_CONTAINS)

##
## Define a macro to check and declare generated models.
##

MACRO(CheckAndDeclareGeneratedModels)

  # the list of C++ files inside the local directory
  FILE(GLOB CPPList "*.cpp")

  FOREACH(CPPFile ${CPPList})

    FILE(READ "${CPPFile}" CPPcontents)

    # Filter the generated files
    STRING(REGEX MATCH "@@tag([^@])+" TAGLine ${CPPcontents})
    IF(NOT ${TAGLine} STREQUAL "")

      # loocking for the plugin name
      STRING(REGEX MATCH " ([^ ])+" EXT ${TAGLine})
      STRING(REGEX MATCH "([^ ])+" cleanEXT ${EXT})

      # name of the file without the path
      GET_FILENAME_COMPONENT(SHORTName ${CPPFile} NAME)

      # name of the file without the extension
      GET_FILENAME_COMPONENT(BASEName ${CPPFile} NAME_WE)

      # DECISION
      IF(${cleanEXT} STREQUAL "Decision")

	VLE_CHECK_PACKAGE(vle.extension.decision)
	if (NOT vle.extension.decision_FOUND)
	  message(SEND_ERROR "Missing vle.extension.decision")
	endif (NOT vle.extension.decision_FOUND)

	vle_declare_dynamics(vle.extension.decision ${BASEName} ${SHORTName})

	# adding the path if needed
	LIST_CONTAINS(alreadythere ${vle.extension.decision_INCLUDE_DIRS} ${INCLUDIR})
	IF (NOT alreadythere)
	  SET(INCLUDIR ${INCLUDIR} ${vle.extension.decision_INCLUDE_DIRS})
	ENDIF(NOT alreadythere)

	# DIFFERENCE EQUATION
      ELSEIF(${cleanEXT} STREQUAL "DifferenceEquationMultiple"
	  OR ${cleanEXT} STREQUAL "DifferenceEquationGeneric"
	  OR ${cleanEXT} STREQUAL "DifferenceEquationSimple")

	VLE_CHECK_PACKAGE(vle.extension.difference-equation)
	if (NOT vle.extension.difference-equation_FOUND)
	  message(SEND_ERROR "Missing vle.extension.difference-equation")
	endif (NOT vle.extension.difference-equation_FOUND)

	vle_declare_dynamics(vle.extension.difference-equation ${BASEName} ${SHORTName})

	# adding the path if needed
	LIST_CONTAINS(alreadythere ${vle.extension.difference-equation_INCLUDE_DIRS} ${INCLUDIR})
	IF (NOT alreadythere)
	  SET(INCLUDIR ${INCLUDIR} ${vle.extension.difference-equation_INCLUDE_DIRS})
	ENDIF(NOT alreadythere)

	# DIFFRERENTIAL
      ELSEIF(${cleanEXT} STREQUAL "Forrester")

	VLE_CHECK_PACKAGE(vle.extension.differential-equation)
	if (NOT vle.extension.differential-equation_FOUND)
	  message(SEND_ERROR "Missing vle.extension.differential-equation")
	endif (NOT vle.extension.differential-equation_FOUND)

	vle_declare_dynamics(vle.extension.differential-equation ${BASEName} ${SHORTName})

	# adding the path if needed
	LIST_CONTAINS(alreadythere ${vle.extension.differential-equation_INCLUDE_DIRS} ${INCLUDIR})
	IF (NOT alreadythere)
	  SET(INCLUDIR ${INCLUDIR} ${vle.extension.differential-equation_INCLUDE_DIRS})
	ENDIF(NOT alreadythere)

	# vle.extension.fsa
      ELSEIF(${cleanEXT} STREQUAL "Statechart")

	VLE_CHECK_PACKAGE(vle.extension.fsa)
	if (NOT vle.extension.fsa_FOUND)
	  message(SEND_ERROR "Missing vle.extension.fsa")
	endif (NOT vle.extension.fsa_FOUND)

	vle_declare_dynamics(vle.extension.fsa ${BASEName} ${SHORTName})

	# adding the path if needed
	LIST_CONTAINS(alreadythere ${vle.extension.fsa_INCLUDE_DIRS} ${INCLUDIR})
	IF (NOT alreadythere)
	  SET(INCLUDIR ${INCLUDIR} ${vle.extension.fsa_INCLUDE_DIRS})
	ENDIF(NOT alreadythere)

	# PETRINET
      ELSEIF(${cleanEXT} STREQUAL "Petrinet")

	VLE_CHECK_PACKAGE(vle.extension.petrinet)
	if (NOT vle.extension.petrinet_FOUND)
	  message(SEND_ERROR "Missing vle.extension.petrinet")
	endif (NOT vle.extension.petrinet_FOUND)

	vle_declare_dynamics(vle.extension.petrinet ${BASEName} ${SHORTName})

	# adding the path if needed
	LIST_CONTAINS(alreadythere ${vle.extension.petrinet_INCLUDE_DIRS} ${INCLUDIR})
	IF (NOT alreadythere)
	  SET(INCLUDIR ${INCLUDIR} ${vle.extension.petrinet_INCLUDE_DIRS})
	ENDIF(NOT alreadythere)

      ENDIF()
    ENDIF(NOT ${TAGLine} STREQUAL "")
  ENDFOREACH(CPPFile)

  include_directories(${INCLUDIR})

endmacro(CheckAndDeclareGeneratedModels)

CheckAndDeclareGeneratedModels()