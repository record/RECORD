#include <boost/lexical_cast.hpp>
#include <boost/assert.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <fstream>
#include <iterator>
#include <iostream>
#include <algorithm>

#include <vle/devs/Dynamics.hpp>
#include <vle/devs/DynamicsDbg.hpp>
#include <vle/utils/Package.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

namespace tpForrester {

class BasicFileReader : public vd::Dynamics
{
public:

    BasicFileReader(const vd::DynamicsInit& mdl,
                    const vd::InitEventList& events) :
        vd::Dynamics(mdl, events)
    {
        std::string fileNameParam;
        std::string dataPackageParam;

        if (events.exist("fileName"))
            fileNameParam = events.getString("fileName");
        else
            throw vle::utils::ModellingError("Parameter fileName not set");

        if (events.exist("dataPackage"))
            dataPackageParam = events.getString("dataPackage");
        else
            throw vle::utils::ModellingError("Package where to find the data is not set");

        vle::utils::Package mPack(dataPackageParam);

        std::string mFileName = mPack.getDataFile(fileNameParam);

        std::ifstream inFile(mFileName.c_str());

        std::string line;
        std::vector <std::string> splitVec;

        getline(inFile, line);

        // fing the name of the event, expected to be the name of the
        // second column
        {
            std::vector <std::string> splitVec;
            boost::trim_if(line,  boost::is_any_of(" \t\""));
            boost::split(splitVec,
                         line,
                         boost::is_any_of(" \t\""),
                         boost::token_compress_on);

            mEventName = splitVec[1];
        }

        double date;
        double value;
        while (getline (inFile,line))
        {
            std::vector <std::string> splitVec;
            boost::split(splitVec,
                         line,
                         boost::is_any_of(" \t"),
                         boost::token_compress_on);


            date = boost::lexical_cast<double>(splitVec[0]);
            value = boost::lexical_cast<double>(splitVec[1]);

            mSchedule.push_back(std::make_pair(vd::Time(date), value));
        }
        inFile.close();
    }

    virtual ~BasicFileReader() {}

    virtual vd::Time init(const vd::Time& time)
    {
        mNextDate = mSchedule.front().first;
        mNextValue = mSchedule.front().second;

        return mNextDate;
    }

    vd::Time timeAdvance() const
    {
        return mNextDate;
    }

    void internalTransition(const vd::Time& time)
    {
        mSchedule.erase(mSchedule.begin());

        if (!mSchedule.empty()) {
            mNextDate =  mSchedule.front().first - time;
            mNextValue =  mSchedule.front().second;
        } else {
            mNextDate = vd::infinity;
        }
    }

    virtual void output(const vd::Time& /* time */,
                        vd::ExternalEventList& output) const
    {
        vd::ExternalEvent* e = new vd::ExternalEvent("output");
        e->putAttribute("name", new vv::String(mEventName));
        e->putAttribute("value", new vv::Double(mNextValue));

        output.push_back(e);
    }

    std::vector< std::pair< vd::Time, double > > mSchedule;

    std::string mEventName;
    vd::Time mNextDate;
    double mNextValue;
};

} // namespace

DECLARE_DYNAMICS(tpForrester::BasicFileReader)
