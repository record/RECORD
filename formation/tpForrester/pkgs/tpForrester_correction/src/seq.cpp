/*
 * @file seq.cpp
 *
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/devs/DynamicsDbg.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace tpForrester {

class Seq : public vd::Dynamics
{
public:
    Seq(const vd::DynamicsInit& model,
        const vd::InitEventList& events) :
        vd::Dynamics(model, events)
    {
        if (events.exist("Value"))
            mValue = events.getDouble("Value");
        else
            throw vle::utils::ModellingError("Parameter Value not set");

        if (events.exist("Frequency"))
            mFrequence = events.getDouble("Frequency");
        else
            throw vle::utils::ModellingError("Parameter Frequency not set");

        if (events.exist("Name"))
            mName = events.getString("Name");
        else
            throw vle::utils::ModellingError("Parameter Name not set");
    }

    virtual ~Seq()
    {
    }

    virtual vd::Time init(const vd::Time& /* time */)
    {
        return mFrequence;
    }

    virtual vd::Time timeAdvance() const
    {
        return mFrequence;
    }

    virtual void output(const vd::Time& /* time */,
                        vd::ExternalEventList& output) const
    {
        vd::ExternalEvent* e = new vd::ExternalEvent(mName);
        e->putAttribute("name", new vv::String(mName));
        e->putAttribute("value", new vv::Double(mValue));

        output.push_back(e);

    }

private:

    double mValue;
    double mFrequence;

    std::string mName;
};

} // namespace tpForrester

DECLARE_DYNAMICS_DBG(tpForrester::Seq)
