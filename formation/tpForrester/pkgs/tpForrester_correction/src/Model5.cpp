/**
  * @file Model5.cpp
  * @author ...
  * ...
  * @@tag Forrester@vle.forrester @@namespace:tpForrester;class:Model5;658|470;c:C2,200,222,90,25|C3,363,334,90,25|dataC1,247,43,90,25|C4,558,435,90,25|C1,40,116,90,25|;f:F3,474,396,50,20,p3 * C3,false,,,|fdataC1,171,94,59,20,dataC1,false,,,|F2,294,281,50,20,p2 * C2,false,,,|F0,159,182,50,20,p1 * C1,false,,,|;fa:F0,C2,0,0|C2,F2,5,0|F2,C3,0,0|C3,F3,5,0|F3,C4,0,0|C1,F0,5,0|dataC1,fdataC1,4,0|fdataC1,C1,0,5|;p:p1,350,133,30,16|p2,465,229,30,16|p3,589,336,30,16|;pa:p1,F0,3,0,256,145|p2,F2,3,1,368,238|p3,F3,3,1,533,344|C1,F0,2,2,70,198|C2,F2,2,2,245,293|C3,F3,2,2,399,399|dataC1,fdataC1,3,4,288,98|;ev:;v:;@@end tag@@
  */

#include <vle/extension/differential-equation/DifferentialEquation.hpp>

//@@begin:include@@

//@@end:include@@
namespace vd = vle::devs;
namespace ve = vle::extension::differential_equation;
namespace vv = vle::value;

namespace tpForrester {

class Model5 : public ve::DifferentialEquation
{
public:
    Model5(
       const vd::DynamicsInit& init,
       const vd::InitEventList& events)
    : ve::DifferentialEquation(init, events)
    {
        C2 = createVar("C2");
        C3 = createVar("C3");
        dataC1 = createVar("dataC1");
        C4 = createVar("C4");
        C1 = createVar("C1");


        if (events.exist("p1"))
            p1 = events.getDouble("p1");
        else
            throw vle::utils::ModellingError("Parameter p1 not found");

        if (events.exist("p2"))
            p2 = events.getDouble("p2");
        else
            throw vle::utils::ModellingError("Parameter p2 not found");

        if (events.exist("p3"))
            p3 = events.getDouble("p3");
        else
            throw vle::utils::ModellingError("Parameter p3 not found");

    }

    virtual void compute(const vd::Time& t)
    {
        F3 = p3 * C3();
        fdataC1 = dataC1();
        F2 = p2 * C2();
        F0 = p1 * C1();
        grad(C2) = (F0) - (F2);
        grad(C3) = (F2) - (F3);
        grad(dataC1) = 0 - (fdataC1);
        grad(C4) = (F3);
        grad(C1) = (fdataC1) - (F0);
    }

    virtual ~Model5()
    { }

private:
    Var C2;
    Var C3;
    Var dataC1;
    Var C4;
    Var C1;

    double p1;
    double p2;
    double p3;
    double F3;
    double fdataC1;
    double F2;
    double F0;
virtual vv::Value* observation(
    const vd::ObservationEvent& event) const
{
   const std::string& port = event.getPortName();
    if (port == "F3" ) return new vv::Double(F3);
    if (port == "fdataC1" ) return new vv::Double(fdataC1);
    if (port == "F2" ) return new vv::Double(F2);
    if (port == "F0" ) return new vv::Double(F0);
   return ve::DifferentialEquation::observation(event);
}
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::Model5)

