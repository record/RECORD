/**
  * @file Model3.cpp
  * @author ...
  * ...
  * @@tag Forrester@vle.forrester @@namespace:tpForrester;class:Model3;658|470;c:C4,558,435,90,25|C1,37,119,90,25|C2,200,222,90,25|C3,363,334,90,25|;f:F0,159,182,50,20,p1 * C1,false,,,|F3,474,396,50,20,p3 * C3,false,,,|FdataC1,29,64,61,20,dataC1,false,,,|F2,294,281,50,20,p2 * C2,false,,,|;fa:C1,F0,5,0|F0,C2,0,0|C2,F2,5,0|F2,C3,0,0|C3,F3,5,0|F3,C4,0,0|FdataC1,C1,6,0|;p:p3,557,319,30,16|p1,314,127,30,16|p2,434,233,30,16|;pa:p1,F0,3,0,220,139|p2,F2,3,1,341,239|p3,F3,3,1,504,339|C1,F0,2,2,70,198|C2,F2,2,2,245,293|C3,F3,2,2,399,399|dataC1,FdataC1,7,4,141,67|;ev:dataC1,222,55,22,22|;v:;@@end tag@@
  */

#include <vle/extension/differential-equation/DifferentialEquation.hpp>

//@@begin:include@@

//@@end:include@@
namespace vd = vle::devs;
namespace ve = vle::extension::differential_equation;
namespace vv = vle::value;

namespace tpForrester {

class Model3 : public ve::DifferentialEquation
{
public:
    Model3(
       const vd::DynamicsInit& init,
       const vd::InitEventList& events)
    : ve::DifferentialEquation(init, events)
    {
        C4 = createVar("C4");
        C1 = createVar("C1");
        C2 = createVar("C2");
        C3 = createVar("C3");

        dataC1 = createExt("dataC1");

        if (events.exist("p3"))
            p3 = events.getDouble("p3");
        else
            throw vle::utils::ModellingError("Parameter p3 not found");

        if (events.exist("p1"))
            p1 = events.getDouble("p1");
        else
            throw vle::utils::ModellingError("Parameter p1 not found");

        if (events.exist("p2"))
            p2 = events.getDouble("p2");
        else
            throw vle::utils::ModellingError("Parameter p2 not found");

    }

    virtual void compute(const vd::Time& t)
    {
        F0 = p1 * C1();
        F3 = p3 * C3();
        FdataC1 = dataC1();
        F2 = p2 * C2();
        grad(C4) = (F3);
        grad(C1) = (FdataC1) - (F0);
        grad(C2) = (F0) - (F2);
        grad(C3) = (F2) - (F3);
    }

    virtual ~Model3()
    { }

private:
    Var C4;
    Var C1;
    Var C2;
    Var C3;

    double p3;
    double p1;
    double p2;
    Ext dataC1;
    double F0;
    double F3;
    double FdataC1;
    double F2;
virtual vv::Value* observation(
    const vd::ObservationEvent& event) const
{
   const std::string& port = event.getPortName();
    if (port == "F0" ) return new vv::Double(F0);
    if (port == "F3" ) return new vv::Double(F3);
    if (port == "FdataC1" ) return new vv::Double(FdataC1);
    if (port == "F2" ) return new vv::Double(F2);
   return ve::DifferentialEquation::observation(event);
}
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::Model3)

