/**
  * @file Model0.cpp
  * @author ...
  * ...
  * @@tag Forrester@vle.forrester @@namespace:tpForrester;class:Model0;584|415;c:C3,305,257,90,25|C2,148,182,90,25|C4,484,380,90,25|C1,21,55,90,25|;f:F2,239,237,50,20,p2 * C2,false,,,|F3,413,339,50,20,p3 * C3,false,,,|F1,109,139,50,20,p1 * C1,false,,,|;fa:C1,F1,5,0|F1,C2,0,0|C2,F2,5,0|F2,C3,0,0|C3,F3,5,0|F3,C4,0,0|;p:p1,248,57,30,16|p2,367,154,30,16|p3,510,259,30,16|;pa:p1,F1,3,0,160,77|p2,F2,3,1,284,173|p3,F3,3,1,449,272|C1,F1,2,2,54,147|C2,F2,2,2,185,254|C3,F3,2,2,325,334|;ev:;v:;@@end tag@@
  */

#include <vle/extension/differential-equation/DifferentialEquation.hpp>

//@@begin:include@@

//@@end:include@@
namespace vd = vle::devs;
namespace ve = vle::extension::differential_equation;
namespace vv = vle::value;

namespace tpForrester {

class Model0 : public ve::DifferentialEquation
{
public:
    Model0(
       const vd::DynamicsInit& init,
       const vd::InitEventList& events)
    : ve::DifferentialEquation(init, events)
    {
        C3 = createVar("C3");
        C2 = createVar("C2");
        C4 = createVar("C4");
        C1 = createVar("C1");


        if (events.exist("p1"))
            p1 = events.getDouble("p1");
        else
            throw vle::utils::ModellingError("Parameter p1 not found");

        if (events.exist("p2"))
            p2 = events.getDouble("p2");
        else
            throw vle::utils::ModellingError("Parameter p2 not found");

        if (events.exist("p3"))
            p3 = events.getDouble("p3");
        else
            throw vle::utils::ModellingError("Parameter p3 not found");

    }

    virtual void compute(const vd::Time& t)
    {
        F2 = p2 * C2();
        F3 = p3 * C3();
        F1 = p1 * C1();
        grad(C3) = (F2) - (F3);
        grad(C2) = (F1) - (F2);
        grad(C4) = (F3);
        grad(C1) = 0 - (F1);
    }

    virtual ~Model0()
    { }

private:
    Var C3;
    Var C2;
    Var C4;
    Var C1;

    double p1;
    double p2;
    double p3;
    double F2;
    double F3;
    double F1;
virtual vv::Value* observation(
    const vd::ObservationEvent& event) const
{
   const std::string& port = event.getPortName();
    if (port == "F2" ) return new vv::Double(F2);
    if (port == "F3" ) return new vv::Double(F3);
    if (port == "F1" ) return new vv::Double(F1);
   return ve::DifferentialEquation::observation(event);
}
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::Model0)

