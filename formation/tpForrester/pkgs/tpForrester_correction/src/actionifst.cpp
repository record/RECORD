/**
  * @file actionifst.cpp
  * @author ...
  * ...
  * @@tag Statechart@vle.extension.fsa @@namespace:tpForrester;class:actionifst;400|400;s:check,44,263,50,50,0,,,,|reset,276,308,50,50,0,,,,|wait,114,65,76,60,1,,,,C3!keepC3!C4!keepC4!|;t:wait,check,,toCheck,,,willBeChecked,,0,139/125!60/263!|check,wait,,C3bigC4,,,,,0,94/279!164/125!|check,reset,,C4bigC3,,,,,0,77/313!276/341!|reset,wait,,,,,,C4equalZero,1,292/308!190/105!|;T:@@end tag@@
  */

#include <vle/extension/fsa/Statechart.hpp>

//@@begin:include@@
//@@end:include@@
namespace vd = vle::devs;
namespace vf = vle::extension::fsa;
namespace vv = vle::value;

namespace tpForrester {

class actionifst : public vf::Statechart
{
    enum state_t { check, reset, wait };

public:
    actionifst(
       const vd::DynamicsInit& init,
       const vd::InitEventList& evts)
        : vf::Statechart(init, evts)
    {
//@@begin:constructorUser@@
C3 = 0;
C4 = 0;
checked = true;











//@@end:constructorUser@@
      // structure
      states(this) << check << reset << wait;

      transition(this, wait, check) << guard(&actionifst::toCheck) << action(&actionifst::willBeChecked);
      transition(this, check, wait) << guard(&actionifst::C3bigC4);
      transition(this, check, reset) << guard(&actionifst::C4bigC3);
      transition(this, reset, wait) << send(&actionifst::C4equalZero);

      state(this, wait) << eventInState("C3", &actionifst::keepC3) << eventInState("C4", &actionifst::keepC4);

      initialState(wait);
    }

    virtual ~actionifst()
    {}

private:

//@@begin:action(willBeChecked)@@
void willBeChecked(const vd::Time& /*time*/)
{
	checked = true;
}//@@end:action@@

//@@begin:eventAction(keepC3)@@
void keepC3(const vd::Time& /*time*/,
         const vd::ExternalEvent* event)
{
	checked = false;
	C3 = event->getDoubleAttributeValue("value");
}//@@end:eventAction@@

//@@begin:eventAction(keepC4)@@
void keepC4(const vd::Time& /*time*/,
         const vd::ExternalEvent* event)
{
   checked = false;
   C4 = event->getDoubleAttributeValue("value");
}//@@end:eventAction@@

//@@begin:guard(C3bigC4)@@
bool C3bigC4(const vd::Time& /*time*/)
{
   return C3 >= C4;
}//@@end:guard@@

//@@begin:guard(C4bigC3)@@
bool C4bigC3(const vd::Time& /*time*/)
{
   return C4 > C3;
}//@@end:guard@@

//@@begin:guard(toCheck)@@
bool toCheck(const vd::Time& /*time*/)
{
   return checked == false;
}//@@end:guard@@

//@@begin:send(C4equalZero)@@
void C4equalZero(const vd::Time& /*time*/,
         vd::ExternalEventList& output) const
{
   vd::ExternalEvent* e = new vd::ExternalEvent("C4equalZero");
   e->putAttribute("name", new vv::String("C4"));
   e->putAttribute("value", new vv::Double(0));

   output.push_back(e);
}//@@end:send@@
//@@begin:definitionUser@@
double C3;
double C4;
bool checked;










//@@end:definitionUser@@
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::actionifst)

