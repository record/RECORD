/**
  * @file Model2.cpp
  * @author ...
  * ...
  * @@tag Forrester@vle.forrester @@namespace:tpForrester;class:Model2;586|423;c:C4,486,388,90,25|C5,152,383,90,25|C2,148,182,90,25|C3,305,257,90,25|C1,21,64,90,25|;f:F3,417,338,50,20,p3 * C3,false,,,|F2,239,237,50,20,V1 * C2,false,,,|F1,109,139,50,20,(p1 * t / (t + p4))  * C1,false,,,|F4,244,330,50,20,0,true,C3 >= C3max,p5 * (C3 - C3max),0|;fa:C1,F1,5,0|F1,C2,0,0|C2,F2,5,0|F2,C3,0,0|C3,F3,5,0|F3,C4,0,0|C3,F4,4,0|F4,C5,0,1|;p:p1,248,57,30,16|p3,510,259,30,16|p4,248,91,30,16|p2,364,146,30,16|C3max,94,347,30,16|p5,92,295,30,16|;pa:p1,F1,3,0,132,95|p2,F2,3,1,284,173|p3,F3,3,1,449,272|C1,F1,2,2,66,150|C2,F2,2,2,179,242|C3,F3,2,2,348,356|p4,F1,3,1,176,99|p2,V1,2,3,399,164|V1,F2,7,1,299,192|C3,F4,3,4,371,368|p5,F4,2,0,164,293|C3max,F4,2,2,159,343|;ev:;v:V1,350,169,25,25, p2  *  (1 + sin(t / 5)),false,,,|;@@end tag@@
  */

#include <vle/extension/differential-equation/DifferentialEquation.hpp>

//@@begin:include@@

//@@end:include@@
namespace vd = vle::devs;
namespace ve = vle::extension::differential_equation;
namespace vv = vle::value;

namespace tpForrester {

class Model2 : public ve::DifferentialEquation
{
public:
    Model2(
       const vd::DynamicsInit& init,
       const vd::InitEventList& events)
    : ve::DifferentialEquation(init, events)
    {
        C4 = createVar("C4");
        C5 = createVar("C5");
        C2 = createVar("C2");
        C3 = createVar("C3");
        C1 = createVar("C1");


        if (events.exist("p1"))
            p1 = events.getDouble("p1");
        else
            throw vle::utils::ModellingError("Parameter p1 not found");

        if (events.exist("p3"))
            p3 = events.getDouble("p3");
        else
            throw vle::utils::ModellingError("Parameter p3 not found");

        if (events.exist("p4"))
            p4 = events.getDouble("p4");
        else
            throw vle::utils::ModellingError("Parameter p4 not found");

        if (events.exist("p2"))
            p2 = events.getDouble("p2");
        else
            throw vle::utils::ModellingError("Parameter p2 not found");

        if (events.exist("C3max"))
            C3max = events.getDouble("C3max");
        else
            throw vle::utils::ModellingError("Parameter C3max not found");

        if (events.exist("p5"))
            p5 = events.getDouble("p5");
        else
            throw vle::utils::ModellingError("Parameter p5 not found");

    }

    virtual void compute(const vd::Time& t)
    {
        V1 =  p2  *  (1 + std::sin(t / 5));
        F4 = (C3() >= C3max)? p5 * (C3() - C3max) : 0;
        F3 = p3 * C3();
        F2 = V1 * C2();
        F1 = (p1 * t / (t + p4))  * C1();
        grad(C4) = (F3);
        grad(C5) = (F4);
        grad(C2) = (F1) - (F2);
        grad(C3) = (F2) - (F3 + F4);
        grad(C1) = 0 - (F1);
    }

    virtual ~Model2()
    { }

private:
    Var C4;
    Var C5;
    Var C2;
    Var C3;
    Var C1;

    double p1;
    double p3;
    double p4;
    double p2;
    double C3max;
    double p5;
    double V1;
    double F3;
    double F2;
    double F1;
    double F4;
virtual vv::Value* observation(
    const vd::ObservationEvent& event) const
{
   const std::string& port = event.getPortName();
    if (port == "F3" ) return new vv::Double(F3);
    if (port == "F2" ) return new vv::Double(F2);
    if (port == "F1" ) return new vv::Double(F1);
    if (port == "F4" ) return new vv::Double(F4);
    if (port == "V1" ) return new vv::Double(V1);
   return ve::DifferentialEquation::observation(event);
}
};

} // namespace tpForrester

DECLARE_DYNAMICS(tpForrester::Model2)

