dataC1 = read.table("dataC1.txt", header = TRUE)
plot(dataC1$t, dataC1$dataC1)
points(approx(dataC1$t, dataC1$dataC1, n=2001), col = 2, pch = "°")
dataC1Interpol = data.frame(approx(dataC1$t, dataC1$dataC1, n=2001))
names(dataC1Interpol) = names(dataC1)
write.table(dataC1Interpol, file = "dataC1Interpol.txt", row.names = FALSE)