<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata version="1.x" author="meto">
 <dataPlugin package="vle.discrete-time.decision" name="Plan Activities"/>
 <dataModel outputsType="discrete-time" package="tp5_1_solution" conf="farmerAccessibility"/>
 <definition>
  <activities>
   <activity timeLag="1" value="-82" y="-106" minstart="+0-3-17" x="63" name="Sowing" maxfinish="+" maxiter="1">
    <outputParams>
     <outputParam value="1" name="Sowing"/>
    </outputParams>
    <rulesAssigment>
     <rule name="ruleSowing"/>
    </rulesAssigment>
   </activity>
  <activity timeLag="1" value="-22" y="107" minstart="" x="293" name="Harvesting" maxfinish="" maxiter="1">
    <outputParams>
     <outputParam value="1" name="Harvesting"/>
    </outputParams>
    <rulesAssigment>
     <rule name="ruleHarvesting"/>
    </rulesAssigment>
   </activity>
  </activities>
  <predicates>
   <predicate operator="&lt;=" leftValue="days3rain" leftType="Var" rightValue="10" name="plotAccesibility" rightType="Val"/>
  </predicates>
  <rules>
   <rule name="ruleSowing">
    <predicate name="plotAccesibility"/>
   </rule>
   <rule name="ruleHarvesting">
    <predicate name="plotAccesibility"/>
   </rule>
  </rules>
  <precedences>
   <precedence first="Sowing" maxtimelag="" mintimelag="220" second="Harvesting" type="FS"/>
  </precedences>
 </definition>
 <configuration>
  <dynamic library="agentDTG" package="vle.discrete-time.decision" name="dynagentDTG"/>
  <observable name="obsfarmerAccessibility">
   <port name="Knowledgebase"/>
   <port name="Activities"/>
   <port name="Activity_Sowing"/>
   <port name="Activity(state)_Sowing"/>
   <port name="Activity(ressources)_Sowing"/>
  <port name="Sowing"/>
  <port name="Activity_Harvesting"/>
   <port name="Activity(state)_Harvesting"/>
   <port name="Activity(ressources)_Harvesting"/>
   <port name="Harvesting"/>
  <port name="days3rain"/>
  </observable>
  <condition name="condfarmerAccessibility">
   <port name="dyn_allow">
    <boolean>1</boolean>
   </port>
   <port name="dyn_denys">
    <set/>
   </port>
   <port name="autoAck">
    <boolean>1</boolean>
   </port>
   <port name="PlansLocation">
    <string>tp5_1_solution</string>
   </port>
   <port name="Rotation">
    <map>
     <key name="">
      <set>
       <integer>1</integer>
       <set>
        <integer>1</integer>
        <string>farmerAccessibility</string>
       </set>
      </set>
     </key>
    </map>
   </port>
  </condition>
  <in>
   <port name="days3rain"/>
  </in>
  <out>
   <port name="Sowing"/>
  <port name="Harvesting"/>
  </out>
 </configuration>
</vle_project_metadata>
