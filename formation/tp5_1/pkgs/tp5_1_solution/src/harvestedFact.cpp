/**
  * @file harvestedFact.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace tp5_1_solution {

class harvestedFact : public DiscreteTimeDyn
{
public:
harvestedFact(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    Harvesting.init(this, "Harvesting", evts);
    Harvested.init(this, "Harvested", evts);


}

virtual ~harvestedFact()
{}

void compute(const vle::devs::Time& t)
{
if ( Harvesting() == 1) {
   Harvested = 1;
}
}

    Var Harvesting;
    Var Harvested;

};

} // namespace tp5_1_solution
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::tp5_1_solution::harvestedFact)

