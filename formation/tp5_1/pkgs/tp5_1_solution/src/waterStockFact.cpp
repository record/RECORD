/**
  * @file waterStockFact.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace tp5_1_solution {

class waterStockFact : public DiscreteTimeDyn
{
public:
waterStockFact(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    IrrigationAmount.init(this, "IrrigationAmount", evts);
    WaterStock.init(this, "WaterStock", evts);


}

virtual ~waterStockFact()
{}

void compute(const vle::devs::Time& t)
{
WaterStock = WaterStock(-1) - IrrigationAmount();
}

    Var IrrigationAmount;
    Var WaterStock;

};

} // namespace tp5_1_solution
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::tp5_1_solution::waterStockFact)

