/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/utils/unit-test.hpp>

#include <vle/vle.hpp>
#include <vle/value/Map.hpp>
#include <vle/value/Matrix.hpp>
#include <vle/vpz/Vpz.hpp>
#include <vle/vpz/Model.hpp>
#include <vle/vpz/AtomicModel.hpp>
#include <vle/utils/Context.hpp>
#include <vle/utils/Package.hpp>
#include <vle/manager/Simulation.hpp>


namespace vz = vle::vpz;
namespace vu = vle::utils;
namespace vm = vle::manager;
namespace va = vle::value;

struct F
{
    F()
    {
    }

    vle::Init app;
};

int
ttgetColumnFromView(const va::Matrix& view, const std::string& model,
                    const std::string& port)
{
    for(unsigned int j=1; j < view.columns(); j++){
        if(view.getString(j,0) == (model + std::string(".") + port)){
            return j;
        }
    }
    return -1;
}


void
ttconfOutputPlugins(vz::Vpz& vpz)
{
    //set all output plugin to storage
    vz::Outputs::iterator itb =
        vpz.project().experiment().views().outputs().begin();
    vz::Outputs::iterator ite =
        vpz.project().experiment().views().outputs().end();
    for(;itb!=ite;itb++) {
        std::unique_ptr<va::Map> configOutput(new va::Map());
        configOutput->addInt("rows",10000);
        configOutput->addInt("inc_rows",10000);
        configOutput->addString("header","top");
        vz::Output& output = itb->second;
        output.setStream("", "storage", "vle.output");
        output.setData(std::move(configOutput));
    }
}

void test_tp5_2()
{
    auto ctx = vu::make_context();
    std::cout << "  test_tp5_2 " << std::endl;
    vle::utils::Package pack(ctx, "tp5_2_correction");


///constructor.vpz
    std::unique_ptr<vz::Vpz> vpz(new vz::Vpz(pack.getExpFile("constructor.vpz", vle::utils::PKG_BINARY)));
    ttconfOutputPlugins(*vpz);
    //simulation
    vm::Error error;
    vm::Simulation sim(ctx, vm::LOG_NONE, vm::SIMULATION_NONE,
            std::chrono::milliseconds(0), &std::cout);
    std::unique_ptr<va::Map> out = sim.run(std::move(vpz), &error);
    //checks that simulation has succeeded
    EnsuresEqual(error.code, 0);
    //checks the number of views
    EnsuresEqual(out->size(),1);
    //checks the selected view
    const va::Matrix& view = out->getMatrix("view");
    std::cout << "dbg!!colnum:" << view.columns() << std::endl;
    EnsuresEqual(view.columns(),18);
    Ensures(view.rows() <= 352);
    Ensures(view.rows() >= 351);
    //gets X,Y
    int colX = ttgetColumnFromView(view, "top,PlotC:SoilSurfaceIncomingWater", "RunOff");



///water_infiltration_runOff.vpz
    std::unique_ptr<vz::Vpz> vpz2(new vz::Vpz(pack.getExpFile("water_infiltration_runOff.vpz", vle::utils::PKG_BINARY)));
    ttconfOutputPlugins(*vpz2);
    //simulation
    vm::Error error2;
    vm::Simulation sim2(ctx, vm::LOG_NONE, vm::SIMULATION_NONE,
            std::chrono::milliseconds(0), &std::cout);
    std::unique_ptr<va::Map> out2 = sim2.run(std::move(vpz2), &error2);
    //checks that simulation has succeeded
    EnsuresEqual(error2.code, 0);
    //checks the number of views
    EnsuresEqual(out2->size(),1);
    //checks the selected view
    const va::Matrix& view2 = out2->getMatrix("view");
    EnsuresEqual(view2.columns(),18);
    Ensures(view2.rows() <= 352);
    Ensures(view2.rows() >= 351);
    //gets X,Y
    int colX2 = ttgetColumnFromView(view2, "top,PlotC:SoilSurfaceIncomingWater_C", "RunOff");


///comparison of the 2 simulators
    EnsuresApproximatelyEqual(view.getDouble(colX,339)-view2.getDouble(colX2,339), 0., 10e-5);
}

int main()
{
    F fixture;
    test_tp5_2();

    return unit_test::report_errors();
}
