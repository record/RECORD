/**
 * @file src/Constructor.cpp
 * @author R.Trepos-(The RECORD team -INRA )
 */

/*
 * Copyright (C) 2013-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <vle/devs/Executive.hpp>


namespace tp5_2_correction {
class Constructor : public vle::devs::Executive
{
public :
    Constructor(const vle::devs::ExecutiveInit& mdl, const vle::devs::InitEventList& events) :
        vle::devs::Executive(mdl, events)
    { }
 
    virtual ~Constructor() { }

    virtual vle::devs::Time init(vle::devs::Time /*time*/) override
    {

        //Modèles Decision et Meteo
        createModelFromClass("DecisionClass", "Decision");
        createModelFromClass("MeteoClass", "Meteo");
        //PlotA
        createModelFromClass("Plot0inputClass", "PlotA");
        addConnection("Meteo", "Rain", "PlotA", "Rain");
        addConnection("Decision", "Irrigation", "PlotA", "Irrigation");
        //PlotD
        createModelFromClass("Plot0inputClass", "PlotD");
        addConnection("Meteo", "Rain", "PlotD", "Rain");
        addConnection("Decision", "Irrigation", "PlotD", "Irrigation");
        //PlotB
        createModelFromClass("Plot1inputClass", "PlotB");
        addConnection("Meteo", "Rain", "PlotB", "Rain");
        addConnection("Decision", "Irrigation", "PlotB", "Irrigation");
        addConnection("PlotA", "RunOff", "PlotB", "IncomingRunOff");
        //PlotC
        createModelFromClass("Plot2inputClass", "PlotC");
        addConnection("Meteo", "Rain", "PlotC", "Rain");
        addConnection("Decision", "Irrigation", "PlotB", "Irrigation");
        addConnection("PlotD", "RunOff", "PlotC", "IncomingRunOff_1");
        addConnection("PlotB", "RunOff", "PlotC", "IncomingRunOff_2");

        return vle::devs::infinity;

    }

};
}//namespace tp5_2_correction
DECLARE_EXECUTIVE(tp5_2_correction::Constructor)
