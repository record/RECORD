/**
 * @file src/SoilSurfaceIncomingWater.cpp
 * @author R.Trepos-(The RECORD team -INRA )
 */

/*
 * Copyright (C) 2013-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vle {
namespace discrete_time {
namespace tp5_2_correction {

    class SoilSurfaceIncomingWater : public DiscreteTimeDyn
    {
	public:
	    SoilSurfaceIncomingWater(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& evts)
            : DiscreteTimeDyn(init, evts)
	    {
		    // Variables d'etat gerees par ce composant
		    AvailableWater.init(this, "AvailableWater", evts);
		    RunOff.init(this, "RunOff", evts);
		    Infiltration.init(this, "Infiltration", evts);
		
		    // Variables  gerees par un autre  composant
		    Irrigation.init(this, "Irrigation", evts);
		    Rain.init(this, "Rain", evts);
            getOptions().syncs.insert(std::make_pair("Rain", 1));
		    IncomingRunOff.init(this, "IncomingRunOff", evts);
            getOptions().syncs.insert(std::make_pair("IncomingRunOff", 1));
		
		    // Lecture des parametres
		    fracRunOff = (evts.exist("fracRunOff")) 
		             ?  evts.getDouble("fracRunOff") 
		             :  0.;
	    }


	    virtual ~SoilSurfaceIncomingWater() {};


   	    virtual void compute(const vle::devs::Time& /*julianDay*/)
	    {
            Irrigation = 0.;
		    //autres variables
		    AvailableWater = Rain() + Irrigation() + IncomingRunOff();
		    RunOff = AvailableWater() * fracRunOff;
		    Infiltration = AvailableWater() * (1. - fracRunOff);
        }


        private:
	    //Variables d'etat
	    Var AvailableWater;
	    Var RunOff;
	    Var Infiltration;
	     
	    //Entrées
	    Var Irrigation;
	    Var Rain;
	    Var IncomingRunOff;
	    
	    //Parametres du modele
	    double fracRunOff; 
    };
}}}//namespaces tp5_2_correction
DECLARE_DYNAMICS(vle::discrete_time::tp5_2_correction::SoilSurfaceIncomingWater); // balise specifique VLE


