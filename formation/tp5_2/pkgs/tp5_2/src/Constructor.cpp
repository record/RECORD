/**
 * @file src/Constructor.cpp
 * @author R.Trepos-(The RECORD team -INRA )
 */

/*
 * Copyright (C) 2013-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include <vle/devs/Executive.hpp>


namespace tp5_2 {
class Constructor : public vle::devs::Executive
{
public :
    Constructor(const vle::devs::ExecutiveInit& mdl, const vle::devs::InitEventList& events) :
        vle::devs::Executive(mdl, events)
    { }
 
    virtual ~Constructor() { }

    virtual vle::devs::Time init(vle::devs::Time /*time*/) override
    {

        //A remplir :
        // en utilisant les fonction de DS-DEVS:
        // 'createModelFromClass' et 'addConnection'

        ///...

        return vle::devs::infinity;

    }

};
}//namespace tp5_2
DECLARE_EXECUTIVE(tp5_2::Constructor)
