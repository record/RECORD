/**
  * @file ThermalTime.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace tp3_2_correction {

class ThermalTime : public DiscreteTimeDyn
{
public:
ThermalTime(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    ThermTime.init(this, "ThermTime", evts);
    Tmin.init(this, "Tmin", evts);
    Tmax.init(this, "Tmax", evts);


}

virtual ~ThermalTime()
{}

void compute(const vle::devs::Time& t)
{
ThermTime = ThermTime(-1) + std::max(0.0, (Tmin()+Tmax())/2);
}

    Var ThermTime;
    Var Tmin;
    Var Tmax;

};

} // namespace tp3_2_correction
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::tp3_2_correction::ThermalTime)

