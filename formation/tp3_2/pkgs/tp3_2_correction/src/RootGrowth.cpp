/**
  * @file RootGrowth.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace tp3_2_correction {

class RootGrowth : public DiscreteTimeDyn
{
public:
RootGrowth(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    RootDepthDelta.init(this, "RootDepthDelta", evts);
    RootDepth.init(this, "RootDepth", evts);
    ThermTime.init(this, "ThermTime", evts);
    maxDepth.init(this, "maxDepth", evts);
    growthRate.init(this, "growthRate", evts);


}

virtual ~RootGrowth()
{}

void compute(const vle::devs::Time& t)
{
RootDepthDelta = growthRate() * (ThermTime() - ThermTime(-1));
RootDepth = std::min(RootDepth(-1) + RootDepthDelta(), maxDepth());
}

    Var RootDepthDelta;
    Var RootDepth;
    Var ThermTime;
    Var maxDepth;
    Var growthRate;

};

} // namespace tp3_2_correction
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::tp3_2_correction::RootGrowth)

