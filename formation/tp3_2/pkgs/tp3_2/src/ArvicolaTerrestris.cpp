/*
 * @@tagdynamic@@
 * @@tagdepends: vle.extension.fsa @@endtagdepends
*/

#include <vle/extension/fsa/Statechart.hpp>

namespace vd = vle::devs;
namespace vf = vle::extension::fsa;
namespace vv = vle::value;

namespace tp3_2_correction {

class ArvicolaTerrestris : public vf::Statechart
{
    enum state_t { AfterTunnel, BeforeTunnel };

public:
    ArvicolaTerrestris(
       const vd::DynamicsInit& init,
       const vd::InitEventList& evts)
        : vf::Statechart(init, evts)
    {
        states(this) << AfterTunnel << BeforeTunnel;

        transition(this, BeforeTunnel, AfterTunnel)
           << when(&ArvicolaTerrestris::dateTunnel)
           << send(&ArvicolaTerrestris::cutRoot);
        
        initialState(BeforeTunnel);
    }

    virtual ~ArvicolaTerrestris()
    {}

private:

    void cutRoot(const vd::Time&,vd::ExternalEventList& output)
    {
        output.emplace_back("RootDepth");
        output.back().addDouble(700);
    }

    vd::Time dateTunnel(const vd::Time&)
    {
       return 200;
    }
};

}

DECLARE_DYNAMICS(tp3_2_correction::ArvicolaTerrestris)

