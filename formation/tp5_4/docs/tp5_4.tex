% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
% Copyright (c)  2010 - Ronan Trépos -
%                       rtrepos@toulouse.inra.fr
%                       TP Illustration de l'exploration de modèles avec rvle
%
% Permission is granted to copy, distribute and/or modify this document
% under the terms of the GNU Free Documentation License, Version 1.2
% or any later version published by the Free Software Foundation;
% with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
% A copy of the license is included in the section entitled "GNU
% Free Documentation License".
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

\documentclass[12pt,a4paper,oneside]{article}
\usepackage{txfonts}
%\usepackage[latin1]{inputenc}
\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}
\usepackage{fontenc}
\usepackage{lmodern}
\usepackage[pdftex]{thumbpdf}
\usepackage[citecolor={purple},linkcolor={blue},urlcolor={blue},
   a4paper,colorlinks,breaklinks]{hyperref}
\usepackage{txfonts}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{listings}

\graphicspath{{../../figures/}}

\renewcommand\familydefault{\sfdefault}

\usepackage{vmargin}
\setmarginsrb{3.3cm}{2cm}{1cm}{1cm}{1cm}{1cm}{0.5cm}{0.5cm}

\usepackage{color}
\usepackage{url}
\usepackage[french]{babel}
\usepackage{listings}

\author{RECORD team\\Ronan Trépos\\\url{ronan.trepos@inra.fr}}
\title{\textbf{Exploring VLE models with RVLE}}
\date{2017 October 2-6 }
\newcommand{\orangeline}{\rule{\linewidth}{1mm}}

\lstset{language=C++,extendedchars=true,inputencoding=latin1,
    basicstyle=\ttfamily\small, commentstyle=\ttfamily\color{red},
      showstringspaces=false,basicstyle=\ttfamily\small}

\newcommand{\background}{
\setlength{\unitlength}{1in}
\begin{picture}(0,0)
 \put(-1.4,-7.45){\includegraphics[height=28.7cm]{background_tp_5_4}}
\end{picture}
\begin{picture}(0,0)
 \put(0.5,3){\includegraphics{header}}
\end{picture}
}

\begin{document}
\maketitle
\background

\begin{flushright}
 Duration : 1.5 hours
\end{flushright}

\noindent\orangeline

%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Objectives}

The objectives of this practical course are:
\begin{itemize}
\item to learn how to run experiment plan simulation with {\it rvle}.
\item to be aware of some statistical tools to use with VLE models.
\end{itemize}

It is advised to do the record tp\_2\_4 before this one. 


\section*{Software Requirements}
\begin{itemize}
 \item \texttt{vle} (2.0.0), 
 
 and the following packages :
 \begin{itemize}
  \item \texttt{vle.discrete-time}
  \item \texttt{vle.extension.decision}
  \item \texttt{vle.discrete-time.decision}
  \item \texttt{vle.extension.fsa}
  \item \texttt{vle.reader}
  \item \texttt{record.meteo}
  \item \texttt{2CV}
  \end{itemize}
  \item R
 
 and the following packages :
 \begin{itemize}
  \item rvle
  \item sensitivity
  \end{itemize}
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Overview of crop model}

We will perform statistical work on the model {\tt 2CV-exploration.vpz}
from package {\tt 2CV}. 
~\\
Ouptut variables of the model are:
\begin{itemize}
\item {\tt LAI} (leaf area index): it gives the state dynamic 
of the growth of maize.
\item {\tt margin} it gives an economic assesment of the plot for the farmer 
(selling of the maize yield minus the cost of water used for irrigation).
\end{itemize}
~\\
Parameters of the crop model are:
\begin{itemize}
\item {\tt p2logi} : this parameter impacts the increase of the LAI of the maize. 
It will be calibrated from observations. 
\end{itemize}
~\\
Inputs of the agent model that simulates technical operations regarding 
irrigation of the maize are:
\begin{itemize}
\item {\tt irrigationAmount} : amount of water used for each irrigation.
\item {\tt nbDaysBetweenIrrigations} : minimal number of days between two irrigations.
\item {\tt waterAmountForDryPeriod} : rain cumulated over a short period
below which the farmer experiences a dry period.
\item {\tt waterAmountForPlotAccessibility} : rain cumulated over a short period
above which the farmer cannot work on his maize plot.
\item {\tt waterStock} : total water available for irrigation during the whole 
cultural season.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Overview of the exploration processes}


We propose to perfom three experiments:
\begin{itemize}
\item[1] \textbf{Parameter estimation}: from observations of LAI during 8 successive
cultural season of maize crop, we aim at estimating the {\tt p2logi} parameter.
\item[2] \textbf{Sensitivity analysis}: we aim at ordering the inputs of the agent model,
regarding there impact on the margin. The goal is to identify how input variability 
leads to margin variability.
\item[3] \textbf{Simulation based optimization}: Once a key factor from farmer inputs 
is identified, we aim at finding a value to set in order to maximize the margin.
\end{itemize}  
~\\
R procedures used for this work will be {\it nls} for estimation, {\it optim}
for the optimization and  {\it fast99} (from package {\it sensitivity}) for the 
sensitivity analysis experiment.
~\\
To get help on these R functions: use e.g. {\tt help(nls)} in the R session.


%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exercice 1 : estimation of parameter {\tt p2logi}}
\label{ex:exo1}

During the 8 last cultural season of maize crops, the LAI has been observed
at the 150th and stored into the vector {\tt LAIobs}. Climatic data of thes 8 years 
are meteo1.txt, ..., meteo8.txt and they are in the directory 'data' of {\tt 2CV} package.
We will rely on these values to estimate the value of parameter {\tt p2logi}, using a minimization 
of least squares. Formally we seek the best estimate defined as:
\begin{equation}
 \hat{p2logi} = \arg\min_{p2logi} {\sum_{c \in \{1,...,8\}} (LAIsim(p2logi)[c] -
		 LAIobs[c])^2 }
\end{equation}
~\\
Step by step:
~\\
\begin{itemize}
\item Write the function {\tt LAIsim} that produces, for a given value of {\tt  p2logi}, 
the vector of LAI at day 150 of the 8 climatic years.
\begin{eqnarray*}
  \textrm{LAIsim}: & \mathbb{R}   &\to \mathbb{R}^8\\
              ~    & p2logi       &\to \textrm{LAI at day 150 for the 8 years using } p2logi.
\end{eqnarray*}

\item For the exercice purpose we will generate the vector {\tt LAIobs} with the following code:
\begin{lstlisting}
 p2logiTrue <- 0.008645
 LAItrue <- LAIsim(p2logiTrue)
 noise <- rnorm(8,0,0.001)
 LAIobs <- LAItrue + noise
\end{lstlisting}
What is the value of $\hat{p2logi}$ we expect to find with estimation procedure ? 
What is the interest of using the {\tt rnorm} function ?

\item Use the {\tt nls} R function to estimate $\hat{p2logi}$ using {\tt LAIsim}
and {\tt LAIobs}.
\end{itemize}


%%%%%%%%%%%%%%%%%
\section{Exercice 2 : sensitivity analysis of agent model}

This exercice requires the use of the R package {\tt sensitivity}: 
\begin{lstlisting}
install.packages("sensitivity")
\end{lstlisting}

Sensitivity analysis builds for each model input (or parameter) an index 
that quantifies the impact of the input variability on output variability. 
These methods allow the modeler to identify inputs or parameters the most
impacting on model outputs. 
~\\
Step by step: 
\begin{itemize}
\item Write a R function {\tt matrixMargins} that computes the margin
simulated by the model, depending on the values of the agent model inputs:
{\tt irrigationAmount}, \\
{\tt nbDaysBetweenIrrigations}, {\tt waterAmountForDryPeriod},\\
{\tt waterAmountForPlotAccessibility} and {\tt waterStock}. This function
takes as input a matrix of $n$ lines and $5$ columns. $n$ corresponds to the number of 
input combinations to simulate.
\begin{eqnarray*}
  \textrm{matrixMargins}: & \mathbb{R}^{n*5}   &\to \mathbb{R}^n\\
              ~           & inputMatrix        &\to \textrm{margins for the $n$ input combinations}.
\end{eqnarray*}
\item Use the R function {\tt fast99}  from package {\tt sensitivity} to 
perfom a sensitivity analysis. Input bounds are required, they are defined in table \ref{tab:bounds}
\begin{center}
\begin{table}[h]
\begin{tabular}{|c|c|c|}\hline
  input name                      & min. & max. \\\hline
  irrigationAmount                & 5    & 50   \\
  nbDaysBetweenIrrigations        & 1    & 20   \\
  waterAmountForDryPeriod         & 5    & 20   \\
  waterAmountForPlotAccessibility & 1    & 100  \\
  waterStock                      & 50   & 300  \\\hline
\end{tabular}
\caption{\label{tab:bounds} Bounds (minimal and maximal values) of the inputs
used during the sensitivity analysis preocedure.}
\end{table}
\end{center}
\item What are the two most impacting inputs ?
\end{itemize}

%%%%%%
\section{Exercice 3 : optimisation of input {\tt irrigationAmount}}

Here, we aim at finding the amount of water to use for every irrigation activity in order to 
maximize the margin, without knowing the climate series. The irrigation amount should nevertheless
lie into the interval $[28;48]$. We will maximize this input with
respect to the mean margin obtained on the 8 climatic series used in exercice \ref{ex:exo1}.
Formally we want to find the value of {\tt irrigationAmount} that fulfils the following equation:
\begin{equation}
   \textrm{irrigationAmount}^* =  \arg\max_{x \in [28;48]} margin(x)
\end{equation}
~\\
Step by step: 
\begin{itemize}
\item Write a R function {\tt meanMargin} that computes the mean margin, over the
8 climatic series, depending on the value of irrigation amount:
\begin{eqnarray*}
  \textrm{meanMargin}: & \mathbb{R}       &\to \mathbb{R}\\
              ~        & irrigationAmount &\to \textrm{mean margin over the 8 climatic series}.
\end{eqnarray*}
\item Use the R function {\tt optim} to perfom the optimization of the irragtion amount 
with respect to the mean margin.
\end{itemize}

\end{document}
