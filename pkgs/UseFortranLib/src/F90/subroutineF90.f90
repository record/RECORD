      subroutine outputdisplay(c)
      
      USE myModule
      implicit none
      type(coeff_),   intent(INOUT) :: c
      
      open(35,FILE='out.txt',ACCESS='SEQUENTIAL',STATUS='REPLACE')
  100 format(2(f20.7,";"),f20.7)
      write(*, 100) c%alpha, c%beta
      write(35, 100) c%alpha, c%beta
      close(35)
      
      end


      subroutine testsub1(c)
      
      USE myModule
      implicit none
      type(coeff_),   intent(INOUT) :: c
      
      c%alpha = c%alpha + 1.1
      c%beta = c%beta - 1.1
      end


      subroutine testsub2(c)
      
      USE myModule
      implicit none
      type(coeff_),   intent(INOUT) :: c
      
      c%alpha = 20.0
      c%beta = 20.0
      end


      subroutine testsub3(c,a,b)
      
      USE myModule
      implicit none
      type(coeff_),   intent(INOUT) :: c
      real,          intent(IN) :: a
      real,          intent(IN) :: b
      
      c%alpha = a
      c%beta = b
      end


      subroutine testsub4(c)
      USE myModule
      implicit none
      type(coeff_),   intent(INOUT) :: c
      
      call testsub3(c, 10.5,10.5)
      end


      subroutine readtest(FilePath)
      
      implicit none
      character(len=255),       intent(IN) :: FilePath
      integer iyr1,jdate1
      Real solrad,tmx1,tmn1,prec1,drsun1,etp1
      OPEN(2,FILE=FilePath,ACCESS='SEQUENTIAL',STATUS='OLD')
      read (2,'(/,/)')
      read(2,1000) iyr1,jdate1,solrad,tmx1,tmn1,prec1,drsun1,etp1
      write(*,*) iyr1,jdate1,solrad,tmx1,tmn1,prec1,drsun1,etp1
      close(2)
 1000 FORMAT (2I7,6F7.2)

      end


