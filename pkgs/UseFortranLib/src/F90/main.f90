      program testFortran90exe
      
      USE myModule
      implicit none
      type(coeff_)        :: c

      call testsub2(c)
      call outputdisplay(c)
      call testsub1(c)
      call outputdisplay(c)
      call testsub3(c, 10.5, 10.5)
      call outputdisplay(c)
      call testsub1(c)
      call outputdisplay(c)
      
      end program testFortran90exe
