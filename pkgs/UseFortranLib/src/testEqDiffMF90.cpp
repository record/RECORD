#include <vle/DiscreteTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>

#include <string.h>  //memcpy

#include "CStructF90.hpp" // liste des structures de données (ici dans un module) F90 utilisables (lecture et ecriture) depuis le C++
#include "CFuncF90.hpp"  // liste des fonctions (subroutines) F90 utilisable depuis le code C++

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace vle::discrete_time;

namespace UseFortranLib {

class testEqDiffMF90 : public DiscreteTimeDyn
{
public:
    testEqDiffMF90(
       const vd::DynamicsInit& atom,
       const vd::InitEventList& evts)
        : DiscreteTimeDyn(atom, evts),mCtx(vu::make_context())
    {
        x.init(this,"x", evts);
        y.init(this,"y", evts);
        initValue();
    }

    virtual ~testEqDiffMF90()
    {}

    virtual void compute(const vd::Time& time)
    { 
      std::cout << time <<  "\tStart Compute" <<  std::endl;

      std::cout <<  "Call : testsub2_(c), outputdisplay_(c) :" <<  std::endl;
      testsub2_(&c);
      outputdisplay_(&c);

      std::cout <<  "Call : testsub1_(c), outputdisplay_(c) :" <<  std::endl;
      testsub1_(&c);
      outputdisplay_(&c);

      std::cout <<  "Call : testsub3_(c, 10.5, 10.5), outputdisplay_(c) :" <<  std::endl;
      double v1 = 10.5;
      float v2 = 10.5;
      float v1tmp = (float)v1;
      testsub3_(&c, &v1tmp, &v2);
      outputdisplay_(&c);
/*
      std::cout << "DBG!!\t" << *(float*)(&v1) << std::endl;
      testsub3_((float*)(&v1), &v2);
      outputdisplay_();
      
      std::cout << "DBG!!\t" << *reinterpret_cast< float * >(&v1) << std::endl;
      testsub3_(reinterpret_cast< float * >(&v1), &v2);
      outputdisplay_();
*/
      std::cout <<  "Call : testsub1_(c), outputdisplay_(c) :" <<  std::endl;
      testsub1_(&c);
      outputdisplay_(&c);

      x = c.alpha;
      y = c.beta;

      std::cout << c.alpha << " __ " << c.beta << std::endl;
      std::cout << "End Compute" << std::endl;    
    }

    void initValue()
    {
      std::cout <<  "Start Init" <<  std::endl;
      setlocale(LC_NUMERIC,"C");

      std::cout <<  "Call : readtest_(\'gri06-09.met\') :" <<  std::endl;
      char filepath[255];
      memset(filepath, ' ', 255);
      vu::Package pack(mCtx);
      pack.select("UseFortranLib");
      std::string filepath_s = pack.getDataFile("gri06-09.met");
      memcpy(filepath, filepath_s.c_str(), strlen(filepath_s.c_str()));
      readtest_(filepath);

      c.alpha = 50.0;
      c.beta = -50.0;
      std::cout << c.alpha << " __ " << c.beta << std::endl;

      std::cout <<  "Call : testsub4_(c) :" <<  std::endl;
      testsub4_(&c);
      outputdisplay_(&c);
      std::cout << c.alpha << " __ " << c.beta << std::endl;

      std::cout <<  "Call : testsub3_(c, 100.0, 100.0) :" <<  std::endl;
      float initValue = 100.0;
      testsub3_(&c, &initValue, &initValue);      
      std::cout << c.alpha << " __ " << c.beta << std::endl;

      std::cout <<  "End Init" <<  std::endl;
    }

private:
    Var x;
    Var y;
    
    coeff_ c;
    vu::ContextPtr mCtx;

};

} // namespace UseFortranLib

DECLARE_DYNAMICS(UseFortranLib::testEqDiffMF90)

