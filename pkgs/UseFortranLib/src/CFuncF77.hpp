extern "C" {
    extern void testsub1_();

    extern void testsub2_();

    extern void testsub3_(float* alpha, float* beta);

    extern void testsub4_();

    extern void outputdisplay_();  

    extern void readdummy_();

    extern void readtest_(char* FilePath);
}
