#include <vle/DiscreteTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>

#include <string.h>  //memcpy

#include "CFuncF77.hpp"  // liste des fonctions (subroutines) F77 utilisable depuis le code C++
#include "CStructF77.hpp" // liste des structures de données (ici de type COMMON) F77 utilisables (lecture et ecriture) depuis le C++

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace vle::discrete_time;

namespace UseFortranLib {

class testEqDiffMF77 : public DiscreteTimeDyn
{
public:
    testEqDiffMF77(
       const vd::DynamicsInit& atom,
       const vd::InitEventList& evts)
        : DiscreteTimeDyn(atom, evts),mCtx(vu::make_context())
    {
        x.init(this,"x", evts);
        y.init(this,"y", evts);
        initValue();
    }

    virtual ~testEqDiffMF77()
    {}

    virtual void compute(const vd::Time& time)
    { 
      std::cout << time <<  "\tStart Compute" <<  std::endl;

      std::cout <<  "Call : testsub2_(), outputdisplay_() :" <<  std::endl;
      testsub2_();
      outputdisplay_();

      std::cout <<  "Call : testsub1_(), outputdisplay_() :" <<  std::endl;
      testsub1_();
      outputdisplay_();

      std::cout <<  "Call : testsub3_(10.5, 10.5), outputdisplay_() :" <<  std::endl;
      double v1 = 10.5;
      float v2 = 10.5;
      float v1tmp = (float)v1;
      testsub3_(&v1tmp, &v2);
      outputdisplay_();
/*
      std::cout << "DBG!!\t" << *(float*)(&v1) << std::endl;
      testsub3_((float*)(&v1), &v2);
      outputdisplay_();
      
      std::cout << "DBG!!\t" << *reinterpret_cast< float * >(&v1) << std::endl;
      testsub3_(reinterpret_cast< float * >(&v1), &v2);
      outputdisplay_();
*/
      std::cout <<  "Call : testsub1_(), outputdisplay_() :" <<  std::endl;
      testsub1_();
      outputdisplay_();

      x = coeff_.alpha;
      y = coeff_.beta;

      std::cout << coeff_.alpha << " __ " << coeff_.beta << std::endl;
      std::cout << "End Compute" << std::endl;    
    }

    void initValue()
    {
      std::cout <<  "Start Init" <<  std::endl;
      setlocale(LC_NUMERIC,"C");

      std::cout <<  "Call : readtest_(\'gri06-09.met\') :" <<  std::endl;
      char filepath[255];
      memset(filepath, ' ', 255);
      vu::Package pack(mCtx);
      pack.select("UseFortranLib");
      std::string filepath_s = pack.getDataFile("gri06-09.met");
      memcpy(filepath, filepath_s.c_str(), strlen(filepath_s.c_str()));
      readtest_(filepath);

      coeff_.alpha = 50.0;
      coeff_.beta = -50.0;
      std::cout << coeff_.alpha << " __ " << coeff_.beta << std::endl;

      std::cout <<  "Call : testsub4_ :" <<  std::endl;
      testsub4_();
      outputdisplay_();
      std::cout << coeff_.alpha << " __ " << coeff_.beta << std::endl;

      std::cout <<  "Call : testsub3_(100.0, 100.0) :" <<  std::endl;
      float initValue = 100.0;
      testsub3_(&initValue, &initValue);      
      std::cout << coeff_.alpha << " __ " << coeff_.beta << std::endl;

      std::cout <<  "End Init" <<  std::endl;
    }

private:
    Var x;
    Var y;
    vu::ContextPtr mCtx;

};

} // namespace UseFortranLib

DECLARE_DYNAMICS(UseFortranLib::testEqDiffMF77)

