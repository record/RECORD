extern "C" {
    extern void testsub1_(coeff_* c);

    extern void testsub2_(coeff_* c);

    extern void testsub3_(coeff_* c, float* alpha, float* beta);

    extern void testsub4_(coeff_* c);

    extern void outputdisplay_(coeff_* c);

    extern void readtest_(char* FilePath);
}

