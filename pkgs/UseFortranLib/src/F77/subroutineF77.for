      subroutine outputdisplay
      
      Include 'com.blk'
      open(35,FILE='out.txt',ACCESS='SEQUENTIAL',STATUS='REPLACE')
  100 format(2(f20.7,";"),f20.7)
      write(*, 100) alpha, beta
      write(35, 100) alpha, beta
      close(35)
      end


      subroutine testsub1 
      
      Include 'com.blk'
      alpha = alpha + 1.1
      beta = beta - 1.1
      end


      subroutine testsub2
      
      Include 'com.blk'
      alpha = 20.0
      beta = 20.0
      end


      subroutine testsub3(a,b)
      
      Include 'com.blk'
      real a, b
      alpha = a
      beta = b
      end


      subroutine testsub4
      
      call testsub3(10.5,10.5)
      end


      subroutine readtest(FilePath)
      
      character(len=255),       intent(IN) :: FilePath
      integer iyr1,jdate1
      Real solrad,tmx1,tmn1,prec1,drsun1,etp1
      OPEN(2,FILE=FilePath,ACCESS='SEQUENTIAL',STATUS='OLD')
      read (2,'(/,/)')
      read(2,1000) iyr1,jdate1,solrad,tmx1,tmn1,prec1,drsun1,etp1
      write(*,*) iyr1,jdate1,solrad,tmx1,tmn1,prec1,drsun1,etp1
      close(2)
 1000 FORMAT (2I7,6F7.2)

      end


