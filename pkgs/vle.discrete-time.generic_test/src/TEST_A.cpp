/**
  * @file TEST_A.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace generic {
namespace test {

class TEST_A : public DiscreteTimeDyn
{
public:
TEST_A(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    a.init(this, "a", evts);


}

virtual ~TEST_A()
{}

void compute(const vle::devs::Time& /*t*/)
{
a = a(-1) + 1;
}

    Var a;

};

} // namespace test
} // namespace generic
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::generic::test::TEST_A)

