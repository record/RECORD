/**
 * @file src/GenCSVcan.cpp
 * @author Eric Casellas (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2013-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@

#include <vle/devs/Executive.hpp>

#include <vle/value/Matrix.hpp>

#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>

#include <vle/utils/Exception.hpp>

#include <vle/utils/Tools.hpp>

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>


namespace vd = vle::devs;
namespace vv = vle::value;
namespace vp = vle::vpz;
namespace vu = vle::utils;


namespace GenCSVcan {

    class GenCSVcan : public vd::Executive
    {
    public:
        GenCSVcan(const vd::ExecutiveInit& model, const vd::InitEventList& events)
             : vd::Executive(model, events),mCtx(vu::make_context())
        {
            getVPZParameters(events);

            checkVPZParametersValidity();
        }

        
        virtual ~GenCSVcan() {}


        virtual vd::Time init(vd::Time /*time*/) override
        {
            getCSVParameters();
            
            addAllParcellesWithConditions();
            
            if (!RelaiClassName.empty()) {
            editRelaiPorts();
            
            connectIO();
            } else {
                if (DbgLog >= 1.)
                    std::cout << "\t[GenCSVcan]\t\tNo RelaiClassName defined"<< std::endl;
            }
            
            dumpVPZ();
            
            return vd::infinity;
        }


    private:
        //paramètres VPZ
        std::string CsvFileName; //Nom du fichier CSV
        std::string RelaiClassName; //Nom de la classe du modèle Relai
        std::string RelaiName; //Nom du modèle Relai
        std::string ParcelleClassName; //Nom de la classe du vpz à utiliser pour les modèles Parcelle
        std::string ParcellePrefix; //Prefixe des instances Parcelle
        std::string R_inputPortsInd; //Noms des ports d'entrée du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature) 
        std::string R_inputPortsCom; //Noms des ports d'entrée du modèle Relai qui sont communes à toutes les Parcelles
        std::string R_outputPortsInd; //Noms des ports de sortie du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature) 
        std::string R_outputPortsCom; //Noms des ports de sortie du modèle Relai qui sont communes à toutes les Parcelles
        std::string R_Condition; //Nom de la condition de la classe cRelai qui contient le paramètre n
        std::string P_Conditions; //Nom de la condition de la classe cParcelle qui contient les paramètres spécifiques à chaque parcelle
        std::string P_ParamPorts; //Noms des ports la condition P_Conditions de la classe cParcelle des paramètres spécifiques à chaque parcelle
        std::string P_ParamPortsType; //Types des ports la condition P_Conditions de la classe cParcelle des paramètres spécifiques à chaque parcelle
        std::string P_ParamPortsDBName; //Noms des champs de la BDD du fichier CSV contenant les valeurs des paramètres spécifiques à chaque parcelle
        std::string CsvFileDecimalLocale; //Nom de l'option "locale" a utiliser pour le séparateur décimale
        double DbgLog; //Verbosité des sorties
        std::string ConditionSeparator; //séparateur utilisé pour séparer les différents elements des listes contenus dans les string
        std::string CSVSeparator; //séparateur utilisé pour séparer les différents elements du fichier .csv
        std::string P_Condition_id; //Nom de la condition de la classe cParcelle qui contient le paramètre id identifiant spécifiques à chaque parcelle
        std::string DataPackage; //Nom du pkg contenant le dossier data
        
        
        //Variables locales
        unsigned int n; //nombre de parcelles définies dans le fichier CSV
        std::vector<std::string> r_inputPortsInd; //vecteur de R_inputPortsInd
        std::vector<std::string> r_inputPortsCom; //vecteur de R_inputPortsCom
        std::vector<std::string> r_outputPortsInd; //vecteur de R_outputPortsInd
        std::vector<std::string> r_outputPortsCom; //vecteur de R_outputPortsCom
        std::vector<std::string> p_ParamPorts; //vecteur de P_ParamPorts
        std::vector<std::string> p_ParamPortsType; //vecteur de P_ParamPortsType
        std::vector<std::string> p_ParamPortsDBName; //vecteur de P_ParamPortsDBName
        std::vector<std::string> p_Conditions; //vecteur de P_Conditions
        vv::Matrix p; //stocke les valeurs de paramètres lues dans le fichier csv (colonnes=parametres, ligne=parcelles)
 
        vu::ContextPtr mCtx;
     
        //séparation des conditions en vecteurs 
        std::vector<std::string> stringParser (const std::string & Msg, const std::string & Separators)
        {
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            return lst;
        }
        
        //Lecture configuration de l'Executive (paramètres VPZ)
        void getVPZParameters (const vd::InitEventList& events)
        {
            DbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;
            CsvFileName = vv::toString(events.get("CsvFileName"));
            CsvFileDecimalLocale = (events.exist("CsvFileDecimalLocale")) ? vv::toString(events.get("CsvFileDecimalLocale")) : "en_US";

            RelaiClassName = (events.exist("RelaiClassName")) ? vv::toString(events.get("RelaiClassName")) : "cRelai";
            RelaiName = (events.exist("RelaiName")) ? vv::toString(events.get("RelaiName")) : "Relai";
            ParcelleClassName = (events.exist("ParcelleClassName")) ? vv::toString(events.get("ParcelleClassName")) : "cParcelle";
            ParcellePrefix = (events.exist("ParcellePrefix")) ? vv::toString(events.get("ParcellePrefix")) : "Parcelle_";

            ConditionSeparator = (events.exist("ConditionSeparator")) ? vv::toString(events.get("ConditionSeparator")) : ";";
            CSVSeparator = (events.exist("CSVSeparator")) ? vv::toString(events.get("CSVSeparator")) : ";";

            R_inputPortsInd = (events.exist("R_inputPortsInd")) ? vv::toString(events.get("R_inputPortsInd")) : "";
            R_inputPortsCom = (events.exist("R_inputPortsCom")) ? vv::toString(events.get("R_inputPortsCom")) : "";
            R_outputPortsInd = (events.exist("R_outputPortsInd")) ? vv::toString(events.get("R_outputPortsInd")) : "";
            R_outputPortsCom = (events.exist("R_outputPortsCom")) ? vv::toString(events.get("R_outputPortsCom")) : "";

            R_Condition = (events.exist("R_Condition")) ? vv::toString(events.get("R_Condition")) : "";

            P_Conditions = (events.exist("P_Conditions")) ? vv::toString(events.get("P_Conditions")) : "";
            P_ParamPorts = (events.exist("P_ParamPorts")) ? vv::toString(events.get("P_ParamPorts")) : "";
            P_ParamPortsType = (events.exist("P_ParamPortsType")) ? vv::toString(events.get("P_ParamPortsType")) : "";
            P_ParamPortsDBName = (events.exist("P_ParamPortsDBName")) ? vv::toString(events.get("P_ParamPortsDBName")) : "";

            P_Condition_id = (events.exist("P_Condition_id")) ? vv::toString(events.get("P_Condition_id")) : "";
            
            DataPackage = vv::toString(events.get("PkgName"));

            //Vectorisation des conditions
            r_inputPortsInd = stringParser(R_inputPortsInd, ConditionSeparator);
            r_inputPortsCom = stringParser(R_inputPortsCom, ConditionSeparator);
            r_outputPortsInd = stringParser(R_outputPortsInd, ConditionSeparator);
            r_outputPortsCom = stringParser(R_outputPortsCom, ConditionSeparator);
            p_ParamPorts = stringParser(P_ParamPorts, ConditionSeparator);
            p_ParamPortsType = stringParser(P_ParamPortsType, ConditionSeparator);
            p_ParamPortsDBName = stringParser(P_ParamPortsDBName, ConditionSeparator);
            p_Conditions = stringParser(P_Conditions, ConditionSeparator);
        }

        
        //Vérification de la validité de la configuration en fonction du format attendu
        void checkVPZParametersValidity ()
        {
            //Vérification de la présence du parametre obligatoire ParcelleClassName
            if (ParcelleClassName.empty()) {
                std::stringstream ss;
                ss << "Invalid Parameter!!\nParcelleClassName shall not be empty" << std::endl;
                throw vu::ModellingError(ss.str());
            }
            
                //Vérification de la validité du format du fichier CSV ( .csv)
            if (CsvFileName.find(".csv") == std::string::npos) {
                    std::stringstream ss;
                     ss << "Invalid Parameter!!\nCsvFileName :" << CsvFileName
                        << "\nFile format not handled by the model.\nChoose a file with .csv extension" << std::endl;
                    throw vu::ModellingError(ss.str());
            }

            //Verification du même nombre d'éléments dans les paramètres P_Conditions, P_ParamPorts, P_ParamPortsType et P_ParamPortsDBName
            if (!((p_ParamPorts.size() == p_ParamPortsType.size()) && 
                  (p_ParamPorts.size() == p_ParamPortsDBName.size()) &&
                  (p_ParamPorts.size() == p_Conditions.size()))) {
                std::stringstream ss;
                ss << "Invalid Parameters!! All should have the same number of elements :\nP_ParamPorts :" 
                   << p_ParamPorts.size() << "\nP_ParamPortsType :" << p_ParamPortsType.size()
                   << "\nP_ParamPortsDBName :" << p_ParamPortsDBName.size()
                   << "\nP_Conditions :" << p_Conditions.size() << std::endl;
                throw vu::ModellingError(ss.str());
            }
            
            //Vérification si les types de P_ParamPortsType sont pris en charge par le modèle
            for (unsigned int j = 0; j < p_ParamPortsType.size(); j++) { //boucle sur les différents paramètres
                if ((p_ParamPortsType[j] != "double") && 
                    (p_ParamPortsType[j] != "integer") && 
                    (p_ParamPortsType[j] != "string")) {
                    std::stringstream ss;
                     ss << "Invalid Parameter!!\nP_ParamPortsType :" << p_ParamPortsType[j]
                        << "\nData type not handled by the model. Will use string\nChoose values from : { double ; integer ; string }" << std::endl;
                    throw vu::ModellingError(ss.str());
                }
            }
        }
        
        
        //Lecture du fichier CSV et recupération des informations (paramètres spécifiques et nbr de parcelles)
        void getCSVParameters ()
        {
//Lecture du fichier CSV et recupération des informations (nbr de parcelles et paramètres spécifiques)
            if (setlocale(LC_NUMERIC,NULL)!=CsvFileDecimalLocale)
                setlocale(LC_NUMERIC, CsvFileDecimalLocale.c_str());

            vu::Package mPack(mCtx);
            mPack.select(DataPackage);
            std::string csvfilepath = mPack.getDataFile(CsvFileName);
            std::ifstream mFile;
            mFile.open(csvfilepath.c_str(),std::ios_base::in);
            std::string line;

            getline(mFile, line);
            std::vector<std::string> header;
            header = stringParser(line,CSVSeparator);

            std::vector<int> colIndex; // get column index in corresponding to p_ParamPortsDBName string
            for (unsigned int j=0;j<p_ParamPorts.size();j++) //boucle sur les différents paramètres
                for (unsigned int i=0;i<header.size();i++)
                    if (header[i]==p_ParamPortsDBName[j])
                        colIndex.push_back(i);
                
                
            std::vector< std::vector<std::string> > lineVec;
            int count=0;
            while (not line.empty()) {
                getline(mFile, line);
                lineVec.push_back(stringParser(line,CSVSeparator));
                count++;
            }
            n = count - 1;
            if (DbgLog >= 2.) 
                std::cout << std::endl << "\t[GenCSVcan]\tNumber of plots defined in " << csvfilepath << " file : " << n << std::endl;

            p.clear();
            for (unsigned int i = 0; i < p_ParamPorts.size(); i++)
                p.addColumn();
            for (unsigned int i = 0; i < n; i++)
                p.addRow();

            for (unsigned int i = 0; i < n; i++) { //boucle sur les différentes parcelles
                if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\t\tReading parameters for plot n°" << i << std::endl;
                for (unsigned int j=0;j<p_ParamPorts.size();j++) { //boucle sur les différents paramètres
                    if (p_ParamPortsType[j] == "double") {
                        double buffer = atof( lineVec[i][colIndex[j]].c_str() );
                        p.set(j, i, vv::Double::create(buffer));
                    } else if (p_ParamPortsType[j] == "integer") {
                        int buffer = atoi( lineVec[i][colIndex[j]].c_str() );
                        p.set(j, i, vv::Integer::create(buffer));
                    } else if (p_ParamPortsType[j] == "string") {
                        std::string buffer = lineVec[i][colIndex[j]];
                        p.set(j, i, vv::String::create(buffer));
                    }
                    if (DbgLog >= 2.)
                        std::cout << "\t[GenCSVcan]\t\t\t" << p_ParamPortsDBName[j] << " ->\t"<< p_ParamPorts[j] << " =\t" << lineVec[i][colIndex[j]].c_str() << std::endl;
                }
            }
        }
        
        void addAllParcellesWithConditions ()
        {
            vp::Conditions& cond_l = conditions(); //référence aux conditions du VPZ
            
        //Modification de la condition contenant le paramètre n du modèle couplé Relai
            if (!R_Condition.empty()) {
                 vp::Condition& cond_Relai = cond_l.get(R_Condition); //référence à la condition R_Condition de la classe cRelai contenant le paramètre n
                 cond_Relai.setValueToPort("n", vv::Integer::create(n)); //modification de la valeur du port n de la condition R_Condition
                 if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tEditting relay condition\n\t[GenCSVcan]\t\t[" << R_Condition << "].n = " << n << std::endl;
            } else if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tNo condition editting associated to relay" << std::endl;
        
        
            for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 std::stringstream ss;
                ss << ParcellePrefix << i;
                 if (DbgLog >= 2.) {
                    std::cout << "\t[GenCSVcan]\tEditting condition associated to plot " << ss.str() << std::endl;
                }
            //Modification de la condition contenant le paramètre id de chaque Parcelle 
                if (!P_Condition_id.empty()) {
                    vp::Condition& cond_Parcelle = cond_l.get(P_Condition_id);
                    cond_Parcelle.setValueToPort("id", vv::Integer::create(i));
                    if (DbgLog >= 2.)
                        std::cout << "\t[GenCSVcan]\t\t[" << P_Condition_id << "].id = " << i << std::endl;
                }
            //Modification Conditions spécifiques de chaque Parcelle 
                if (p_ParamPorts.size() > 0) {
                     for (unsigned int j = 0; j < p_ParamPorts.size(); j++) { //boucle sur les différents paramètres
                         std::stringstream valstr;
                        vp::Condition& cond_Parcelle = cond_l.get(p_Conditions[j]); //référence à la condition p_Conditions  contenant les paramètres spécifiques des parcelles
                        if (p_ParamPortsType[j] == "double") {
                            const double v = p.getDouble(j,i);
                            cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::Double::create(v));
                            valstr << v;
                        } else if (p_ParamPortsType[j] == "integer") {
                            const int v = p.getInt(j,i);
                            cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::Integer::create(v));
                            valstr << v;
                        } else if (p_ParamPortsType[j] == "string") {
                            const std::string v = p.getString(j, i);
                            cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::String::create(v));
                            valstr << v;
                        } else {
                            const std::string v = p.getString(j, i);
                            cond_Parcelle.setValueToPort(p_ParamPorts[j], vv::String::create(v));
                            valstr << v;
                        }
                         if (DbgLog >= 2.)
                            std::cout << "\t[GenCSVcan]\t\t[" << p_Conditions[j] << "]." << p_ParamPorts[j] <<" = " << valstr.str() << std::endl;
                    }
                } else if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tNo condition editting associated to plot " << ss.str() << std::endl;
                
                 if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tAdding plot model " << ss.str() << " from class " << ParcelleClassName << std::endl;
                vd::Executive::createModelFromClass(ParcelleClassName, ss.str());
            }    
        }
        
        //Creation des ports d'entrée/sortie du modèle Relai
        void editRelaiPorts ()
        {
            if (DbgLog >= 2.)
                std::cout << "\t[GenCSVcan]\tAdding relay model " << RelaiName << " from class " << RelaiClassName << std::endl;
            vd::Executive::createModelFromClass(RelaiClassName, RelaiName);
            if ((r_outputPortsCom.size() > 0) || (r_inputPortsCom.size() > 0)) {
                 if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tAdding common ports to relay model : " << RelaiName << std::endl;
                for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) { //boucle sur les différentes sorties communes entre parcelles
                    AddOutputPort(RelaiName, r_outputPortsCom[j]);
                }
                for (unsigned int j = 0; j < r_inputPortsCom.size(); j++) { //boucle sur les différentes entrées communes entre parcelles
                    AddInputPort(RelaiName, r_inputPortsCom[j]);
                }
            }
            
            if ((r_outputPortsInd.size() > 0) || (r_inputPortsInd.size() > 0)) {
                 if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tAdding individual ports to relay model : " << RelaiName << std::endl;
                for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                     for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                        std::stringstream ss2;
                        ss2 << r_outputPortsInd[j] << "_" << i;
                        AddOutputPort(RelaiName, ss2.str());
                    }
                }
                for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                    for (unsigned int j = 0; j < r_inputPortsInd.size(); j++) { //boucle sur les différentes entrées spécifiques aux parcelles
                        std::stringstream ss2;
                        ss2 << r_inputPortsInd[j] << "_" << i;
                        AddInputPort(RelaiName, ss2.str());
                    }
                }
            }            
        }
        
        void AddInputPort(const std::string &model, const std::string &inputport)
        {
            vd::Executive::addInputPort(model, inputport);
            if (DbgLog >= 2.)
                std::cout << "\t[GenCSVcan]\t\t(input) \t" << inputport << std::endl;
        }

        void AddOutputPort(const std::string &model, const std::string &outputport)
        {
            vd::Executive::addOutputPort(model, outputport);
            if (DbgLog >= 2.)
                std::cout << "\t[GenCSVcan]\t\t(output)\t" << outputport << std::endl;
        }
        
        
        //Connections des Entrées/Sorties entre le modèle Relai et les n modèles Parcelle
        void connectIO ()
        {
            if ((r_outputPortsCom.size() > 0) || (r_inputPortsCom.size() > 0) || (r_outputPortsInd.size() > 0) || (r_inputPortsInd.size() > 0)) {
                if (DbgLog >= 2.)
                    std::cout << "\t[GenCSVcan]\tConnecting inputs/outputs between relay and plot models : " << std::endl;
                for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                    std::stringstream ss;
                    ss << ParcellePrefix << i;
                    if (DbgLog >= 2.)
                        std::cout << "\t[GenCSVcan]\t\tConnecting plot model " << ss.str() << std::endl;
                    for (unsigned int j = 0; j < r_outputPortsCom.size(); j++)//boucle sur les différentes sorties communes entre parcelles
                        AddConnection(RelaiName, r_outputPortsCom[j], ss.str(), r_outputPortsCom[j]);
                    for (unsigned int j = 0; j < r_inputPortsCom.size(); j++) //boucle sur les différentes entrées communes entre parcelles
                        AddConnection(ss.str(), r_inputPortsCom[j], RelaiName, r_inputPortsCom[j]);
                     for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                        std::stringstream ss2;
                        ss2 << r_outputPortsInd[j] << "_" << i;
                        AddConnection(RelaiName, ss2.str(), ss.str(), r_outputPortsInd[j]);
                     }
                    for (unsigned int j = 0; j < r_inputPortsInd.size(); j++) { //boucle sur les différentes entrées spécifiques aux parcelles
                        std::stringstream ss2;
                        ss2 << r_inputPortsInd[j] << "_" << i;
                        AddConnection(ss.str(), r_inputPortsInd[j], RelaiName, ss2.str());
                    }
                }
            } else if (DbgLog >= 2.)
                std::cout << "\t[GenCSVcan]\tNo inputs/outputs connections between relay and plot models." << std::endl;
        }
        
        void AddConnection (const std::string &modelsource, const std::string &outputport, const std::string &modeldestination, const std::string &inputport)
        {
            vd::Executive::addConnection(modelsource, outputport, modeldestination, inputport);
            if (DbgLog >= 2.)
                std::cout << "\t[GenCSVcan]\t\t\t[" << modelsource << "]." << outputport << "\t->\t[" << modeldestination << "]." << inputport << std::endl;
        }
        
        void dumpVPZ ()
        {
            if (DbgLog >= 1.) {
                std::cout << "\t[GenCSVcan]\tDumping file : output_GenCSVcan.vpz" << std::endl;
                std::ofstream file("output_GenCSVcan.vpz");
                dump(file, "dump");
            }
        }        
        
    };
}
DECLARE_EXECUTIVE(GenCSVcan::GenCSVcan);

