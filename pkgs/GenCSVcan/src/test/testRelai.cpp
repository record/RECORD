/**
 * @file src/testRelai.cpp
 * @author Eric Casellas (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <iostream>
#include <sstream>
#include <vector>

namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

namespace GenCSVcan {

class testRelai : public DiscreteTimeDyn
{
    public:
        testRelai(const vd::DynamicsInit& atom, const vd::InitEventList& evts) :
        DiscreteTimeDyn(atom, evts)
        {    
            std::cout << "\t[testRelai]\tConstructor..." << std::endl;
            n = vv::toInteger(evts.get("n"));
            std::cout << "\t[testRelai]\tn:" << n << std::endl;
            
            Tmoy.init(this,"Tmoy", evts);
            
            for (unsigned int j = 0; j < n; j++) {
                std::stringstream ss1;
                ss1 << "Revenu_" << j;
                Revenus.push_back(Var());
                Revenus[Revenus.size()-1].init(this, ss1.str(), evts);
                
                std::stringstream ss2;
                ss2 << "Production_" << j;
                Productions.push_back(Var());
                Productions[Productions.size()-1].init(this, ss2.str(), evts);
                
                std::cout << "\t[testRelai]\t\tadded Var:" << ss1.str() << std::endl;
            }
        }

        virtual ~testRelai() {}

        virtual void compute(const vd::Time& time)
        { 
            std::cout << time << "\t[testRelai]\tcompute... " << std::endl;
            Tmoy = std::max(0.1, cos(time));
               for (unsigned int j = 0; j < n; j++) //boucle sur les n parcelles
                Revenus[j] = Productions[j](-1) * 1.5;
        }

    private:
        std::vector<Var> Revenus;
        Var Tmoy;

        std::vector<Var> Productions;

        unsigned int n; // nombre de parcelles
};

} // namespace GenCSVcan

DECLARE_DYNAMICS(GenCSVcan::testRelai)

