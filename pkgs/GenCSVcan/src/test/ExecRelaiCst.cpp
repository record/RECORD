/**
 * @file src/ExecRelaiCst.cpp
 * @author Eric Casellas (The RECORD team -INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@


#include <vle/devs/Executive.hpp>

#include <vle/utils/Tools.hpp>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vp = vle::vpz;
namespace vu = vle::utils;

namespace GenCSVcan {

    class ExecRelaiCst : public vd::Executive
    {
    public:
        ExecRelaiCst(const vd::ExecutiveInit& model, const vd::InitEventList& events)
             : vd::Executive(model, events)
        {
            std::cout << "\t[ExecRelaiCst]\tConstructor..." << std::endl;
            //Lecture configuration de l'Executive (paramètres VPZ)
            n = vv::toInteger(events.get("n"));
            std::cout << "\t[ExecRelaiCst]\tn:" << n << std::endl;
            R_outputPortsInd = vv::toString(events.get("R_outputPortsInd"));
            R_outputPortsCom = vv::toString(events.get("R_outputPortsCom"));
            
            r_outputPortsInd = stringParser(R_outputPortsInd, " ");
            r_outputPortsCom = stringParser(R_outputPortsCom, " ");
        }

        
        virtual ~ExecRelaiCst() {}


        virtual vd::Time init(vd::Time /*time*/) override
        {
            return 0.;
        }
        
            virtual void internalTransition(vd::Time /*time*/) override
            {
                std::cout << "\t[ExecRelaiCst]\tinternalTransition..." << std::endl;
            //Ajout des ports d'entrée/sortie du modèle Relai
            std::vector < std::string > inputs;
            std::vector < std::string > outputs;
            {
                std::stringstream ss;
                ss << "update";
                outputs.push_back(ss.str());
            }
            
            //Association des conditions du modèle
            std::vector <std::string> vzs;
            vzs.push_back("cond_cst");
    
            //Création du modèle atomique avec ses ports d'entrée/sortie
            std::cout << "\t[ExecRelaiCst]\t\tadding model Constant" << std::endl;
            createModel("Constant", inputs, outputs, "dyn_cst", vzs, "");
            
            std::cout << "\t[ExecRelaiCst]\t\tConnecting Constant and Relai" << std::endl;
            //Connections des ports d'entrée/sortie du modèle atomique Relai_atom avec les ports du modèle couplé Relai contenant cet executif
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) //boucle sur les différentes sorties communes entre parcelles
                vd::Executive::addConnection("Constant", "update", "Relai", r_outputPortsCom[j]);

            for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss2;
                    ss2 << r_outputPortsInd[j] << "_" << i;
                    vd::Executive::addConnection("Constant", "update", "Relai", ss2.str());
                }
            }

            std::cout << "\t[ExecRelaiCst]\tDumping output_ExecRelaiCst.vpz... " << std::endl;
            std::ofstream file("output_ExecRelaiCst.vpz");
            dump(file, "dump");

        }


    private:
        //paramètres VPZ
        unsigned int n; //nombre de parcelles définies dans le fichier SIG
        std::string R_inputPorts; //Noms des ports d'entrée du modèle Relai
        std::string R_outputPortsInd; //Noms des ports de sortie du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature) 
        std::string R_outputPortsCom; //Noms des ports de sortie du modèle Relai qui sont communes à toutes les Parcelles
        std::string R_ConditionVar; //Nom de la condition de la classe cRelai qui contient le port variables de declaration des Var
        
        //Variables locales
        std::vector<std::string> r_inputPorts; //vecteur de R_inputPorts
        std::vector<std::string> r_outputPortsInd; //vecteur de R_outputPortsInd
        std::vector<std::string> r_outputPortsCom; //vecteur de R_outputPortsCom


        //séparation des conditions en vecteurs 
        std::vector<std::string> stringParser (const std::string & Msg, const std::string & Separators)
        {
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            return lst;
        }

    };
}
DECLARE_EXECUTIVE(GenCSVcan::ExecRelaiCst);

