			GenCSVcan package development team
			=============================

Copyright
=========

- INRA
  - Copyright © 2009-2017
  - French National Institute for Agricultural Research
  - Institut National de la Recherche Agronomique
  - http://www.inra.fr

Authors
=======

Integrator / Release manager
----------------------------

- Helene Raynal <Helene.raynal@inra.fr>


Main programmer and founder
---------------------------

- Eric Casellas <eric.casellas@inra.fr>


Contributors
------------

- Eric Casellas <eric.casellas@inra.fr>
- Patrick Chabrier <Patrick.Chabrier@inra.fr>
