/**
  * @file DateTimeSample.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp> //vu::DateTime::toJulianDay

#include <iostream> // std::cout
#include <iomanip> // std::setprecision


namespace vu = vle::utils;
namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace DateTime {

class DateTimeSample : public DiscreteTimeDyn
{
public:
DateTimeSample(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    in.init(this, "in", evts);

if (evts.get("begin_date")->isDouble()) {
	begin_time = evts.getDouble("begin_date");
	begin_date = vu::DateTime::toJulianDay(begin_time);
} else if (evts.get("begin_date")->isString()) {
	begin_date = evts.getString("begin_date");
	begin_time = vu::DateTime::toJulianDay(begin_date);
}

std::cout << std::setprecision(10)<< "******\nbegin_time:" << begin_time << std::endl;
std::cout << "begin_date:" << begin_date << std::endl;

}

virtual ~DateTimeSample()
{}

void compute(const vle::devs::Time& t)
{

std::cout << "------\nt:" << t << std::endl;

vle::devs::Time time = begin_time+t;
std::cout << std::setprecision(10)<< "time=begin_time+time:" << time << std::endl;

std::cout << "in(-1):" << in(-1) << std::endl;

std::cout << "currentDate(): " << vu::DateTime::currentDate() << std::endl;

std::cout << "year(time): " << vu::DateTime::year(time) << std::endl;

std::cout << "month(time): " << vu::DateTime::month(time) << std::endl;

std::cout << "dayOfMonth(time): " << vu::DateTime::dayOfMonth(time) << std::endl;

std::cout << "dayOfWeek(time): " << vu::DateTime::dayOfWeek(time) << std::endl;

std::cout << "dayOfYear(time): " << vu::DateTime::dayOfYear(time) << std::endl;

std::cout << "weekOfYear(time): " << vu::DateTime::weekOfYear(time) << std::endl;

std::cout << "isLeapYear(time): " << vu::DateTime::isLeapYear(time) << std::endl;

std::cout << "aYear(time): " << vu::DateTime::aYear(time) << std::endl;

std::cout << "aMonth(time): " << vu::DateTime::aMonth(time) << std::endl;

std::cout << "aWeek(): " << vu::DateTime::aWeek() << std::endl;

std::cout << "aDay(): " << vu::DateTime::aDay() << std::endl;

std::cout << "years(time, 1): " << vu::DateTime::years(time, 1) << std::endl;

std::cout << "months(time, 1): " << vu::DateTime::months(time, 1) << std::endl;

std::cout << "weeks(1): " << vu::DateTime::weeks(1) << std::endl;

std::cout << "days(1): " << vu::DateTime::days(1) << std::endl;

std::cout << "days(1.5): " << vu::DateTime::days(1.5) << std::endl;

std::cout << "convertUnit('day'): " << enum2str(vu::DateTime::convertUnit("day")) << std::endl;

std::cout << "convertUnit('week'): " << enum2str(vu::DateTime::convertUnit("week")) << std::endl;

std::cout << "convertUnit('month'): " << enum2str(vu::DateTime::convertUnit("month")) << std::endl;

std::cout << "convertUnit('year'): " << enum2str(vu::DateTime::convertUnit("year")) << std::endl;

std::cout << "duration(time, 10, convertUnit('day')): " << vu::DateTime::duration(time, 10, vu::DateTime::convertUnit("day")) << std::endl;

std::cout << "duration(time, 10, convertUnit('week')): " << vu::DateTime::duration(time, 10, vu::DateTime::convertUnit("week")) << std::endl;

std::cout << "duration(time, 10, convertUnit('month')): " << vu::DateTime::duration(time, 10, vu::DateTime::convertUnit("month")) << std::endl;

std::cout << "duration(time, 10, convertUnit('year')): " << vu::DateTime::duration(time, 10, vu::DateTime::convertUnit("year")) << std::endl;

std::cout << "toJulianDayNumber(time): " << vu::DateTime::toJulianDayNumber(time) << std::endl;

std::cout << "toJulianDayNumber(toJulianDayNumber(time)): " << vu::DateTime::toJulianDayNumber(vu::DateTime::toJulianDayNumber(time)) << std::endl;

std::cout << "toJulianDay(time): " << vu::DateTime::toJulianDay(time) << std::endl;

std::cout << "toJulianDay(toJulianDay(time)): " << vu::DateTime::toJulianDay(vu::DateTime::toJulianDay(time)) << std::endl;

std::cout << "isValidYear(time): " << vu::DateTime::isValidYear(time) << std::endl;

int year, month, day, hours, minutes, seconds;
std::cout << "toTime(time+10, year, month, day, hours, minutes, seconds): " << vu::DateTime::toTime(time+10, year, month, day, hours, minutes, seconds) << std::endl;
std::cout << "seconds: " << seconds << std::endl;

}

    Var in;

std::string  begin_date;
double begin_time;

enum Unit { None, Day, Week, Month, Year };

char* enum2str(vu::DateTimeUnitOptions u) {
    switch (u) {
        case vu::DATE_TIME_UNIT_NONE:
            return (char*)&"None";
        case vu::DATE_TIME_UNIT_DAY:
            return (char*)&"Day";
        case vu::DATE_TIME_UNIT_WEEK:
            return (char*)&"Week";
        case vu::DATE_TIME_UNIT_MONTH:
            return (char*)&"Month";
        case vu::DATE_TIME_UNIT_YEAR:
            return (char*)&"Year";
        default:
            return (char*)&"INVALID ENUM";
    }
}


};

} // namespace DateTime
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::DateTime::DateTimeSample)

