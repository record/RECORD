Package: DateTime
Version: 1.0.0
Depends:
Build-Depends: vle.discrete-time
Conflicts:
Maintainer: Eric Casellas <eric.casellas@inra.fr>
Description: vle Date function usage sample
 .
Tags: examples record
Url: http://recordb.toulouse.inra.fr/distributions/2.0
Size: 0
MD5sum: xxxx
