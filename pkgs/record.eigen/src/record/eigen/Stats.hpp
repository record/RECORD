/*
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems.
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2013 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2013 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2013 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

//  ###################################################################
//  #
//  #  This file contains several functions for computing quantities related
//  #  to CSN distributions: density, cumulative probabilities, quantiles,
//  #  marginal densities and marginal parameters. Random draws of CSN
//  #  distributions and conditional simulations are also possible
//  #
//  ###################################################################

#ifndef RECORD_EIGEN_STATS
#define RECORD_EIGEN_STATS

#undef NDEBUG
#undef EIGEN_NO_DEBUG
#include <Eigen/Eigen>
#include <vle/utils/Rand.hpp>
#include <vle/value/Matrix.hpp>
#include <vle/value/Table.hpp>
#include <vle/value/Tuple.hpp>
#include <vle/value/Double.hpp>

using namespace Eigen;

namespace record { namespace eigen {

namespace vv = vle::value;
namespace vu = vle::utils;

constexpr double pi() { return std::atan(1)*4; }

class Stats
{
public:
    Stats(){}
    ~Stats(){}

    /**
     * @brief Compute max value of a vv::Tuple
     * @param v, a vv::Tuple
     * @retrurn the max value
     */
    static double max(vv::Tuple& v);

    /**
     * @brief Compute min value of a vv::Tuple
     * @param v, a vv::Tuple
     * @retrurn the min value
     */
    static double min(vv::Tuple& v);

    /**
     * @brief fill vv::Tuple with one dimension eigen object
     * @param the tuple to fill
     * @param, data, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     */
    template <typename ONE_DIM_EIGEN>
    static void fill(vv::Tuple& tofill, ONE_DIM_EIGEN data)
    {
        tofill.value().resize((unsigned int) data.size(),0.0);
        vv::Tuple::iterator itbfill = tofill.value().begin();
        for (typename ONE_DIM_EIGEN::InnerIterator itbdata(data,0);
                 itbdata; ++itbdata, ++itbfill) {
            (*itbfill) = itbdata.value();
        }
    }

    /**
     * @brief Compute mean value of one dimension eigen object
     * @param, data, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     * @return mean value
     */
    template <typename ONE_DIM_EIGEN>
    static double mean(ONE_DIM_EIGEN data)
    {
        double sum = 0;
        double size = 0;
        for (typename ONE_DIM_EIGEN::InnerIterator itbdata(data,0);
                 itbdata; ++itbdata) {
            size += 1;
            sum += itbdata.value();
        }
        return sum/size;
    }

    /**
     * @brief Compute uniased std of one dimension eigen object
     * @param, data, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     * @return unbiased std value
     */
    template <typename ONE_DIM_EIGEN>
    static double std(ONE_DIM_EIGEN data)
    {
        double mean_data = mean(data);
        double moment2 = 0;
        double size = 0;
        for (typename ONE_DIM_EIGEN::InnerIterator itbdata(data,0);
                 itbdata; ++itbdata) {
            size += 1;
            moment2 += std::pow((double) itbdata.value() - mean_data, 2);
        }
        return std::sqrt((double) moment2/(size - 1.0));
    }

    /**
     * @brief Compute variance of one dimension eigen object
     * @param, data, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     * @return variance
     */
    template <typename ONE_DIM_EIGEN>
    static double var(ONE_DIM_EIGEN data)
    {
        double mean_data = mean(data);
        double moment2 = 0;
        double size = 0;
        for (typename ONE_DIM_EIGEN::InnerIterator itbdata(data,0);
                 itbdata; ++itbdata) {
            size += 1;
            moment2 += std::pow((double) itbdata.value() - mean_data, 2);
        }
        return  moment2/(size - 1.0);
    }

    /**
     * @brief Compute covariance of two samples
     * @param, data1, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     * @param, data2, eigen one dim container ( MatrixXd::ColXpr, VectorXd, ...)
     * @return covariance of data1 and data2
     */
    template <typename ONE_DIM_EIGEN>
    static double cov(ONE_DIM_EIGEN data1, ONE_DIM_EIGEN data2)
    {
        double mean1 = mean(data1);
        double mean2 = mean(data2);

        double moment2 = 0;
        double size = 0;
        typename ONE_DIM_EIGEN::InnerIterator itbdata1(data1,0);
        typename ONE_DIM_EIGEN::InnerIterator itbdata2(data2,0);
        for (;itbdata1; ++itbdata1, ++itbdata2) {
            size += 1;
            moment2 += ((double) itbdata1.value() - mean1)*
                       ((double) itbdata2.value() - mean2);
        }
        return  moment2/(size);
    }


    /**
     * @brief Converts a vle matrix to an eigen matrix
     * @param mat, the vle matrix to convert
     * @return the eigen matrix
     */
    static MatrixXd toEigenMat(const vv::Matrix& mat);

    /**
     * @brief Converts a vle table to an eigen matrix
     * @param mat, the vle table to convert
     * @return the eigen matrix
     */
    static MatrixXd toEigenMat(vv::Table& mat);

    /**
     * @brief Find an approximation of M that is definite positive
     * @param M, a square matrix
     * @return a square matrix definite positive
     */
    static MatrixXd def_pos(MatrixXd M);



    /**
     * @brief Computes the square root of a positive definite matrix
     * @param M, a square matrix
     * @return MS, square matrix such that MS * MS = M
     */
    static MatrixXd sqrtm(const MatrixXd& M);

    /**
     * @brief Computes the square root of square matrix not necesseraly
     * positive definite (not sure). SVD more general than eigen decomposition.
     * @param  M, a square matrix
     * @return a square matrix such that MS * MS = M
     */
    static MatrixXd sqrtm_jacobi(const MatrixXd& M);

    /**
     * @brief Check if a matrix is symmetric
     * @param M, the matrix to check
     * @param sendError, if true send an exception if M is not symmetric with
     * a detailed message
     * @return true if M is symmetric
     */
    static bool checkSymmety(const MatrixXd& M, bool sendError=true);

    /**
     * @brief Computes the density of multivariate normal distribution (pdf) :
     *
     * (2 * \pi) * |\Sigma|^(-1/2) * e^((-1/2) * (x-\mu)' \Sigma^(-1) (x-\mu))
     *
     * @param z, vector at which the density is computed
     * @param mu, mean of the normal distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * @return the density of MVN at point z
     */
    static double dmnorm(const VectorXd& z, const VectorXd& mu, const MatrixXd& sigma);

    /**
     * @brief Performs one draw according a multivariate normal distribution
     *
     * @param mu, mean of the normal distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * @return a sample of MVN with mean mu and covariance sigma
     *
     * @notes: implementation comes from wikipedia using spectral decomposition
     * and not Cholesky Decomposition
     */
    static VectorXd rmnorm(const VectorXd& mu, const MatrixXd& sigma, vu::Rand& rng);

    /**
     * @brief Performs man draws according a multivariate normal distribution
     *
     * @param n, the number of points to draw
     * @param mu, mean of the normal distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * @return a sample of MVN with mean mu and covariance sigma
     *
     * @notes: implementation comes from wikipedia using spectral decomposition
     * and not Cholesky Decomposition
     */
    static MatrixXd rmnorm(unsigned int n, const VectorXd& mu,
            const MatrixXd& sigma, vu::Rand& rng);


    /**
     * @brief computes the multivariate kernel density function
     * @brief x, the point where to compute density estimator
     * @brief data, the data matrix, each row is point
     * @return the value of density function at x
     *
     * @note
     * https://en.wikipedia.org/wiki/Multivariate_kernel_density_estimation
     */
    static double kernelDensity(const VectorXd& x, const MatrixXd& data);


    /**
     * @brief Compute a covariance matrix
     * @param mat, a matrix of observation where columns are
     * variables and lines observations
     * @return the coovariance matrix :
     *   C(i,i) represents variance of column of i
     *   C(i,j) represents covariance of column i and j
     */
    static MatrixXd covariance(MatrixXd& mat, bool centered = false);

    static VectorXd subVector_noti(const VectorXd& vec, unsigned int i);

    /**
     * @brief computes the sub matrix that contains only the ith row of
     * a matrix and all the columns except the ith column
     *
     * @param mat, a square matrix
     * @param i, index
     */
    static VectorXd subMatrix_i_noti(const MatrixXd& mat, unsigned int i);

    /**
     * @brief computes the sub matrix that contains all except the ith row and
     * the ith column
     *
     * @param mat, a square matrix
     * @param i, index
     */
    static MatrixXd subMatrix_noti_noti(const MatrixXd& mat, unsigned int i);

    /**
     * @brief Checks wether the variables are within accepted bounds
     * @param X: vector of variables
     * @param vbounds : lower and upper bounds for each variables
     * @return true if X lie into vbounds, false otherwise
     */
    static bool testvariables(const VectorXd& X,const MatrixXd& vbounds);

    /**
     * @brief compute a Vector which contains the max element 
     * for each index of two vectors
     * @param X1: vector
     * @param X2: vector with the same size of X1
     * @return a vector of the same size of X1 which contains the max
     * element of X1 and X2 for each index
     */
    static VectorXd zip_max(const VectorXd& X1,const VectorXd& X2);


    /**
     * @brief Fills a matrix with normal numbers
     * @param m, the eigen matrix to fill
     * @param rng, the random number generator
     */
    static void fillWithNormalDistrib(MatrixXd& m, vu::Rand& rng);

    /**
     * @brief Fills a vector with normal numbers
     * @param v, the eigen vector to fill
     * @param rng, the random number generator
     */
    static void fillWithNormalDistrib(VectorXd& v, vu::Rand& rng);

    static unsigned int sample_int(const VectorXd& probs, vu::Rand& rng);

    static double runif(double min, double max, vu::Rand& rng);

    static VectorXd runif(unsigned int n, double min, double max, vu::Rand& rng);

    static double rnorm(double mean, double sd, vu::Rand& rng);

    static VectorXd rnorm(unsigned int n, double mean, double sd, vu::Rand& rng);

    static double rexp(double lambda, vu::Rand& rng);

    static double dnorm(double loc, double mean, double sd);

    static double pnorm(double loc, double mean, double sd);

    static VectorXd pnorm(const VectorXd& loc, double mean, double sd);

    static VectorXd qnorm(const VectorXd& loc, double mean, double sd);

    static double qnorm(double loc, double mean, double sd);


    /**
     * @brief Performs draws of a truncated univariate normal
     * distribution
     *
     * @param n, number of points to draw
     * @param mean, mean of the normal distribution
     * @param mean_min, min bound of the TMVN distribution
     * @param mean_max, max bound of the TMVN distribution
     * @param sd, sd dev TVN distribution
     * @return n samples of TVN with mean 'mean' and std dev 'sd',
     * lower bound 'mean_min' and upper bound 'max_mean'
     *
     * @notes: implementation according :
     * pkg R "tmvtnorm"
     * see also
     *  Jayesh H. Kotecha and Petar M. Djuric (1999) :
     * GIBBS SAMPLING APPROACH FOR GENERATION OF TRUNCATED MULTIVARIATE
     *  GAUSSIAN RANDOM VARIABLES
     */
    static VectorXd rtnorm(unsigned int n, double mean, double mean_min,
            double mean_max, double sd, vu::Rand& rng);
    static double rtnorm( double mean, double mean_min,
            double mean_max, double sd, vu::Rand& rng);

    /**
     * @brief Performs draw according a truncated multivariate normal
     * distribution, using Gibbs MCMC
     *
     * @param nbPoints, nb points to draw
     * @param mu, mean of the normal distribution
     * @param mu_min, min bound of the TMVN distribution
     * @param mu_max, max bound of the TMVN distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * @param startPoint, starting point of the markov chain
     * @param burn_in, number of samples to discard at the beginning of
     * the process
     *
     * @return a sample of TMVN with mean mu, covariance sigma, min bound
     * 'mu_min' and max bound 'mu_max'
     *
     * @notes: implementation according
     *  Jayesh H. Kotecha and Petar M. Djuric (1999) :
     * GIBBS SAMPLING APPROACH FOR GENERATION OF TRUNCATED MULTIVARIATE
     *  GAUSSIAN RANDOM VARIABLES
     */
    static MatrixXd rtmnorm_gibbs(unsigned int nbPoints,
            const VectorXd& mu, const VectorXd& mu_min,
            const VectorXd& mu_max, const MatrixXd& sigma,
            const VectorXd& startPoint, unsigned int burn_in,
            vu::Rand& rng);

    /**
     * @brief Performs draw according a truncated multivariate normal
     * distribution, using simple rejection algorithm
     *
     * @param nbPoints, nb points to draw
     * @param mu, mean of the normal distribution
     * @param mu_min, min bound of the TMVN distribution
     * @param mu_max, max bound of the TMVN distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * the process
     *
     * @return a sample of TMVN with mean mu, covariance sigma, min bound
     * 'mu_min' and max bound 'mu_max'
     *
     * @notes: implementation according
     * Chrstian P. Robert : "Simulation of truncated normal variables" (2009)
     */
    static MatrixXd rtmnorm_rejection(unsigned int nbPoints,
            const VectorXd& mu, const VectorXd& mu_min,
            const VectorXd& mu_max, const MatrixXd& sigma, 
            vu::Rand& rng);

    /**
     * @brief Computes the distribution function of multivariate normal
     * distribution (cdf) in the special case were sigma is diagonal
     * (no correlation). In the general case, an optimization algorithm is
     * required (no analytic function).
     *
     * We use the property :
     * Phi_k(x, Mu, Sigma) =
     *   Phi_1(x[1], Mu[1], Sigma[1,1]) * ... *  Phi_1(x[k], Mu[k], Sigma[k,k])
     * where Phi_k is the multivariate normal cdf for dimension k.
     *
     * @param z, vector at which the density is computed
     * @param mu, mean of the normal distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * (required to be diagonal)
     * @return the cumulative distribution function of MVN at point z
     */
    static double pmnorm_indep(const VectorXd& z, const VectorXd& mu,
            const MatrixXd& sigma);

//###################################################################
//#
//#
//# Set of CSN functions for computing the density, cumulative function or quantiles
//# of a univariate RV
//#     CNS(location,scale^2,skew)= CSN_{1,1}(location,scale^2,skew/scale,0,1-skew^2)
//#
//# Note: scale^2 must be thought of as variance, and skew is in [-1,1].
//#
//# ARGUMENT
//#    z or p: value at which the function is computed
//#    n    : number of random draws
//#
//# VALUE
//#   density, cpf, quantile or random draw
//#
//###################################################################

    /**
     * @brief Computes the density of a CNS*(location,scale^2,shape)
     * = CSN_{1,1}(location,scale^2,\delta,1-d^2)
     */
    static double dcsn (double /*z*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/);

    /**
     * @brief Computes the cumulative probability of a
     * CNS*(location,scale^2,shape) = CSN_{1,1}(location,scale^2,\delta,1-d^2)
     */
    static double pcsn(double /*z*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/);

    /**
     * @brief Computes the quantile of a CNS*(location,scale^2,shape)
     * = CSN_{1,1}(location,scale^2,\delta,1-d^2)
     */
    static double qcsn(double /*p*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/);

    /**
     * @brief random generation of a CNS*(location,scale^2,shape)
     * = CSN_{1,1}(location,scale^2,\delta,1-d^2)
     */
    static double rcsn(double /*n=1*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/);

    //###################################################################
    //#
    //#
    //#  Functions for computing the density of a multivariate CSN(Mu,Sigma,D,Nu,Delta)
    //#  and for drawinf random vectors.
    //#  Coded according to Karimi and Mohammadzadeh (2012), Proposition 1(ii)
    //#
    //# ARGUMENT
    //#     n     number of random draws
    //#     y     vector at which the density is computed
    //#     Mu    vector of joint location parameters
    //#     Sigma matrix of joint covariance matrix
    //#     D     matrix with skewness parameters
    //#     Nu    vector of joint location parameters
    //#     Delta matrix of joint location parameters
    //#
    //#
    //# VALUE
    //#     value of the density (dmcsn), or n random draws (rmcsn)
    //#
    //###################################################################

    static double dmcsn(const MatrixXd& /*W*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/);


    /**
     * @brief Draws points from a CSN distribution
     *
     * @param n, number of points to drax
     * @param Mu, location of the CSN distribution
     * @param Sigma, covariance of the CSN distribution
     * @param D
     * @param Nu
     * @param Delta
     *
     * @return n points drawn according the CSN distribution
     */
    static MatrixXd rmcsn(unsigned int n, const VectorXd& Mu, const MatrixXd& Sigma,
            const MatrixXd& D, const VectorXd&Nu, const MatrixXd& Delta, 
            vu::Rand& rng);


    /**
     * @brief computes the density of a  multivariate CNS*(Mu,Sigma,Skew)
     * CNS*(Mu,Sigma,Skew)  = CSN_{k,k}(Mu,Sigma,Skew%*%Sigma^{-1/2},0,Id_k-Skew^2)
     * @param z, vector at which the density is computed
     * @param mu, location of the CSN* distribution
     * @param sigma, covariance of the CSN* distribution
     * @param skew, skewness paramaters (in [-1;1])
     * @return density of the CSN* distribution at point z
     */
    static double dmcsnstar(const VectorXd& z, const VectorXd& mu,
            const MatrixXd& sigma, const VectorXd& skew);

    /**
     * @brief draws points from a CSN* distribution
     * @param n, number of points to drax
     * @param mu, location of the CSN* distribution
     * @param sigma, covariance of the CSN* distribution
     * @param skew, skewness paramaters (in [-1;1])
     * @return n points drawn according the CSN* distribution
     */
    static MatrixXd rmcsnstar(unsigned int n, const VectorXd& Mu,
            const MatrixXd& Sigma, const VectorXd& Skew, 
            vu::Rand& rng);

    /**
     * @brief Computes the marginal density of a CSN distribution.
     * Coded according to Dominguez-Molina, Gonzales-Farias and Gupta, 2003.
     * @param y vector at which the density is computed
     * @param sel selection of variables of the marginal density
     * @param Mu, vector of joint location parameters
     * @param Sigma, matrix of joint covariance matrix
     * @param D, matrix with skewness parameters
     * @param Nu, vector of joint location parameters
     * @param Delta, matrix of joint location parameters
     * @return value of the density
     */
    static double dmcsn_marg(double /*y*/, double /*sel*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/);

    /**
     * @brief Extracts the parameters of a marginal density of a
     * CSN distribution
     * @param sel selection of variables ofr the marginal density
     * @param Mu    vector of joint location parameters
     * @param Sigma matrix of joint covariance matrix
     * @param D     matrix with skewness parameters
     * @param Nu    vector of joint location parameters
     * @param Delta matrix of joint location parameters
     * @return  A list containing Mu.1, Sigma.11, D.start, Delta.star, DD
     */
    static double extract_csn_marg(double /*sel*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/);

    /**
     * @brief  performs one random draw of a conditional multivariate CSN distribution:
     * - length of y (say n.o) must be < length of Mu
     * - it is assumed that the n.o first coordinates of mu correspond to that of y.
     * - it is also assumed that the n.o x n.o upper block of Sigma corresponds to that of y
     * - similar requirement for D
     * @param  n        number of random draws
     * @param  y_cond   vector of conditioning values
     * @param  Mu       vector of location parameters
     * @param  Sigma    matrix of covariance matrix
     * @param  D        matrix with skewness parameters
     * @param  Nu       vector of location parameters
     * @param  Delta    matrix of location parameters
     */
    static MatrixXd rmcsn_cond(unsigned int n, const VectorXd& y_cond,
            const VectorXd& Mu, const MatrixXd& Sigma,
            const MatrixXd& D, const VectorXd& Nu, const MatrixXd& Delta,
            vu::Rand& rng);


    //see equation 3.2
    static MatrixXd internal_sigma_noti_noti_inv(
            const MatrixXd& sigma_inv, unsigned int i);


};

}} // namespaces

#endif
