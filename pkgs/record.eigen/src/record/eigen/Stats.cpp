/*
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems.
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2013 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2013 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2013 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>
#include <boost/math/distributions/normal.hpp>
#include <Eigen/SVD>
#include <Eigen/LU>
#include <vle/utils/Exception.hpp>
#include <vle/utils/Tools.hpp>


#include "Stats.hpp"


namespace record { namespace eigen {

using namespace Eigen;
namespace vv = vle::value;
namespace vu = vle::utils;


    double Stats::max(vv::Tuple& v)
    {
        vv::Tuple::iterator itb = v.value().begin();
        vv::Tuple::iterator ite = v.value().end();
        double max = -9999999;
        for (; itb != ite; ++itb) {
            if (max < *itb) {
                max = *itb;
            }
        }
        return max;
    }


    double Stats::min(vv::Tuple& v)
    {
        vv::Tuple::iterator itb = v.value().begin();
        vv::Tuple::iterator ite = v.value().end();
        double min = 9999999;
        for (; itb != ite; ++itb) {
            if (min > *itb) {
                min = *itb;
            }
        }
        return min;
    }


    MatrixXd Stats::toEigenMat(const vv::Matrix& mat)
    {
        MatrixXd res(mat.rows(),mat.columns());
        for (unsigned int i=0; i < mat.rows(); i++) {
            for (unsigned int j=0; j < mat.columns(); j++) {
                res(i,j) = mat.get(j,i)->toDouble().value();
            }
        }
        return res;
    }

    MatrixXd Stats::toEigenMat(vv::Table& mat)
    {
	return Map<MatrixXd>(mat.value().data(), mat.width(), mat.height());
    }


    MatrixXd Stats::def_pos(MatrixXd M)
    {
        unsigned int N = M.rows();
        if (N != M.cols()) {
            throw vle::utils::ArgError("def_pos: M is not square");
        }
        MatrixXd Mdp = M;
        EigenSolver<MatrixXd> M_eigen(M);

        if (! M_eigen.pseudoEigenvalueMatrix().isDiagonal()) {
            throw vle::utils::ArgError("I don't know TODO");
        }
        VectorXd M_eigen_values = M_eigen.pseudoEigenvalueMatrix().diagonal();
        double ddmin = 99999;
        for (unsigned int i =0; i < M_eigen_values.size(); i++) {
            double ddi = M_eigen_values[i];
            if ((ddi > 0) && (ddi <= ddmin)) {
                ddmin = ddi;
            }
        }

        if (M_eigen_values.array().minCoeff() < 0) {
            unsigned int neg = 1;
            for (unsigned int i=0; i < N; i++) {
                //double ddmin = -999;
                if (M_eigen_values[i] < 0) {
                    M_eigen_values[i] = ddmin*(std::pow(0.1,neg));
                    neg=neg+1;
                }
            }
            Mdp = M_eigen.pseudoEigenvectors() *  M_eigen_values.asDiagonal() *
                    M_eigen.pseudoEigenvectors().transpose();
        }
        return Mdp;
    }

    MatrixXd Stats::sqrtm(const MatrixXd& M)
    {
        EigenSolver<MatrixXd> x1(M);
        VectorXd x1d = x1.pseudoEigenvalueMatrix().diagonal();
        if (x1d.array().minCoeff() < 0) {
            /*std::cout << " dbg sqrtm \n" << M << "\n";
            std::cout << " dbg x1d \n" << x1d << "\n";
            std::cout << " dbg jacobi \n" << sqrtm_jacobi(M) << "\n";*/
            throw vle::utils::ArgError("[record.eigen sqrtm] the matrix should "
                    "be positive definite");
        }
        MatrixXd x1s = x1d.asDiagonal();
        x1s = x1s.array().sqrt();
        MatrixXd u1 = x1.pseudoEigenvectors();
        MatrixXd u1i = u1.inverse();
        MatrixXd MS = u1 * x1s * u1i;
        return MS;
    }

    MatrixXd Stats::sqrtm_jacobi(const MatrixXd& M)
    {
        JacobiSVD<MatrixXd> svd(M, ComputeFullU | ComputeFullV);
        const MatrixXd& U = svd.matrixU();
        const MatrixXd& V = svd.matrixV();
        MatrixXd Sr = svd.singularValues().asDiagonal();
        Sr = Sr.array().sqrt();
        return U * Sr * V.transpose();
    }

    bool Stats::checkSymmety(const MatrixXd& M, bool sendError)
    {
        if (M.rows() != M.cols()) {
            if (sendError) {
                throw vle::utils::ModellingError(vle::utils::format(
                        "[record.eigen checkSymmety] "
                        "the matrix is not square rows=%ld, cols=%ld",
                        M.rows(), M.cols()));
            }
            return false;
        }
        for (unsigned int i=0; i < M.rows(); i++) {
            for (unsigned int j=0; j < M.cols(); j++) {
                if (M(i,j) != M(j,i)) {
                    if (sendError) {
                        throw vle::utils::ModellingError(
                                "[record.eigen checkSymmety] "
                                "the matrix is not sym ");
                    }
                    return false;
                }
            }
        }
        return true;
    }

    double Stats::dmnorm(const VectorXd& z, const VectorXd& mu, const MatrixXd& sigma)
    {
        unsigned int N = z.rows();
        if ((mu.rows() != N) || (sigma.rows() != N) || (sigma.rows() != N)) {
            throw vle::utils::ArgError(vu::format("It is required that N (%i),"
                    " mu.rows (%li), sigma.rows (%li), sigma.cols (%li)"
                    " have the same values", N, mu.cols(), sigma.rows(),
                    sigma.cols()));
        }
        VectorXd x_minus_mu = z.array() - mu.array();
        MatrixXd sigma_inv = sigma.inverse();
        //computation of (x-\mu)' \Sigma^(-1) (x-\mu)
        double e_value = x_minus_mu.transpose() * sigma_inv * x_minus_mu;
        double res = pow(2 * M_PI, - ((double) N)/2)
                                *  pow(sigma.determinant(), -0.5)
                                * exp(-0.5 * e_value);
        return res;
    }

    VectorXd Stats::rmnorm(const VectorXd& mu, const MatrixXd& sigma, vu::Rand& rng)
    {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigenSolver(sigma);
        Eigen::MatrixXd evect = eigenSolver.eigenvectors();
        Eigen::VectorXd eval = eigenSolver.eigenvalues();
        for (int i=0; i < mu.size(); i++) {
            eval[i] = std::sqrt((double) eval[i])* rnorm(0,1,rng);
        }
        return mu + evect * eval;
    }

    /**
     * @brief Performs man draws according a multivariate normal distribution
     *
     * @param n, the number of points to draw
     * @param mu, mean of the normal distribution
     * @param sigma, covariance matrix of the multivariate normal distribution
     * @return a sample of MVN with mean mu and covariance sigma
     *
     * @notes: implementation comes from wikipedia using spectral decomposition
     * and not Cholesky Decomposition
     */
    MatrixXd Stats::rmnorm(unsigned int n, const VectorXd& mu,
            const MatrixXd& sigma, vu::Rand& rng)
    {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigenSolver(sigma);
        Eigen::MatrixXd evect = eigenSolver.eigenvectors();
        Eigen::VectorXd eval = eigenSolver.eigenvalues();
        Eigen::VectorXd gen(eval.size());
        MatrixXd res(n,mu.size());
        for (unsigned p=0; p<n; p++) {
            for (int i=0; i < mu.size(); i++) {
                gen[i] = std::sqrt((double) eval[i])* rnorm(0,1,rng);
            }
            res.row(p) << (mu + evect * gen).transpose();
        }
        return res;
    }

    double Stats::kernelDensity(const VectorXd& x, const MatrixXd& data)
    {
        //bandwith using the Silverman's rule of thumb
        unsigned int d = data.cols();//dimension
        unsigned int n = data.rows();//nb points
        double D = (double)data.cols();//dimension
        double N = (double)data.rows();//nb points
        MatrixXd H = MatrixXd::Zero(d,d);

        for (unsigned int r=0; r<d; r++) {
            H(r,r) = std::pow(std::pow(4/(D+2),(1/(D+4)))* 
			      std::pow(N, (-1/(D+4)))*
                              std(data.col(r)), 2.0);

        }
        MatrixXd Hi = H.inverse();//d*d matrix
        MatrixXd Hi_sq = sqrtm(Hi);//d*d matrix
        double det_Hi_sq=Hi_sq.determinant();

        double sum=0;
        for (unsigned int r=0; r<n; r++) {
            //compute H^(-1)*(x - xi)
            VectorXd vect = Hi_sq * (x.transpose().array() - data.row(r).array()).matrix().transpose();
            //use gaussian kernel
            double v = -(vect.transpose()*Hi*vect)(0,0)/2.0;
            sum += det_Hi_sq * std::pow(2*pi(), (-D/2.0))* std::exp(v);
        }
        return sum/n;
    }

    MatrixXd Stats::covariance(MatrixXd& mat, bool centered)
    {
        if (centered) {
            MatrixXd centered = mat.rowwise() - mat.colwise().mean();
            return (centered.adjoint()*centered)/double(mat.rows());
        } else {
            MatrixXd res(mat.cols(),mat.cols());
            for (unsigned int i=0; i<mat.cols();i++) {
                res(i,i) = var(mat.col(i));
                for (unsigned int j=i+1; j<mat.cols();j++) {
                    double covar = cov(mat.col(i),mat.col(j));
                    res(i,j) = covar;
                    res(j,i) = covar;
                }
            }
            return res;
        }
    }

    VectorXd Stats::subVector_noti(const VectorXd& vec, unsigned int i)
    {
        unsigned int n = vec.size();
        VectorXd res(n-1);
        if (i == 0){
            res << vec.tail(n-1);
        } else if (i == n-1){
            res << vec.head(i);
        } else {
            res << vec.head(i), vec.tail(n-i-1);
        }
        return res;
    }


    VectorXd Stats::subMatrix_i_noti(const MatrixXd& mat, unsigned int i)
    {
        VectorXd rowi = mat.row(i);
        unsigned int n = rowi.size();
        VectorXd res(n-1);
        if (i == 0){
            res << rowi.tail(n-1);
        } else if (i == n-1){
            res << rowi.head(i);
        } else {
            res << rowi.head(i), rowi.tail(n-i-1);
        }
        return res;
    }


    MatrixXd Stats::subMatrix_noti_noti(const MatrixXd& mat, unsigned int i)
    {
        unsigned int n = mat.rows();
        MatrixXd res(n-1, n-1);

        if (i==0) {
            res << mat.bottomRightCorner(n-1,n-1);
        } else if (i == n-1) {
            res << mat.topLeftCorner(i,i);
        } else {
            res << mat.topLeftCorner(i,i), mat.topRightCorner(i, n-i-1),
                mat.bottomLeftCorner(n-i-1,i), mat.bottomRightCorner(n-i-1,n-i-1);
        }
        return res;
    }

    bool Stats::testvariables(const VectorXd& X,const MatrixXd& vbounds)
    {
        unsigned int Nv = X.rows();
        bool OK = true;
        for (unsigned int v=0; v < Nv; v++){
            if ((X(v) < vbounds(0,v)) || (X(v) > vbounds(1,v))) {
                OK = false;
            }
        }
        return OK;
    }

    VectorXd Stats::zip_max(const VectorXd& X1,const VectorXd& X2)
    {
       VectorXd ret = VectorXd::Zero(X1.rows());
       for (unsigned int i = 0; i<X1.rows(); i++) {
          ret[i] = std::max(X1[i], X2[i]);
       }  
       return ret;
    }


    void Stats::fillWithNormalDistrib(MatrixXd& m, vu::Rand& rng)
    {
        for (unsigned int i=0; i < m.rows(); i++) {
            for (unsigned int j=0; j < m.cols(); j++) {
                m(i,j) = rng.normal(0, 1);
            }
        }
    }


    void Stats::fillWithNormalDistrib(VectorXd& v, vu::Rand& rng)
    {
        for (unsigned int i=0; i < v.rows(); i++) {
            v(i) = rng.normal(0, 1);
        }
    }

    unsigned int Stats::sample_int(const VectorXd& probs, vu::Rand& rng)
    {

        double unif = rng.getDouble(0,1);
        double sum_prob = 0;
        for (unsigned int i=0; i < probs.rows(); i++) {
            if (unif < sum_prob + probs(i)) {
                return i;
            }
            sum_prob += probs(i);
        }
        throw vle::utils::ArgError(
                vu::format("error in sample_int (unif='%f')", unif));
    }

    double Stats::runif(double min, double max, vu::Rand& rng)
    {
        return rng.getDouble(min,max);
    }

    VectorXd Stats::runif(unsigned int n, double min, double max, vu::Rand& rng)
    {
        VectorXd res(n);
        for (unsigned int i=0; i<n; i++) {
            res(i) = rng.getDouble(min,max);
        }

        return res;
    }

    double Stats::rnorm(double mean, double sd, vu::Rand& rng)
    {
        return rng.normal(mean,sd);
    }

    VectorXd Stats::rnorm(unsigned int n, double mean, double sd, vu::Rand& rng)
    {
        VectorXd res(n);
        for (unsigned int i=0; i <n; i++) {
            res(i) = rng.normal(mean,sd);
        }
        return res;
    }

    double Stats::rexp(double lambda, vu::Rand& rng)
    {
        return rng.exponential(lambda);
    }

    double Stats::dnorm(double loc, double mean, double sd)
    {
        boost::math::normal_distribution<> my_norm(mean, sd);
        return boost::math::pdf(my_norm, loc);
    }

    double Stats::pnorm(double loc, double mean, double sd)
    {
        boost::math::normal_distribution<> my_norm(mean, sd);
        return boost::math::cdf(my_norm, loc);
    }

    VectorXd Stats::pnorm(const VectorXd& loc, double mean, double sd)
    {
        VectorXd res(loc.size());
        boost::math::normal_distribution<> my_norm(mean, sd);
        for (unsigned int i=0; i<loc.size();i++) {
            res(i) = boost::math::cdf(my_norm, (double) loc(i));
        }
        return res;
    }

    VectorXd Stats::qnorm(const VectorXd& loc, double mean, double sd)
    {
        VectorXd res(loc.size());
        boost::math::normal_distribution<> my_norm(mean, sd);
        for (unsigned int i=0; i<loc.size();i++) {
            res(i) = boost::math::quantile(my_norm, (double) loc(i));
        }
        return res;
    }

    double Stats::qnorm(double loc, double mean, double sd)
    {
        boost::math::normal_distribution<> my_norm(mean, sd);
        if (loc >= 1) {
            return boost::math::quantile(my_norm, 0.9999);
        } else {
            return boost::math::quantile(my_norm, loc);
        }
    }


    VectorXd Stats::rtnorm(unsigned int n, double mean, double mean_min,
            double mean_max, double sd, vu::Rand& rng)
    {
        // Draw from Uni(0,1)
        VectorXd F = runif(n, 0, 1, rng);
        //Phi(mean_min), Phi(mean_max)
        double F_min = pnorm(mean_min, mean, sd);
        double F_max = pnorm(mean_max, mean, sd);

        //Truncated Normal Distribution, see equation (6), but F(x) ~ Uni(0,1),
        //
        //so we directly draw from Uni(0,1) instead of doing:
        //
        //x <- rnorm(n, mu, sigma)
        //
        //y <-  mu + sigma * qnorm(pnorm(x)*(Fb - Fa) + Fa)

        VectorXd tmp = F.array() * (F_max - F_min) + F_min;

        return (qnorm(tmp, 0, 1).array() * sd + mean );
    }

    double Stats::rtnorm( double mean, double mean_min,
            double mean_max, double sd, vu::Rand& rng)
    {
        double F = runif(0, 1, rng);
        double F_min = pnorm(mean_min, mean, sd);
        double F_max = pnorm(mean_max, mean, sd);
        double tmp = F * (F_max - F_min) + F_min;
        return (qnorm(tmp, 0, 1) * sd + mean );
    }

    MatrixXd Stats::rtmnorm_gibbs(unsigned int nbPoints,
            const VectorXd& mu, const VectorXd& mu_min,
            const VectorXd& mu_max, const MatrixXd& sigma,
            const VectorXd& startPoint, unsigned int burn_in,
            vu::Rand& rng)
    {
        // dimension of X
        unsigned int d = mu.size();
        if (d != startPoint.size()) {
           throw vle::utils::ArgError(vle::utils::format("[rtmnorm_gibbs] "
                "size of mu (%i) and size of startPoint (%i) must be equal",
                int(d), int(startPoint.size())));
        }
        MatrixXd thetas = MatrixXd::Zero(nbPoints, d);

        //Draw from uniform
        VectorXd U = runif((burn_in + nbPoints) * d, 0,1, rng);
        std::vector<MatrixXd> Ps;
        std::vector<double> sds;
        for (unsigned int i=0; i<d; i++) {
           MatrixXd sigma_noti_noti = subMatrix_noti_noti(sigma,i);
           MatrixXd sigma_noti_noti_inv = sigma_noti_noti.inverse();
           double sigma_i_i = sigma(i,i);
           MatrixXd sigma_i_noti = subMatrix_i_noti(sigma,i);
           MatrixXd P = sigma_i_noti.transpose() * sigma_noti_noti_inv;
           double sd = std::sqrt((double) (sigma_i_i - (P * sigma_i_noti)(0,0)));
           Ps.push_back(P);
           sds.push_back(sd);
       }
       unsigned int l=0;
       VectorXd theta = startPoint;
       for (unsigned int j=0; j<burn_in+nbPoints;j++) {
          for (unsigned int i=0; i<d; i++) {
            VectorXd theta_minus_mu_noti(d-1);
            theta_minus_mu_noti <<
                    subVector_noti(theta, i).array() -
                    subVector_noti(mu, i).array();
            double mu_i = mu(i) + (Ps[i] * theta_minus_mu_noti)[0];
            //transformation (equiv rtnorm)
            VectorXd bounds(2);
            bounds << mu_min(i), mu_max(i);

            VectorXd F_tmp = pnorm(bounds,mu_i, sds[i]);

            double F_min = F_tmp(0);
            double F_max = F_tmp(1);
            theta(i) = mu_i + sds[i] * qnorm(
                    ((double) U(l) * (F_max - F_min) + F_min),
                    0, 1);
            l++;
         }
         if (j>=burn_in) {
            thetas.row(j-burn_in) << theta.transpose();
         } 
      }
      return thetas;
    }


    MatrixXd Stats::rtmnorm_rejection(unsigned int nbPoints,
            const VectorXd& mu, const VectorXd& mu_min,
            const VectorXd& mu_max, const MatrixXd& sigma, 
            vu::Rand& rng)
    {
      
      unsigned int n = mu.size();
      MatrixXd thetas = MatrixXd::Zero(nbPoints, n);
      unsigned int itofill = 0;
      unsigned int miss = 0;
      MatrixXd vbounds(n, 2);
      vbounds << mu_min, mu_max;
      while (itofill < nbPoints) {
        VectorXd multi_norm = rmnorm(mu, sigma, rng);
        if (testvariables(multi_norm, vbounds)) {
            thetas.row(itofill) = multi_norm.transpose();
            itofill++;
        } else {
            miss++;
        }
      }
      return thetas;
    }

    double Stats::pmnorm_indep(const VectorXd& z, const VectorXd& mu,
            const MatrixXd& sigma)
    {
      unsigned int N = z.rows();
      if (not sigma.isDiagonal()) {
        throw vle::utils::ArgError(vle::utils::format("It is required that Sigma"
                "is diagonal"));
      }

      double res = 1;
      for (unsigned int i=0; i < N; i++) {
        res = res * pnorm((double) z(i),
                (double) mu(i), std::sqrt((double) sigma(i,i)));
      }
      return res;
    }

    double Stats::dcsn (double /*z*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/)
    {
        return 0;
    }

    double Stats::pcsn(double /*z*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/)
    {
        return 0;
    }

    double Stats::qcsn(double /*p*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/)
    {
        return 0;
    }

    double Stats::rcsn(double /*n=1*/, double /*location=0*/, double /*scale=1*/,
            double /*skew=0*/)
    {
        return 0;
    }

    double Stats::dmcsn(const MatrixXd& /*W*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/)
    {
        return 0;
    }

    MatrixXd Stats::rmcsn(unsigned int n, const VectorXd& Mu, const MatrixXd& Sigma,
            const MatrixXd& D, const VectorXd&Nu, const MatrixXd& Delta, 
            vu::Rand& rng)
    {
      //Check dimensions
      if ((Sigma.rows() != Sigma.cols()) || (Delta.rows() != Delta.cols())){
        throw vle::utils::ArgError(vle::utils::format("[rmcsn] covariance matrices must"
                " be squared matrices"));
      }
      if ((Mu.size() != Sigma.rows()) || (Mu.size() != D.cols())
            || (Sigma.rows() != D.cols())) {
        throw vle::utils::ArgError(vle::utils::format("[rmcsn] dimension error --"
                " vector Mu not compatible with Sigma or D"));
      }
      if ((Nu.size() != Delta.rows()) || (Nu.size() != D.rows())
            || (D.rows() != Delta.cols())) {
        throw vle::utils::ArgError(vle::utils::format("[rmcsn] dimension error --"
                " vector Nu not compatible with Delta or D"));
      }

      //Computes parameters of the marginal distribution
      MatrixXd QQ = Delta + D * Sigma * D.transpose();
      MatrixXd FF = Sigma * D.transpose() * QQ.inverse();

      MatrixXd GG = sqrtm(Sigma - FF * D * Sigma);

      VectorXd mu_max(Nu.size());
      for (unsigned int i =0; i < mu_max.size(); i++) {
        mu_max(i) = std::numeric_limits<double>::infinity();
      }
      VectorXd mean(Nu.size());
      mean.setZero();
      MatrixXd u(n, Nu.size());

      //Does the computation
      bool OK = false;
      double add = 0;
      unsigned int niter = 0;
      while (!OK){
        niter = niter + 1;
        add = add + 0.1;
        VectorXd u_start = Nu.array()
                + rnorm((unsigned int) Nu.size(), 0.0, add, rng).array().abs();
        u  = rtmnorm_gibbs(n,mean, Nu, mu_max, QQ,u_start,200, rng);

        OK = true;
//        if (niter == 20) {
//            std::cout << " niter == 20 ?? " << std::endl;
//          u = rmnorm(n,mean, QQ);
//          //TODO check if not an error on R code
//          for (unsigned int i=0; i<n;i++) {
//              u.row(i) = u.row(i) + Nu.transpose();
//          }
//          OK = true;
//        }
      }

      MatrixXd v(n, (unsigned int) Mu.size());
      MatrixXd z(n, Mu.size());
      for (unsigned int i=0; i<n;i++) {
        v.row(i) << rnorm((unsigned int) Mu.size(),0,1, rng).transpose();
        z.row(i) << Mu.transpose();
      }
      z = z + u * FF.transpose() + v *GG;
      return z;
    }


    double Stats::dmcsnstar(const VectorXd& z, const VectorXd& mu,
            const MatrixXd& sigma, const VectorXd& skew)
    {
      unsigned int N = z.rows();
      if ((mu.rows() != N) || (sigma.rows() != N) || (sigma.cols() != N) ||
            (skew.rows() != N)) {
        throw vle::utils::ArgError(vle::utils::format("It is required that z.rows (%i),"
                " mu.rows (%i), sigma.rows (%i), sigma.cols (%i),"
                " skew.rows (%i)"
                " have the same values", int(N), int(mu.rows()),
                int(sigma.cols()), int(sigma.rows()), int(skew.rows())));
      }
      MatrixXd diag_skew = skew.asDiagonal();
      MatrixXd units = VectorXd::Ones(N).asDiagonal();
      MatrixXd D = skew.asDiagonal() * sqrtm(sigma).inverse();
      MatrixXd Delta = units.array() - diag_skew.array().pow(2.0);
      //computation
      VectorXd z_for_pmnorm = D * (z-mu);

      double d = dmnorm(z, mu, sigma) *
            pmnorm_indep(z_for_pmnorm, VectorXd::Zero(N), Delta);
      return d*std::pow(2, (double) N);
    }

    MatrixXd Stats::rmcsnstar(unsigned int n, const VectorXd& Mu,
            const MatrixXd& Sigma, const VectorXd& Skew, 
            vu::Rand& rng)
    {
      unsigned int Nv = Mu.size();
      MatrixXd z = MatrixXd::Zero(n, Nv);
      VectorXd NonSkew = ((ArrayXXd) (1 - Skew.array().square())).sqrt();

      MatrixXd SS = sqrtm(Sigma);

      for (unsigned int i=0; i < n; i++){

        VectorXd u = rnorm(Nv, 0, 1, rng).array().abs();
        VectorXd v = rnorm(Nv, 0, 1, rng);
        VectorXd y = (Skew.array() * u.array() +
                NonSkew.array() * v.array());
        z.row(i) << (Mu + SS * y).transpose();
      }
      return z;
    }

    double Stats::dmcsn_marg(double /*y*/, double /*sel*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/)
    {
        return 0;
    }

    double Stats::extract_csn_marg(double /*sel*/, const MatrixXd& /*Mu*/,
            const MatrixXd& /*Sigma*/, const MatrixXd& /*D*/,
            const MatrixXd& /*Nu*/, const MatrixXd& /*Delta*/)
    {
        return 0;
    }

    MatrixXd Stats::rmcsn_cond(unsigned int n, const VectorXd& y_cond,
            const VectorXd& Mu, const MatrixXd& Sigma,
            const MatrixXd& D, const VectorXd& Nu, const MatrixXd& Delta,
            vu::Rand& rng)
    {
      // Check dimensions
      unsigned int N_y  = y_cond.size();
      unsigned int N_Mu  = Mu.size();
      if (Sigma.rows() != Sigma.cols() || Delta.rows() != Delta.cols()) {
        throw vle::utils::ArgError(vle::utils::format(
                "[rmcsn.cond] covariance matrices must be squared"));
      }
      if (N_y >= N_Mu) {
        throw vle::utils::ArgError(vle::utils::format(
                "[rmcsn.cond] dimension of y must be < dimension of Mu"));
      }
      {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(Sigma);
        if (eigensolver.eigenvalues().array().minCoeff() < 0.0) {
            throw vle::utils::ArgError(vle::utils::format(
                            "[rmcsn.cond] Sigma be positive definite"));
        }
      }
      {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigensolver(Delta);
        if (eigensolver.eigenvalues().array().minCoeff() < 0.0) {
            throw vle::utils::ArgError(vle::utils::format(
                    "[rmcsn.cond] Delta be positive definite"));
        }
      }

      if ((Mu.size() <= N_y) || (Sigma.rows() <= N_y) ||
            (Delta.rows() <= N_y)) {
        throw vle::utils::ArgError(vle::utils::format(
                "[rmcsn.cond] dim error : y_cond is too long"));
      }
      if ((Mu.size() != Sigma.rows()) || (Mu.size() != D.cols())
            || (Sigma.rows() != D.cols()) ) {
        throw vle::utils::ArgError(vle::utils::format(
                "[rmcsn.cond] dim error : vector Mu not compatible with "
                "Sigma or D"));
      }
      if ((Nu.size() != Delta.rows()) || (Nu.size() != D.rows())
            || (Delta.rows() != D.rows())){
        throw vle::utils::ArgError(vle::utils::format(
                        "[rmcsn.cond] dim error : vector Nu not compatible with"
                        "Delta or D"));
      }

      MatrixXd Sigma11 = Sigma.topLeftCorner(N_y, N_y);
      MatrixXd Sigma12 = Sigma.topRightCorner(N_y, N_Mu - N_y);
      MatrixXd Sigma22 = Sigma.bottomRightCorner(N_Mu - N_y, N_Mu - N_y);
      MatrixXd Sigma_cond = Sigma22 -
            Sigma12.transpose() * Sigma11.inverse() * Sigma12;
      VectorXd Mu_cond = Mu.tail(N_Mu - N_y) + Sigma12.transpose()
            * Sigma11.inverse() * (y_cond-Mu.head(N_y));
      MatrixXd D1 = D.topLeftCorner(D.rows(), N_y);
      MatrixXd D2 = D.topRightCorner(D.rows(), D.cols()-N_y);
      MatrixXd D_star = D1 + D2 * Sigma12.transpose() * Sigma11.inverse();
      VectorXd Nu_cond = Nu - D_star * (y_cond - Mu.head(N_y));

      MatrixXd z = rmcsn(n,Mu_cond,Sigma_cond,D2,Nu_cond,Delta, rng);
      return z;
    }



    //see equation 3.2
    MatrixXd Stats::internal_sigma_noti_noti_inv(
            const MatrixXd& sigma_inv, unsigned int i)
    {
        unsigned int n = sigma_inv.rows();
        MatrixXd v_noti_noti = subMatrix_noti_noti(sigma_inv, i);
        VectorXd v_i_noti = subMatrix_i_noti(sigma_inv, i);
        double v_i_i = sigma_inv(i,i);
        MatrixXd tmp = (v_i_noti * v_i_noti.transpose());
        MatrixXd res(n-1,n-1);
        res << v_noti_noti.array() - tmp.array() / v_i_i;
        return res;
    }



}} // namespaces

