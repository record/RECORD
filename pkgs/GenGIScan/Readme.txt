Requirements
============

vle      2.0  http://www.vle-project.org
cmake    2.8  http://www.cmake.org
gdal  1.11.3  http://www.gdal.org

Installation
============

$ vle --package=GenGIScan configure
$ vle --package=GenGIScan build
