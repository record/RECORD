/**
 * @file src/ExecRelai.cpp
 * @author Eric Casellas (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@


#include <vle/devs/Executive.hpp>

#include <vle/value/Set.hpp>

#include <vle/utils/Tools.hpp>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vp = vle::vpz;
namespace vu = vle::utils;

namespace GenGIScan {

    class ExecRelai : public vd::Executive
    {
    public:
        ExecRelai(const vd::ExecutiveInit& model, const vd::InitEventList& events)
             : vd::Executive(model, events)
        {
            std::cout << "\t[ExecRelai]\tConstructor..." << std::endl;
            //Lecture configuration de l'Executive (paramètres VPZ)
            n = (events.exist("n")) ? vv::toInteger(events.get("n")) : 1;
            std::cout << "\t[ExecRelai]\tn:" << n << std::endl;
            ConditionSeparator = (events.exist("ConditionSeparator")) ? vv::toString(events.get("ConditionSeparator")) : ";";
            R_inputPortsInd = (events.exist("R_inputPortsInd")) ? vv::toString(events.get("R_inputPortsInd")) : "";
            R_inputPortsCom = (events.exist("R_inputPortsCom")) ? vv::toString(events.get("R_inputPortsCom")) : "";
            R_outputPortsInd = (events.exist("R_outputPortsInd")) ? vv::toString(events.get("R_outputPortsInd")) : "";
            R_outputPortsCom = (events.exist("R_outputPortsCom")) ? vv::toString(events.get("R_outputPortsCom")) : "";

            R_outputPortsInd_isConnected = (events.exist("R_outputPortsInd_isConnected")) ? vv::toString(events.get("R_outputPortsInd_isConnected")) : "";
            R_outputPortsCom_isConnected = (events.exist("R_outputPortsCom_isConnected")) ? vv::toString(events.get("R_outputPortsCom_isConnected")) : "";
            R_outputPortsInd_initValue = (events.exist("R_outputPortsInd_initValue")) ? vv::toString(events.get("R_outputPortsInd_initValue")) : "";
            R_outputPortsCom_initValue = (events.exist("R_outputPortsCom_initValue")) ? vv::toString(events.get("R_outputPortsCom_initValue")) : "";
            
            r_inputPortsInd = stringParser(R_inputPortsInd, ConditionSeparator);
            r_inputPortsCom = stringParser(R_inputPortsCom, ConditionSeparator);
            r_outputPortsInd = stringParser(R_outputPortsInd, ConditionSeparator);
            r_outputPortsCom = stringParser(R_outputPortsCom, ConditionSeparator);
            
            std::vector<bool> defaultBoolVectorInd(r_outputPortsInd.size(), true);
            r_outputPortsInd_isConnected = (R_outputPortsInd_isConnected != "") ? stringParserToBool(R_outputPortsInd_isConnected, ConditionSeparator) : defaultBoolVectorInd;
            std::vector<bool> defaultBoolVectorCom(r_outputPortsCom.size(), true);
            r_outputPortsCom_isConnected = (R_outputPortsCom_isConnected != "") ? stringParserToBool(R_outputPortsCom_isConnected, ConditionSeparator) : defaultBoolVectorCom;
            std::vector<double> defaultDoubleVectorInd(r_outputPortsInd.size(), 0.);
            r_outputPortsInd_initValue = (R_outputPortsInd_initValue != "") ? stringParserToDouble(R_outputPortsInd_initValue, ConditionSeparator) : defaultDoubleVectorInd;
            std::vector<double> defaultDoubleVectorCom(r_outputPortsCom.size(), 0.);
            r_outputPortsCom_initValue = (R_outputPortsCom_initValue != "") ? stringParserToDouble(R_outputPortsCom_initValue, ConditionSeparator) : defaultDoubleVectorCom;
            
            
            RelaiName = vd::Executive::coupledmodelName();
        }

        
        virtual ~ExecRelai() 
        {}


        virtual vd::Time init(vd::Time /*time*/) override
        {
            return 0.;
        }
        
        virtual void internalTransition(vd::Time /*time*/) override
        {
            std::cout << "\t[ExecRelai]\tinternalTransition..." << std::endl;
            //Modification de la condition contenant la déclaration des Var du modèle Relai
            vp::Conditions& cond_l = conditions(); //référence aux conditions du VPZ
             vp::Condition& cond_RelaiVar = cond_l.get("condVar_R"); //référence à la condition R_Condition du modèle Relai contenant le paramètre variables de declaration des Var
             for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss;
                    ss << "init_value_" << r_outputPortsInd[j] << "_" << i;
                    cond_RelaiVar.setValueToPort(ss.str(), vv::Double::create(r_outputPortsInd_initValue[j]));
                }
            }
            
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) { //boucle sur les différentes sorties communes entre parcelles
                std::stringstream ss;
                ss << "init_value_" << r_outputPortsCom[j];
                cond_RelaiVar.setValueToPort(ss.str(), vv::Double::create(r_outputPortsCom_initValue[j]));
            }

            //Ajout des ports d'entrée/sortie du modèle Relai
            std::vector < std::string > inputs;
            std::vector < std::string > outputs;
             for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_inputPortsInd.size(); j++) { //boucle sur les différentes entrées spécifiques aux parcelles
                    std::stringstream ss;
                    ss << r_inputPortsInd[j] << "_" << i;
                    inputs.push_back(ss.str());
                    std::cout << "\t[ExecRelai]\t\tRelai_atom Input:" << ss.str() << std::endl;
                }
                for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss;
                    ss << r_outputPortsInd[j] << "_" << i;
                    outputs.push_back(ss.str());
                    std::cout << "\t[ExecRelai]\t\tRelai_atom Output:" << ss.str() << std::endl;
                }
            }
            for (unsigned int j = 0; j < r_inputPortsCom.size(); j++) {//boucle sur les différentes entrées communes entre parcelles
                inputs.push_back(r_inputPortsCom[j]);
                std::cout << "\t[ExecRelai]\t\tRelai_atom Input:" << r_inputPortsCom[j] << std::endl;
            }
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) {//boucle sur les différentes sorties communes entre parcelles
                outputs.push_back(r_outputPortsCom[j]);
                std::cout << "\t[ExecRelai]\t\tRelai_atom Output:" << r_outputPortsCom[j] << std::endl;
            }
            //Association des conditions du modèle
            std::vector <std::string> vzs;
            vzs.push_back("cond_DE");
            vzs.push_back("condParam_R");
            vzs.push_back("condVar_R");
            
            vp::Condition& cond_Relai = cond_l.get("condParam_R"); //référence à la condition R_Condition de la classe cRelai contenant le paramètre n
            cond_Relai.del("n");
            cond_Relai.addValueToPort("n", vv::Integer::create(n)); //modification de la valeur du port n de la condition R_Condition
            
            //Modification des observations du modèle atomique Relai
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) //boucle sur les différentes sorties communes entre parcelles
                observables().get("obs_R").add(r_outputPortsCom[j]);

            for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss2;
                    ss2 << r_outputPortsInd[j] << "_" << i;
                    observables().get("obs_R").add(ss2.str());
                }
            }
    
            std::cout << "\t[ExecRelai]\t\tadding model Relai_atom" << std::endl;
            //Création du modèle atomique avec ses ports d'entrée/sortie
            createModel("Relai_atom", inputs, outputs, "dynR", vzs, "obs_R");

            //Modification des observations du modèle atomique Relai
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) //boucle sur les différentes sorties communes entre parcelles
                vd::Executive::addObservableToView("Relai_atom", r_outputPortsCom[j], "vue");
                
            for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss2;
                    ss2 << r_outputPortsInd[j] << "_" << i;
                    vd::Executive::addObservableToView("Relai_atom", ss2.str(), "vue");
                }
            }
            
            std::cout << "\t[ExecRelai]\t\tConnecting Relai_atom and " << RelaiName << std::endl;
            //Connections des ports d'entrée/sortie du modèle atomique Relai_atom avec les ports du modèle couplé Relai contenant cet executif
            for (unsigned int j = 0; j < r_outputPortsCom.size(); j++) //boucle sur les différentes sorties communes entre parcelles
                if (r_outputPortsCom_isConnected[j])
                    vd::Executive::addConnection("Relai_atom",r_outputPortsCom[j],RelaiName,r_outputPortsCom[j]);
            for (unsigned int j = 0; j < r_inputPortsCom.size(); j++) //boucle sur les différentes entrées communes entre parcelles
                vd::Executive::addConnection(RelaiName,r_inputPortsCom[j],"Relai_atom",r_inputPortsCom[j]);

            for (unsigned int i = 0; i < n; i++) { //boucle sur les n parcelles
                 for (unsigned int j = 0; j < r_outputPortsInd.size(); j++) { //boucle sur les différentes sorties spécifiques aux parcelles
                    std::stringstream ss2;
                    ss2 << r_outputPortsInd[j] << "_" << i;
                    if (r_outputPortsInd_isConnected[j])
                        vd::Executive::addConnection("Relai_atom", ss2.str(), RelaiName, ss2.str());
                }
                for (unsigned int j = 0; j < r_inputPortsInd.size(); j++) { //boucle sur les différentes entrées spécifiques aux parcelles
                    std::stringstream ss2;
                    ss2 << r_inputPortsInd[j] << "_" << i;
                    vd::Executive::addConnection(RelaiName, ss2.str(), "Relai_atom", ss2.str());
                }
            }

            std::cout << "\t[ExecRelai]\tDumping output_ExecRelai.vpz... " << std::endl;

            std::ofstream file("output_ExecRelai.vpz");
            dump(file, "dump");
        }


    private:
        //paramètres VPZ
        unsigned int n; //nombre de parcelles définies dans le fichier SIG
        std::string ConditionSeparator; //séparateur utilisé pour séparer les différents elements des listes contenus dans les string
        std::string R_inputPortsInd; //Noms des ports d'entrée du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature) 
        std::string R_inputPortsCom; //Noms des ports d'entrée du modèle Relai qui sont communes à toutes les Parcelles
        std::string R_outputPortsInd; //Noms des ports de sortie du modèle Relai qui sont spécifiques à chaque Parcelle (nomenclature) 
        std::string R_outputPortsCom; //Noms des ports de sortie du modèle Relai qui sont communes à toutes les Parcelles
        std::string R_ConditionVar; //Nom de la condition de la classe cRelai qui contient le port variables de declaration des Var
        std::string R_outputPortsInd_isConnected;
        std::string R_outputPortsCom_isConnected;
        std::string R_outputPortsInd_initValue;
        std::string R_outputPortsCom_initValue;
        
        //Variables locales
        std::vector<std::string> r_inputPortsInd; //vecteur de R_inputPortsInd
        std::vector<std::string> r_inputPortsCom; //vecteur de R_inputPortsCom
        std::vector<std::string> r_outputPortsInd; //vecteur de R_outputPortsInd
        std::vector<std::string> r_outputPortsCom; //vecteur de R_outputPortsCom
        std::vector<bool> r_outputPortsInd_isConnected; //vecteur de R_outputPortsInd_isConnected;
        std::vector<bool> r_outputPortsCom_isConnected; //vecteur de R_outputPortsCom_isConnected;
        std::vector<double> r_outputPortsInd_initValue; //vecteur de R_outputPortsInd_initValue;
        std::vector<double> r_outputPortsCom_initValue; //vecteur de R_outputPortsCom_initValue;
        std::string RelaiName; //Nom du modèle Relai

        //séparation des conditions en vecteurs 
        std::vector<std::string> stringParser (const std::string & Msg, const std::string & Separators)
        {
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            return lst;
        }

        std::vector<bool> stringParserToBool (const std::string & Msg, const std::string & Separators)
        {
            std::vector<bool> output;
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            // itérer la séquence de tokens
            for (std::vector< std::string >::const_iterator i = lst.begin(); i != lst.end(); ++i) {
                if (*i == "0") {
                    output.push_back(false);
                } else {
                    output.push_back(true);
                }
            }
                
            return output;
        }

        std::vector<double> stringParserToDouble (const std::string & Msg, const std::string & Separators)
        {
            std::vector<double> output;
            std::vector< std::string > lst;
            vu::tokenize(Msg, lst, Separators, true);
            // itérer la séquence de tokens
            for (std::vector< std::string >::const_iterator i = lst.begin(); i != lst.end(); ++i)
                output.push_back(atof((*i).c_str()));// stocker chaque token extrait
                
            return output;
        }

    };
}
DECLARE_EXECUTIVE(GenGIScan::ExecRelai);

