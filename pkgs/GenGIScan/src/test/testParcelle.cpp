/**
 * @file src/testParcelle.cpp
 * @author Eric Casellas (The RECORD team -INRA)
 */

/*
 * Copyright (C) 2009-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

#include <vector>
#include <iostream>

namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;


namespace GenGIScan {

class testParcelle : public DiscreteTimeDyn
{
    public:
        testParcelle(const vd::DynamicsInit& atom, const vd::InitEventList& evts) :
        DiscreteTimeDyn(atom, evts)
        {
            Tmoy.init(this,"Tmoy", evts);
            Revenu.init(this,"Revenu", evts);
            getOptions().syncs.insert(std::make_pair("Tmoy", 1)); 
            getOptions().syncs.insert(std::make_pair("Revenu", 1)); 

            Production.init(this,"Production", evts);

            p1 = vv::toDouble(evts.get("p1"));
            p2 = vv::toDouble(evts.get("p2"));
            p3 = vv::toString(evts.get("p3"));

            std::cout << "\t[testParcelle]\tParcelle initialisée avec les paramètres :\n\t\t\t\tp1 = " << p1
               << "\n\t\t\t\tp2 = " << p2 << "\n\t\t\t\tp3 = " << p3 << std::endl;
        }

        virtual ~testParcelle() {}

        virtual void compute(const vd::Time& time)
        {
            std::cout << time << "\t[testParcelle]\tcompute...\t- " << getModel().getCompleteName()<< std::endl;
            Production = Tmoy() * p1 + std::max(0., log(Revenu() * p2));
            std::cout << "\t[testParcelle]\t\tTmoy():" << Tmoy() << "\tRevenu():" << Revenu() << "\t-> Production=" << Production()<< std::endl;
        }

    private:
        Var Revenu;
        Var Tmoy;

        Var Production;

        double p1;
        double p2;
        std::string p3;

};

} // namespace GenGIScan

DECLARE_DYNAMICS(GenGIScan::testParcelle)
