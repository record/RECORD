/**
 * @file d.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>

class d : public vle::extension::DifferenceEquation::Simple
{
    Var _d;
    Sync SSBa;
    Nosync S;

public:
    d(const vle::devs::DynamicsInit& model,
      const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Simple(model, events)
    {
        delta_new = vle::value::toDouble(events.get("delta_new"));
        delta_old = vle::value::toDouble(events.get("delta_old"));
        beta = vle::value::toDouble(events.get("beta"));
        phi = vle::value::toDouble(events.get("phi"));
        v = vle::value::toDouble(events.get("v"));
        _d = createVar("d");
        SSBa = createSync("SSBa");
        S = createNosync("S");
    }

    virtual ~d() { }

    virtual double compute(const vle::devs::Time& /* time */)
    {
        return delta_new * (S(-1) * v * (1 - phi)*(1 - beta)) +
            delta_old * (SSBa(0) -
                         (S(-1) * v * (1 - phi) * (1 - beta)));
    }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double delta_new;
    double delta_old;
    double beta;
    double phi;
    double v;
};

DECLARE_NAMED_DYNAMICS(d, d);
