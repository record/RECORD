/**
 * @file sD.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>

class D : public Noise
{
    Var _D;
    Sync d;

public:
    D(const vle::devs::DynamicsInit& model,
      const vle::devs::InitEventList& events) :
	Noise(model, events)
    {
        mh = vle::value::toDouble(events.get("mh"));
        mc = vle::value::toDouble(events.get("mc"));
        T = vle::value::toDouble(events.get("T"));
        _D = createVar("D");
        d = createSync("d");
    }

    virtual ~D() { }

    virtual double compute(const vle::devs::Time& /* time */)
    {
        if (d(0) > T) return (1 - mh) * (1 - mc) * d(0);
        else return (1 - mc) * d(0);
    }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double mh;
    double mc;
    double T;
};

DECLARE_NAMED_DYNAMICS(D, D);
