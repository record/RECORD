/**
 * @file weed-vle.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/devs/Dynamics.hpp>
#include <vle/value/Tuple.hpp>

class Weed : public vle::devs::Dynamics
{
public:
    Weed(const vle::devs::DynamicsInit& model,
	 const vle::devs::InitEventList& events) :
	vle::devs::Dynamics(model, events)
    { }

    virtual ~Weed() { }

    virtual vle::devs::Time init(const vle::devs::Time& /* time */)
    {
        S[1] = 45000;
        d[1] = 500;
        SSBa[1] = 4000;
        DSBa[1] = 200;
        return 0;
    }

    virtual vle::devs::Time timeAdvance() const
    { return 1; }

    virtual void internalTransition(const vle::devs::Time& /* time */)
    {
        // SSBb
        // SSBb.i<-(1-mu)*(SSBa.im1-d.im1)+v*(1-phi)*S.im1
        SSBb[0] = (1 - mu) * (SSBa[1] - d[1]) + v * (1 - phi) * S[1];

        // DSBb
        // DSBb.i<-(1-mu)*DSBa.im1
        DSBb[0] = (1 - mu) * DSBa[1];

        // SSBa
        // SSBa.i<-(1-beta)*SSBb.i+chsi*DSBb.i
        SSBa[0] = (1 - beta) * SSBb[0] + psi * DSBb[0];

        // DSBa
        // DSBa.i<-(1-chsi)*DSBb.i+beta*SSBb.i
        DSBa[0] = (1 - psi) * DSBb[0] + beta * SSBb[0];

        // d
        // d.i<-delta.new*S.im1*v*(1-phi)*(1-beta)+delta.old*(SSBa.i-S.im1*v*(1-phi)*(1-beta))
        d[0] = delta_new * (S[1] * v * (1 - phi)*(1 - beta)) +
            delta_old * (SSBa[0] - (S[1] * v * (1 - phi) * (1 - beta)));
        // D
        // if (d.i>T) {D.i<-(1-mh)*(1-mc)*d.i}
        // if (d.i<=T) {D.i<-(1-mc)*d.i}
        if (d[0] > T)
            D[0] = (1 - mh) * (1 - mc) * d[0];
        else
            D[0] = (1 - mc) * d[0];

        // S
        // S.i<-D.i*Smax/(1+alpha*D.i)
        S[0] = D[0] * Smax / (1 + alpha * D[0]);

        SSBa[1] = SSBa[0];
        SSBb[1] = SSBb[0];
        DSBa[1] = DSBa[0];
        DSBb[1] = DSBb[0];
        d[1] = d[0];
        D[1] = D[0];
        S[1] = S[0];
    }

    virtual vle::value::Value* observation(
	const vle::devs::ObservationEvent& event) const
    {
        vle::value::Tuple* values = vle::value::Tuple::create();

        values->add(SSBa[0]);
        values->add(SSBb[0]);
        values->add(DSBa[0]);
        values->add(DSBb[0]);
        values->add(d[0]);
        values->add(D[0]);
        values->add(S[0]);
        return values;
    }

private:
    static double mu;
    static double v;
    static double phi;
    static double alpha;
    static double beta;
    static double psi;
    static double delta_new;
    static double delta_old;
    static double mh;
    static double mc;
    static double Smax;
    static double T;

    double SSBa[2];
    double SSBb[2];
    double DSBa[2];
    double DSBb[2];
    double d[2];
    double D[2];
    double S[2];
};

double Weed::mu = 0.84;
double Weed::v = 0.6;
double Weed::phi = 0.55;
double Weed::alpha = 445./160000;
double Weed::beta = 0.95;
double Weed::psi = 0.3;
double Weed::delta_new = 0.15;
double Weed::delta_old = 0.3;
double Weed::mh = 0.98;
double Weed::mc = 0;
double Weed::Smax = 445;
double Weed::T = 0.1;

DECLARE_NAMED_DYNAMICS(Weed, Weed);
