/**
 * @file DSBb.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>

class DSBb : public Noise
{
    Var _DSBb;
    Nosync DSBa;

public:
    DSBb(const vle::devs::DynamicsInit& model,
	 const vle::devs::InitEventList& events) :
	Noise(model, events)
    {
        mu = vle::value::toDouble(events.get("mu"));
        _DSBb = createVar("DSBb");
        DSBa = createNosync("DSBa");
    }

    virtual ~DSBb() { }

    virtual double compute(const vle::devs::Time& /* time */)
    { return (1 - mu) * DSBa(-1); }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double mu;
};

DECLARE_NAMED_DYNAMICS(DSBb, DSBb);
