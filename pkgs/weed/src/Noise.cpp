/**
 * @file Noise.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>
#include <boost/algorithm/string.hpp>

Noise::Noise(const vle::devs::DynamicsInit& model,
	     const vle::devs::InitEventList& events) :
    vle::extension::DifferenceEquation::Simple(model, events)
{
    std::string variableName;

    if (events.exist("name")) {
        variableName = toString(events.get("name"));
    } else {
        variableName = getModelName();
    }

    if (events.exist("active"))
	mActive = vle::value::toBoolean(events.get("active"));
    else
	mActive = false;
    if (mActive) {
	mPath = vle::value::toString(events.get("dataPath"));
	mIndex = vle::value::toInteger(events.get("index"));
	std::ifstream f((boost::format("%1%/%2%_%3%.dat")
			 % mPath % variableName % mIndex).str().c_str());
	char line[64];

	f.getline(line, 64);

	unsigned int lineNumber = boost::lexical_cast < double >(line);

	for (unsigned int i = 0; i < lineNumber; ++i) {
	    f.getline(line, 64);
	    mNoiseValues.push_back(boost::lexical_cast < double >(line));
	}
	f.close();
    }
}

double Noise::noise(const vle::devs::Time& time) const
{
    if (mActive) return mNoiseValues[(long)(time.getValue())];
    else return 0;
}
