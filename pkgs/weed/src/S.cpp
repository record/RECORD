/**
 * @file S.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>

class S : public Noise
{
    Var _S;
    Sync D;

public:
    S(const vle::devs::DynamicsInit& model,
      const vle::devs::InitEventList& events) :
	Noise(model, events)
    {
        alpha = vle::value::toDouble(events.get("alpha"));
        Smax = vle::value::toDouble(events.get("Smax"));
        _S = createVar("S");
        D = createSync("D");
    }

    virtual ~S() { }

    virtual double compute(const vle::devs::Time& /* time */)
    { return D(0) * Smax / (1 + alpha * D(0)); }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double alpha;
    double Smax;
};

DECLARE_NAMED_DYNAMICS(S, S);
