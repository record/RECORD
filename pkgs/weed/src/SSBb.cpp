/**
 * @file SSBb.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>

class SSBb : public Noise
{
    Var _SSBb;
    Nosync SSBa;
    Nosync d;
    Nosync S;

public:
    SSBb(const vle::devs::DynamicsInit& model,
	 const vle::devs::InitEventList& events) :
	Noise(model, events)
    {
        mu = vle::value::toDouble(events.get("mu"));
        v = vle::value::toDouble(events.get("v"));
        phi = vle::value::toDouble(events.get("phi"));
        _SSBb = createVar("SSBb");
        SSBa = createNosync("SSBa");
        d = createNosync("d");
        S = createNosync("S");
    }

    virtual ~SSBb() { }

    virtual double compute(const vle::devs::Time& /* time */)
    {
        return (1 - mu) * (SSBa(-1) - d(-1))
            +  v * (1 - phi) * S(-1);
    }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double mu;
    double v;
    double phi;
};

DECLARE_NAMED_DYNAMICS(SSBb, SSBb);
