/**
 * @file Executive.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <Executive.hpp>

Executive::Executive(const vle::devs::ExecutiveInit& model,
		     const vle::devs::InitEventList& events) :
    vle::devs::Executive(model, events)
{
    number = vle::value::toInteger(events.get("number"));

    std::string stype = vle::value::toString(events.get("type"));

    if (stype == "standard") mType = STANDARD;
    else if (stype == "auxiliary") mType = AUXILIARY;
    else if (stype == "para") mType = PARA;

    lambda = vle::value::toDouble(events.get("lambda"));
    if (mType == PARA) {
	h = vle::value::toDouble(events.get("h"));
	a = std::sqrt(1. - h * h);
    }

    const vle::value::Set& measures =
	vle::value::toSetValue(events.get("measures"));
    const vle::value::Tuple& date(vle::value::toTupleValue(measures.get(0)));
    const vle::value::Tuple& wd(vle::value::toTupleValue(measures.get(1)));
    const vle::value::Tuple& var(vle::value::toTupleValue(measures.get(2)));

    mWD.resize(wd.size());
    mVAR.resize(var.size());
    for (int i = 0; i < date.size(); ++i) {
	mDate.push_back(date[i]);
	mWD(i) = wd[i];
	mVAR(i) = var[i];
    }

    const vle::value::Set& state =
	vle::value::toSetValue(events.get("state"));

    for (int i = 0; i < state.size(); ++i) {
	std::string name = vle::value::toString(state.get(i));

	mStateVariables.push_back(name);
	mStateIndex[name] = i;
    }
}

void Executive::createModel(unsigned int index)
{
    std::string modelName((boost::format("model_%1%") % index).str());
    std::string out((boost::format("out_%1%") % index).str());

    // Build model
    vle::graph::AtomicModel* atom(new vle::graph::AtomicModel(modelName,
							      &coupledmodel()));

    // input ports
    atom->addInputPort("perturb");

    // output ports
    coupledmodel().findModel("Executive")->addOutputPort(out);

    {
	std::vector < std::string >::const_iterator it =
	    mStateVariables.begin();
	while (it != mStateVariables.end()) {
	    atom->addOutputPort(*it++);
	}
    }

    vle::vpz::Strings conditions;

    conditions.push_back("cond");

    // connections
    {
	std::vector < std::string >::const_iterator it =
	    mStateVariables.begin();
	while (it != mStateVariables.end()) {
	    coupledmodel().addInternalConnection(modelName, *it++,
						 "Executive", "update");
	}
    }
    coupledmodel().addInternalConnection("Executive", out,
					 modelName, "perturb");

    // create model
    vle::devs::Executive::createModel(atom, "dyn", conditions, "");
    mIndex[modelName] = index;
}

void Executive::filter(bool init)
{
//   Y<-WD[i]
    double Y = mWD(mMeasureIndex);

//   S<-matrix(c(SD[i]))
    boost::numeric::ublas::matrix < double > S(1, 1);
    S(0, 0) = mVAR(mMeasureIndex);

//   A<-matrix(nrow=1, ncol=3, c(1,0,0))
    boost::numeric::ublas::matrix < double > A(1, mStateVariables.size());
    A(0, 0) = 1;
    A(0, 1) = 0;
    A(0, 2) = 0;

//   MU<-0
    double MU = 0;

//   W<-rep(1/Num, Num)
    if (init) {
	for (int k = 0; k < number; ++k) mW(k) = 1./number;
    } else {
	for (int k = 0; k < number; ++k) mW(k) = 1./mW(k);
    }

//   Wpost<-W
    boost::numeric::ublas::vector < double > Wpost(number);
//	Wpost = W;

//   EV<-A%*%X
    boost::numeric::ublas::matrix < double > EV(1, number);
    EV = prod(A, mX);

//   EV<-MU+EV

//   Ln<-dnorm(Y, mean=EV,sd=rep(sqrt(S), length(W)))
    boost::numeric::ublas::vector < double > Ln(number);
    for (int k = 0; k < number; ++k) {
	double e = Y - EV(0, k);
	Ln(k) = 1. / (std::sqrt(2 * 3.1415 * S(0, 0)))
	    * std::exp((-0.5 / S(0, 0)) * e * e);
    }

//   Wpost<-Ln*W
    Wpost = element_prod(Ln, mW);

//   ConstNorm<-sum(Wpost)
    double s = sum(Wpost);

//   Wpost<-Wpost/ConstNorm
    Wpost /= s;

//   SelectedParticles<-sample(1:length(W), length(W), replace=T, prob=Wpost)
    boost::numeric::ublas::matrix < double > Xtemp(mStateVariables.size(),
						   number);
    boost::numeric::ublas::vector < double > Wtemp(number);

    mSelect.clear();
    for (int k = 0; k < number; ++k) {
	int j = 0;
	double _s = Wpost(0);
	double r = rand().getDouble();

	while (_s < r) {
	    ++j;
	    _s += Wpost(j);
	}
	column(Xtemp, k) = column(mX, j);
	Wtemp(k) = Wpost(j);
	mSelect.push_back(j);
    }
    mX = Xtemp;
    mW = Wtemp;

    if (mType == PARA) {
	boost::numeric::ublas::vector < double > tempPara(number);

	for (int p = 0; p < number; ++p) {
	    tempPara(p) = para(mSelect[p]);
	}
	para = tempPara;
    }
}

vle::devs::Time Executive::init(const vle::devs::Time& /* time */)
{
    mMeasureIndex = 0;
    mX.resize(mStateVariables.size(), number);
    mW.resize(number);

    if (mType == PARA) {
	para.resize(number);
	for (int i = 0; i < number; ++i) {
	    double l = rand().getDouble(0., lambda);

	    lambdas.push_back(l);
	    para(i) = l;
	}
    }

    for (int i = 0; i < number; ++i) {
	createModel(i);
    }
    mState = IDLE;
    mSigma = vle::devs::Time::infinity;
    return vle::devs::Time::infinity;
}

vle::devs::Time Executive::timeAdvance() const
{
    return mSigma;
}

void Executive::output(const vle::devs::Time& /* time */,
		       vle::devs::ExternalEventList& output) const
{
    if (mState == OUT) {
	for (int k = 0; k < number; ++k) {
	    std::string out((boost::format("out_%1%") % k).str());

	    std::vector < std::string >::const_iterator it =
		mStateVariables.begin();
	    unsigned int i = 0;

	    while (it != mStateVariables.end()) {
		vle::devs::ExternalEvent* ee =
		    new vle::devs::ExternalEvent(out);

		ee << vle::devs::attribute(
		    "name", vle::value::String::create(*it));
		ee << vle::devs::attribute(
		    "value", vle::value::Double::create(mX(i, k)));
		output.push_back(ee);
		++it;
		++i;
	    }
	}
    }
}

void Executive::internalTransition(const vle::devs::Time& time)
{
    if (mState == RUN) {
	if (mType == STANDARD) {
	    filter(true); // Standard
	} else {
	    filter(false); // Auxiliary or para
	}
	++mMeasureIndex;
	mState = OUT;
	mSigma = 0;
    } else if (mState == OUT) {
	mState = IDLE;
	mSigma = vle::devs::Time::infinity;
    }
}

void Executive::externalTransition(const vle::devs::ExternalEventList& events,
				   const vle::devs::Time& time)
{
    boost::numeric::ublas::matrix < double > oldX(mStateVariables.size(),
						  number);
    vle::devs::ExternalEventList::const_iterator it = events.begin();

    oldX = mX;
    while (it != events.end()) {
	if ((*it)->onPort("update")) {
	    int index = mIndex[(*it)->getSourceModelName()];
	    std::string name = (*it)->getStringAttributeValue("name");
	    double value = (*it)->getDoubleAttributeValue("value");

	    if (mType == STANDARD) {
		double oldValue = mX(mStateIndex[name], index);
		double newValue = value + rand().normal(lambda * oldValue, 0.0);

		if (newValue < 0) newValue = 0;
		mX(mStateIndex[name], index) = newValue;
	    } else { // Auxiliary or para
		mX(mStateIndex[name], index) = value;
	    }

	}
	++it;
    }
    if (mDate[mMeasureIndex] == time.getValue()) {
	if (mType == AUXILIARY or mType == PARA) {
	    filter(true);

	    double av = 0.0;
	    double var = 0.0;

	    if (mType == PARA) {
		av = sum(para) / number;
		boost::numeric::ublas::vector < double > d(number);
		for (int p = 0; p < number; ++p) {
		    d(p) = para(p) - av;
		}
		var = sum(element_prod(d, d)) / (number - 1);
	    }

	    for (int p = 0; p < number; ++p) {
		double sigma = 0.0;

		if (mType == PARA) {
// Lambda<-Para[1,]
// m<-a*Lambda+(1-a)*mean(Lambda)
// sdPara<-sqrt(var(Lambda)*h^2)
// Lambda<-rnorm(Num, mean=m, sd=sdPara)
// Lambda[Lambda<0]<-0
		    double l = para(p);
		    double m = a * l + (1 - a) * av;
		    double sd = std::sqrt(var * h * h);

		    sigma = rand().normal(sd, m);
		    if (sigma < 0) sigma = 0;
		}

		for (int s = 0; s < mStateVariables.size(); ++s) {
		    double value = mX(s, p);
// ************
// BIZARRE !!!!
		    double oldValue = oldX(s, mSelect[p]);
//		    double oldValue = oldX(s, p);
// ************
		    double eps;

		    if (mType == AUXILIARY) {
			eps = rand().normal(lambda * oldValue, 0.0);
		    } else {
			eps = rand().normal(sigma * oldValue, 0.0);
		    }

		    double newValue = value + eps;

		    if (newValue < 0) newValue = 0;
		    mX(s, p) = newValue;
		}
	    }
	}

	mState = RUN;
	mSigma = 0;
    }
}

vle::value::Value* Executive::observation(
    const vle::devs::ObservationEvent& event) const
{
    if (event.onPort(mStateVariables[0])) {
	return vle::value::Double::create(sum(row(mX, 0)) / number);
    } else if (event.onPort("WD")) {
	if (event.getTime() == mDate[mMeasureIndex - 1]) {
	    return vle::value::Double::create(
		mWD(mMeasureIndex - 1));
	}
    } else if (event.onPort("lambda")) {
	return vle::value::Double::create(sum(para) / number);
    }
    return 0;
}

