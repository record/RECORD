/**
 * @file System.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>

#include <iostream>

class System : public vle::extension::DifferenceEquation::Multiple
{
    Var SSBa;
    Var SSBb;
    Var DSBa;
    Var DSBb;
    Var S;
    Var D;
    Var d;

public:
    System(const vle::devs::DynamicsInit& model,
	   const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Multiple(model, events)
    {
        alpha = vle::value::toDouble(events.get("alpha"));
        Smax = vle::value::toDouble(events.get("Smax"));
        mh = vle::value::toDouble(events.get("mh"));
        mc = vle::value::toDouble(events.get("mc"));
        T = vle::value::toDouble(events.get("T"));
        delta_new = vle::value::toDouble(events.get("delta_new"));
        delta_old = vle::value::toDouble(events.get("delta_old"));
        beta = vle::value::toDouble(events.get("beta"));
        phi = vle::value::toDouble(events.get("phi"));
        v = vle::value::toDouble(events.get("v"));
        psi = vle::value::toDouble(events.get("psi"));
        mu = vle::value::toDouble(events.get("mu"));

        SSBa = createVar("SSBa");
        SSBb = createVar("SSBb");
        DSBa = createVar("DSBa");
        DSBb = createVar("DSBb");
        S = createVar("S");
        D = createVar("D");
        d = createVar("d");
    }

    virtual ~System() { }

    virtual void compute(const vle::devs::Time& /* time */)
    {
        // SSBb
        SSBb = (1 - mu) * (SSBa(-1) - d(-1)) + v * (1 - phi) * S(-1);

        // DSBb
        DSBb = (1 - mu) * DSBa(-1);

        // SSBa
        SSBa = (1 - beta) * SSBb(0) + psi * DSBb(0);

        // DSBa
        DSBa = (1 - psi) * DSBb(0) + beta * SSBb(0);

        // d
        d = delta_new * (S(-1) * v * (1 - phi)*(1 - beta)) +
            delta_old * (SSBa(0) - (S(-1) * v * (1 - phi) * (1 - beta)));
        // D
        if (d(0) > T)
            D = (1 - mh) * (1 - mc) * d(0);
        else
            D = (1 - mc) * d(0);

        // S
        S = D(0) * Smax / (1 + alpha * D(0));
    }

    virtual void initValue(const vle::devs::Time& time)
    { computeInit(time); }

private:
    double alpha;
    double Smax;
    double mh;
    double mc;
    double T;
    double delta_new;
    double delta_old;
    double beta;
    double phi;
    double v;
    double mu;
    double psi;
};

DECLARE_NAMED_DYNAMICS(System, System);
