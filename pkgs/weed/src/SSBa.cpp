/**
 * @file SSBa.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Noise.hpp>

class SSBa : public Noise
{
    Var _SSBa;
    Sync SSBb;
    Sync DSBb;

public:
    SSBa(const vle::devs::DynamicsInit& model,
	 const vle::devs::InitEventList& events) :
	Noise(model, events)
    {
        beta = vle::value::toDouble(events.get("beta"));
        psi = vle::value::toDouble(events.get("psi"));
        _SSBa = createVar("SSBa");
        SSBb = createSync("SSBb");
        DSBb = createSync("DSBb");
    }

    virtual ~SSBa() { }

    virtual double compute(const vle::devs::Time& /* time */)
    { return (1 - beta) * SSBb(0) + psi * DSBb(0); }

    virtual double initValue(const vle::devs::Time& time)
    { return compute(time); }

private:
    double beta;
    double psi;
};

DECLARE_NAMED_DYNAMICS(SSBa, SSBa);
