/**
 * @file Executive.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef WEED_EXECUTIVE_HPP
#define WEED_EXECUTIVE_HPP 1

#include <vle/devs.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

class Executive : public vle::devs::Executive
{
public:
    Executive(const vle::devs::ExecutiveInit& model,
	      const vle::devs::InitEventList& events);

    virtual ~Executive() { }

    virtual vle::devs::Time init(const vle::devs::Time& /* time */);

    virtual vle::devs::Time timeAdvance() const;

    virtual void output(const vle::devs::Time& time,
			vle::devs::ExternalEventList& output) const;

    virtual void internalTransition(const vle::devs::Time& ev);

    virtual void externalTransition(
	const vle::devs::ExternalEventList& events,
	const vle::devs::Time& time);

    virtual vle::value::Value* observation(
	const vle::devs::ObservationEvent& event) const;

private:
    void createModel(unsigned int index);
    void filter(bool init);

    enum state { IDLE, RUN, OUT };
    enum type { STANDARD, AUXILIARY, PARA };

    type mType;
    state mState;
    vle::devs::Time mSigma;

// Particles
    unsigned int number;
    double lambda;
    double h;
    double a;
    std::vector < double > lambdas;
    boost::numeric::ublas::vector < double > para;
    std::map < std::string, int > mIndex;

// Measures
    int mMeasureIndex;
    std::vector < double > mDate;
    boost::numeric::ublas::vector < double > mWD;
    boost::numeric::ublas::vector < double > mVAR;

// State
    std::vector < std::string > mStateVariables;
    std::map < std::string, int > mStateIndex;
    boost::numeric::ublas::matrix < double > mX;
    boost::numeric::ublas::vector < double > mW;
    std::vector < int > mSelect;
};

DECLARE_NAMED_EXECUTIVE(Executive, Executive)

#endif
