/**
 * @file System.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2010 INRA
 * Copyright (C) 2008-2010 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>
#include <vle/value/Tuple.hpp>
#include <iostream>

class System2 : public vle::extension::DifferenceEquation::Multiple
{
    Var SSBa;
    Var SSBb;
    Var DSBa;
    Var DSBb;
    Var S;
    Var d;

public:
    System2(const vle::devs::DynamicsInit& model,
	   const vle::devs::InitEventList& events) :
	vle::extension::DifferenceEquation::Multiple(model, events)
    {
        alpha = vle::value::toDouble(events.get("alpha"));
        Smax = vle::value::toDouble(events.get("Smax"));
        mh = vle::value::toDouble(events.get("mh"));
        mc = vle::value::toDouble(events.get("mc"));
        T = vle::value::toDouble(events.get("T"));
        delta_new = vle::value::toDouble(events.get("delta_new"));
        delta_old = vle::value::toDouble(events.get("delta_old"));
        beta1 = vle::value::toDouble(events.get("beta1"));
        beta2 = vle::value::toDouble(events.get("beta2"));
        phi = vle::value::toDouble(events.get("phi"));
        v = vle::value::toDouble(events.get("v"));
        psi1 = vle::value::toDouble(events.get("psi1"));
        psi2 = vle::value::toDouble(events.get("psi2"));
        mu = vle::value::toDouble(events.get("mu"));
        alphaG = alpha * (1 - mh) * (1 - mc);
        SmaxG = Smax * (1 - mh) * (1 - mc);

        const vle::value::Tuple& soil =
            vle::value::toTupleValue(events.get("soil"));
        for (unsigned int i = 0; i < soil.size(); ++i) {
            mSoil.push_back(soil[i]);
        }

        SSBa = createVar("SSBa");
        SSBb = createVar("SSBb");
        DSBa = createVar("DSBa");
        DSBb = createVar("DSBb");
        d = createVar("d");
    }

    virtual ~System2() { }

    virtual void compute(const vle::devs::Time& time)
    {
// S.im1<-d.im1*SmaxG/(1+alphaG*d.im1)
// SSBb.i<-(1-mu)*(SSBa.im1-d.im1)+v*(1-phi)*S.im1
// DSBb.i<-(1-mu)*DSBa.im1
// SSBa.i<-(1-beta)*SSBb.i+chsi*DSBb.i
// DSBa.i<-(1-chsi)*DSBb.i+beta*SSBb.i
// d.i<-delta.new*v*(1-phi)*(1-beta)*S.im1+delta.old*(SSBa.i-S.im1*v*(1-phi)*(1-beta))

        double beta, psi;

        if (mSoil[time.getValue()] == 1) {
            beta = beta1;
            psi = psi1;
        } else {
            beta = beta2;
            psi = psi2;
        }

        double _S = d(-1) * SmaxG / (1 + alphaG * d(-1));

        // SSBb
        SSBb = (1 - mu) * (SSBa(-1) - d(-1)) + v * (1 - phi) * _S;

        // DSBb
        DSBb = (1 - mu) * DSBa(-1);

        // SSBa
        SSBa = (1 - beta) * SSBb(0) + psi * DSBb(0);

        // DSBa
        DSBa = (1 - psi) * DSBb(0) + beta * SSBb(0);

        // d
        d = delta_new * (_S * v * (1 - phi)*(1 - beta)) +
            delta_old * (SSBa(0) - (_S * v * (1 - phi) * (1 - beta)));
    }

    virtual void initValue(const vle::devs::Time& time)
    {
        double beta, psi;

        if (mSoil[time.getValue()] == 1) {
            beta = beta1;
            psi = psi1;
        } else {
            beta = beta2;
            psi = psi2;
        }

// d.0<-runif(Num, 300, 500)
// SSBa.0<-runif(Num, 2700, 4000)
// DSBa.0<-runif(Num, 60, 500)

        double d0 = rand().getDouble(300, 500);
        double SSBa0 = rand().getDouble(2700, 4000);
        double DSBa0 = rand().getDouble(60, 500);

        double Sm1 = d0 * SmaxG / (1 + alphaG * d0);

        // SSBb
        SSBb = (1 - mu) * (SSBa0 - d0) + v * (1 - phi) * Sm1;

        // DSBb
        DSBb = (1 - mu) * DSBa0;

        // SSBa
        SSBa = (1 - beta2) * SSBb(0) + psi2 * DSBb(0);

        // DSBa
        DSBa = (1 - psi2) * DSBb(0) + beta2 * SSBb(0);

        // d
        d = delta_new * (Sm1 * v * (1 - phi)*(1 - beta2)) +
            delta_old * (SSBa(0) - (Sm1 * v * (1 - phi) * (1 - beta2)));
    }

private:
    double alpha;
    double Smax;
    double alphaG;
    double SmaxG;
    double mh;
    double mc;
    double T;
    double delta_new;
    double delta_old;
    double beta1;
    double beta2;
    double phi;
    double v;
    double mu;
    double psi1;
    double psi2;
    std::vector < double > mSoil;

    double lambda;
};

DECLARE_NAMED_DYNAMICS(System2, System2);
