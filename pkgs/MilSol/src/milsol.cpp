/**
 * @file src/milsol.cpp
 * @author Eric Casellas (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Tools.hpp>

#include <vector>
#include <cohorte.cpp>


namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace MilSol {

    class milsol: public DiscreteTimeDyn
    {
    public:
        milsol(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
            DiscreteTimeDyn(atom, events)
        {
            //Variables d'etat
            TotalSporeReady.init(this,"TotalSporeReady", events);
             
            //inputs
            InoculumPrimaire.init(this,"InoculumPrimaire", events);
            getOptions().syncs.insert(std::make_pair("InoculumPrimaire", 1));
            for (int i=0;i<24;i++) {
                std::string TmoyName = vu::format("%s%02d", "Tmoy_", i+1);
                Tmoy.push_back(Var());
                Tmoy[Tmoy.size()-1].init(this, TmoyName, events);
                getOptions().syncs.insert(std::make_pair(TmoyName, 1));

                std::string HRName = vu::format("%s%02d", "HR_", i+1);
                HR.push_back(Var());
                HR[HR.size()-1].init(this, HRName, events);
                getOptions().syncs.insert(std::make_pair(HRName, 1));
            }
            
            //paramètres
            p1 = (events.exist("p1")) ? vle::value::toDouble(events.get("p1")) : 0.5; 
            p2 = (events.exist("p2")) ? vle::value::toDouble(events.get("p2")) : 1.0; 
            p3 = (events.exist("p3")) ? vle::value::toDouble(events.get("p3")) : 10.0; 
            p4 = (events.exist("p4")) ? vle::value::toDouble(events.get("p4")) : 0.05; 
            p5 = (events.exist("p5")) ? vle::value::toDouble(events.get("p5")) : 0.025; 
            p6 = (events.exist("p6")) ? vle::value::toDouble(events.get("p6")) : 1.5; 
            p7 = (events.exist("p7")) ? vle::value::toDouble(events.get("p7")) : 75.0; 
            p8 = (events.exist("p8")) ? vle::value::toDouble(events.get("p8")) : 150.0; 
            p9 = (events.exist("p9")) ? vle::value::toDouble(events.get("p9")) : 225.0; 
            p10 = (events.exist("p10")) ? vle::value::toDouble(events.get("p10")) : 0.004; 
            p11 = (events.exist("p11")) ? vle::value::toDouble(events.get("p11")) : 0.009; 
            p12 = (events.exist("p12")) ? vle::value::toDouble(events.get("p12")) : 2.0; 
            p13 = (events.exist("p13")) ? vle::value::toDouble(events.get("p13")) : 1.0; 
            p14 = (events.exist("p14")) ? vle::value::toDouble(events.get("p14")) : 0.037; 
            D0 = (events.exist("D0")) ? vle::value::toDouble(events.get("D0")) : 100.0; 
            D1 = (events.exist("D1")) ? vle::value::toDouble(events.get("D1")) : 150.0; 
            Dc = (events.exist("Dc")) ? vle::value::toDouble(events.get("Dc")) : 100.0; 
            CUM0 = (events.exist("CUM0")) ? vle::value::toDouble(events.get("CUM0")) : 6.0; 
            CUM1 = (events.exist("CUM1")) ? vle::value::toDouble(events.get("CUM1")) : 10.0; 
            Topt = (events.exist("Topt")) ? vle::value::toDouble(events.get("Topt")) : 18.0; 
            Tmin = (events.exist("Tmin")) ? vle::value::toDouble(events.get("Tmin")) : 3.0; 
            FACT = (events.exist("FACT")) ? vle::value::toDouble(events.get("FACT")) : 2000.0; 
            SSA = (events.exist("SSA")) ? vle::value::toDouble(events.get("SSA")) : 1 / 17700; 
            
            initValue(0.);
        }


        virtual ~milsol() {};


        virtual void compute(const vle::devs::Time& /* time */)
        {
            tempTotalSporeReady = 0.0;
        
            createCohorte();
                
            for (unsigned int i=0; i< Cohortes.size();i++) //boucle sur les cohortes présente
            {
//                std::cout<<"Cohorte n°: "<< i<<std::endl;
                for (int j=0; j<24 ; j++) //boucle sur les 24 heures de la journée
                {
//                    std::cout<< "Heure: " << j+1<< std::endl;
                    Cohortes[i].compute(Tmoy[j](), HR[j]()); //mise a jour des cohortes
                    
                    tempTotalSporeReady+=Cohortes[i].SPORUL;
                    
                    if (condEndCohorte(i)) // supprime les cohortes remplissant les conditions définies dans la fonction condEndCohorte(k)
                    {
//                        std::cout<< "Cohorte n°"<<i<<" supprimée - ID: "<<Cohortes[i].ID<<std::endl;
                        Cohortes.erase(Cohortes.begin()+i);
                        i--;
                        break;
                    }
                }
            }    
            tempTotalSporeReady2 = tempTotalSporeReady - tempTotalSporeReady3;
            tempTotalSporeReady3 = tempTotalSporeReady;
            TotalSporeReady = tempTotalSporeReady2;
        }


        void initValue(const vle::devs::Time& /* time */)
        {
            //Var
            TotalSporeReady = 0.0;
            //paramètres
            count = 0;
        }


    private:
        //Variables d'état
        Var TotalSporeReady; //nombre de spores prets a etre dissemines

        //Variables d'état issues d'autres modèles
        Var InoculumPrimaire; //Inoculum primaire
        std::vector<Var> Tmoy; //Températures moyennes (horaire)
        std::vector<Var> HR; //Humidités relatives (horaire)
        
        //Paramètres issus du VPZ
        double p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14;
        double D0, D1, Dc;
        double CUM0, CUM1;
        double Topt, Tmin;
        double FACT, SSA;
        
        //variables locales
        std::vector<cohorte> Cohortes; //vecteur de cohortes (taille variable)
        int count;//compteur du nombre total de cohortes de spores crées
        double tempTotalSporeReady;//variable temporaire pour le calcul de la somme des cohortes
        double tempTotalSporeReady2;//variable temporaire pour le calcul de la somme des cohortes
        double tempTotalSporeReady3;//variable temporaire pour le calcul de la somme des cohortes
        
        
        //condition de suppression d'une cohorte
        bool condEndCohorte(int k)
        {
            return (Cohortes[k].AGE > p9);
        }
        
        //condition d'ajout d'une nouvelle cohorte
        bool condBeginCohorte()
        {
            return (InoculumPrimaire() > 0.0);
        }
        
        //ajout d'une nouvelle cohorte
        void createCohorte()
        {
            if (condBeginCohorte())
            {
                count ++;
                Cohortes.push_back(cohorte(count, InoculumPrimaire(), p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, D0, D1, Dc, CUM0, CUM1, Topt, Tmin, FACT, SSA));
            }
        }
        
    };
}
DECLARE_DYNAMICS(MilSol::milsol); // balise specifique VLE

