/**
  * @file TemperatureSum.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
using namespace vle::discrete_time;

namespace gluePhysic {
    class TemperatureSum : public DiscreteTimeDyn
    {
    public:
        TemperatureSum(
           const vd::DynamicsInit& atom,
           const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
        {
            maxTemp = (evts.exist("maxTemp")) ? vv::toDouble(evts.get("maxTemp")) : 500.0;  
            minTemp = (evts.exist("minTemp")) ? vv::toDouble(evts.get("minTemp")) : 0.0;  
            baseTemp = (evts.exist("baseTemp")) ? vv::toDouble(evts.get("baseTemp")) : 0.0;  
            maxEff = (evts.exist("maxEff")) ? vv::toDouble(evts.get("maxEff")) : 1.0;  

            ThermalTime.init(this,"ThermalTime", evts);
            Active.init(this,"Active", evts);
            TmoyEff.init(this,"TmoyEff", evts);
            MaxTemp.init(this,"MaxTemp", evts);
            MinTemp.init(this,"MinTemp", evts);
            BaseTemp.init(this,"BaseTemp", evts);
            MaxEff.init(this,"MaxEff", evts);
            
            Tmin.init(this,"Tmin", evts);
            getOptions().syncs.insert(std::make_pair("Tmin", 1));
            Tmax.init(this,"Tmax", evts);
            getOptions().syncs.insert(std::make_pair("Tmax", 1));
            
            initValue(0.);
        }

        virtual ~TemperatureSum() {};

        virtual void compute(const vd::Time& /*time*/)
        {
            if (Active() == 0.0) {
                TmoyEff = 0.0;
            } else {
                double TminLimited = std::min( MaxTemp(), std::max( MinTemp(), Tmin() ) );
                double TmaxLimited = std::min( MaxTemp(), std::max( MinTemp(), Tmax() ) );
                double TmoyLimited = (TminLimited + TmaxLimited) / 2;
                TmoyEff = std::max( 0.0, MaxEff() * (TmoyLimited - BaseTemp() ) );
            }
            ThermalTime = ThermalTime(-1) + TmoyEff();
        }

        void initValue(const vd::Time& /*time*/)
        {
            ThermalTime = 0.0;
            TmoyEff = 0.0;
            Active = 0.0;
            MaxTemp = maxTemp;
            MinTemp = minTemp;
            BaseTemp = baseTemp;
            MaxEff = maxEff;
        }

    private:
        double maxTemp;
        double minTemp;
        double baseTemp;
        double maxEff;
        Var ThermalTime;
        Var Active;
        Var TmoyEff;
        Var MinTemp;
        Var MaxTemp;
        Var BaseTemp;
        Var MaxEff;
        Var Tmin; //Sync
        Var Tmax; //Sync
    };
} // namespace gluePhysic
DECLARE_DYNAMICS(gluePhysic::TemperatureSum)
