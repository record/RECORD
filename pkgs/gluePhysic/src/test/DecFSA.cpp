/**
  * @file DecFSA.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
  
// @@tagdynamic@@
// @@tagdepends: vle.discrete-time, vle.extension.fsa @@endtagdepends

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp> //vu::DateTime::toJulianDay

using namespace std;
using namespace vle;
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace gluePhysic {
    enum DecState {DEC_BARE_SOIL, DEC_PLANTED, DEC_STADE, DEC_HARVEST};
    class DecFSA: public vle::extension::fsa::Statechart
    {
    public:
        DecFSA(const devs::DynamicsInit& atom,
                const devs::InitEventList& events) :
                Statechart(atom, events), Temp(0)
        {
            { // detect begin time
                std::string  begin_date;

                if (events.get("begin_date")->isDouble()) {
                    begin_time = events.getDouble("begin_date");
                    begin_date = vle::utils::DateTime::toJulianDay(begin_time);
                } else if (events.get("begin_date")->isString()) {
                    begin_date = events.getString("begin_date");
                    begin_time = vle::utils::DateTime::toJulianDay(begin_date);
                }
            }

            
            states(this) << DEC_BARE_SOIL << DEC_PLANTED << DEC_STADE << DEC_HARVEST;

            transition(this, DEC_BARE_SOIL, DEC_PLANTED)
                << guard(&DecFSA::sowing)
                << send(&DecFSA::out_sowing);
            transition(this, DEC_PLANTED, DEC_STADE)
                << guard(&DecFSA::stade)
                << send(&DecFSA::out_stade);
            transition(this, DEC_STADE, DEC_HARVEST)
                << guard(&DecFSA::harvesting)
                << send(&DecFSA::out_harvesting);

            initialState(DEC_BARE_SOIL);        
            
                eventInState(this, "Tmin", &DecFSA::inTemp)
            >> DEC_BARE_SOIL >> DEC_PLANTED >> DEC_STADE >> DEC_HARVEST;
        }
        
        virtual ~DecFSA() {};
        
        virtual std::unique_ptr<vv::Value> observation(const vd::ObservationEvent& /*event*/) const
        { return vv::Integer::create(currentState()); }

        
    private :
        //Sowing 
        bool sowing(const vd::Time& julianDay)
        { return (vle::utils::DateTime::dayOfYear(julianDay+begin_time) == 30); }
        
        void out_sowing(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
        {
            sendDEoutput(output, "Active", 1.0);
            sendDEoutput(output, "ThermalTime", 0.0);
       }
        
        //Stade
        bool stade(const vd::Time& julianDay)
        { return (vle::utils::DateTime::dayOfYear(julianDay+begin_time) == 150); }
        
        void out_stade(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
        {
            sendDEoutput(output, "BaseTemp", 1.0);
            sendDEoutput(output, "MaxTemp", 20.0);
            sendDEoutput(output, "MinTemp", 0.0);
            sendDEoutput(output, "MaxEff", 0.9);
        }
        
        //Harvesting
        bool harvesting(const vd::Time& julianDay)
        { return (vle::utils::DateTime::dayOfYear(julianDay+begin_time) == 300); }
        
        void out_harvesting(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
        {
            sendDEoutput(output, "Active", 0.0);
            sendDEoutput(output, "ThermalTime", 0.0);
        }

       void inTemp(const vd::Time& /* time */, const vd::ExternalEvent& event)
       { Temp = event.attributes()->toDouble().value(); }

        //consition variables
        double Temp;
        double begin_time;
        
        void sendDEoutput(vd::ExternalEventList& output, const std::string name, const double value) const
        {
            output.emplace_back(name);
            vv::Map& attrs = output.back().addMap();
            attrs.addString("name", name);
            attrs.addDouble("value", value);
        }
    };
}
DECLARE_DYNAMICS(gluePhysic::DecFSA);
