/**
  * @file DecFSAbl.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
  
// @@tagdynamic@@
// @@tagdepends: vle.discrete-time, vle.extension.fsa @@endtagdepends
  
#include <vle/extension/fsa/Statechart.hpp>
#include <vle/DiscreteTime.hpp>

using namespace std;
using namespace vle;
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace gluePhysic {
    enum DecState {DEC_STATE1, DEC_STATE2, DEC_STATE3};
    class DecFSAbl: public vle::extension::fsa::Statechart
    {
    public:
        DecFSAbl(const devs::DynamicsInit& atom,
                const devs::InitEventList& events) :
                Statechart(atom, events), lai(0)
        {
            states(this) << DEC_STATE1 << DEC_STATE2 << DEC_STATE3;

            transition(this, DEC_STATE1, DEC_STATE2)
                << guard(&DecFSAbl::cond1)
                << send(&DecFSAbl::out_1);
            transition(this, DEC_STATE2, DEC_STATE3)
                << guard(&DecFSAbl::cond2)
                << send(&DecFSAbl::out_2);

            initialState(DEC_STATE1);        
            
                eventInState(this, "LAI", &DecFSAbl::inLai)
            >> DEC_STATE1 >> DEC_STATE2 >> DEC_STATE3;
        }
        virtual ~DecFSAbl() {};
        
        virtual std::unique_ptr<vv::Value>  observation(const vd::ObservationEvent& /*event*/) const
        { return vv::Integer::create(currentState()); }
        
    private :
        //First transition 
        bool cond1(const vd::Time& julianDay)
        { return (julianDay == 30); }
        
        void out_1(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
        {
            sendDEoutput(output, "MaxEff", 0.7);
            sendDEoutput(output, "K", 0.9);
        }
        
        //Second transition
        bool cond2(const vd::Time& julianDay)
        { return (julianDay == 300); }
        
        void out_2(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
        {
            sendDEoutput(output, "MaxEff", 0.5);
            sendDEoutput(output, "K", 0.5);
        }

       void inLai(const vd::Time& /* time */, const vd::ExternalEvent& event)
       { lai = event.attributes()->toDouble().value(); }

        //consition variables
        double lai;
        
        void sendDEoutput(vd::ExternalEventList& output, const std::string name, const double value) const
        {
            output.emplace_back(name);
            vv::Map& attrs = output.back().addMap();
            attrs.addString("name", name);
            attrs.addDouble("value", value);
        }
    };
}
DECLARE_DYNAMICS(gluePhysic::DecFSAbl);
