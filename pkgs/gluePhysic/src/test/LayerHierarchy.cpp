/**
  * @file LayerHierarchy.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Tools.hpp>

#include <vector>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace gluePhysic {
    class LayerHierarchy : public DiscreteTimeDyn
    {
    public:
        LayerHierarchy(
           const vd::DynamicsInit& atom,
           const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
        {
            NumberOfLayers = (evts.exist("NumberOfLayers")) ? vv::toInteger(evts.get("NumberOfLayers")) : 4;
            
           for (int i=0;i<NumberOfLayers;i++) {
                std::string VarName = vu::format("%s%d", "RadiationIntercepted_Layer", i+1);
                RadiationIntercepted_Layers.push_back(Var());
                RadiationIntercepted_Layers[i].init(this, VarName, evts);
                
                std::string SyncName = vu::format("%s%d", "FractionIntercepted_Layer", i+1);
                FractionIntercepted_Layers.push_back(Var());
                FractionIntercepted_Layers[i].init(this, SyncName, evts);
                getOptions().syncs.insert(std::make_pair(SyncName, 1));
            }

            RadiationIntercepted_Soil.init(this,"RadiationIntercepted_Soil", evts);
            
            RG.init(this,"RG", evts);
            getOptions().syncs.insert(std::make_pair("RG", 1));
            
            initValue(0.);
        }

        virtual ~LayerHierarchy() {};

        virtual void compute(const vd::Time& /*time*/)
        {
            double residualRG = RG();
            
            for (int i=0;i<NumberOfLayers;i++) {
                RadiationIntercepted_Layers[i] = residualRG * FractionIntercepted_Layers[i]();
                residualRG = residualRG * ( 1 - FractionIntercepted_Layers[i]() );
            }
        
            RadiationIntercepted_Soil = residualRG;
        }


        void initValue(const vd::Time& /*time*/)
        {
            for (int i=0;i<NumberOfLayers;i++) {
                RadiationIntercepted_Layers[i] = 0.0;
            }
            RadiationIntercepted_Soil = 0.0;
        }


    private:
        std::vector<Var> RadiationIntercepted_Layers;
        Var RadiationIntercepted_Soil;
        
        Var RG;
        std::vector<Var> FractionIntercepted_Layers;
        
        int NumberOfLayers;
    };
} // namespace gluePhysic
DECLARE_DYNAMICS(gluePhysic::LayerHierarchy)
