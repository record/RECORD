/**
  * @file testBL.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
using namespace vle::discrete_time;

namespace gluePhysic {
    class testBL : public DiscreteTimeDyn
    {
    public:
        testBL(           
        const vd::DynamicsInit& atom,
        const vd::InitEventList& evts)
         : DiscreteTimeDyn(atom, evts)
        {
            rateLAI = (evts.exist("rateLAI")) ? vv::toDouble(evts.get("rateLAI")) : 0.1;
            
            LAI.init(this,"LAI", evts);
            initValue(0.);
        }

        virtual ~testBL() {};

        virtual void compute(const vd::Time& /*time*/)
        { 
            LAI = LAI(-1) + rateLAI; 
        }

        void initValue(const vd::Time& /*time*/)
        { 
            LAI = 0; 
        }

    private:
        double rateLAI;
        Var LAI;
    };
} // namespace gluePhysic
DECLARE_DYNAMICS(gluePhysic::testBL)
