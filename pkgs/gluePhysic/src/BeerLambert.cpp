/**
  * @file BeerLambert.cpp
  * @author E.Casellas-(The RECORD team -INRA )
  *
  * Copyright (C) 2009-2017 INRA
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
using namespace vle::discrete_time;

namespace gluePhysic {
    class BeerLambert : public DiscreteTimeDyn
    {
    public:
        BeerLambert(
           const vd::DynamicsInit& atom,
           const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
        {
            maxEff = (evts.exist("maxEff")) ? vv::toDouble(evts.get("maxEff")) : 1.0;  
            k = (evts.exist("k")) ? vv::toDouble(evts.get("k")) : 0.7;  
            
            FractionIntercepted_Plant.init(this,"FractionIntercepted_Plant", evts);
            FractionIntercepted_Soil.init(this,"FractionIntercepted_Soil", evts);
            MaxEff.init(this,"MaxEff", evts);
            K.init(this,"K", evts);

            LAI.init(this,"LAI", evts);
            getOptions().syncs.insert(std::make_pair("LAI", 1));
            
            initValue(0.);
        }

        virtual ~BeerLambert() {};

        virtual void compute(const vd::Time& /*time*/)
        {
            FractionIntercepted_Plant = MaxEff() * (1 - exp(-K() * LAI()));
            FractionIntercepted_Soil = 1 - FractionIntercepted_Plant();
        }


        void initValue(const vd::Time& /*time*/)
        {
            MaxEff = maxEff;
            K = k;
            
            FractionIntercepted_Plant = 0.0;
            FractionIntercepted_Soil = 1.0;
        }

    private:
        double maxEff;
        double k;
        
        Var FractionIntercepted_Plant;
        Var FractionIntercepted_Soil;
        Var MaxEff;
        Var K;
        
        Var LAI;
    };
} // namespace gluePhysic
DECLARE_DYNAMICS(gluePhysic::BeerLambert)
