==== Description ====
Le modèle BeerLambert du paquet gluePhysic est un modèle simple qui estime la fraction de rayonnement intercepté par une couche homogène de feuilles ainsi que la fraction transmise complémentaire selon la loi de Beer-Lambert (aussi connu comme loi de Beer-Lambert-Bouguer).

Le modèle prend en entrée la valeur de LAI (en m²/m²) et donne en sortie les fractions intercepté et transmise. Les paramètres de ce modèle sont le coefficient d'extinction de la lumière et le coefficient d'efficacité maximale d'interception de la lumière.

La loi de Beer-Lambert établie une proportionnalité entre la concentration d'une entité chimique en solution, l'absorbance de celle-ci et la longueur du trajet parcouru par la lumière dans la solution. La loi de Beer-Lambert n'est cependant valable que sous certaines conditions : la lumière doit être monochromatique, la concentration des solutions doit être faible (de l’ordre de 10-4 mol.L-1), les solutions doivent être homogènes et le soluté ne doit pas réagir sous l’action de la lumière incidente. Cette loi de Beer-Lambert est communément utilisé pour décrire la relation entre la fraction de lumière pénétrant la canopé et l'indice de surface foliaire (LAI). Note: la distribution géometrique des surfaces foliaire n'est pas prise en compte dans ce modèle.


==== Port d'entrée/sortie ====
 - entrée : LAI
 - sorties : FractionIntercepted_Plant , FractionIntercepted_Soil(, MaxEff, K) 


==== Paramètres ====
 - maxEff : coefficient d'efficacité maximale d'interception de la lumière (facultatif, utilise une valeur par défaut de 1 si non spécifié dans le vpz) ;
 - k : coefficient d'extinction de la lumière (facultatif, utilise une valeur par défaut de 0.7 si non spécifié dans le vpz) ;


==== Détails ====
Ce modèle utilise l'extension vle.discrete-time.

Les variables d'état MaxEff et K prennent et gardent par défaut la valeur de leur paramètre associé (maxEff et k respectivement). Etant des variables d'état ceci offre la possibilité de venir les forcer en cours de simulation pour changer leur valeur.

Equations:
	FractionIntercepted_Plant = MaxEff() * ( 1 - exp( -K() * LAI() ) );
	FractionIntercepted_Soil = 1 - FractionIntercepted_Plant();


==== Exemple ====
voir exp/test/test-BeerLambert.vpz (test d'utilisation simple couche)
     exp/test/test-BeerLambert-Multi.vpz (exemple d'utilisation dans le cas de multiples couches superposées)
     exp/test/test-BeerLambert-perturb.vpz (test d'utilisation simple couche avec modification de MaxEff et K en cours de simulation)


==== Source ====
voir src/BeerLambert.cpp
