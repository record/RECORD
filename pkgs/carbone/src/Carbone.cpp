/**
 * @file src/Carbone.cpp
 * @author The Record Development Team
 */

/*
 * Copyright (C) 2009-2017 The Record Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Rand.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
using namespace vle::discrete_time;

namespace carbone {
class Carbone : public DiscreteTimeDyn
{

public:
    Carbone(const vle::devs::DynamicsInit& model,
        const vle::devs::InitEventList& events) :
    DiscreteTimeDyn(model, events), mrand()
    {
        if (events.exist("seed")) {
            if (events.get("seed")->isInteger()) {
                mrand.seed(events.getInt("seed"));
            } else if (events.get("seed")->isDouble()) {
                mrand.seed((int) events.getDouble("seed"));
            }
        }

        Z0 = vv::toDouble(events.get("Z0"));
        sigmaZ0 = std::sqrt(vv::toDouble(events.get("dvtZ0")));
        U0 = vv::toDouble(events.get("U0"));
        sigmaEpsilon = std::sqrt(vv::toDouble(events.get("dvtEpsilon")));
        sigmaR = std::sqrt(vv::toDouble(events.get("dvtR")));

        R = vv::toDouble(events.get("R"));
        b = vv::toDouble(events.get("b"));

        Z.init(this,"Z", events);
        U.init(this,"U", events);

        initValue(0.);

    }

    virtual ~Carbone() { }

    virtual void compute(const vle::devs::Time& /* time */)
    {
        double epsilon = mrand.normal(0, sigmaEpsilon);
        double r = R + mrand.normal(0, sigmaR);
        double z = (1 - std::max(0., r)) * Z(-1) + b * U(-1) + epsilon;

        Z = std::max(0., z);
    }

    void initValue(const vle::devs::Time& /* time */)
    {
        double z0 = mrand.normal(Z0, sigmaZ0);

        Z = std::max(0., z0);
        U = U0;
    }

private:
    double Z0;
    double sigmaZ0;
    double U0;
    double sigmaEpsilon;
    double sigmaR;

    double R;
    double b;
    vle::utils::Rand mrand;
    
    Var Z;
    Var U;

};
} // namespace carbone

DECLARE_DYNAMICS(carbone::Carbone);
