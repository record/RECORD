/**
 * @file src/Executive.cpp
 * @author The Record Development Team
 *   Applied Mathematics and Informatics Research Division &
 *   Environment and Agronomy Research Division,
 *   French National Institute for Agricultural Research
 * @version $Id: $
 */

/*
 * Copyright (C) 2009-2017 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: @@endtagdepends


#include <vle/devs/Executive.hpp>
#include <vle/vpz/BaseModel.hpp>
#include <vle/vpz/Vpz.hpp>
#include <vle/utils/Rand.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/value/Set.hpp>
#include <vle/value/Tuple.hpp>


#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;
namespace vu = vle::utils;


namespace carbone
{

class Executive: public vd::Executive
{
public:
    Executive(const vd::ExecutiveInit& mdl, const vd::InitEventList& events) :
        vd::Executive(mdl, events), mrand()
    {
        if (events.exist("seed")) {
           if (events.get("seed")->isInteger()) {
             mrand.seed(events.getInt("seed"));
           } else if (events.get("seed")->isDouble()) {
             mrand.seed((int) events.getDouble("seed"));
           }
        }

        number = vv::toInteger(events.get("number"));

        std::string stype = vv::toString(events.get("type"));

        if (stype == "standard") {
            mType = STANDARD;
        } else if (stype == "auxiliary") {
            mType = AUXILIARY;
        } else if (stype == "para"){
            mType = PARA;
        }

        lambda = vv::toDouble(events.get("lambda"));
        if (mType == PARA) {
            h = vv::toDouble(events.get("h"));
            a = std::sqrt(1. - h * h);
        }

        const vv::Set& measures = vv::toSetValue(events.get("measures"));
        const vv::Tuple& date(vv::toTupleValue(measures.get(0)));
        const vv::Tuple& wd(vv::toTupleValue(measures.get(1)));
        const vv::Tuple& var(vv::toTupleValue(measures.get(2)));

        mWD.resize(wd.size());
        mVAR.resize(var.size());
        for (unsigned int i = 0; i < date.size(); ++i) {
            mDate.push_back(date[i]);
            mWD(i) = wd[i];
            mVAR(i) = var[i];
        }

        const vv::Set& state = vv::toSetValue(events.get("state"));

        for (unsigned int i = 0; i < state.size(); ++i) {
            std::string name = vv::toString(state.get(i));

            mStateVariables.push_back(name);
            mStateIndex[name] = i;
        }
    }

    void createModel(unsigned int index)
    {
        std::string modelName(vu::format("model_%i", index));
        std::string out(vu::format("out_%i", index));

        std::vector < std::string > inputports, outputports, conditions;
        outputports.push_back(out);
        std::vector < std::string >::const_iterator it = mStateVariables.begin();
        while (it != mStateVariables.end()) {
            outputports.push_back(*it++);
        }

        inputports.push_back("U");

        // output ports
        vd::Executive::addOutputPort("Executive", out);

        conditions.push_back("cond");
        conditions.push_back("cond_seed");

        // create model
        vd::Executive::createModel(modelName, inputports, outputports, "dyn", conditions, "");

        // connections
        {
            std::vector < std::string >::const_iterator it = mStateVariables.begin();
            while (it != mStateVariables.end()) {
                vd::Executive::addInputPort("Executive", modelName);
                vd::Executive::addConnection(modelName, *it++, "Executive", modelName);
            }
        }
        vd::Executive::addConnection("Executive", out, modelName, "U");

        mIndex[modelName] = index;
    }

    void filter(bool init)
    {
    //   Y<-WD[i]
        double Y = mWD(mMeasureIndex);

    //   S<-matrix(c(SD[i]))
        boost::numeric::ublas::matrix < double > S(1, 1);
        S(0, 0) = mVAR(mMeasureIndex);

    //   A<-matrix(nrow=1, ncol=3, c(1,0,0))
        boost::numeric::ublas::matrix < double > A(1, mStateVariables.size());
        A(0, 0) = 1;
        A(0, 1) = 0;
        A(0, 2) = 0;

    //   MU<-0
//        double MU = 0;

    //   W<-rep(1/Num, Num)
        if (init) {
            for (unsigned int k = 0; k < number; ++k) mW(k) = 1./number;
        } else {
            for (unsigned int k = 0; k < number; ++k) mW(k) = 1./mW(k);
        }

    //   Wpost<-W
        boost::numeric::ublas::vector < double > Wpost(number);
    //    Wpost = W;

    //   EV<-A%*%X
        boost::numeric::ublas::matrix < double > EV(1, number);
        EV = prod(A, mX);

    //   EV<-MU+EV

    //   Ln<-dnorm(Y, mean=EV,sd=rep(sqrt(S), length(W)))
        boost::numeric::ublas::vector < double > Ln(number);
        for (unsigned int k = 0; k < number; ++k) {
            double e = Y - EV(0, k);
            Ln(k) = 1. / (std::sqrt(2 * 3.1415 * S(0, 0)))
                * std::exp((-0.5 / S(0, 0)) * e * e);
        }

    //   Wpost<-Ln*W
        Wpost = element_prod(Ln, mW);

    //   ConstNorm<-sum(Wpost)
        double s = sum(Wpost);

    //   Wpost<-Wpost/ConstNorm
        Wpost /= s;

    //   SelectedParticles<-sample(1:length(W), length(W), replace=T, prob=Wpost)
        boost::numeric::ublas::matrix < double > Xtemp(mStateVariables.size(), number);
        boost::numeric::ublas::vector < double > Wtemp(number);

        mSelect.clear();
        for (unsigned int k = 0; k < number; ++k) {
            int j = 0;
            double _s = Wpost(0);
            double r = mrand.getDouble();

            while (_s < r) {
                ++j;
                _s += Wpost(j);
            }
            column(Xtemp, k) = column(mX, j);
            Wtemp(k) = Wpost(j);
            mSelect.push_back(j);
        }
        mX = Xtemp;
        mW = Wtemp;

        if (mType == PARA) {
            boost::numeric::ublas::vector < double > tempPara(number);

            for (unsigned int p = 0; p < number; ++p) {
                tempPara(p) = para(mSelect[p]);
            }
            para = tempPara;
        }
    }

    virtual vd::Time init(vd::Time /* time */) override
    {
        mMeasureIndex = 0;
        mX.resize(mStateVariables.size(), number);
        mW.resize(number);

        if (mType == PARA) {
            para.resize(number);
            for (unsigned int i = 0; i < number; ++i) {
                double l = mrand.getDouble(0., lambda);

                lambdas.push_back(l);
                para(i) = l;
            }
        }

        for (unsigned int i = 0; i < number; ++i) {
            createModel(i);
        }
        mState = IDLE;
        mSigma = vd::infinity;
        return vd::infinity;
    }

    virtual vd::Time timeAdvance() const override
    {
        return mSigma;
    }

    virtual void output(vd::Time /* time */,
            vd::ExternalEventList& output) const override
    {
        if (mState == OUT) {
        for (unsigned int k = 0; k < number; ++k) {
            std::string out(vu::format("out_%i",  k));

            std::vector < std::string >::const_iterator it = mStateVariables.begin();
            unsigned int i = 0;

            while (it != mStateVariables.end()) {
                output.emplace_back(out);
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name", *it);
                attrs.addDouble("value", mX(i, k));
                ++it;
                ++i;
            }
        }
        }
    }

    virtual void internalTransition(vle::devs::Time /*time*/) override
    {
        if (mState == RUN) {
            if (mType == STANDARD) {
                filter(true); // Standard
            } else {
                filter(false); // Auxiliary or para
            }
            ++mMeasureIndex;
            mState = OUT;
            mSigma = 0;
        } else if (mState == OUT) {
            mState = IDLE;
            mSigma = vd::infinity;
        }
    }

    virtual void externalTransition(const vd::ExternalEventList& events,
        vd::Time time) override
    {
        boost::numeric::ublas::matrix < double > oldX(mStateVariables.size(), number);
        vle::devs::ExternalEventList::const_iterator it = events.begin();

        oldX = mX;
        while (it != events.end()) {
            std::string name = it->getPortName();
            if ( mIndex.find(name) != mIndex.end() ) {
                int index = mIndex[name];
                double value = it->attributes()->toDouble().value();

                if (mType == STANDARD) {
                    double oldValue = mX(mStateIndex[name], index);
                    double newValue = std::max(0., value + mrand.normal(lambda * oldValue, 0.0));
                    mX(mStateIndex[name], index) = newValue;
                } else { // Auxiliary or para
                    mX(mStateIndex[name], index) = value;
                }

            }
        ++it;
        }
        if (mDate[mMeasureIndex] == time) {
            if (mType == AUXILIARY or mType == PARA) {
                filter(true);

                double av = 0.0;
                double var = 0.0;

                if (mType == PARA) {
                    av = sum(para) / number;
                    boost::numeric::ublas::vector < double > d(number);
                    for (unsigned int p = 0; p < number; ++p) {
                        d(p) = para(p) - av;
                    }
                    var = sum(element_prod(d, d)) / (number - 1);
                }

                for (unsigned int p = 0; p < number; ++p) {
                double sigma = 0.0;

                if (mType == PARA) {
        // Lambda<-Para[1,]
        // m<-a*Lambda+(1-a)*mean(Lambda)
        // sdPara<-sqrt(var(Lambda)*h^2)
        // Lambda<-rnorm(Num, mean=m, sd=sdPara)
        // Lambda[Lambda<0]<-0
                    double l = para(p);
                    double m = a * l + (1 - a) * av;
                    double sd = std::sqrt(var * h * h);

                    sigma = std::max(0., mrand.normal(sd, m));
                }

                for (unsigned int s = 0; s < mStateVariables.size(); ++s) {
                    double value = mX(s, p);
        // ************
        // BIZARRE !!!!
                    double oldValue = oldX(s, mSelect[p]);
        //            double oldValue = oldX(s, p);
        // ************
                    double eps;

                    if (mType == AUXILIARY) {
                    eps = mrand.normal(lambda * oldValue, 0.0);
                    } else {
                    eps = mrand.normal(sigma * oldValue, 0.0);
                    }

                    double newValue = value + eps;

                    if (newValue < 0) newValue = 0;
                    mX(s, p) = newValue;
                }
                }
            }

            mState = RUN;
            mSigma = 0;
        }
    }

    virtual std::unique_ptr<vv::Value> observation(const vd::ObservationEvent& event) const
    {
        if (event.onPort(mStateVariables[0])) {
            return vv::Double::create(sum(row(mX, 0)) / number);
        } else if (event.onPort("WD")) {
            if (event.getTime() == mDate[mMeasureIndex - 1]) {
                return vv::Double::create(mWD(mMeasureIndex - 1));
            }
        } else if (event.onPort("lambda")) {
            return vv::Double::create(sum(para) / number);
        }
        return vv::Double::create(0.);
    }
    
private:
    enum state { IDLE, RUN, OUT };
    enum type { STANDARD, AUXILIARY, PARA };

    vle::utils::Rand mrand;
    type mType;
    state mState;
    vle::devs::Time mSigma;

// Particles
    unsigned int number;
    double lambda;
    double h;
    double a;
    std::vector < double > lambdas;
    boost::numeric::ublas::vector < double > para;
    std::map < std::string, int > mIndex;

// Measures
    int mMeasureIndex;
    std::vector < double > mDate;
    boost::numeric::ublas::vector < double > mWD;
    boost::numeric::ublas::vector < double > mVAR;

// State
    std::vector < std::string > mStateVariables;
    std::map < std::string, int > mStateIndex;
    boost::numeric::ublas::matrix < double > mX;
    boost::numeric::ublas::vector < double > mW;
    std::vector < int > mSelect;
    
};

}; // namespace carbone

DECLARE_EXECUTIVE(carbone::Executive)
