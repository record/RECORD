/*--------------- COPYRIGHT ------------------------
 * | INRA - Unit� MIA de Jouy en Josas                |
 * --------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
 * | Derniere mise a jour : oct 2013               |
 * | Role                 : les fonctions de          |
 * |  dispersion individuelle                         |
 * --------------------------------------------------*/
///////////////////////////////////////////////////////

#include <math.h>
#include "caliconfig.h"
#include "calitypes.h"
#include "Point.h"
#include <stdarg.h>

////////////////////////////////////////////////////////////////
// la fonction de dispersion du pollen:
// celle  de Klein modifi�e par Nathalie Colbach le 11/09/2006
// pour les distances > 50m
////////////////////////////////////////////////////////////////
real
        f1 (const Point & p, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/ )
{
    real r, rt, rr, gamma, K;
    
    r = (p.dist0 () / SCALE);
    
    if (r <= 1.5)
        rt = 0.340 - 0.405 * r + 0.128 * pow (r, 2.0);
    else if (r <= 50)
        rt = 0.03985 / (1.0 + pow (r, 3.12) / 3.80);
    else
    {
        rr = 50;
        gamma = -2.29;
        //valeur la plus lourde -2.14, + l�g�re - 2.56
        K = (0.03985 / (1.0 + pow (rr, 3.12) / 3.80)) / pow (1.0 + rr, gamma);
        rt = K * pow (1.0 + r, gamma);
    }
    //modif selon Devaux 2006 pour longue distance NC 11 9 2006
    return rt;
}



////////////////////////////
// fonction de dispersion des graines (Colbach et al. 2000b)
////////////////////////////
real
        f2 (const Point & p, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/ )
{
    real grainesB = 1.38930;
    real grainesC = 2.08686;
    real distance, fd;
    distance = (p.dist0 () / SCALE);
    fd = (grainesB * grainesC) * pow (distance, grainesC - 2) *
            exp (-grainesB * pow (distance, grainesC)) / (2.0 * M_PI);
    
    return (fd);
}

////////////////////////////
//  Fonction constante qui renvoie tjrs 1
////////////////////////////

real
        f3 (const Point & /*p*/, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    return 1.0;
}

////////////////////////////////////////////////////////////////
// Function with discontinuities to illustrate what happens
// in that case
// KK: 7/3/2007
// This dispersal function has a compact support and, for well chosen
// a,b, becomes rather suddenly null.
// This implies that we can have a triangle which intersects a little
// the support of the dispersal function but where all the points
// of the cubature rule are outside the support.
// The integrale on this  triangle will be considered as null and
// the triangle will no more be divided.
////////////////////////////////////////////////////////////////

real
        f4 (const Point & p, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    real distance, a = 10.0, b = 20.0;
    distance = (p.dist0 () / SCALE);
    if (fabs (distance) <= sqrt (a / b))
    {
        return (a - b * distance * distance);
    }
    else
        return 0.0;
}

//////////////////////
// Gaussian
////////////////////////////////////////////////////////////////
real
        f5 (const Point & p, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    real sigma = 100.0, mu = 0.0;
    real distance, fd, a, b;
    
    
    distance = (p.dist0 () / SCALE);
    
    a = 1.0 / (sigma * sqrt (2 * M_PI));
    b = - ((distance - mu) * (distance - mu)) / (2.0 * sigma * sigma);
    fd = a * exp (b);
    return (fd);
}

////////////////////////////////////////////////////////////////
// Pollen dispersal function by Klein before 13/09/2006
////////////////////////////////////////////////////////////////
real
        f6 (const Point & p, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    real r, rt;
    
    r = (p.dist0 () / SCALE);
    if (r <= 1.5)
        rt = 0.340 - 0.405 * r + 0.128 * pow (r, 2.0);
    else
        rt = 0.03985 / (1 + pow (r, 3.12) / 3.80);
    return rt;
}
////////////////////////////////////////////////////////////////
// Exemple pour FlorSys
////////////////////////////////////////////////////////////////
real
        f7 (const Point & p, int /*narg*/, real targ[MAX_NARGFUNCTIONS])
{
    // les arguments suppl�mentaires
    real height, mass, mode;
    real mu,a,b,c;
    real muprime, aprime, bprime, cprime;
    // travail
    real r, dist_mean, dist_max;
    
    int i=0;
    height=targ[i++]; mass=targ[i++]; mode=targ[i++];
    mu=targ[i++];a=targ[i++];b=targ[i++];c=targ[i++];
    muprime=targ[i++];aprime=targ[i++];bprime=targ[i++];cprime=targ[i++];
    
    dist_mean= mu+a*log10(height) +b*log10(mass) + c*mode;
    dist_max= muprime+aprime*log10(height) +bprime*log10(mass) + cprime*mode;
    
    // fonction bidon
    r = (p.dist0 () / SCALE)*(dist_max/dist_mean);
    return r;
}


//////////////////////
// Inutilises
////////////////////////////////////////////////////////////////
real
        f8 (const Point & /*p*/, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    return 0;
}
real
        f9 (const Point & /*p*/, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    return 0;
}
real
        f10 (const Point & /*p*/, int /*narg*/, real* /*targ[MAX_NARGFUNCTIONS]*/)
{
    return 0;
}
