///////////////////////////////////////////////////////
//  includes
///////////////////////////////////////////////////////

#include <iostream>
#include <cstring>
#include <string>

#include "caliinclc.h"
#include "calidefs.h"
#include "caliconfig.h"
#include "util.h"
#include "calierror.h"
#include "readPoly.h"

#include "Iparam.h"
#include "Ipoly.h"
#include "litpoly.h"
#include "cubature.h"
#include <time.h>


///////////////////////////////////////////////////////
// Programme principal
///////////////////////////////////////////////////////

int
main (int /*argc*/, char **/*argv*/)
{
  
  /////////////////////////////////////////////////
  // Affecter des valeurs aux paramètres de califlopp
  /////////////////////////////////////////////////

  Boolean verbose = Boolean (True);	// Impressions intermédiaires
  int input = 2;		// Format du fichier des polygones
  char delim[2] = "\t";		// Séparateur des valeurs sur le fichier des polygones    
  int output = ALL;		// Sorties voulues a l'ecran
  int nfunct = 2;

  int ifunct[MAX_NFUNCTIONS];
  real abser[MAX_NFUNCTIONS], reler[MAX_NFUNCTIONS];
  long int maxpts[MAX_NFUNCTIONS];
  // Travail
  int i, j, err = 0;

  // Mesure du temps
  real temps;
  time_t now, debut;
  time (&debut);




  /////////////////////////////////////////////////
  //  fonction de dispersion
  /////////////////////////////////////////////////
      ifunct[0] = 1;		// indice de la fonction voulue
      ifunct[1] = 2;		// indice de la fonction voulue
  for (i = 0; i < nfunct; i++)
    {
      abser[i] = DEFAULT_ABS_ERR;
      reler[i] = DEFAULT_REL_ERR;
      maxpts[i] = 800000000;
    }

  /////////////////////////////////////////////////
  // Creer la structure qui contient les parametres fixes
  /////////////////////////////////////////////////

  Iparam param = Iparam (verbose, input, delim, output,
			 nfunct, ifunct,
			 abser, reler, maxpts);



  /////////////////////////////////////////////////
  // Lire le nombre de polys
  /////////////////////////////////////////////////

  int npoly;
  char lu[MAX_LINE_POLY];

  /* Pour stocker les messages d'erreur */
  char errmess[CHAR_MAX];
  char moi[] = "litpoly";
  char errlect[] = "Error in reading";

/* REMPLACE LE 6/1/2014
  char *filenamepoly = new char[PATH_MAX];
PAR:
  */
  char *filenamepoly= new char[sizeof( "../data/examples/ex-nc/data-nc")];
  strcpy (filenamepoly, "../data/examples/ex-nc/data-nc");

  FILE *fpi;
  fpi = fopen (filenamepoly, "r");
  if (!fpi)
    {
      sprintf (errmess, "cannot open polygons file %s\n", filenamepoly);
      return (ecrmess (CALI_ERFIC1, moi, errmess));
    }


  // Lire avec fgets ce fichier, car il a un nbre va. de données
  // sur chaque ligne et il ne faut pas mélanger les fscanf et fgets
  if (fgets (lu, MAX_LINE_POLY, fpi) == NULL)
    return (ecrmess (CALI_ERLECT, moi, errlect, True));
  npoly = atoi (lu);

  /////////////////////////////////////////////////
  // Creer la structure qui va contenir les polys
  /////////////////////////////////////////////////

  const Ipoly polys = Ipoly (npoly, (char *) (filenamepoly));
  const Ipoly *lespolys = &(polys);


  /////////////////////////////////////////////////
  // Lire les polys
  /////////////////////////////////////////////////

  err = litpoly (fpi, &param, lespolys);
  int couples[MAX_NPAIRS][2];	// pour stocker les nos des couples voulus */

  /////////////////////////////////////////////////
  // Determiner les parcelles voulues
  /////////////////////////////////////////////////

  //   int ncouples=0; // Tous les polys

  int ncouples = 4;
  for (i = 0; i < ncouples; i++)
    {
      couples[i][0] = 1;
      couples[i][1] = i + 1;
    }


  /////////////////////////////////////////////////
  // Définir les traitements initiaux des especes
  /////////////////////////////////////////////////
  /* Ce seront les arguments de la fonction  de dispersion.
     Les arguments sont dans l'ordre:
     height, mass, mode, mu, a,b,c, muprime, aprime, bprime, cprime
   */
  int nargu[2] = {0}; // pas d'argu supplementaire a la fonction de dispersion
  real traits[2][MAX_NARGFUNCTIONS]; // ignore


  /////////////////////////////////////////////////
  // Le fichier resultat
  /////////////////////////////////////////////////
  /* si on veut un fichier resultat (6/1/2014)
  char *filenamer = new char[sizeof("RES")];
  sprintf (filenamer, "RES");
  sinon            : */
     char * filenamer =NULL;      


  /* mode d'ouverture du fichier resultat */
  char openr = 'w';		// 'a' si append         


//////////////////////////////////////////////////////////
// Allouer la sortie
//////////////////////////////////////////////////////////
  int reslignes = 0;
  real **sortie = NULL;

  /* si on veut les résultats en retour : */
  reslignes = calcnres (npoly, ncouples, couples);

  CREER_T2 (sortie, reslignes, real);
  for (i = 0; i < reslignes; i++)
    {
      CREER_T1 (sortie[i], (4 + nfunct), real);
    }

  /////////////////////////////////////////////////
  // Lancer les calculs
  /////////////////////////////////////////////////


  err = cubature (lespolys, &param, ncouples, couples, nargu, traits,
		  filenamer, openr, reslignes, sortie);


  time (&now);			/* get current time; */
  temps = difftime (now, debut);
  printf ("Total elapsed time: %.f seconds\n ", temps);

  /////////////////////////////////////////////////
  // Ecrire les sorties 
  /////////////////////////////////////////////////
  if (reslignes > 0)
    {
      printf ("\n\n\n****** VALEURS RETOURNEES ******\n");
      for (i = 0; i < reslignes; i++)
	{
	  printf ("\n POLYGONE 1: %g; ", sortie[i][0]);
	  printf ("POLYGONE 2: %g\n", sortie[i][1]);
	  printf ("aire poly1: %g; ", sortie[i][2]);
	  printf ("aire poly2: %g\n", sortie[i][3]);
	  for ( j = 0; j < nfunct; j++)
	    {
	      printf ("Espece: %d (fonction %d) \n", j + 1, ifunct[j]);
	      printf ("Flux total: %g; ", sortie[i][j + 4]);
	      printf ("par unité de surface du poly1: %g; ",
		      (sortie[i][j + 4] / sortie[i][2]));
	      printf ("par unité de surface du poly2: %g\n",
		      (sortie[i][j + 4] / sortie[i][3]));

	      // Modifier les traitements selon les résultats pour le jour suivant
	      traits[j][3]= traits[j][3] + sortie[i][j + 4]/ 1.0e+15;

	    }
	} // fin i
    } // fin reslignes

  /////////////////////////////////////////////////
  // Désallouer les structures de l'étude
  // AJOUT le 6/1/2014
  /////////////////////////////////////////////////
  delete []filenamepoly;
  if (filenamer != NULL)
    delete []filenamer;

  DETRU_T2 (sortie, reslignes);


  return err;
}
