///////////////////////////////////////////////////////
//  includes
///////////////////////////////////////////////////////

#include <iostream>
#include <cstring>
#include <string>

/*
#include "caliinclc.h"
#include "calidefs.h"
#include "caliconfig.h"
#include "readPoly.h"
#include "calierror.h"
*/
#include "util.h"
#include "Iparam.h"
#include "Ipoly.h"
#include "litpoly.h"
#include "cubature.h"

#define NESPECES 3 // nombre d'especes


///////////////////////////////////////////////////////
// Programme principal
///////////////////////////////////////////////////////

int
main (int /*argc*/, char **/*argv*/)
{

  int NJOURS=2; // nombre de jours de l'étude
  
  /////////////////////////////////////////////////
  // Affecter des valeurs aux paramètres de califlopp
  /////////////////////////////////////////////////

  Boolean verbose = Boolean (True);	// Impressions intermédiaires
  int input = 2;		// Format du fichier des polygones
  char delim[2] = "\t";		// Séparateur des valeurs sur le fichier des polygones    
  int output = ALL;		// Sorties voulues a l'écran
  int nfunct =  NESPECES;		// Nombre que de fonctions voulues; Ici, autant que d'especes

  int ifunct[MAX_NFUNCTIONS];
  real abser[MAX_NFUNCTIONS], reler[MAX_NFUNCTIONS];
  long int maxpts[MAX_NFUNCTIONS];
  // Travail
  int i, j, err, jour = 0;



  /////////////////////////////////////////////////
  // Quelques vérifications a oter quand OK
  /////////////////////////////////////////////////
  if (nfunct > MAX_NFUNCTIONS)
    {
      fprintf (stderr,
	       "Le nombre de fonctions excède le maximum %d. Augmenter MAX_NFUNCTIONS dans le fichier calidefs.h\n",
	       MAX_NFUNCTIONS);
      return 1;
    }

  /////////////////////////////////////////////////
  // Chaque espece est traitée par la même fonction de dispersion
  /////////////////////////////////////////////////
  for (i = 0; i < nfunct; i++)
    {
      ifunct[i] = 7;		// indice de la fonction voulue
      abser[i] = DEFAULT_ABS_ERR;
      maxpts[i] = DEFAULT_MAX_PTS;
      reler[i] = DEFAULT_REL_ERR;
    }

  /////////////////////////////////////////////////
  // Creer la structure qui contient les parametres fixes
  /////////////////////////////////////////////////

  Iparam param = Iparam (verbose, input, delim, output,
			 nfunct, ifunct,
			 abser, reler, maxpts);


  /////////////////////////////////////////////////
  // Lire le nombre de polys
  /////////////////////////////////////////////////

  int npoly;
  char lu[MAX_LINE_POLY];

  /* Pour stocker les messages d'erreur */
  char errmess[CHAR_MAX];
  char moi[] = "litpoly";
  char errlect[] = "Error in reading";


  /* REMPLACE LE 6/1/2014
  char *filenamepoly = new char[PATH_MAX];
PAR:
  */
  char *filenamepoly= new char[sizeof("../data/examples/ex-sausse/data")];
  strcpy (filenamepoly, "../data/examples/ex-sausse/data");

  FILE *fpi;
  fpi = fopen (filenamepoly, "r");
  if (!fpi)
    {
      sprintf (errmess, "cannot open polygons file %s\n", filenamepoly);
      return (ecrmess (CALI_ERFIC1, moi, errmess));
    }


  // Lire avec fgets ce fichier, car il a un nbre va. de données
  // sur chaque ligne et il ne faut pas mélanger les fscanf et fgets
  if (fgets (lu, MAX_LINE_POLY, fpi) == NULL)
    return (ecrmess (CALI_ERLECT, moi, errlect, True));
  npoly = atoi (lu);

  /////////////////////////////////////////////////
  // Creer la structure qui va contenir les polys
  /////////////////////////////////////////////////

  const Ipoly polys = Ipoly (npoly, (char *) (filenamepoly));
  const Ipoly *lespolys = &(polys);

  /////////////////////////////////////////////////
  // Lire les polys
  /////////////////////////////////////////////////
  err = litpoly (fpi, &param, lespolys);
  int couples[MAX_NPAIRS][2];	// pour stocker les nos des couples voulus */

  /////////////////////////////////////////////////
  // Determiner les parcelles voulues
  /////////////////////////////////////////////////

  //   int ncouples=0; // Tous les polys

  int ncouples = 2;
  for (i = 0; i < ncouples; i++)
    {
      couples[i][0] = 1;
      couples[i][1] = i + 1;
    }


  /////////////////////////////////////////////////
  // Définir les traitements initiaux des especes
  /////////////////////////////////////////////////
  /* Ce seront les arguments supplémentaires de la fonction  de dispersion.
     Les arguments sont dans l'ordre:
     height, mass, mode, mu, a,b,c, muprime, aprime, bprime, cprime
   */
  int nargu[NESPECES] = { 11, 11, 11 };
  real traits[NESPECES][MAX_NARGFUNCTIONS] = {
    {1.2, 0.003, 1, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08},
    {1.8, 0.002, 1, 0.015, 0.025, 0.035, 0.045, 0.055, 0.065, 0.075, 0.085},
    {1.6, 0.003, 1, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08}
  };

  /* Si les fonctions  de dispersion n'ont pas d'arguments supplémentaires: affecter 0 à tous les elts de nargu et traits est ignoré */

  /////////////////////////////////////////////////
  // Le fichier resultat
  /////////////////////////////////////////////////
  /* si on veut un fichier resultat (6/1/2014)
  char *filenamer= new char[sizeof("RES")];
  sprintf (filenamer, "RES");
  sinon            : */
     char * filenamer =NULL;      


  /* mode d'ouverture du fichier resultat */
  char openr = 'w';		// 'a' si append         


//////////////////////////////////////////////////////////
// Allouer la sortie
//////////////////////////////////////////////////////////
  int reslignes = 0;
  real **sortie = NULL;

  /* si on veut les résultats en retour : */
  reslignes = calcnres (npoly, ncouples, couples);
  std::cout << "reslignes:" << reslignes << std::endl;


  CREER_T2 (sortie, reslignes, real);
  for (i = 0; i < reslignes; i++)
    {
      CREER_T1 (sortie[i], (4 + nfunct), real);
    }

  /////////////////////////////////////////////////
  // Boucle sur les jours d'étude
  /////////////////////////////////////////////////
  for (jour=0; jour < NJOURS; jour++) {

  /////////////////////////////////////////////////
  // Lancer les calculs
  /////////////////////////////////////////////////

  err = cubature (lespolys, &param, ncouples, couples, nargu, traits,
		  filenamer, openr, reslignes, sortie);


  /////////////////////////////////////////////////
  // Ecrire les sorties 
  /////////////////////////////////////////////////
  if (reslignes > 0)
    {
      printf ("\n\n\n****** VALEURS RETOURNEES AU JOUR %d ******\n", jour+1);
      for (i = 0; i < reslignes; i++)
	{
	  printf ("\n POLYGONE 1: %g; ", sortie[i][0]);
	  printf ("POLYGONE 2: %g\n", sortie[i][1]);
	  printf ("aire poly1: %g; ", sortie[i][2]);
	  printf ("aire poly2: %g\n", sortie[i][3]);
	  for ( j = 0; j < nfunct; j++)
	    {
	      printf ("Espece: %d (fonction %d) \n", j + 1, ifunct[j]);
	      printf ("Flux total: %g; ", sortie[i][j + 4]);
	      printf ("par unité de surface du poly1: %g; ",
		      (sortie[i][j + 4] / sortie[i][2]));
	      printf ("par unité de surface du poly2: %g\n",
		      (sortie[i][j + 4] / sortie[i][3]));

	      // Modifier les traitements selon les résultats pour le jour suivant
	      traits[j][3]= traits[j][3] + sortie[i][j + 4]/ 1.0e+15;

	    }
	} // fin i
    } // fin reslignes
// Modifier les polys traites au temps suivant
  for (i = 0; i < ncouples; i++)
    {
      couples[i][0] = 2;
    }
  } // fin jour

  /////////////////////////////////////////////////
  // Désallouer les structures de l'étude
  // AJOUT le 6/1/2014
  /////////////////////////////////////////////////
  delete []filenamepoly;
  if (filenamer != NULL)
    delete []filenamer;

  DETRU_T2 (sortie, reslignes);



  return err;
}


