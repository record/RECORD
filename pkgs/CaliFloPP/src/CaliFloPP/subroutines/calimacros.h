#ifndef _CALIMACROS_H
#define _CALIMACROS_H

/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : macros de base            |
--------------------------------------------------*/
///////////////////////////////////////////////
// Macros de debug
///////////////////////////////////////////////#i
#include "caliinclc.h"
#include <iostream>
using std::endl;

#define DebugPrint (std::cout<<"$$ File: "<<__FILE__<<" Line: "<<__LINE__<<" $$ "<<endl)

////////////////////////////////////////////////
// NEW, ADD and DELETE macros
////////////////////////////////////////////////

#define NEW(p, type)\
  if ((p=(type *) malloc (sizeof(type)))==NULL) {\
  std::cerr << "NEW: Out of Memory!\n"<<endl;\
  exit(EXIT_FAILURE);\
}


#define ADD(head, p) if(head){\
  p->prev = head;\
  p->next = head->next;\
  head->next = p;\
  p->next->prev = p;\
}\
else {\
  head = p;\
  head->next = head->prev = p;\
}

#define ADDP(head,p) if(head){\
  p->next = head;\
  p->prev = head->prev;\
  head->prev = p;\
  p->prev->next = p;\
}\
else {\
  head = p;\
  head->next = head->prev = p;\
}

#define FREE(p)  if(p) { free((char *)p); p = NULL;}

#define DELETE( head, p ) if( head ) {\
  if ( head == head->next )\
     head = NULL; \
  else if ( p==head ) \
     head = head->next;\
  p->next->prev = p->prev;\
  p->prev->next = p->next;\
  FREE( p );\
}
////////////////////////////////////////////////
// CREER/DETRU macros
/////////////////////////////////////////////////
/* .......... Allocation de `n0' emplacements de type `type' ............. */
/*            Pour les vecteurs, on met un `*' dans l'instruction
              `type', pour les matrices on en met 2, etc            */
/*            Si le pointeur retourne est NULL, c'est que ca a pas marche:
              on imprime un message et on sort  */
/* on ne met pas unsigned devant les nombres d'octets a allouer
   au cas ou la macro aurait ete appelee avec un nombre negatif:
   il faut que ca plante dans ce cas et non pas que ca convertisse
   en non-signe */

#define CREER_T1(vect,n0,type)  \
  { \
  vect = (type *) calloc (n0, sizeof (type)); \
  if (vect == NULL) \
    { \
  std::cerr << "CREER_T1: Memory allocation problem\n" << endl; \
    exit(CALI_ERRALLOC); \
    } \
  }

#define CREER_T2(mat,n0,type) \
  { \
  mat = (type **) calloc (n0, sizeof (type *)); \
  if (mat == NULL) \
    { \
  std::cerr << "CREER_T2: Memory allocation problem\n" << endl; \
    exit(CALI_ERRALLOC); \
    } \
  }


#define CREER_T3(tab,n0,type) \
  { \
  tab = (type ***) calloc (n0, sizeof (type **)); \
  if (tab == NULL) \
    { \
  std::cerr << "CREER_T3: Memory allocation problem\n" << endl; \
    exit(CALI_ERRALLOC); \
    } \
  }



#define RECREER_T1(vect,n0,type)   \
  { \
  vect = (type *) realloc ((char *)vect,  n0*sizeof(type)); \
  if (vect == NULL) \
    { \
  std::cerr << "RECREER_T1: Memory allocation problem\n" << endl; \
    exit(CALI_ERRALLOC); \
    } \
  }


/* .......... Desallocation de `n0' emplacements de type `type' ........   */
/*           le char * dans le free evite message de lint   */
#define DETRU_T1(vect) (  \
free ((char *) vect))
#define DETRU_T2(mat,n0)  \
{ \
for (int imem=0; imem < n0; imem++) \
  free ((char *) mat[imem]); \
free ((char *) mat); \
}

////////////////////////////////////////////////
/* ............ Fonctions MIN, MAX  ........................*/
////////////////////////////////////////////////
#define MIN(a,b) ( (a)<(b) ? (a) : (b))
#define MAX(a,b) ( (a)>(b) ? (a) : (b))

#endif
