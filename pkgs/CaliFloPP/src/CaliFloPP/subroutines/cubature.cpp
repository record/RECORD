
#include <cstring>
#include <string>


#include "caliconfig.h"
#include "util.h"
#include "calierror.h"
#include "go.h"
#include "Iparam.h"
#include "Ipoly.h"
///////////////////////////////////////////////////////
// Calcule une somme (pour le nombre de lignes des sorties)
///////////////////////////////////////////////////////
int sommen(int n) {
int i,resultat;
        resultat = 1; 

        for (i=2;i<=n;i++)

        {

                resultat = resultat+i; //resultat = 1 + 2 + 3 + ...

        }

	return(resultat);
}

///////////////////////////////////////////////////////
// Calcule le nombre de lignes des sorties
///////////////////////////////////////////////////////
int calcnres(const int npoly,
	     const int ncouples, int[MAX_NPAIRS][2] /*couples[MAX_NPAIRS][2]*/) {

  int  reslignes=0;
    if (ncouples==0) {
         // tous les couples
      reslignes= sommen(npoly);
    }  else    {
      // des paires choisies de polygones
      reslignes=ncouples;
    }

    return(reslignes);
}

///////////////////////////////////////////////////////
// Launch the computations
///////////////////////////////////////////////////////

int cubature(const Ipoly *lespolys, Iparam *param,
        const int ncouples, int couples[MAX_NPAIRS][2],
        int narg[MAX_NFUNCTIONS],
        real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS],
	     char *filenamer, char openr,
	     int  reslignes,  real **sortie  )
{
    /* ----------------------------------------------------
INPUT
lespolys: object which contains the polygons
param: object which contains the parameters values
ncouples: number of the required pairs of polygons
or 0 for all
couples: indexes of the required pairs of polygons
narg: number of extra arguments of the dispersal functions
targ: extra arguments of the dispersal functions
filenamer: name of the result file or NULL
openr: open mode of the result file ('w' or 'a')
reslignes: number of lines of 'sortie' ou 0 si none required
OUTPUT
 sortie: results if reslignes >0
Each line contains, the number of the first polygon, 
the number of the second polygon, 
the areas of each polygon, the total flow of each function
RETURN
error code or 0
  ------------------------------------------------ */
    
    
    int i, erreur=0, cas=2; // par defaut, tous les couples de polygones
    /* Pour stocker les nos des polys voulus */
    int emet[MAX_NPAIRS], recoit[MAX_NPAIRS];
    
    
    /* Pour stocker les messages */
    char errmess[CHAR_MAX];
    char moi[] = "cubature";
    Boolean grid = Boolean (DEFAULT_METHOD);	// l'indicateur de la méthode voulue
    /* parametres ignorés avec cette methode */
    int  clu=0; int  dlu=0;
    int   pstepx=0; int  pstepy=0;
    int  pnr=0; int  pseed=0;
    
// Les méthodes
    methodIntegr *methode=NULL;
    
//////////////////////////////////////////////////////////
// Verif
//////////////////////////////////////////////////////////
    
    
    if (grid !=0) {
        sprintf (errmess,  " method %d not valid. Only method cubature in this version\n", grid);
        return (ecrmess (CALI_ERSYNTAXE, moi, errmess));
    }
    
    if (ncouples>MAX_NPAIRS) {
        sprintf (errmess, "the number of polygon pairs should be less than %d\n", MAX_NPAIRS);
        return (ecrmess (CALI_ERSYNTAXE, moi, errmess));
    }
    
//////////////////////////////////////////////////////////
// Set the required pairs
//////////////////////////////////////////////////////////
    if (ncouples==0) {
        cas=2; // tous les couples
    }  else    {
        cas=5; // des paires choisies de polygones
        for (i=0; i< ncouples; i++) {
            emet[i]=couples[i][0];
            recoit[i]=couples[i][1];
        }
    }
 

//////////////////////////////////////////////////////////
// Launch calculations
//////////////////////////////////////////////////////////
    
  erreur =
            suite (cas, param->pverbose, param->pinput, param->poutput, grid,
            pstepx, pstepy, pnr, pseed,
            param->preler, param->pabser, param->pmaxpts,
            param->nfunct, param->ifunct,
            lespolys->npoly, clu, dlu,
            ncouples, emet, recoit,
            lespolys->a, lespolys->area, lespolys->bary,
            lespolys->ni, lespolys->Poly,
            lespolys->numPoly, lespolys->nomPoly,
            lespolys->filenamei, filenamer, &openr, methode,
            narg, targ, reslignes, sortie);

    return(erreur);
} // fin function cubature
