/////////////////////////////////////////////////////////
#ifndef _FUNCTIONS_H
#define _FUNCTIONS_H
/////////////////////////////////////////////////////////

// Les fonctions de dispersion individuelle
extern real f1 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f2 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f3 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f4 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f5 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f6 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f7 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f8 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f9 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);
extern real f10 (const Point & p, int narg, real targ[MAX_NARGFUNCTIONS]);


#endif
