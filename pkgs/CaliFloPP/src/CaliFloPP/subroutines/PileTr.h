#ifndef _PILETR_H
#define _PILETR_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : classe de la pile         |
--------------------------------------------------*/


#include "caliinclc.h"
#include "calitypes.h"
//////////////////////////////////////////////////
class PileTr
{
private:
  int *liste;
public:
    PileTr (const int maxtri);

   ~PileTr ();
  int Get (const int i) const;
  void Ajout (const int nregions, const real * plusgrand, const int nouveau);
  void Ote (int &nregions, const real * plusgrand);

};

//////////////////////////////////////////////////
#endif
