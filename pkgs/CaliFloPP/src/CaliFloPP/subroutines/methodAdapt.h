#ifndef _methodAdapt_H
#define _methodAdapt_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : la m�thode par cubature   |
| Classe de base       :  methodIntegr             | 
--------------------------------------------------*/
/////////////////////////////////////////////////////////


#include "methodIntegr.h"
#include "Triangle.h"


/////////////////////////////////////////////////////////

class methodAdapt:public methodIntegr
{

private:
  // mode de triangulation: 1 si a partir de 0
  Boolean tzero[MAX_NFUNCTIONS];

  real reqabser[MAX_NFUNCTIONS];	// les precisions absolues demandees
  real reqreler[MAX_NFUNCTIONS];	// les precisions relatives demandees
  long int reqmaxpts[MAX_NFUNCTIONS];	// le nbre max de points demand�s


  real abser[MAX_NFUNCTIONS];	// les precisions absolues en sortie
  unsigned long nbeval[MAX_NFUNCTIONS];	// le nbre d'evaluations  en sortie
  Boolean pasatteint[MAX_NFUNCTIONS];	// True si le nbre max d'evaluations atteint
  tPolygond octo[MAX_NFUNCTIONS];	// octogone centr� en 0,0 englobantla distance max
  tPolygoni octoi[MAX_NFUNCTIONS];	// version enti�re 
  int kocto;			// le nbre de sommets

  // Private print method
  void PrintMethResults (FILE * fp);

public:
  ///////////////////////////////////////////////
  // Constructeurs
  ///////////////////////////////////////////////
  void Initialisation ();

    methodAdapt ();

    methodAdapt (const int nfunc, const int *ifunct);

    methodAdapt (const int nfunc,
		 const int *ifunct,
		 const real * reqreler, const real * reqabser,
		 const long int *reqmaxpts);



  ///////////////////////////////////////////////
  // Destructeur
  ///////////////////////////////////////////////
   ~methodAdapt ();

  ///////////////////////////////////////////////
  // M�thodes d'acc�s
  ///////////////////////////////////////////////
  void getArgu (real * reqreler, real * reqabser, long int *reqmaxpts)
  {
    int ifunc;
    for (ifunc = 0; ifunc < this->nfunct; ifunc++)
      {
	reqreler[ifunc] = this->reqreler[ifunc];
	reqabser[ifunc] = this->reqabser[ifunc];
	reqmaxpts[ifunc] = this->reqmaxpts[ifunc];
      }				// fin ifunc
  }


  ///////////////////////////////////////////////
  // Gestion des param�tres de la m�thode
  ///////////////////////////////////////////////

  int ReadArgu ();
  int VerifArgu ();

  ///////////////////////////////////////////////
  // Impression des r�sultats
  ///////////////////////////////////////////////
  void Print (const int poutput, const real areac, const real aread);

  void
    PrintFic (FILE * fp, const int noa, const int nob,
	      const real temps, const real areac, const real aread);
  ///////////////////////////////////////////////
  // Fonctions de calcul
  ///////////////////////////////////////////////

  void
    Triangulation (int numbera, int numberb,
		   tPolygoni sommeM, int k,
		   int polya, int polyb,
		   int *lpolya, int *lpolyb,
		   int maxlpoly, int &ivertce, Triangle * vertce);

  void
    Triangulation0 (int numbera, int numberb,
		    tPolygoni sommeM, int k,
		    int polya, int polyb,
		    int *lpolya, int *lpolyb,
		    int maxlpoly, int &ivertce, Triangle * vertce);


  void
    CalcR (const int poutput,
	   Function * dispf,
	   const real areac, const real aread,
	   const real mindist, const Point lepoint,
	   const int numbera, const int numberb,
	   const int ac, const int ad,
	   const int nic[MAX_VERTICES],
	   const int nid[MAX_VERTICES],
	   tPolygoni * Polyc, tPolygoni * Polyd, real &temps,
	   int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS]);

};

//////////////////////////////////////////////////
#endif
