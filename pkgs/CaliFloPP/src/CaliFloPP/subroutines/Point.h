#ifndef _POINT_H
#define _POINT_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : classe Point           |
--------------------------------------------------*/
/////////////////////////////////////////////
#include <iostream>
using std::ostream;

#include "caliinclc.h"
#include "calitypes.h"
#include "calierror.h"
#include "util.h"

/////////////////////////////////////////////
class Point
{
private:

  real x, y;
  friend ostream & operator<< (ostream &, const Point &);
public:
    Point ();
    Point (const real, const real);
    Point & operator= (const Point &);
  Point operator+ (const Point &) const;
  Point operator- (const Point &) const;
  Point operator* (const real d) const;
  Point operator/ (const real d) const;
    Point & operator += (const Point & v);
    Point & operator -= (const Point & v);
    Point & operator *= (real);
    Point & operator /= (real);
  Boolean operator == (const Point &) const;
  Boolean operator != (const Point &) const;
  real getX () const;
  real getY () const;
  real dist0 () const;		// retourne la distance de (0,0) au point.
  real angle0 () const; // angle in degree from the horizontale which go through the origine
};
/////////////////////////////////////////////
// Fonctions inline
/////////////////////////////////////////////
inline real Point::angle0  () const
{
  return(atan2(y, x)*180/M_PI);
}
////////////////////////////////////////////////////
inline real Point::dist0 () const
{
  return sqrt (x * x + y * y);
}

////////////////////////////////////////////////////
inline Point & Point::operator= (const Point & v)
{
  x = v.x;
  y = v.y;
  return *this;
}

/////////////////////////////////////////////
inline Point Point::operator+ (const Point & v) const 
{
  Point
  r (*this);
  r.x += v.x;
  r.y += v.y;
  return r;
}

/////////////////////////////////////////////////////////
inline Point Point::operator- (const Point & v) const
{
  Point
  r (*this);
  r.x -= v.x;
  r.y -= v.y;
  return r;
}

////////////////////////////////////////////////////
inline Point Point::operator* (const real d) const
{
  Point
  r (*this);
  r.x *= d;
  r.y *= d;
  return r;
}

////////////////////////////////////////////////////
inline Point Point::operator/ (const real d) const
{
  char 
    moi[] = "Point::operator/";

/* Pour stocker les messages */
  char
    errmess[CHAR_MAX];

  const real
    Tol = DBL_EPSILON;		//  Pour les comparaisons de r��ls
  if (fabs (d - 0.0) < Tol)
    {
      sprintf (errmess, "Internal error: division by zero");
      // Fatal error
      ecrmess (CALI_ERINTERNAL, moi, errmess, True);
    }
  Point
  r (*this);
  r.x /= d;
  r.y /= d;
  return r;
}

////////////////////////////////////////////////////
inline Point & Point::operator += (const Point & v)
{
  x += v.x;
  y += v.y;
  return (*this);
}

///////////////////////////////////////////////////
inline Point & Point::operator -= (const Point & v)
{
  x -= v.x;
  y -= v.y;
  return (*this);
}

///////////////////////////////////////////////////
inline Point & Point::operator *= (real d)
{
  x *= d;
  y *= d;
  return (*this);
}

///////////////////////////////////////////////////
inline Point & Point::operator /= (real d)
{
  char 
    moi[] = "Point::operator/=";

/* Pour stocker les messages */
  char
    errmess[CHAR_MAX];

  const real
    Tol = DBL_EPSILON;		//  Pour les comparaisons de r��ls
  if (fabs (d - 0.0) < Tol)
    {
      sprintf (errmess, "Internal error: division by zero");
      // Fatal error
      ecrmess (CALI_ERINTERNAL, moi, errmess, True);
    }
  x /= d;
  y /= d;
  return (*this);
}

///////////////////////////////////////////////
inline Boolean Point::operator== (const Point & v) const
{
  const real
    Tol = DBL_EPSILON;		//  Pour les comparaisons de r��ls
  int
    b = (fabs (x - v.x) < Tol) && (fabs (y - v.y) < Tol);
  return (Boolean) b;
}

////////////////////////////////////////////////////
inline Boolean Point::operator!= (const Point & v) const
{
  const real
    Tol = DBL_EPSILON;		//  Pour les comparaisons de r��ls
  int
    b = (fabs (x - v.x) >= Tol) || (fabs (y - v.y) >= Tol);
  return (Boolean) b;
}

/////////////////////////////////////////////////////
inline ostream & operator<< (ostream & os, const Point & p)
{
  os << " (" << p.x << "," << p.y << ") ";
  return os;
}

///////////////////////////////////////////////

#endif
