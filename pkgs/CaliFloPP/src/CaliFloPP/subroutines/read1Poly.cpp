/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 15 Fev 2006               |
| Role                 : lit les coordonn�es d'un  |
|                        polygone                  |
--------------------------------------------------*/

///////////////////////////////////////////////////////
#include "caliinclc.h"
#include "caliconfig.h"
#include "calierror.h"
#include "calimacros.h"
#include "read1Poly.h"
#include "util.h"
#include <iostream>


///////////////////////////////////////////////////////
/* Lecture d'un polygone
Arguments d'entr�e:
fp: pointeur sur le fichier en entr�e
Arguments de sortie:
ID: identificateur du polygone
nsom: nombre de sommets
lesx: les abscisses
lesy: les ordonn�es
Retour:
0 si pas d'erreur
et une valeur diff�rente sinon.
*/
///////////////////////////////////////////////////////
// Format 1:
// npoly
// identificateur, suivi par les x
// m�me identificateur, suivi par les y
///////////////////////////////////////////////////////

int
read1Poly (FILE * fp, char *pdelim,
	   int &ID, int &nsom, real lesx[], real lesy[])
{
  char lu[MAX_LINE_POLY];
  char *p;
  int idim, iget, nsompred=0, idpred=0;
  real lucoord;
  char moi[] = "read1Poly";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];



  for (idim = 0; idim < DIM; idim++)
    {
      iget = atoi (fgets (lu, MAX_LINE_POLY, fp));


      // Detecter les lignes vides et fin de fichier
      if ((iget == 0) || (strpbrk (lu, "0123456789") == NULL))
	{
	  sprintf (errmess, "premature end of file\n");
	  return (ecrmess (CALI_ERPOLY1, moi, errmess));
	}

      p = strtok (lu, pdelim);
      ID = atoi (p);

      if (idim == 0)
	idpred = ID;
      else
	{
	  if (ID != idpred)
	    {
	      sprintf (errmess,
		       "premature end of file\npoly %d not finished\n",
		       idpred);
	      return (ecrmess (CALI_ERPOLY2, moi, errmess));
	    }			// fin ( ID != idpred)
	}			// fin else


      nsom = 0;
      while ((p = strtok (NULL, pdelim)) != NULL)
	{
	  lucoord = atof (p);

	  if (lucoord <= -real (INT_MAX))
	    {
	      sprintf (errmess,
		       "polygon ident %d has too small coordinates (%g <= %d)\n",
		       ID, lucoord, INT_MAX);
	      return (ecrmess (CALI_ERPOLY8, moi, errmess));
	    }

	  //      if (lucoord < 0) {
//         sprintf (errmess,
//                  "polygon ident %d has negative coordinates (%g)\n",
//                  ID, lucoord);
//         return (ecrmess(CALI_ERPOLY8, moi, errmess));
//       }

	  if (nsom > MAX_VERTICES)
	    {
	      sprintf (errmess,
		       "polygon ident %d has %d vertices  (maximum MAX_VERTICES= %d)\n",
		       ID, nsom, MAX_VERTICES);
	      return (ecrmess (CALI_ERPOLY7, moi, errmess));
	    }

	  if (idim == 0)
	    lesx[nsom] = lucoord;
	  else
	    lesy[nsom] = lucoord;
	  nsom++;

	}			// fin while

      if (idim == 0)
	nsompred = nsom;
      else
	{
	  if (nsompred != nsom)
	    {
	      sprintf (errmess,
		       "polygon ident %d has %d x-vertices and %d y-vertices.\n",
		       ID, nsompred, nsom);
	      return (ecrmess (CALI_ERPOLY3, moi, errmess));
	    }			// fin if ( nsompred != nsom)
	}			// fin else
    }				// fin idim


  return (OK);
}				// fin read1Poly

///////////////////////////////////////////////////////
// Format 2:
// npoly
// identificateur, nom, nsom
// les x
// les y
///////////////////////////////////////////////////////
int
read2Poly (FILE * fp, char *pdelim, int &ID, char *nom,
	   int &nsom, real lesx[], real lesy[])
{
  char lu[MAX_LINE_POLY];
  char *p;
  int idim, isom, iget;
  real lucoord;
  char moi[] = "read2Poly";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];

  iget = atoi (fgets (lu, MAX_LINE_POLY, fp));


  // Detecter les lignes vides et fin de fichier
  // if (   (iget == EOF) 
  // ne marche pas si lignes vides ou blanches
  // en fin de fichier, donc, on recherche s'il y a des chiffres
  // sur la ligne
  /* 23/08/2010: oter ce qui suit: un identificateur peut etre nul
  if ((iget == 0) || (strpbrk (lu, "0123456789") == NULL))
    {
      sprintf (errmess, "premature end of file\n");
      return (ecrmess (CALI_ERPOLY1, moi, errmess));
    }
  */

  // Lecture du no de poly, de son nom, et nbre de sommets
  // Lire avec un fgets, car le nom peut �tre sur pls mots
  p = strtok (lu, pdelim);
  if (p == NULL)
    {
      sprintf (errmess, "premature end of file\n");
      return (ecrmess (CALI_ERPOLY1, moi, errmess));
    }
  ID = atoi (p);
  p = strtok (NULL, pdelim);

  if (p == NULL)
    {
      sprintf (errmess, "Bad format or delimitor at line %s\n", lu);
      return (ecrmess (CALI_ERPOLY9, moi, errmess));
    }
  strcpy (nom, p);


  p = strtok (NULL, pdelim);
  nsom = atoi (p);
  if (nsom > MAX_VERTICES)
    {
      sprintf (errmess,
	       "polygon ident %d has %d vertices  (maximum MAX_VERTICES=%d)\n",
	       ID, nsom, MAX_VERTICES);
      return (ecrmess (CALI_ERPOLY7, moi, errmess));
    }


  // Lecture des coordonn�es:

  for (idim = 0; idim < DIM; idim++)
    {
      iget = atoi (fgets (lu, MAX_LINE_POLY, fp));
      // Detecter les lignes vides et fin de fichier
  /* 23/08/2010: oter ce qui suit: un identificateur peut etre nul
      if ((iget == 0) || (strpbrk (lu, "0123456789") == NULL))
	{
	 sprintf (errmess,
		   "premature end of file\npoly %d not finished\n", ID);
	  return (ecrmess (CALI_ERPOLY2, moi, errmess));
	}
  */

      for (isom = 0; isom < nsom; isom++)
	{
	  if (isom == 0)
	    p = strtok (lu, pdelim);
	  else
	    p = strtok (NULL, pdelim);
	  if (p == NULL)
	    {
		 sprintf (errmess,
		       "polygon ident %d should have %d vertices", ID, nsom);
	      return (ecrmess (CALI_ERPOLY4, moi, errmess));
	    }
	  lucoord = atof (p);

	  if (lucoord <= -real (INT_MAX))
	    {
	      sprintf (errmess,
		       "polygon ident %d has too small coordinates (%g <= %d)\n",
		       ID, lucoord, INT_MAX);
	      return (ecrmess (CALI_ERPOLY8, moi, errmess));
	    }

	  if (idim == 0)
	    lesx[isom] = lucoord;
	  else
	    lesy[isom] = lucoord;
	}			// fin isom

    }				// fin idim
  return (OK);
}				// fin read2Poly
