/*--------------- COPYRIGHT ------------------------
| INRA - Unité MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 15 Jan 2006               |
| Role                 : programmes utilitaires    |
--------------------------------------------------*/
/////////////////////////////////////////////////////////
#include <iostream>

using std::cerr;
using std::endl;

#include "caliinclc.h"
#include "util.h"
#include "calierror.h"
#include "calimacros.h"
#include "caliconfig.h"
#include "calidefs.h"



/////////////////////////////////////////////////////////////////
// Fonction de comparaison de reels  à Tolerance pres
/////////////////////////////////////////////////////////////////
// voir aussi  calidefs.h: TOL = REAL_MIN

Boolean
realequal (real a, real b, real Tolerance)
{
  if (fabs (a - b) < real (Tolerance))
    return True;
  else
    return False;
}

/////////////////////////////////////////////////////////////////
// Ecrire les messages d''erreur
// code: code négatif dont les valeurs valides sont dans erreur.h
//  mess: le texte à écrire
/////////////////////////////////////////////////////////////////


int
ecrmess (int code, char *moi, char *mess, Boolean fatal)
{
  if ((OUTPUT_WARNING != NOTHING) && (fatal == True))
    {
      cerr << endl;
      if (code > 0)
	{
	  cerr << "Error " << code;
	}
      if (strlen (moi) > 0)
	{
	  cerr << "(" << moi << "): ";
	}
      cerr << mess << endl;
    }

  if (fatal == True)
    exit (code);
  else
    return (code);
}				/* fin ecrmess */



////////////////////////////////////////////////////
