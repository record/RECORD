///////////////////////////////////////////////////////
// Lecture du fichier des polygones
///////////////////////////////////////////////////////
#include "caliinclc.h"
#include "calidefs.h"
#include "util.h"
#include "calierror.h"
#include "readPoly.h"

#include "Iparam.h"
#include "Ipoly.h"



int  litpoly( FILE *fpi, Iparam* param, const Ipoly* lespolys)
{
    /*   ------------------------------------------------
INPUT
fpi: pointer to the polygon file
param: object which contains the parameters values
INPUT/OUTPUT
lespolys: object which contains the polygons
(input: contains npoly; output: all the slots are filled in)
RETURN
0 or >0 if error
  ------------------------------------------------ */
    
    /* Pour stocker les messages */
    char errmess[CHAR_MAX];
    char moi[] = "litpoly";
    
    
    
//////////////////////////////////////////////////////////
// Lecture des polygones
//////////////////////////////////////////////////////////
    Boolean calcSurf = False;	// on ne veut pas que les surfaces
    // (calcSurf n'est vrai que pour le programme surface)
    
    tPolygond **Polyd = NULL;	// on ne veut pas les polys corrigés
    // (Polyd n'est pas NULL que pour le programme polysimplif)
    
    int npolybons, erreur=0;
    
    
    erreur = ReadPoly (fpi, param->pverbose, calcSurf, param->pinput, param->pdelim,
            lespolys->npoly, lespolys->nomPoly, lespolys->numPoly, npolybons,
            lespolys->a, lespolys->area, lespolys->ni, lespolys->Poly, Polyd, lespolys->bary);
    
    
    if ((erreur != OK) && (erreur != CALI_WARNPOLY))
    {
        return (erreur);		// fatal error
    }
    
    if (npolybons == 0)
    {
        sprintf (errmess, "All polygons are invalid\n");
        return (ecrmess (CALI_ERPOLY7, moi, errmess));
    }
    if ((param->poutput != NOTHING) && (param->poutput != LIGHT))
    {
        printf ("\nNumber of polygons: %d\n-------------------\n", lespolys->npoly);
    }
    
    return (erreur);
}

