/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : g�re la m�thode par       |
|                        cubature                  |
--------------------------------------------------*/

////////////////////////////////////////////
#include "caliinclc.h"
#include "calimacros.h"
#include "methodAdapt.h"
#include "caliconfig.h"
#include "califunc.h"
#include "geom.h"
#include "intersection.h"
#include "zoneintegration.h"
#include "Adapt.h"
#include "Point.h"
#include "Triangle.h"
#include "Vector.h"
#include <time.h> 



///////////////////////////////////////////////
// GLOBALS
///////////////////////////////////////////////
Function dispf;			// la fonction � int�grer
tPolygoni *polycg, *polydg;	// les polys
int *nicg, *nidg;		// nbres de sommets courants des polys
int nargg;
real targg[MAX_NARGFUNCTIONS]; // extra argu des fonctions de dispersion

////////////////////////////////////////
// Detruire une liste chaine tdVertex
// Ajout 6/1/2014
////////////////////////////////////////
void
DetrutdVertex (tdVertex vertices)
{

  tdVertex v, v1;
  if (vertices == vertices->next) {
    FREE(vertices);
    return;
  }

  v=vertices;
  do
    {
      v1=v;
      v=v->next;
    }
  while ( v != vertices);
  do
    {
      v=v1->prev;
      FREE(v1);
      v1=v;
    }
  while ( v != vertices);
  FREE(vertices);
}

///////////////////////////////////////////////////////
// Integrande: sa dcl doit �tre celle d�finie dans Integrand.h
///////////////////////////////////////////////////////

void
f_ (const Point & p, int /*nfun*/, int polya, int polyb, Vector funvls)
{

  tPolygond D;
  Boolean z;
  real area;
  tdVertex pointer, ipointer;
  int c;
  tdVertex intersection;
  real retour;


  for (c = 0; c < nidg[polyb]; c++)
    {
      D[c][XX] = polydg[polyb][c][XX] - p.getX ();
      D[c][YY] = polydg[polyb][c][YY] - p.getY ();
    }

  NEW (intersection, tdsVertex);
  intersection->next = intersection->prev = intersection;
  //  Initialisation du 1ier sommet
  intersection->v[XX] = intersection->v[YY] = 0;
  //AB: rajout du 18/06/2007
  z = ConvexInclus (polycg[polya], nicg[polya], D, nidg[polyb], intersection);
  if (z == False)

    z = ConvexIntersect (polycg[polya], nicg[polya],
			 D, nidg[polyb], intersection);


  if (z == True)
    {
      area = polygon_area_2 (intersection) / 2;
      pointer = intersection->next;
      do
	{
	  ipointer = pointer;
	  pointer = ipointer->next;
	  FREE (ipointer);
	}
      while (pointer != intersection);
      // Diviser par SCALE**4 car produit d'aires
      retour = (area / (SCALE * SCALE * SCALE * SCALE)) * (*dispf) (p, nargg, targg);
    }
  else
    {
      //      area = 0;
      retour = 0.0;
    }

  FREE (intersection);

// Dans cette version nfun=1:
// une seule fonction individuelle a la fois
  funvls[0] = retour;
}


///////////////////////////////////////////////
// Constructeurs
///////////////////////////////////////////////

///////////////////////////////////////////////
// Programme appel� par les constructeurs
///////////////////////////////////////////////

void
methodAdapt::Initialisation ()
{
  real dmax;
  int i, ifunc;


  this->tzero[0] = TZ1;
  this->tzero[1] = TZ2;
  this->tzero[2] = TZ3;
  this->tzero[3] = TZ4;
  this->tzero[4] = TZ5;
  this->tzero[5] = TZ6;
  this->tzero[6] = TZ7;
  this->tzero[7] = TZ8;
  this->tzero[8] = TZ9;
  this->tzero[9] = TZ10;

  for (ifunc = 0; ifunc < MAX_NFUNCTIONS; ifunc++)
    {
      dmax = this->dzero[ifunc];

      if (dmax > 0)
	{

	  /* Calculer un octogone centr� en 0,0 
	     qui englobe le cercle de rayon dmax
	   */

	  /* Sommets de l'octogone, anticlockwise */
	  i = 0;
	  this->octo[ifunc][i++][XX] = cos (M_PI / 4);
	  this->octo[ifunc][i++][XX] = 0.0;
	  this->octo[ifunc][i++][XX] = cos (3 * M_PI / 4);
	  this->octo[ifunc][i++][XX] = -1.0;
	  this->octo[ifunc][i++][XX] = cos (3 * M_PI / 4);
	  this->octo[ifunc][i++][XX] = 0.0;
	  this->octo[ifunc][i++][XX] = cos (M_PI / 4);
	  this->octo[ifunc][i++][XX] = 1.0;

	  i = 0;
	  this->octo[ifunc][i++][YY] = sin (M_PI / 4);
	  this->octo[ifunc][i++][YY] = 1.0;
	  this->octo[ifunc][i++][YY] = sin (M_PI / 4);
	  this->octo[ifunc][i++][YY] = 0.0;
	  this->octo[ifunc][i++][YY] = sin (-M_PI / 4);
	  this->octo[ifunc][i++][YY] = -1.0;
	  this->octo[ifunc][i++][YY] = sin (-M_PI / 4);
	  this->octo[ifunc][i++][YY] = 0.0;

	  for (i = 0; i < 8; i++)
	    {
	      this->octo[ifunc][i][XX] *= (SCALE * (dmax / cos (M_PI / 8)));
	      this->octo[ifunc][i][YY] *= (SCALE * (dmax / cos (M_PI / 8)));
	    }
	  /* Fermer le poly */
	  this->octo[ifunc][8][XX] = this->octo[ifunc][0][XX];
	  this->octo[ifunc][8][YY] = this->octo[ifunc][0][YY];
	  this->kocto = 9;



	  /* La version polygoni */
	  for (i = 0; i < this->kocto; i++)
	    {
	      this->octoi[ifunc][i][XX] = int (this->octo[ifunc][i][XX]);
	      this->octoi[ifunc][i][YY] = int (this->octo[ifunc][i][YY]);
	    }

	}			// fin dmax
    }				// fin ifunc
}				// fin Initialisation

////////////////////////////////////////////////////////////

methodAdapt::methodAdapt ():methodIntegr ()
{
}

////////////////////////////////////////////////////////////
methodAdapt::methodAdapt (const int nfunc, const int *ifunct):
methodIntegr (nfunc, ifunct)
{
  int  i;
  this->Initialisation ();

  for (i = 0; i < this->nfunct; i++)
    {
      this->reqreler[i] = DEFAULT_REL_ERR;
      this->reqabser[i] = DEFAULT_ABS_ERR;
      this->reqmaxpts[i] = 0;	// calcul� selon le nbre de triangles courant
    }				// fin ifunc

}				// fin constructor

////////////////////////////////////////////////////////////
methodAdapt::methodAdapt (const int nfunc,
			  const int *ifunct,
			  const real * reqreler, const real * reqabser,
			  const long int *maxpts):
methodIntegr (nfunc, ifunct)
{

  int  i;
  this->Initialisation ();

  for (i = 0; i < this->nfunct; i++)
    {
      this->reqreler[i] = reqreler[i];
      this->reqabser[i] = reqabser[i];
      this->reqmaxpts[i] = maxpts[i];

    }				// fin ifunc

}



///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////

methodAdapt::~methodAdapt ()
{
  // RAF
}


  ///////////////////////////////////////////////
  // Gestion des param�tres de la m�thode
  ///////////////////////////////////////////////
int
methodAdapt::VerifArgu ()
{
  // Verif des indices de fonctions
  return (this->VerifFunct ());
}

int
methodAdapt::ReadArgu ()
{

  char change[3];
  int ifunc;
  char moi[] = "methodAdapt::ReadArgu";
  char errlect[]="Error in reading";

  for (ifunc = 0; ifunc < this->nfunct; ifunc++)
    {

      printf
	("Relative precision for function %d: %g; do you want to change it ? (y/n)",
	 this->ifunct[ifunc], this->reqreler[ifunc]);
      if (scanf ("%1s", change)<1) {
	 return (ecrmess (CALI_ERLECT, moi, errlect, True));
      }
      if (change[0] == 'y')
	{
	  printf (" type in the new precision:");
	  if (scanf ("%lf", &(this->reqreler[ifunc]))<1) {
	    return (ecrmess (CALI_ERLECT, moi, errlect, True));
	  }
	}

      printf
	("Absolute precision for function %d: %g; do you want to change it ? (y/n)",
	 this->ifunct[ifunc], this->reqabser[ifunc]);
      if (scanf ("%1s", change)<1) {
	return (ecrmess (CALI_ERLECT, moi, errlect, True));
      }
      if (change[0] == 'y')
	{
	  printf (" type in the new precision:");
	  if (scanf ("%lf", &(this->reqabser[ifunc]))<1) {
	    return (ecrmess (CALI_ERLECT, moi, errlect, True));
	  }
	}


      printf
	("Maximal number of evaluation points for function %d is automatically calculated; do you want to set it ? (y/n)",
	 this->ifunct[ifunc]);
      if (scanf ("%1s", change)<1) {
	return (ecrmess (CALI_ERLECT, moi, errlect, True));
      }
      if (change[0] == 'y')
	{
	  printf (" type in the new value:");
	  if (scanf ("%ld", &(this->reqmaxpts[ifunc]))<1) {
	    return (ecrmess (CALI_ERLECT, moi, errlect, True));
	  }
	}
    }				// fin ifunc

  return (0);
}				// fin ReadArgu

  ///////////////////////////////////////////////
  // Impression des r�sultats
  ///////////////////////////////////////////////

void
methodAdapt::Print (const int poutput, const real areac, const real aread)
{
  real amplip;
  real areacc, areadd;

  int ifunc;


  areacc = areac / (SCALE * SCALE);
  areadd = aread / (SCALE * SCALE);

  if ((areacc <= 0) || (areadd <= 0))
    {
      printf ("\n Careful:\n");
      if (areacc <= 0)
	printf ("   area of polygon 1 is null\n");
      if (areadd <= 0)
	printf ("   area of polygon 2 is null\n");
    }
  else
    {
      for (ifunc = 0; ifunc < this->nfunct; ifunc++)
	{
	  // l'amplitude de l'IC est calcul�e � partir
	  // de la precision absolue en sortie
	  // (HM,29/1/2007):
	  // Borner la borne inf. � z�ro
	  amplip = this->abser[ifunc];

	  printf ("\nIntegrated flow for function %d:\n",
		  this->ifunct[ifunc]);
	  printf (" mean: %g mean/area1: %g mean/area2: %g\n",
		  this->rp[ifunc], (this->rp[ifunc] / areacc),
		  (this->rp[ifunc] / areadd));
	  fflush (stdout);

	  if ((poutput == ALL) && (this->nbeval[ifunc] > 0))
	    {
	      /* Mettre un ast�ristique quand la convergence
	         n'a pas �t� atteinte */
	      if (this->pasatteint[ifunc] == True)
		printf ("*");

	      printf
		(" absolute error: %g relative error: %g\n confidence interval: [%g, %g]\n",
		 this->abser[ifunc], (this->abser[ifunc] / this->rp[ifunc]),
		 (this->rp[ifunc] - amplip), (this->rp[ifunc] + amplip));
	      if (poutput == ALL)
		printf (" nb. evaluations: %ld\n", this->nbeval[ifunc]);
	    }
	}			// fin ifunc

      if (poutput == ALL)
	printf ("\narea1: %g area2: %g \n", areacc, areadd);
      else
	printf ("\n");
      fflush (stdout);
    }				// fin else

}				// fin Print


/////////////////////////////////////////////
// Impression des r�sultats sp�cifiques � la m�thode
// sur le fichier des r�sultats
/////////////////////////////////////////////
void
methodAdapt::PrintMethResults (FILE * fp)
{
  int ifunc, nombreval;
  real amplip;

  for (ifunc = 0; ifunc < this->nfunct; ifunc++)
    {
      // l'amplitude de l'IC est calcul�e � partir
      // de la precision absolue en sortie
      amplip = this->abser[ifunc];
      nombreval = int (this->nbeval[ifunc]);
      fprintf (fp, "\t%g\t%g\t%g\t%g\t%d",
	       this->rp[ifunc],
	       (this->rp[ifunc] - amplip),
	       (this->rp[ifunc] + amplip), this->abser[ifunc], nombreval);
    }				// fin ifunc
}				// fin PrintMethResults

/////////////////////////////////////////////
// Impression d'une ligne 
// sur le fichier des r�sultats
/////////////////////////////////////////////

void
methodAdapt::PrintFic (FILE * fp, const int noa, const int nob,
		       const real temps, const real areac,
		       const real aread)
{
  // Ecrire les infos communes aux 2 m�thodes:
  this->PrintFicOut (fp, noa, nob, areac, aread);

  // Ecrire ce qui est sp�cifique � la m�thode
  if ((OUTPUT_FILE_FORMAT == ALL) || (OUTPUT_FILE_FORMAT == LIGHT))
    {
      this->PrintMethResults (fp);
      if (OUTPUT_FILE_FORMAT == ALL)
	fprintf (fp, "\t%.f", temps);
    }				// fin format ALL ou LIGHT


  fprintf (fp, "\n");
  if (noa != nob)
    {
      this->PrintFicOut (fp, nob, noa, aread, areac);
      // On repete les valeurs specifiques � la methode:
      if ((OUTPUT_FILE_FORMAT == ALL) || (OUTPUT_FILE_FORMAT == LIGHT))
	{
	  this->PrintMethResults (fp);
	  if (OUTPUT_FILE_FORMAT == ALL)
	    fprintf (fp, "\t%.f", temps);
	}			// fin format ALL ou LIGHT
      fprintf (fp, "\n");
    }
  fflush (fp);

}				// fin  PrintFic




//////////////////////////////////////////////////////////////
// Triangulation a partir d'un sommet quelconque
//////////////////////////////////////////////////////////////
void
methodAdapt::Triangulation (int numbera, int numberb,
			    tPolygoni sommeM, int k,
			    int polya, int polyb,
			    int *lpolya, int *lpolyb,
			    int maxlpoly, int &ivertce, Triangle * vertce)
{
  int i;
  tPointi ip1, ip2, ip3;
  char moi[] = "methodAdapt::Triangulation";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];


  Point p1 (sommeM[0][XX], sommeM[0][YY]);
  ip1[XX] = sommeM[0][XX];
  ip1[YY] = sommeM[0][YY];



  for (i = 1; i < k - 1; i++)
    {
      Point p2 (sommeM[i][XX], sommeM[i][YY]);
      ip2[XX] = sommeM[i][XX];
      ip2[YY] = sommeM[i][YY];
      Point p3 (sommeM[i + 1][XX], sommeM[i + 1][YY]);
      ip3[XX] = sommeM[i + 1][XX];
      ip3[YY] = sommeM[i + 1][YY];

      if ((p1 == p2) || (p1 == p3) || (p2 == p3))
	{
	  // des points confondus
	  continue;
	}
      /* dcutri n'accepte pas les triangles d'aire nulle */
      if (AireNulle (ip1, ip2, ip3) == True)
	{
	  //      printf("Tion AIRE NULLE\n");
	  continue;
	}

      //      printf("Tion AIRE %d\n", Area2i(ip1, ip2, ip3));

      /* Stocker les nos des sous-polys peres */
      if (ivertce >= maxlpoly)
	{
	  sprintf (errmess,
		   "Maximal number of regions reached on polys (%d, %d).\n",
		   numbera, numberb);
	  ecrmess (CALI_MAXSREGIONS, moi, errmess, True);
	}


      lpolya[ivertce] = polya;
      lpolyb[ivertce] = polyb;


      Triangle Ti (p1, p3, p2);
      vertce[ivertce] = Ti;

      ivertce++;
    }				// fin i

}				// fin Triangulation 



//////////////////////////////////////////////////////////////
// Triangulation quand un z�ro est inclus dans la
// somme de M.: d�coupage de la somme de M. en triangles
// en prenant 0,0 comme origine  (HM,AB, feb 2006)
//////////////////////////////////////////////////////////////

void
methodAdapt::Triangulation0 (int numbera, int numberb,
			     tPolygoni sommeM, int k,
			     int polya, int polyb,
			     int *lpolya, int *lpolyb,
			     int maxlpoly, int &ivertce, Triangle * vertce)
{
  int i, i2, i3;
  tPointi ip1, ip2, ip3;
  char moi[] = "methodAdapt::Triangulation0";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];



  Point p1 (0.0, 0.0);
  ip1[XX] = 0;
  ip1[YY] = 0;

  for (i = 0; i < k; i++)
    {
      i2 = i;
      if (i == (k - 1))
	{
	  i3 = 0;		// pour consid�rer le dernier triangle
	}
      else
	{
	  i3 = i + 1;
	}
      Point p2 (sommeM[i2][XX], sommeM[i2][YY]);
      ip2[XX] = sommeM[i2][XX];
      ip2[YY] = sommeM[i2][YY];
      Point p3 (sommeM[i3][XX], sommeM[i3][YY]);
      ip3[XX] = sommeM[i3][XX];
      ip3[YY] = sommeM[i3][YY];


      if ((p1 == p2) || (p1 == p3) || (p2 == p3))
	{
	  continue;
	}
      /* dcutri n'accepte pas les triangles d'aire nulle */
      if (AireNulle (ip1, ip2, ip3) == True)
	{
	  continue;
	}


      /* Stocker les nos des sous-polys peres */
      if (ivertce >= maxlpoly)
	{
	  sprintf (errmess,
		   "Maximal number of regions reached on polys (%d, %d).\n",
		   numbera, numberb);
	  ecrmess (CALI_MAXSREGIONS, moi, errmess, True);
	}


      lpolya[ivertce] = polya;
      lpolyb[ivertce] = polyb;

      Triangle Ti (p1, p3, p2);
      vertce[ivertce] = Ti;
      ivertce++;

    }				// fin i

}				// fin Triangulation0 

//////////////////////////////////////////////////////////
// Conversion d'une intersection en Polygoni dans socto
// Renvoie le nbre de sommets
//////////////////////////////////////////////////////////
int
Intersection2Polygoni (tdVertex intersection, tPolygoni socto)
{


  tdVertex p;
  int k = 0;


// Arrondir � l'entier sup�rieur 
  socto[k][XX] = int (ceil (intersection->v[0]));
  socto[k][YY] = int (ceil (intersection->v[1]));

  p = intersection->next;

  do
    {
      k++;
      socto[k][XX] = int (ceil (p->v[0]));
      socto[k][YY] = int (ceil (p->v[1]));
      p = p->next;

    }
  while (p->next != intersection);
  return (k + 1);

}				// fin Intersection2Polygoni


/////////////////////////////////////////////
// Lancement des calculs
/////////////////////////////////////////////


void
methodAdapt::CalcR (const int poutput,
		    Function * dispfunct,
		    const real areac, const real aread,
		    const real mindist, const Point lepoint,
		    const int numbera, const int numberb,
		    const int ac, const int ad,
		    const int nic[MAX_VERTICES],
		    const int nid[MAX_VERTICES],
		    tPolygoni * Polyc, tPolygoni * Polyd, real &temps,
		    int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS])
{
  int i, j, k, ifunc;

  // Pour le calcul de l'intersection de la somme M avec l'octogone
  tdVertex interocto;
  Boolean z;
  tPolygoni sommeM;


  // Travail: 
  int ii;
  tPolygoni socto;		// pour stocker le polygone o� on int�gre
  tPointd unpoint;
  tPointd zero;
  char moi[] = "methodAdapt::CalcR";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];


// integre sera faux quand la sommeM et l'octogone sont disjoints
// ou la distance entre polys au-dela des seuils,
// i.e quand on n'a pas � int�grer
  Boolean integre;


  int ivertce = 0;		// nbre de triangles
  Triangle *stvertce;

  int *lpolya, *lpolyb;
  // Mesure du temps
  time_t now, debut;


  /* Les sommes de M ne peuvent pas etre triangul�es
     en plus de maxlpoly triangles. */
  int maxlpoly = MAX_SREGIONS;
  CREER_T1 (lpolya, maxlpoly, int);
  CREER_T1 (lpolyb, maxlpoly, int);
  CREER_T1 (stvertce, maxlpoly, Triangle);
  zero[XX] = 0.0;
  zero[YY] = 0.0;

  time(&debut); 


  /* Mettre les polys courants dans les va globales */
  nicg = (int *) nic;
  nidg = (int *) nid;
  polycg = Polyc;
  polydg = Polyd;

  /* Boucle sur les fonctions de dispersion */
  for (ifunc = 0; ifunc < this->nfunct; ifunc++)
    {

      /* Initialisation des structures relatives aux
         fonctions de dispersion */
      this->abser[ifunc] = 0.0;
      this->rp[ifunc] = 0.0;
      this->nbeval[ifunc] = 0;

      /* pasatteint:
         deviendra True si la convergence n'est pas
         atteinte sur un des couples de sous-polys */
      this->pasatteint[ifunc] = False;
      /* Mettre dans un global, la fonction de dispersion 
         utilis�e par "f" */
      dispf = dispfunct[this->ifunct[ifunc] - 1];
      nargg= narg[ifunc];
      for (ii=0; ii< nargg; ii++) {
	targg[ii]= targ[ifunc][ii];
      }

      /* Ne faire l'int�gration que si necessaire */
      /* La fonction s'annule-t-elle? */
      if ((this->dzero[this->ifunct[ifunc] - 1] > 0.0) &&
	  (mindist >= this->dzero[this->ifunct[ifunc] - 1]))
	{
	  /* Les points les plus proches sont �loign�s de plus
	     de la distance � partir de laquelle la fonction 
	     s'annule */
	  integre = False;
	  if (poutput == ALL)
	    printf
	      ("Minimal distance between polygons %d,%d=%g (>=%g):\n   function %d set to zero.\n",
	       numbera, numberb,
	       mindist, this->dzero[this->ifunct[ifunc] - 1],
	       this->ifunct[ifunc]);
	}
      else
	{
	  /* Calcule-t-on   entre les barycentres? */
	  if ((this->dpoint[this->ifunct[ifunc] - 1] > 0.0) &&
	      (mindist >= this->dpoint[this->ifunct[ifunc] - 1]))
	    {
	      /* on calcule entre les barycentres */

	      if (poutput == ALL)
		printf
		  ("Minimal distance between polygons %d,%d=%g (>=%g):\n   fonction %d calculated between centroids.\n",
		   numbera, numberb,
		   mindist, this->dpoint[this->ifunct[ifunc] - 1],
		   this->ifunct[ifunc]);
	      /* Les points les plus proches sont �loign�s de plus
	         de la distance � partir de laquelle on ne calcule
	         la fonction qu'en un point */
	      integre = False;
	      this->rp[ifunc] +=
		((areac / (SCALE * SCALE)) *
		 (aread / (SCALE * SCALE)) *
		 (*dispfunct[this->ifunct[ifunc] - 1]) (lepoint,
 nargg, targg));
	    }			// fin (mindist >= dpoint[ifunc] )

	  else
	    {
	      /* Integration */
	      ivertce = 0;	// indice de triangle

	      /* Boucle sur les couples de sous-polys */
	      for (i = 0; i < ac; i++)
		for (j = 0; j < ad; j++)
		  {



		    integre = True;	// deviendra false si dispersion nulle


		    k =
		      SommeMinkowski (Polyc[i], nic[i], Polyd[j], nid[j],
				      sommeM);



		    if (this->dzero[this->ifunct[ifunc] - 1] <= 0)
		      {
//  La dispersion est suppos�e aller � l'infini
			if (this->tzero[this->ifunct[ifunc] - 1] == True)
			  {
			    // Triangulation a partir de 0 si
			    // l'origine est incluse dans la surface � trianguler
// Regarder si l'origine est incluse dans la surface � trianguler
			    z = InPolyConvex (zero, sommeM, k);

			    if (z == True)
			      {
				// Le zero est dans la somme de M
				// triangulation a partir de zero:
				// k est le nombre de sommets de la sommeM
				Triangulation0 (numbera, numberb,
						sommeM, k,
						i, j,
						lpolya, lpolyb,
						maxlpoly, ivertce, stvertce);
			      }
			    else
			      {
				// triangulation a partir de 
				//n'importe quel sommet
				Triangulation (numbera, numberb, sommeM, k,
					       i, j,
					       lpolya, lpolyb,
					       maxlpoly, ivertce, stvertce);
			      }
			  }	// fin tzero
			else
			  Triangulation (numbera, numberb,
					 sommeM, k,
					 i, j,
					 lpolya, lpolyb,
					 maxlpoly, ivertce, stvertce);

		      }		// fin (this->dzero[ifunc] <=0)
		    else
		      {
			/* Dispersion limit�e � dz�ro
			   Calculer l'intersection de la somme M. avec l'octogone:
			   pas besoin de calculer au-del� */
			NEW (interocto, tdsVertex);
			interocto->next = interocto->prev = interocto;
			//  Initialisation du 1ier sommet 
			interocto->v[XX] = interocto->v[YY] = 0;

			/* Pour convexintersect, 
			   les polys doivent �tre anticlock  */

			//AB: rajout du 18/06/2007
			z =
			  ConvexInclus (sommeM, k,
					this->octo[this->ifunct[ifunc] -
						   1], kocto, interocto);

			if (z == False)
			  z =
			    ConvexIntersect (sommeM, k,
					     this->octo[this->ifunct[ifunc] -
							1], (kocto - 1),
					     interocto);


			if (z == False)
			  {
			    /* Pas d'intersection.
			       Poly disjoints ou bien inclus.
			       Tester si l'octogone est inclus dans la sommeM 
			       Si un de ses points y est inclus, c'est qu'il y est tout entier */
			    z =
			      InPolyConvex (this->
					    octo[this->ifunct[ifunc] - 1][0],
					    sommeM, k);
			    if (z == True)
			      {
				/* Int�grer sur l'octo */
				//              printf(" Int�grer sur l'octo\n");

				k = 8;
				for (ii = 0; ii < k; ii++)
				  {
				    socto[ii][XX] =
				      int (ceil
					   (this->
					    octo[this->ifunct[ifunc] -
						 1][ii][XX]));
				    socto[ii][YY] =
				      int (ceil
					   (this->
					    octo[this->ifunct[ifunc] -
						 1][ii][YY]));
				  }
			      }	// fin z=true
			    else
			      {
				/* Poly disjoints ou bien inclus et
				   l'octogone n'est pas inclus dans la sommeM.
				   Tester si la somme de M est incluse dans l'octo */
				unpoint[XX] = sommeM[0][XX];
				unpoint[YY] = sommeM[0][YY];
				z =
				  InPolyConvex (unpoint,
						this->octoi[this->
							    ifunct[ifunc] -
							    1], 8);
				if (z == True)
				  {
				    /* Int�grer sur la somme M */
				    //        printf(" Int�grer sur la somme M\n");

				    for (ii = 0; ii < k; ii++)
				      {
					socto[ii][XX] = sommeM[ii][XX];
					socto[ii][YY] = sommeM[ii][YY];
				      }
				  }	// fin z, Int�grer sur la somme M
				else
				  {
				    // Le cas else, i.e quand l'octo et la sommeM
				    // sont disjoints et qu'aucun des 2 n'est inclus
				    // dans l'autre:
				    // la dispersion est alors consid�r�e comme nulle.
				    integre = False;
				  }	// fin octo et  sommeM disjoints
			      }	// fin else Poly disjoints ou bien inclus
			  }	//  fin z==false
			else
			  {
			    /* Intersection non nulle */
			    /* Mettre interocto dans un Polygoni
			       et passer celui-ci dans l'appel � integration
			     */
			    //              printf(" Intersection non nulle\n");

			    k = Intersection2Polygoni (interocto, socto);
			  }



			if (z == True)
			  {
			    // Intersection octo et sommeM non nulle,
			    // ou l'un est inclus dans l'autre
			    z = InPolyConvex (zero, socto, k);

			    if (z == True)
			      {
				// triangulation a partir de zero:
				Triangulation0 (numbera, numberb,
						socto, k,
						i, j,
						lpolya, lpolyb,
						maxlpoly, ivertce, stvertce);

			      }
			    else
			      {
				// triangulation a partir 
				//de n'importe quel sommet
				Triangulation (numbera, numberb,
					       socto, k,
					       i, j,
					       lpolya, lpolyb,
					       maxlpoly, ivertce, stvertce);
			      }
			  }	// fin z


		DetrutdVertex (interocto);	// Avant le 6/1/2014 FREE (interocto);
		      }		// fin else (dispersion born�e � une certaine distance)


		  }		// fin boucle sur les sous-polys

	      if (ivertce > maxlpoly)
		{
		  sprintf (errmess,
			   "Maximal number of subregions %d reached for polygons (%d, %d).\n",
			   maxlpoly, numbera, numberb);
		  // Fatal error 
		  ecrmess (CALI_MAXSREGIONS, moi, errmess, True);
		}


	      // le 1ier argu est �gal au nbre d'integrales estim�es d'un coup
	      Adapt lAdapt (1, ivertce,
			    this->reqmaxpts[ifunc],
			    lpolya, lpolyb,
			    this->reqreler[ifunc],
			    this->reqabser[ifunc],
			    stvertce);



	      /* APPEL */

	      lAdapt.Integration (f_, numbera, numberb);
	      if (lAdapt.GetIfail () != OK)
		this->pasatteint[ifunc] = True;

	      // On met les derniers resultats dans les structures ad�quates
	      // m�me quand il y a erreur car aucune n'est fatale

	      this->rp[ifunc] = lAdapt.GetResult0 ();
	      this->abser[ifunc] = lAdapt.GetAbserr0 ();
	      this->nbeval[ifunc] = lAdapt.GetNeval ();




	    }			// fin else (on integre)
	}			// fin else

    }				// fin ifunc
  time(&now);  /* get current time; */
  temps = difftime(now, debut);

  // Signaler si le nbre maxi d'evaluations a �t� atteint 
  if (OUTPUT_WARNING == ALL)
    {
      for (ifunc = 0; ifunc < this->nfunct; ifunc++)
	{
	  if (this->pasatteint[ifunc] == True)
	    {
	      sprintf (errmess,
		       "for polygons (%d, %d) and function %d,\n the convergence is not reached.\n",
		       numbera, numberb, this->ifunct[ifunc]);
	      ecrmess (CALI_MAXITER, moi, errmess, False);
	    }
	}			// fin ifunc
    }				// fin OUTPUT_WARNING 

  DETRU_T1 (stvertce);
  DETRU_T1 (lpolyb);
  DETRU_T1 (lpolya);

  /* pas besoin, c'est deja dit ailleurs 
  if (poutput == ALL)
    cout << endl
      << "Elapsed time in integration: " << temps << " seconds." << endl;
  */

}				// fin CalcR 
