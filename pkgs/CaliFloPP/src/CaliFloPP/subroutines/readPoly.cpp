/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 15 Fev 2006               |
|                        19 Avril 2007             |
| Role                 : lecture et stockage des   |
|    coordonn�es des  polygones                    |
--------------------------------------------------*/
///////////////////////////////////////////////////////
#include <iostream>


#include "caliinclc.h"
#include "readPoly.h"
#include "calimacros.h"
#include "util.h"
#include "geom.h"
#include "crConvexSp.h"
#include "read1Poly.h"

////////////////////////////////////////////////////
// Fonction:
// Detruire la chaine des sommets vertices
// Ajout 6/1/2014
///////////////////////////////////////////////////
void
DetruVertices (tVertex vertices)
{


  tVertex v, v1;


  if (vertices == vertices->next) {
    FREE(vertices);
    return;
  }

  v=vertices;
  do
    {
      //      printf ("%d\t%ld\t%ld\n", v->vnum, v->v[XX], v->v[YY]);
      v1=v;
      v=v->next;
    }
  while ( v != vertices);

  do
    {
      v=v1->prev;
      FREE(v1);
      v1=v;
    }
  while ( v != vertices);

  FREE(vertices);
}

////////////////////////////////////////////////////
// Fonction:
// Imprimer la chaine des sommets vertices
///////////////////////////////////////////////////
void
PrintVertices (tVertex vertices)
{
  tVertex v;

  v = vertices;
  do
    {
      printf ("%d\t%ld\t%ld\n", v->vnum, v->v[XX], v->v[YY]);
      v = v->next;
    }
  while (v != vertices);
}

///////////////////////////////////////////////////////
// Fonction:
// Translater le parcellaire, c.a.d rajouter valx et valy
// aux coordonnees x et y, respect. de tous les polygones.
// Arguments d'entree:
// verbose: si True , afficher ce qu'on fait.
// valx et valy: les valeurs a rajouter aux coordonnees.
// npoly: le nbre de polygones.
// a: tableau des npoly nombres de sous-polygones.
// ni: pour chaque polygone, pour chacun de ses sous-polygones,
//  nombre de ses sommets.
// Arguments d'entree-sortie:
// Poly: pour chaque polygone, pour chacun de ses sous-polygones,
// pour chacun de ses sommets, ses coordonnees.
///////////////////////////////////////////////////////
void
TranslateParcel (Boolean verbose,
		 int valx, int valy,
		 int npoly, int *a, int **ni, tPolygoni ** Poly)
{

  int i, j, k;


  if (verbose == True)
    {
      printf ("\n\n====================================================\n");

      printf ("Translation of the landscape:\n");
      if (valx != 0)
	printf ("x-coordinates are multiplied by %g, then translated by %d\n",
		double (SCALE), valx);
      if (valy != 0)
	printf ("y-coordinates are multiplied by %g, then translated by %d\n",
		double (SCALE), valy);

      printf ("====================================================\n\n");
    }				// fin verbose



  for (i = 0; i < npoly; i++)
    {
      for (j = 0; j < a[i]; j++)
	{

	  for (k = 0; k < ni[i][j]; k++)
	    {
	      Poly[i][j][k][XX] += valx;
	      Poly[i][j][k][YY] += valy;
	    }



	}
    }
}				// fin TranslateParcel

///////////////////////////////////////////////////////
// Fonction:
// Lecture des noms, identificateurs et coordonn�es 
// de tous les polygones. Oter eventuellement la fermeture
// des  polygones. Multiplier les coordonnees par SCALE.
// Verifier qu'ils peuvent etre representes par des entiers.
//  Retourner les min et max sur tout le parcellaire.
// Arguments d'entree:
// fp: pointeur sur le fichier a lire.
// En entree, il est positionne au debut des coordonnees
// et en sortie apres celles du npoly ieme poly.
// pinput: 1 ou 2 selon le format du fichier.
// pdelim: le caractere separateur des valeurs sur le fichier.
// Arguments de sortie:
// npoly: le nbre de polygones.
// a: tableau des npoly nombres de sous-polygones.
// A cette etape, les sous-polys convexes n'etant pas encore
// determines, tous les elements sont egaux a 1 (un seul sous-poly).
// ni: pour chaque polygone, pour le sous-polygone d'indice 0,
//  nombre de ses sommets. 
// Poly: pour chaque polygone, pour le sous-polygone d'indice 0 ,
// pour chacun de ses sommets, ses coordonnees; les sommets
// sont orientes clock. 
// numPoly: identificateurs des npoly polygones.
// nomPoly: noms  des npoly polygones.
// xminmax: minimum et maximum des coordonnes x 
// sur tout le parcellaire.
// yminmax: minimum et maximum des coordonnes y 
// sur tout le parcellaire.
// Retour:
// OK ou un code d'erreur negatif.
///////////////////////////////////////////////////////
int
ReadCoord (FILE * fp,
	   int pinput, char *pdelim,
	   int npoly,
	   int *a, int **ni, tPolygoni ** Poly,
	   int *numPoly, char **nomPoly, int *xminmax, int *yminmax)
{
  int erreur, nsom, isom;
  // Les coordonnes en reel:
  real lesx[MAX_VERTICES], lesy[MAX_VERTICES];
  real lux, luy;
  int ipoly;
/* Pour stocker les messages */
  char moi[] = "ReadCoord";
  char errmess[CHAR_MAX];
  Boolean toobig = False;

  real Tol = REAL_MIN;		// Pour la comparaison des r��ls


  xminmax[0] = yminmax[0] = INT_MAX;	// les min
  xminmax[1] = yminmax[1] = int (-real (INT_MAX) + 1.0);	// les max


  for (ipoly = 0; ipoly < npoly; ipoly++)
    {

      /* Lire le poly */
       switch (pinput)
	{
	case 1:
	  if ((erreur =
	       read1Poly (fp, pdelim, numPoly[ipoly], nsom, lesx,
			  lesy)) != OK)
	    return (erreur);

	  strcpy (nomPoly[ipoly], " ");	// pas de nom de poly dans ce format


	  break;
	case 2:
	  if ((erreur =
	       read2Poly (fp, pdelim, numPoly[ipoly], nomPoly[ipoly], nsom,
			  lesx, lesy)) != OK)
	    return (erreur);
	  break;
	default:
	  sprintf (errmess, "Internal error:  format %d non reconnu\n",
		   pinput);
	  return (ecrmess (CALI_ERINTERNAL, moi, errmess, True));
	  // c'est une erreur de programmation fatale si on est ici
	}			// fin switch


// V�rifier les identificateurs
      if (numPoly[ipoly] <= 0)
	{
	  sprintf (errmess, "Polygon ident should be >0\n");
	  return (ecrmess (CALI_ERPOLY10, moi, errmess));
	}


      // Oter la fermeture du poly:
      // Si le 1ier et dernier sommets sont quasi-egaux,
      // on supprime le dernier
      if ((fabs (lesx[0] - lesx[nsom - 1]) <= Tol) &&
	  (fabs (lesy[0] - lesy[nsom - 1]) <= Tol))
	{
	  nsom = nsom - 1;
	}

      if (nsom < 3)
	{
	  sprintf (errmess,
		   " number of vertices of polygon ident %d is %d (should be>=3)\n",
		   numPoly[ipoly], nsom);
	  return (ecrmess (CALI_ERPOLY5, moi, errmess));
	}


      // Maj les min ,max
      // apr�s multiplication par le facteur d'�chelle
      for (isom = 0; isom < nsom; isom++)
	{
	  lux = lesx[isom] * SCALE;
	  luy = lesy[isom] * SCALE;

	  if ((fabs (lux) >= INT_MAX) || (fabs (luy) >= INT_MAX))
	    {
	      toobig = True;
	      fprintf (stderr,
		       "Too big %dst coordinates in polygon %d: %g %g ",
		       (isom + 1), numPoly[ipoly], lesx[isom], lesy[isom]);
	      if (SCALE > 1)
		fprintf (stderr, " (they will be multiplied by %g)",
			 double (SCALE));
	      fprintf (stderr, "\n");

	    }

	  Poly[ipoly][0][isom][XX] = (long int) (lux);
	  Poly[ipoly][0][isom][YY] = (long int) (luy);

	  xminmax[0] = MIN (Poly[ipoly][0][isom][XX], xminmax[0]);
	  xminmax[1] = MAX (Poly[ipoly][0][isom][XX], xminmax[1]);
	  yminmax[0] = MIN (Poly[ipoly][0][isom][YY], yminmax[0]);
	  yminmax[1] = MAX (Poly[ipoly][0][isom][YY], yminmax[1]);
	}

      a[ipoly] = 1;		// un seul sous-poly � cette �tape
      ni[ipoly][0] = nsom;

    }				// fin boucle sur les polys

  if (toobig == True)
    {
      sprintf (errmess, "Sorry : too big coordinates. \n");
      return (ecrmess (CALI_ERPOLY8, moi, errmess));
    }

  return (OK);
}				// fin readCoord


///////////////////////////////////////////////////////
// Fonction:
// Creer la liste chainee (vertices)
// des sommets orientes anti-clockwise du polygone d'indice ipoly.
// Si verbose=T, afficher le min et max des coordonnees.
// Arguments d'entree:
// ipoly: indice du poly a considerer.
// nsom: le nombre de ses sommets.
// Poly: pour chaque polygone, pour le sous-polygone d'indice 0,
// pour chacun de ses sommets, ses coordonnees.
// A cette etape, les sous-polys convexes n'etant pas encore
// determines, seul le sous-polygone d'indice 0 est rempli.
// Les sommets y sont orientes clockwise.
// verbose: si T,  afficher le min et max des coordonnees.
// Arguments de sortie:
// vertices: liste chainee des sommets orientes anti-clockwise.
//////////////////////////////////////////////////////////
void
ReadVertices (int ipoly, int nsom,
	      tPolygoni ** Poly, Boolean verbose, tVertex vertices)
{
  tVertex v;
  int isom, xminmax[DIM], yminmax[DIM];

  // Initialisation des minmax 
  xminmax[0] = Poly[ipoly][0][0][XX];
  xminmax[1] = Poly[ipoly][0][0][XX];
  yminmax[0] = Poly[ipoly][0][0][YY];
  yminmax[1] = Poly[ipoly][0][0][YY];

  // Creation de vertices et mise dans le sens anti-clockwise
  // des coordonnees.
  // Le 1ier sommet est le meme que celui dans Poly[ipoly][0].
  for (isom = 0; isom < nsom; isom++)
    {
      // MakeNullVertex cree un tVertex et l'insere apres vertices
      // (si vertices n'existe pas encore, on lui affecte
      // le  tVertex cree, les 2 pointeurs prev et next pointant
      // sur lui-meme.) 
      v = MakeNullVertex (vertices);
      v->v[XX] = Poly[ipoly][0][isom][XX];
      v->v[YY] = Poly[ipoly][0][isom][YY];
      if (isom == 0)
	v->vnum = 0;
      else
	v->vnum = nsom - isom;
      // Calcul dans la foulee, des min-max si verbose.
      if (verbose == True)
	{
	  xminmax[0] = MIN (v->v[XX], xminmax[0]);
	  xminmax[1] = MAX (v->v[XX], xminmax[1]);
	  yminmax[0] = MIN (v->v[YY], yminmax[0]);
	  yminmax[1] = MAX (v->v[YY], yminmax[1]);
	}			// fin verbose
    }				// fin isom

  if (verbose == True)
    {
      PrintVertices (vertices);
      printf ("%%Bounding box:\n");
      printf ("xmax = %d; xmin = %d; difference: %d\n",
	      xminmax[1], xminmax[0], xminmax[1] - xminmax[0]);
      printf ("ymax = %d; ymin = %d; difference: %d\n",
	      yminmax[1], yminmax[0], yminmax[1] - yminmax[0]);

    }				// fin verbose


}				// fin ReadVertices

///////////////////////////////////////////////////////
// Fonction: Lecture des polygones
//   Arguments d'entr�e:
// fp: pointeur sur le fichier a lire.
// En entree, il est positionne au debut des coordonnees
// et en sortie apres celles du npoly ieme poly.
// verbose: si True , afficher ce qu'on fait.
// calcSurf: True si on ne veut que les surfaces
// pinput: 1 ou 2 selon le format du fichier.
// pdelim: le caractere separateur des valeurs sur le fichier.
// npoly: le nbre de polys.
//   Arguments de sortie allou�s avant l'appel:
//  nomPoly: noms  des npoly polygones.
//  numPoly: tableau des npoly identificateurs des polys.
// Sont negatives, les identificateurs des polys
// qui comportent <3 sommets apres suppression
// des sommets trop proches d'un voisin (distance<DISTP),
// align�s ou formant des pics ainsi que ceux des polys
// pour lesquels la d�composition en sous-polys a �chou�.
//   a: a[i]= nbre de sous-polygones convexes qui composent
//            le poly i.
//   area: area[i]: aire du poly i.
//   ni:  tableau de npoly lignes; chaque ligne i 
//        comporte a[i] valeurs; ni[i][j] est le nombre de sommets 
//      du ji�me sous-poly convexe du poly i.
//   Poly: tableau de npoly lignes; chaque ligne i est
//         compos�e des a[i] sous-polygones convexes qui composent
//         le poly i. 
// bary: bary[i] est le barycentre du poly i.
// Les sommets dans Poly sont orientes clockwise.
//   Polyd: si non null, coordonn�es r�elles,
//          selon l'�chelle d'origine, et non translates
//          des polys sans les sommets d'identificateurs <0
// (voir ci-dessus) dans le sens clockwise.
// Autres arguments de sortie:
//   npolybons: nbre de polys bons, i,e qui comportent plus de 2
//          sommets valides et pour lesquels
//        la d�composition en sous-polys convexes a r�ussi.
//Retour:
//  - OK ou un code d'erreur negatif.
//////////////////////////////////////////////////

int
ReadPoly (FILE * fp, Boolean verbose,
	  Boolean calcSurf,
	  int pinput,
	  char *pdelim,
	  int npoly,
	  char **nomPoly, int *numPoly,
	  int &npolybons,
	  int *a,
	  real * area, int **ni,
	  tPolygoni ** Poly, tPolygond ** Polyd, real ** bary)
{

  tVertex vertices;
  tVertex p;

  POLYGON_STRUCT PolygonVertices[MAX_VERTICES];
  //  PolygonDiagonals doit etre de dim egale au
  // 2*(nbre de cotes + nbre de diagonales)
  int diagonalsize = 2 * (MAX_VERTICES + (MAX_VERTICES - 1));
  DIAGONAL_STRUCT PolygonDiagonals[2 * (MAX_VERTICES + (MAX_VERTICES - 1))];
  int nvertices, ndiagonals;
  int j, i, isom, valx, valy;
  Boolean convex;
  int pxminmax[DIM], pyminmax[DIM];	// min et max du parcellaire
  char moi[] = "ReadPoly";
  /* Pour la suppression de sommets superflus */
  tPointd aa, bb, cc;
  real angle, distbc;

/* Pour stocker les messages */
  char errmess[CHAR_MAX], typesup[30];
  char polyident[CHAR_MAX];
  int erreur, errglob = OK;
  Boolean aligne = False;
  npolybons = npoly;



  // Lire les coordonnees 
  if ((erreur = ReadCoord (fp, pinput, pdelim, npoly,
			   a, ni, Poly, numPoly, nomPoly,
			   pxminmax, pyminmax)) != OK)
    return (erreur);


  if (((pxminmax[1] - pxminmax[0]) >= SAFE) ||
      ((pyminmax[1] - pyminmax[0]) >= SAFE))
    {
      sprintf (errmess,
	       "\nRange of the landscape (xrange=%d, yrange=%d) should be less than %d\n",
	       (pxminmax[1] - pxminmax[0]), (pyminmax[1] - pyminmax[0]),
	       SAFE);
      return (ecrmess (CALI_ERPOLY6, moi, errmess));
    }

  /*  translater �ventuellement le parcellaire */
  valx = valy = 0;

  // On translate quand les coordonn�es 
  // sont negatives ou nulles
  //  on translate aussi quand valeurs > SAFE
  //       ou bien quand TRANSLATE=1 
  if ((TRANSLATE == 1) || (pxminmax[0] <= 0) || (pxminmax[1] > SAFE))
    valx = -(pxminmax[0] - 1);


  if ((TRANSLATE == 1) || (pyminmax[0] <= 0) || (pyminmax[1] > SAFE))
    valy = -(pyminmax[0] - 1);


  if ((valx != 0) || (valy != 0))
    TranslateParcel (verbose, valx, valy, npoly, a, ni, Poly);


  /* Boucle sur les polys */
  erreur = OK;


  for (i = 0; i < npoly; i++)
    {
      if (verbose == True)
	printf ("****************************************\n");
      if (erreur != OK)
	errglob = erreur;
      /*
AB: 30/09/2009: ai d�plac� cette intialisation de la boucle
dans la boucle sur i
*/
  /* Initialisation de la boucle */
  NEW (vertices, tsVertex);
  vertices->next = vertices->prev = vertices;
  vertices->v[XX] = vertices->v[YY] = 0;



      erreur = OK;
      aligne = False;
      if (verbose == True)
	printf ("Polygon %d (counterclock-wise):\n", numPoly[i]);

      ReadVertices (i, ni[i][0], Poly, verbose, vertices);
      nvertices = ni[i][0];



      /* Identifier le poly pour les messages  */
      sprintf (polyident, "(%d-st polygon - Ident: %d)\n", i + 1, numPoly[i]);
      /* le 28/08/2020, pour NC
      printf("(%d-st polygon - Ident: %d)\n", i + 1, numPoly[i]);
      */


      /* D�celer les sommets align�s ou proches   */
      p = vertices;
      do
	{
	  sprintf (typesup, "OK");	// la raison de l'elimination d'un sommet

	  // Revenir au metre pour le calcul d'angle et distance
      /* le 28/08/2020, pour NC
	  printf("p %d %d\n", p->v[XX], p->v[YY]);
	  printf("p->next %d %d\n", p->next->v[XX], p->next->v[YY]);
	  printf("p->next->next %d %d\n", p->next->next->v[XX], p->next->next->v[YY]);
      */

	  bb[XX] = real (p->v[XX]) / SCALE;
	  bb[YY] = real (p->v[YY]) / SCALE;
	  cc[XX] = real (p->next->v[XX]) / SCALE;
	  cc[YY] = real (p->next->v[YY]) / SCALE;
	  aa[XX] = real (p->next->next->v[XX]) / SCALE;
	  aa[YY] = real (p->next->next->v[YY]) / SCALE;

	  distbc = sqrt ((bb[XX] - cc[XX]) * (bb[XX] - cc[XX]) +
			 (bb[YY] - cc[YY]) * (bb[YY] - cc[YY]));

	  if (distbc <= real (DISTP))
	    {
	      /* Quand la distance entre le 1ier sommet (bb) et le 2ieme (cc)
	         est <= seuil, on enl�ve le 2ieme sommet */
	      sprintf (typesup, "(<%g m. to another)", DISTP);
	    }


	  if (strcmp (typesup, "OK") == 0)
	    {
	      angle = Angle3d (bb, cc, aa);


	      if ((angle >= (M_PI - ANGLEPREC))
		  && (angle <= (M_PI + ANGLEPREC)))
		{
		  /* Oter les sommets alignes */


		  sprintf (typesup, "(aligned with another)");
		}
	      else
		{
		  if ((angle >= -ANGLEPREC) && (angle <= ANGLEPREC))
		    {
		      /* Oter les pics pointus */
		      sprintf (typesup, "(sharp peak)");
		    }
		}
	    }			// fin (typesup == "")


      /* le 28/08/2020, pour NC
	      printf (
		       "vertice %s:\n%g, %g (%g, %g) \n%s\n",
		       typesup,
		       (cc[XX] - (long int) valx / SCALE),
		       (cc[YY] - (long int) valy / SCALE),
		       cc[XX] * SCALE, cc[YY] * SCALE, polyident);
      */


	  if (strcmp (typesup, "OK") != 0)
	    {
	      // Format %g car en %f, il y a generation de chiffres decimaux non exacts
	      sprintf (errmess,
		       "vertice removed %s:\n%g, %g (%g, %g) \n%s\n",
		       typesup,
		       (cc[XX] - (long int) valx / SCALE),
		       (cc[YY] - (long int) valy / SCALE),
		       cc[XX] * SCALE, cc[YY] * SCALE, polyident);
	      ecrmess (0, moi, errmess);

	      aligne = True;
	      // supprimer le sommet du milieu et recommencer avec la
	      // nvelle liste car il peut y avoir plus de 3 sommets
	      // cons�cutifs align�s
	      ni[i][0]--;
	      nvertices--;
	      if (ni[i][0] < 3)
		{
		  if (verbose == True)
		    printf ("Number of valid vertices < 3\n");

		  sprintf (errmess,
			   "Warning: number of valid vertices < 3\n             %s\n\n",
			   polyident);
		  ecrmess (0, moi, errmess);
		  numPoly[i] = -numPoly[i];	// marque le poly invalide
		  erreur = CALI_WARNPOLY;
		  npolybons--;
		  break;	// sortie de la boucle de v�rif des sommets align�s
		}

	      // Suppression de p->next, i.e le sommet du milieu
	      // si celui qu'on supprime est la t�te de liste,
	      // i.e le 1ier sommet, la t�te devient le sommet pr�c�dent
	      // i.e le dernier sommet
	      if (p->next == vertices)
		{
		  vertices = p;
		}

	      p->next = p->next->next;
	      p->next->prev = p;
	    }			// fin align=T
	  else
	    {
	      p = p->next;
	      if (p == vertices)
		break;
	    }

	}			// fin do
      while (True);
      // FIN v�rif des sommets align�s
      // -----------------------------

      if (numPoly[i] < 0)
	{
	  // Le poly est invalide: passer au suivant
	  DetruVertices(vertices); // Avant le 6/1/2014 free (vertices);
	  continue;
	}


      // stocker les coordonn�es corrig�es du poly
      // dans PolygonVertices, eventuellement dans Polyd[i][0]
      // et, si on a ote des sommets, mettre a jour
      // Poly[i][0] et le numero de sommet dans vertices.
      //j: indice dans Polyd[i][0] et Poly[i][0] ou les sommets
      // doivent etre orientes clockwise.
      j = ni[i][0] - 1;
      isom = 0;
      p = vertices;
      if ((aligne == True) && (verbose == True))
	printf ("After aligned vertices removal:\n");
      do
	{
	  PolygonVertices[isom].xv = p->v[XX];
	  PolygonVertices[isom].yv = p->v[YY];

	  if (Polyd != NULL)
	    {

	      //  les coordonn�es corrig�es en r��ls
	      // dans l'�chelle d'origine
	      Polyd[i][0][j][XX] =
		(real (p->v[XX]) - real (valx)) / real (SCALE);
	      Polyd[i][0][j][YY] =
		(real (p->v[YY]) - real (valy)) / real (SCALE);
	    }
	  if (aligne == True)
	    {
	      // On a supprim� au moins 1 sommet
	      if (verbose == True)
		printf ("%d\t%ld\t%ld\n", isom, p->v[XX], p->v[YY]);

	      //  les coordonn�es corrig�es clockwise
	      Poly[i][0][j][XX] = p->v[XX];
	      Poly[i][0][j][YY] = p->v[YY];
	      // Maj du no de sommet dans la liste chainee
	      p->vnum = isom;
	    }			// fin (aligne == True)
	  isom++;
	  j--;
	  p = p->next;
	}
      while (p != vertices);


      if (Polyd != NULL)
	{
	  // Cas o� on  veut  seulement stocker
	  // dans Polyd les polys origines en leur �tant
	  // les sommets align�s (simplification des polys)
	  DetruVertices(vertices); // Avant le 6/1/2014 free (vertices);
	  continue;
	}


      // calcul des aires en r��l:
      p = vertices->next;
      aa[XX] = real (vertices->v[XX]);
      aa[YY] = real (vertices->v[YY]);
      do
	{
	  bb[XX] = real (p->v[XX]);
	  bb[YY] = real (p->v[YY]);
	  cc[XX] = real (p->next->v[XX]);
	  cc[YY] = real (p->next->v[YY]);

	  //      area[i] = area[i] + Area2i (vertices->v, p->v, p->next->v);
	  area[i] = area[i] + Area2 (aa, bb, cc);

	  // si l'angle a,b,c n'est pas convexe
	  // l'aire est negative, donc, les parties
	  // non dans le poly seront bien soustraites.
	  p = p->next;

	}
      while (p->next != vertices);
      // Diviser l'aire par 2 car l'algo calcule le double
      area[i] = area[i] / 2.0;
      // AB: 16/2/2007:
      // Verifier que l'aire est >0; sinon, c'est que les coordonn�es ne sont pas clockwise
      if (area[i] <= 0)
	{
	  sprintf (errmess,
		   "Area of polygon = %g %s.\nAre coordinates clockwise?\n",
		   area[i], polyident);
	  ecrmess (CALI_WARNPOLY, moi, errmess);
	  erreur = CALI_WARNPOLY;
	  npolybons--;
	  numPoly[i] = -numPoly[i];
	  DetruVertices(vertices); // Avant le 6/1/2014 free (vertices);
	  continue;
	}

      // Calculer les barycentres
      PolyCentroid (Poly[i][0], ni[i][0], area[i], bary[i]);

      if (calcSurf == True)
	{
	  // On veut  seulement calculer les surfaces
	  DetruVertices(vertices); // Avant le 6/1/2014 free (vertices);
	  continue;
	}

      // D�coupage en sous-polys convexes
      convex = Convexity (vertices);

      if (!convex)
	{
	  if (verbose == True)
	    {
	      printf ("%d th polygon is nonconvex\n%s\n", (i + 1), polyident);
	    }




	  ndiagonals =
	    Triangulate (PolygonVertices,
			 PolygonDiagonals,
			 vertices, nvertices, diagonalsize, polyident);

	  if (ndiagonals <= 0)
	    // erreur
	    erreur = ndiagonals;
	  else
	    {
	      a[i] =
		createSubPoly (Poly[i], ni[i],
			       PolygonVertices,
			       PolygonDiagonals,
			       nvertices, ndiagonals, verbose, polyident);

	      if (a[i] <= 0)	// erreur
		erreur = a[i];
	    }
	  if (erreur != OK)
	    {
	      // pas la peine d'afficher l'erreur : d�j� dite
	      // stocker les identifications du poly
	      npolybons--;
	      numPoly[i] = -numPoly[i];
	    }

	}			// fin poly non convexe
      else
	{
	  // Mettre les sommets anti-clockwise dans Poly[i][0]
	  for (j = 0; j < nvertices; j++)
	    {
	      Poly[i][0][j][XX] = PolygonVertices[j].xv;
	      Poly[i][0][j][YY] = PolygonVertices[j].yv;
	    }
	  ni[i][0] = nvertices;
	  a[i] = 1;
	  if (verbose == True)
	    printf ("%d th polygon is convex\n%s", i + 1, polyident);
	}


      if (convex)
       	   DetruVertices(vertices); // Avant le 6/1/2014 free (vertices);


    }				// fin boucle sur les poly
  fclose (fp);

  if (verbose == True)
    {
      printf ("****************************************\n");
      printf ("\nRange of the landscape x-coordinates: [%d, %d]",
	      pxminmax[0], pxminmax[1]);
      printf ("\nRange of the landscape y-coordinates: [%d, %d]\n",
	      pyminmax[0], pyminmax[1]);
      printf ("****************************************\n");
    }				// fin verbose


  if ((erreur != OK) || (errglob != OK))
    {
      /*  CALI_WARNPOLY: some polygons erroneous.
         fatal or non fatal errors 
         selon le programme appelant et (cf intflux.h) 
         Cas o� le poly, apr�s �limination des sommets
         align�s, a moins de 3 sommets,
         ou bien cas o� on n'a pas r�ussi � le d�composer 
         en sous-polys convexes
       */

      sprintf (errmess,
	       "Idents of the %d erroneous polygons:\n", (npoly - npolybons));
      for (i = 0; i < npoly; i++)
	{
	  if (numPoly[i] < 0)
	    sprintf (errmess, "%s %d ", errmess, -numPoly[i]);
	}
      sprintf (errmess, "%s\n", errmess);
      if (ERR_POLY == 0)
	return (ecrmess (CALI_WARNPOLY, moi, errmess));
      else
	return (ecrmess (CALI_WARNPOLY, moi, errmess, True));

    }				// fin errglob
  else
    return (OK);
}				// fin ReadPoly

////////////////////////////////////////////////////////////
