/////////////////////////////////////////////////////////
#ifndef _UTIL_H
#define _UTIL_H

#include "calitypes.h"
#include "calidefs.h"


Boolean realequal(real a, real b, real Tolerance=TOL);

int ecrmess (int code, char *moi, char *mess, Boolean fatal = False);

#endif
