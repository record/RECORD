/////////////////////////////////////////////////////////
// PURPOSE:
//   common base for the integration methods
/////////////////////////////////////////////////////////

#ifndef _METHODINTEGR_H
#define _METHODINTEGR_H
//#include <limits.h>

#include "caliconfig.h"
#include "calitypes.h"
#include "calierror.h"
#include "califunc.h"
#include "geom.h"
#include "util.h"
/////////////////////////////////////////////////////////
class methodIntegr
{
 public:
  real rp[MAX_NFUNCTIONS];	//result
  int nfunct;			// number of dispersion functions
protected:
  // *** protected permet aux classes filles 
  // d'acceder � ces attributs
  int ifunct[MAX_NFUNCTIONS];	// index from 1 of the dispersion functions
// Distances a partir desquelles les fonctions s'annulent
  real dzero[MAX_NFUNCTIONS];

// Distances a partir desquelles on ne calcule plus
// qu'en un point
  real dpoint[MAX_NFUNCTIONS];

///////////////////////////////////////////////
// common base function for printing on the result-file
///////////////////////////////////////////////
  void
    PrintFicOut (FILE * fp, const int noa, const int nob,
		 const real areac, const real aread)
  {


    int ifunc;
    real airec, aired;

    // Revenir au m�tre:
      airec = areac / (SCALE * SCALE);
      aired = aread / (SCALE * SCALE);

    // ecrire que les identif de polys et leur taux
    // de dispersion: 2 lignes pour chaque couple
    // d'abord noa �metteur:
    // Ecrire les nos de polys,  et nbre de fonctions
      fprintf (fp, "%d\t%d", noa, nob);

    for (ifunc = 0; ifunc < this->nfunct; ifunc++)
      {
	// Ecrire la moyenne de dispersion/unit� de surface
	if (aired <= 0.0)
	  {
	    fprintf (fp, "\t0");
	  }
	else
	  {
	    fprintf (fp, "\t%g", (this->rp[ifunc] / aired));
	  }
      }				// fin ifunc
    if ((OUTPUT_FILE_FORMAT == ALL) || (OUTPUT_FILE_FORMAT == LIGHT))
      //Ecrire les aires, 
      fprintf (fp, "\t%g\t%g", airec, aired);


  }				// fin PrintFicOut




public:
///////////////////////////////////////////////
// CONSTRUCTORS
///////////////////////////////////////////////

  void InitZero ()
  {
    // Programme appel� par les constructeurs
    this->dzero[0] = DZ1;
    this->dzero[1] = DZ2;
    this->dzero[2] = DZ3;
    this->dzero[3] = DZ4;
    this->dzero[4] = DZ5;
    this->dzero[5] = DZ6;
    this->dzero[6] = DZ7;
    this->dzero[7] = DZ8;
    this->dzero[8] = DZ9;
    this->dzero[9] = DZ10;

    this->dpoint[0] = DP1;
    this->dpoint[1] = DP2;
    this->dpoint[2] = DP3;
    this->dpoint[3] = DP4;
    this->dpoint[4] = DP5;
    this->dpoint[5] = DP6;
    this->dpoint[6] = DP7;
    this->dpoint[7] = DP8;
    this->dpoint[8] = DP9;
    this->dpoint[9] = DP10;
  }				// fin InitZero
/////////////////////////////////////////////

  methodIntegr ()
  {
  };

  methodIntegr (const int nfunct, const int *ifunction)
  {
    int ifunc;

    this->nfunct = nfunct;
    for (ifunc = 0; ifunc < nfunct; ifunc++)
      this->ifunct[ifunc] = ifunction[ifunc];
    this->InitZero ();
  };


///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////

  virtual ~ methodIntegr ()
  {
    // RAF
  }

/////////////////////////////////////////////
// READ THE METHOD ARGUMENTS
// 'pure virtual': function redefined in the daughter-classes
/////////////////////////////////////////////

  virtual int ReadArgu () = 0;
  virtual int VerifArgu () = 0;


/////////////////////////////////////////////
// Verifie les nos de fonctions de dispersion
/////////////////////////////////////////////
  int VerifFunct ()
  {
    char moi[] = "methodIntegr::VerifFunct";
    int ifunc, code = OK;
/* Pour stocker les messages */
    char errmess[CHAR_MAX];

    if ((this->nfunct <= 0) || (this->nfunct > MAX_NFUNCTIONS))
      {
	sprintf (errmess,
		 "Invalid number of dispersion functions: should be in [1, %d]\n",
		 MAX_NFUNCTIONS);
	code = CALI_ERGRID1;
	ecrmess (CALI_ERGRID1, moi, errmess);
      }
    for (ifunc = 0; ifunc < this->nfunct; ifunc++)
      {
	if ((this->ifunct[ifunc] <= 0)
	    || (this->ifunct[ifunc] > MAX_NFUNCTIONS))
	  {
	    sprintf (errmess,
		     "Invalid number of dispersion function: %d, should be in [1, %d]\n",
		     this->ifunct[ifunc], MAX_NFUNCTIONS);
	    code = CALI_ERGRID1;
	    ecrmess (CALI_ERGRID1, moi, errmess);
	  }

      }				// fin ifunc
    return (code);
  }				// fin methodIntegr::VerifFunct



//-------------------------------------------------
// 'pure virtual': function redefined in the daughter-classes
//-------------------------------------------------
/////////////////////////////////////////////
  // PRINT METHODS
/////////////////////////////////////////////
  virtual void Print (const int poutput,
		      const real areac, const real aread) = 0;
  virtual void
    PrintFic (FILE * fp, const int noa, const int nob,
	      const real temps, const real areac, const real aread) = 0;

/////////////////////////////////////////////
// ESTIMATION
/////////////////////////////////////////////
  virtual void
    CalcR (const int poutput,
	   Function * dispf,
	   const real areac, const real aread,
	   const real mindist, const Point lepoint,
	   const int numbera, const int numberb,
	   const int ac, const int ad,
	   const int nic[MAX_VERTICES],
	   const int nid[MAX_VERTICES],
	   tPolygoni * Polyc, tPolygoni * Polyd, real &temps,
	   int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS]) = 0;






};

//////////////////////////////////////////////////
#endif
