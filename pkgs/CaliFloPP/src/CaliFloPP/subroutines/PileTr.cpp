/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : gestion de la pile        |
--------------------------------------------------*/
#include <iostream>
using namespace std;

////////////////////////////////////////////
#include "PileTr.h"
#include "caliinclc.h"
#include "calimacros.h"
#include "calierror.h"

//////////////////////////////////////////
// Macros pour manipuler les indices a partir de 1.
//////////////////////////////////////////
// Rque: on ne surcharge pas les [] pour raison de lisibilit�
#define liste_1(a) this->liste[a - 1]
#define plusgrand_1(a) plusgrand[a - 1]

///////////////////////////////////////////////
// Constructeur
///////////////////////////////////////////////
PileTr::PileTr (const int maxtri)
{
  CREER_T1 (this->liste, maxtri, int);
}

///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////

PileTr::~PileTr ()
{
  // D�sallocations:
  DETRU_T1 (this->liste);
}

//////////////////////////////////////////////////////////
// M�thodes d'acc�s
//////////////////////////////////////////////////////////
int
PileTr::Get (const int i) const
{
  return (liste_1 (i));
}

////////////////////////////////////////////////////////////
// Ajouter nouveau dans la pile
////////////////////////////////////////////////////////////

void
PileTr::Ajout (const int nregions, const real * plusgrand, const int nouveau)
{
  int regcour, regtemp;
  real plusgr;

  plusgr = plusgrand_1 (nouveau);
  regcour = nregions;
  while (True)
    {
      regtemp = regcour / 2;

      if (regtemp >= 1)
	{
	  //  On compare le plus grand des fils avec le p�re
	  if (plusgr > plusgrand_1 (liste_1 (regtemp)))
	    {
	      // D�placer le pointeur en position regtemp en bas de pile
	      liste_1 (regcour) = liste_1 (regtemp);
	      regcour = regtemp;
	      continue;
	    }
	  else
	    break;		// sortie du while
	}			// fin  if (regtemp >=  1)
      else
	break;
    }				// fin while
  // Mettre le pointeur de la r�gion nouveau dans la pile
  liste_1 (regcour) = nouveau;

}				// fin Ajout

////////////////////////////////////////////////////////////////////
// Retirer un �l�ment de la pile
////////////////////////////////////////////////////////////////////

void
PileTr::Ote (int &nregions, const real * plusgrand)
{
  int regcour, regtemp;
  real plusgr;

  plusgr = plusgrand_1 (liste_1 (nregions));
  nregions = nregions - 1;
  regcour = 1;
  while (True)
    {
      regtemp = 2 * regcour;
      if (regtemp <= nregions)
	{
	  if (regtemp != nregions)
	    {
	      // D�terminer le max des fils 
	      if (plusgrand_1 (liste_1 (regtemp)) <
		  plusgrand_1 (liste_1 (regtemp + 1)))
		{
		  regtemp++;
		}
	    }			// fin (regtemp != nregions)

	  //  On compare le plus grand des fils avec le p�re
	  if (plusgr < plusgrand_1 (liste_1 (regtemp)))
	    {
	      // D�placer le pointeur en position regtemp en haut de pile
	      liste_1 (regcour) = liste_1 (regtemp);
	      regcour = regtemp;
	      continue;
	    }
	  else
	    break;
	}			// fin if (regtemp != nregions)
      else
	break;
    }				// fin while

  // Mettre � jour le pointeur
  if (nregions > 0)
    liste_1 (regcour) = liste_1 (nregions + 1);
}				// fin ote

//////////////////////////////////////////////////////

