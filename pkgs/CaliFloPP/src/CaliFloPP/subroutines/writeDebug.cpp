/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 15 Jan 2006               |
| Role                 : programmes d'�criture     |
|                        pour debug                |
--------------------------------------------------*/
//////////////////////////////////////////////////////////
#include "caliinclc.h"
#include "calitypes.h"
#include "writeDebug.h"


void
EcritPoly (int numbera, int longueura, tPolygoni * A, int nia[])
{
  int i, j;
  for (i = 0; i < longueura; i++)
    {
      printf ("%% ID %d triangle %d nombre de sommets %d  \n", numbera,
	      (i + 1), nia[i]);
      for (j = 0; j < nia[i]; j++)
	printf ("%ld, %ld,\n", A[i][j][XX], A[i][j][YY]);
    }
}				// fin EcritPoly

//////////////////////////////////////////////////////////
void
EcritSommeM (int numbera, int numberb,
	     int trianglea, int triangleb, tPolygoni sommeM, int k)
{
  int i;
  printf ("%% MSum %d  %d (%d %d) %d\n",
	  numbera, numberb, trianglea, triangleb, k);
  for (i = 0; i < k; i++)
    printf (" %ld %ld\n", sommeM[i][XX], sommeM[i][YY]);
}				// fin  EcritSommeM




//////////////////////////////////////////////////////////
void
EcritIntersection (char *str, tdVertex intersection)
{


  tdVertex p;

  printf ("%% INTERSECTION %s\n", str);
  printf ("%12.7f, %12.7f, %d\n",
	  intersection->v[0], intersection->v[1], intersection->vnum);

  p = intersection->next;

  do
    {

      printf ("%12.7f, %12.7f, , %d\n", p->v[0], p->v[1], p->vnum);
      p = p->next;

    }
  while (p->next != intersection);

}				// fin EcritIntersection

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
void
EcritNvIntersection (FILE * fic, tdVertex intersection)
{


  tdVertex p;
  int nv = 1;


  p = intersection->next;

  do
    {

      p = p->next;
      nv++;

    }
  while (p->next != intersection);
  fprintf (fic, "%d ", nv);

}				// fin EcritIntersection

//////////////////////////////////////////////////////////
