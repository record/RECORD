#ifndef _Adapt_H
#define _Adapt_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : classe de la m�thode      |
|                        de cubature adaptive      |
--------------------------------------------------*/

#include "caliinclc.h"
#include "calitypes.h"
#include "Triangle.h"
#include "Point.h"
#include "Integrand.h"


/////////////////////////////////////////////////////////
class Adapt
{
private:
  int ifail;			// code d'erreur 
  int nfun;			// nbre de fonctions � �valuer ensemble
  int ntri;			// nbre de triangles en entr�e
  int lgpile;			// longueur de la pile des valeurs courantes
  int neval;			// nbre d'�valuations 
  int nregions;			// nbre de sous-r�gions cr�ees
  int maxtri;			// nbre max. de sous-r�gions cr�ees (=maxnreg+1)
  int maxnreg, minnreg;		// nbre max et min de sous-r�gions cr�ees
  real epsabs, epsrel;		// pr�cisions absolue et relative

  // lpoly*: les indices des 2 sous-polys p�res des triangles
  //     longueur lgpile;
  //     les ntri premiers sont ceux en entr�e
  int *lpolya, *lpolyb;
  // ltri: va contenir tous les triangles;
  //     longueur maxtri;
  //     les ntri premiers sont ceux en entr�e
  Triangle *ltri;

  real **values, **errors, *plusgrand;	// travail(dim=lgpile)
  real *results, *abserr;	// r�sultats finaux (dim=nfun)

    friend ostream & operator<< (ostream &, const Adapt &);

public:
///////////////////////////////////////////////
// Constructeur
///////////////////////////////////////////////

    Adapt (int nfun, int ntri,
	   const long int reqmaxpts,
	   const int *listpolya, const int *listpolyb,
	   const real reqreler, const real reqabser,
	   const Triangle * lestriangles);




///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////
   ~Adapt ();


//////////////////////////////////////////////////////////
// M�thodes d'acc�s
//////////////////////////////////////////////////////////
  int GetIfail () const;
  int GetNregions () const;
  int GetMaxnreg () const;
  int GetNeval () const;
  real GetResult0 () const;
  real GetAbserr0 () const;


////////////////////////////////////////////////////
// Autres
///////////////////////////////////////////////////
  void PrintPlusgrand () const;

  void Integration (Integrand funsub, const int numbera, const int numberb);
};


/////////////////////////////////////////////////////
// Fonctions inline 
/////////////////////////////////////////////////////
inline ostream & operator<< (ostream & os, const Adapt & p)
{
  // si pls fonctions � int�grer d'un coup, 
  // il y a pls resultats et abs err � imprimer

  os << "Adapt results: " << p.results[0]
    << " abserr: " << p.abserr[0] << " ifail " << p.ifail << endl;
  return os;
}

//////////////////////////////////////////////////////////

#endif
