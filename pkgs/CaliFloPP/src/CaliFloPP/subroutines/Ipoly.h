/////////////////////////////////////////////////////////
// CLASS for the polynomes
/////////////////////////////////////////////////////////
#include "calitypes.h"
#include "calierror.h"
#include "calimacros.h"


class Ipoly
{
public:
    int npoly;
    char *filenamei;
    int *a;
    real *area;
    int **ni;
    tPolygoni **Poly;
    int *numPoly;			/* les nos d'identification des polygons */
    char **nomPoly;		/* les noms des polygons */
    real **bary;			/* Les barycentres des polygons */
    
///////////////////////////////////////////////
// CONSTRUCTORS
///////////////////////////////////////////////
    Ipoly()
    {
        this->npoly=0;
    }
    
    Ipoly( const int npoly, char * filenamei)
    {
//////////////////////////////////////////////////////////
        // Allocation des structures de dim npoly:
// memory allocation of the structures of dimension=npoly
///////////////////////////////////////////////////////
        this->npoly=npoly;
        this->filenamei= filenamei;
        CREER_T1 (this->a, npoly, int);
        CREER_T1 (this->area, npoly, real);
        CREER_T2 (this->ni, npoly, int);
        CREER_T2 (this->Poly, npoly, tPolygoni);
        CREER_T1 (this->numPoly, npoly, int);
        CREER_T2 (this->nomPoly, npoly, char);
        CREER_T2 (this->bary, npoly, real);
        for (int i = 0; i < npoly; i++)
        {
            CREER_T1 (ni[i], MAX_TRIANGLES, int);
            CREER_T1 (nomPoly[i], MAX_NAME, char);
            CREER_T1 (Poly[i], MAX_TRIANGLES, tPolygoni);
            CREER_T1 (bary[i], DIM, real);
        }
    }
    
///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////
    ~Ipoly() {
        // Désallocations:
    
        if (this->npoly >0) {
            DETRU_T2(this->bary,  this->npoly);
            DETRU_T2 (this->nomPoly, this->npoly);
            DETRU_T1 (this->numPoly);
            DETRU_T2 (this->Poly, this->npoly);
            DETRU_T2 (this->ni, this->npoly);
            DETRU_T1 (this->area);
            DETRU_T1 (this->a);
        } // fin (this->npoly >0)
        
        
    }
    
    
};

