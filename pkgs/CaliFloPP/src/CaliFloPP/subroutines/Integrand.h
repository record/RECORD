#ifndef _INTEGRAND_H
#define  _INTEGRAND_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Role                 :  type de la  fonction �   |
|                         int�grer                 |
| (i.e type de la fonction f_ dans les programmes  |
|                        ../method*.cpp)           |
--------------------------------------------------*/

#include "caliinclc.h"
#include "calitypes.h"
#include "Point.h"
#include "Vector.h"

/////////////////////////////////////////////////////////

typedef void (*Integrand) (const Point &,
			   int numfun, int polya, int polyb, Vector);
/////////////////////////////////////////////////////////
#endif
