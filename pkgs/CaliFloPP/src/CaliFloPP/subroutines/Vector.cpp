/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : op�rations de base sur    |
|                        vecteur                   |
--------------------------------------------------*/
/////////////////////////////////////////
#include "caliinclc.h"
#include "Vector.h"
#include "calierror.h"
#include "caliboolean.h"
#include "util.h"

/////////////////////////////////////////


/////////////////////////////////////////
// Constructeur
/////////////////////////////////////////
Vector::Vector (unsigned int s)
{
  char moi[] = "Vector::Vector";
  char errmess[CHAR_MAX];

  taille = s;
  x = new real[s];
  if (!s)
    {
      sprintf (errmess, "memory allocation problem. ");
      // Fatal error 
      ecrmess (CALI_ERRALLOC, moi, errmess, True);
    }
}


/////////////////////////////////////////
// Destructeur
/////////////////////////////////////////
Vector::~Vector ()
{
  // Att: ne pas d�truire car structures temporaires
  //  delete [] x;
}

// Explicit destructor:
void Vector::Detru ()
{
delete [] x;
}

/////////////////////////////////////////
// operateur [] en partie droite d'une affectation
/////////////////////////////////////////
const real &
Vector::operator[] (const int ind)
     const
     {
       char 
	 moi[] = "Vector::operator[]";
       char
	 errmess[CHAR_MAX];

       if (ind < 0 || ind >= this->taille)
	 {
	   sprintf (errmess, "index out of range.");
	   // Fatal error 
	   ecrmess (CALI_ERINTERNAL, moi, errmess, True);
	 }

       return (this->x[ind]);
     }

/////////////////////////////////////////
// operateur [] en partie gauche d'une affectation
/////////////////////////////////////////
real & Vector::operator[](const int ind)
{
  char 
    moi[] = "Vector::operator[]";
  char
    errmess[CHAR_MAX];

  if (ind < 0 || ind >= this->taille)
    {
      sprintf (errmess, "index out of range. ");
      // Fatal error 
      ecrmess (CALI_ERINTERNAL, moi, errmess, True);
    }

  return (this->x[ind]);
}

/////////////////////////////////////////
// = vecteur
/////////////////////////////////////////
Vector & Vector::operator = (const Vector & v)
{
  char 
    moi[] = "Vector::operator =";
  char
    errmess[CHAR_MAX];
  int
    i;

  if (this->taille != v.taille)
    {
      sprintf (errmess, "index out of range. ");
      // Fatal error 
      ecrmess (CALI_ERINTERNAL, moi, errmess, True);
    }

  for (i = 0; i < this->taille; i++)
    x[i] = v.x[i];
  return (*this);
}


/////////////////////////////////////////
// += vecteur
/////////////////////////////////////////
Vector & Vector::operator += (const Vector & v)
{
  char 
    moi[] = "Vector::operator +=";
  char
    errmess[CHAR_MAX];
  int
    i;

  if (this->taille != v.taille)
    {
      sprintf (errmess, "index out of range. ");
      // Fatal error 
      ecrmess (CALI_ERINTERNAL, moi, errmess, True);
    }

  for (i = 0; i < this->taille; i++)
    x[i] += v.x[i];
  return (*this);
}

/////////////////////////////////////////
// * par scalaire
/////////////////////////////////////////
Vector  Vector::operator * (const real r)  const
{
  int
    i;
  Vector v(this->taille);

  for (i = 0; i < this->taille; i++)
    v.x[i] = this->x[i] * r;
  return (v);
}

/////////////////////////////////////////
