#include "caliconfig.h"
extern int cubature(const Ipoly *lespolys, Iparam *param,
		    const int ncouples, int couples[MAX_NPAIRS][2],
		    int narg[MAX_NFUNCTIONS], 
		    real argfunct[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS],
		    char *filenamer, char openr,
		    int reslignes, real **sortie  );
extern int calcnres(const int npoly,
		    const int ncouples, int couples[MAX_NPAIRS][2]);

