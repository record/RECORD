#ifndef _UNTRIANGLE_H
#define _UNTRIANGLE_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : classe Triangle           |
--------------------------------------------------*/

#include <iostream>
using std::ostream;
using std::cout;
using std::endl;

#include "Point.h"

class Triangle
{
private:
  Point p1, p2, p3;
  friend ostream & operator<< (ostream &, const Triangle &);
public:

    Triangle ();
    Triangle (const Point &, const Point &, const Point &);
  int Verif () const;
  const Point & Sommet (int) const;
  real Aire () const;
  void printdebug () const;
  Triangle & operator= (const Triangle & v);
};

/////////////////////////////////////////////
// Fonctions inline
/////////////////////////////////////////////
inline Triangle & Triangle::operator= (const Triangle & v)
{
  p1 = v.Sommet (1);
  p2 = v.Sommet (2);
  p3 = v.Sommet (3);

  return *this;
}

///////////////////////////////////////////////
/////////////////////////////////////////////////////
inline ostream & operator<< (ostream & os, const Triangle & triangle)
{
  os << triangle.Sommet (1)
    << triangle.Sommet (2) << triangle.Sommet (3) << endl;

  return os;
}

/////////////////////////////////////////////
 inline  void Triangle::printdebug () const
{
  cout << ", " << this->p1.getX() << ", " << this->p1.getY() << ", ";
  cout << " " << this->p2.getX() << ", " << this->p2.getY() << ", ";
  cout << " " << this->p3.getX() << ", " << this->p3.getY() << ", ";
}


//////////////////////////////////////////////////
#endif
