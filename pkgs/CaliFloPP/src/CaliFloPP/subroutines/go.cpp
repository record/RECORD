/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 15 Fev 2006               |
| Role                 : encha�nement des calculs  |
--------------------------------------------------*/
///////////////////////////////////////////////////////
#include "caliinclc.h"
#include "go.h"
#include "calierror.h"
#include "calimacros.h"
#include "methodIntegr.h"
#include "methodAdapt.h"
#include "functions.h"		// Les dcl des fctions de dispersion individuelle


///////////////////////////////////////////////////////
// Verification d'un polygone � �tudier:
// appel� quand leur num�ro est n�gatif, i.e quand
// il n'est pas valide.
// Dans ce cas, si ERR_POLY=0,  on �crit �ventuellement un warning,
// sinon on appelle ecrmess pour �crire un message d'erreur.
///////////////////////////////////////////////////////
int
verifNumPoly (int numPoly, char *moi, char *errmess)
{
  sprintf (errmess, "Polygon ident %d is erroneous.", -numPoly);
  ecrmess (CALI_WARNPOLY, moi, errmess);

  if (ERR_POLY != 0)
    {
      fprintf (stderr, "Fatal error on polygon %d.\n", -numPoly);
      // Prevenir aussi sur stdout
      printf ("Execution stops because the polygon %d is erroneous.\n",
	      -numPoly);
      printf
	("(probably, it cannot be splitted into convex-subpolygons; see the warnings.)\n");
      return (CALI_WARNPOLY);
    }
  else
    {
      return (OK);
    }
}				// fin verifNumPoly

///////////////////////////////////////////////////////
// G�rer l'enchainement des calculs pour un poly c emetteur
// vers tous ceux d'indices d � findd
// Retourne un code d'erreur s'il y a une erreur sur le poly c
// fatale ((ERR_POLY!=0)
///////////////////////////////////////////////////////
int
gereBoucle (int &iloop, int c, int d, int findd,
	    int *numPoly, char **nomPoly,
	    FILE * fpr,
	    int pinput, int poutput,
	    methodIntegr * methode,
	    Function * pfunction,
	    int *a,
	    tPolygoni ** Poly,
	    int **ni, real * area, real ** bary, real &tempstotal,
	    int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS],
	    int reslignes, int &ires, real **sortie)
{
  char moi[] = "gereBoucle";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];
  int code=0;


  if (numPoly[c] < 0)
    {
      return (verifNumPoly (numPoly[c], moi, errmess));
    }


  do
    {
      if (numPoly[d] < 0)
	{
	  if ((code = verifNumPoly (numPoly[d], moi, errmess)) != OK)
	    return (code);
	  else
	    {
	      d++;
	      continue;
	    }
	}

      /* On y va */

      tempstotal += go (iloop, fpr, pinput, poutput,
			methode, pfunction,
			(c + 1), (d + 1),
			numPoly[c], numPoly[d],
			nomPoly[c], nomPoly[d],
			a[c], a[d],
			Poly[c], Poly[d],
			ni[c], ni[d], area[c], area[d], bary[c], bary[d],
			narg, targ, reslignes, ires, sortie);

      d++;
      ires++;
    }
  while (d < findd);
  return (OK);
}				// fin gereBoucle


///////////////////////////////////////////////////////
// Programme de lancement des calculs
// noa, nob: indices des polygones � partir de 1
// numbera, numberb:  num�ros d'identification des polygones
// ac, ad: nbre de sous-poly convexes dans chacun
// nic, nid: nbre de sommets dans chaque sous-poly
///////////////////////////////////////////////////////
real
go (int &iloop, FILE * fp,
    int pinput, int poutput,
    methodIntegr * methode,
    Function * dispf,
    int noa, int nob,
    int numbera, int numberb,
    char *noma, char *nomb,
    int ac, int ad,
    tPolygoni * Polyc, tPolygoni * Polyd,
    const int nic[MAX_VERTICES],
    const int nid[MAX_VERTICES],
    real areac, real aread, real * baryc, real * baryd,
    int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS],
    int reslignes, int ires, real **sortie)
{



  int i, j;
  real temps;
  real barydist, mindist;


  if ((poutput != NOTHING) && (poutput != LIGHT))
    {
      if (pinput == 2)
	printf ("\nPolygons n� %d (%s), %d (%s)", numbera, noma, numberb,
		nomb);
      else
	printf ("\nPolygons n� %d, %d", numbera, numberb);

      printf ("\n-------------------\n");
    }


  /* Determiner si les polys sont proches */
  mindist = 0.0;
  barydist = 0.0;

  if (noa != nob)
    {
      mindist = real (INT_MAX);
      for (i = 0; i < ac; i++)
	for (j = 0; j < ad; j++)
	  {
	    /* calculer la distance mini entre poly */
	    mindist = MIN (mindist,
			   DistMin (Polyc[i], nic[i], Polyd[j], nid[j]));

	  }			// fin i,j


      /* Cr�ation d'une structure "point"
         de longueur �gale � la
         distance entre les barycentres
         pour pouvoir appliquer la fonction 
         de dispersion quand on n'integre pas */
      barydist =
	(baryc[XX] -
	 baryd[XX]) *
	(baryc[XX] -
	 baryd[XX]) + (baryc[YY] - baryd[YY]) * (baryc[YY] - baryd[YY]);
      barydist = sqrt (barydist);




    }				// fin (numbera != numberb) 


  Point lepoint (0.0, barydist);
  // mindist doit etre en m�tres
  mindist = mindist / SCALE;
  methode->CalcR (poutput, dispf,
		  areac, aread,
		  mindist, lepoint,
		  numbera, numberb, ac, ad, nic, nid, Polyc, Polyd, temps, narg, targ);
  // incr�menter le nbre de boucles
  iloop++;
  if ((poutput != NOTHING) && (poutput != LIGHT))
    methode->Print (poutput, areac, aread);
  if (poutput == LIGHT)
    {
      printf ("%d\n", iloop);
      fflush (stdout);
    }

  if (fp != NULL)
    // �criture sur fichier:
    methode->PrintFic (fp, numbera, numberb, temps, areac, aread);

  // Remplir les sorties
  if (reslignes>0) {
    sortie[ires][0]=real(numbera);
    sortie[ires][1]=real(numberb);
    sortie[ires][2]=areac/(SCALE*SCALE);
    sortie[ires][3]=aread/(SCALE*SCALE);
    j=4;
  for (i=0; i < methode->nfunct; i++) {
    sortie[ires][j++]=methode->rp[i];
  }
  }


  return (temps);

}				// fin go

///////////////////////////////////////////////////////
/* Chercher l'indice d'un polygone d'apr�s son num�ro
   d'identification */
///////////////////////////////////////////////////////
int
getIndexPoly (int npoly, int clu, int *numPoly)
{

//std::cout << "DBG!getIndexPoly\tnpoly" << npoly << " clu" << clu << std::endl;

  int inum;

  for (inum = 0; inum < npoly; inum++)
    {
//std::cout << "DBG!getIndexPoly\tinum" << inum << " numPoly[inum]" << numPoly[inum] << std::endl;
      if (clu == abs (numPoly[inum]))
	return (inum);
    }
  return (-1);
}				// fin getIndexPoly




//////////////////////////////////////////////////
// Ce qu'on fait apr�s avoir lu le fichier de polys
// et les param�tres
//////////////////////////////////////////////////////////
int
suite (int cas,
       Boolean pverbose,
       int pinput,
       int poutput,
       Boolean grid,
       real pstepx, real pstepy, int pnr,
       unsigned int pseed,
       real * creler, real * cabser,
       long int *pmaxpts,
       int nfunc, int *ifunct,
       int npoly, int clu, int dlu,
       int nsend, int *send,
       int *target,
       int *a, real * area,
       real ** bary,
       int **ni, tPolygoni ** Poly,
       int *numPoly, char **nomPoly,
       char *filenamei, char *filenamer, char *openr, methodIntegr * methode,
       int narg[MAX_NFUNCTIONS], real targ[MAX_NFUNCTIONS][MAX_NARGFUNCTIONS],
       int reslignes, real **sortie)
{

  methodAdapt aadapt;

  FILE *fpr;
  char moi[] = "suite";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];
  // Mesure du temps 
  real tempstotal = 0;
  int i, ip;

// pointeurs sur les fonctions de dispersion
  Function pfunction[] = { &f1, &f2, &f3, &f4, &f5, 
			   &f6, &f7, &f8, &f9, &f10 };


//////////////////////////////////////////////////////////
// �crire les valeurs effectives des param�tres et constantes utiles
//////////////////////////////////////////////////////////
//  Informations sur les constantes:
// scale change la valeur des aires et des steps */
  if ((pverbose == 1) && (SCALE != 1))
    printf ("\nCoordinates are multiplied by %g\n", double (SCALE));

 
  // Les pr�cisions demand�es ne peuvent �tre nulles dans
  // la m�thode cubature:
  if (!grid)
    {
      for (i = 0; i < nfunc; i++)
	{

	  if (cabser[i] <= 0.0)
	    {
	      if (poutput != NOTHING)
		printf
		  ("Warning: Required absolute error should be not null for function %d.\n",
		   ifunct[i]);
	      cabser[i] = DEFAULT_ABS_ERR;
	    }

	  if (creler[i] <= 0.0)
	    {
	      if (poutput != NOTHING)
		printf
		  ("Required relative error should be not null for function %d\n",
		   ifunct[i]);
	      creler[i] = DEFAULT_REL_ERR;
	    }

	}			// fin i
    }				// fin (!grid)

  if (poutput == ALL)
    {
// �crire les valeurs effectives des param�tres:
      printf ("\nParameters:\n-----------\n");
      printf ("verbose: %d\n", pverbose);
      printf ("output: %d\n", poutput);
      printf ("scale: %g\n", double (SCALE));
      printf ("method:");

      if (grid == True)
	{
	  printf ("grid\n");
	  printf ("seed: %d\n", pseed);
	  if (SCALE != 1)
	    {
	      printf ("x-axis step: %g m. \n", (pstepx / SCALE));
	      printf ("y-axis step: %g m.\n", (pstepy / SCALE));
	    }
	  else
	    {
	      printf ("x-axis step: %g (in meter) \n", pstepx);
	      printf ("y-axis step: %g (in meter)\n", pstepy);
	    }
	  printf ("number of estimations: %d\n", pnr);

	}
      else
	{
	  printf ("cubature\n");
	  for (i = 0; i < nfunc; i++)
	    {
	      printf
		("function %d: relative precision = %g, absolute precision = %g\n",
		 ifunct[i], creler[i], cabser[i]);
	      if (pmaxpts[i] > 0)
		printf
		  ("            maximal number of evaluations points fixed to %ld \n",
		   pmaxpts[i]);

	    }
	}


      switch (cas)
	{
	case 1:
	  printf ("poly1: %d\n", clu);
	  printf ("poly2: %d\n", dlu);
	  break;
	case 3:
	  printf ("from polygon(s): ");
	  for (ip = 0; ip < nsend; ip++)
	    printf ("%d ", send[ip]);
	  printf ("\n");
	  break;
	case 2:
	  printf ("all pairs of polygons\n");
	  break;
	}			// fin switch
    }				// fin poutput>0



//////////////////////////////////////////////////////////
// Cr�er une m�thode et ses param�tres
//////////////////////////////////////////////////////////

      aadapt = methodAdapt (nfunc, ifunct, creler, cabser, pmaxpts);
      methode = &(aadapt);



  if ((ip = methode->VerifArgu ()) != OK)
    {
      return (ip);
    }


  /* Le stockage des r�sultats */
  fpr = NULL;




//////////////////////////////////////////////////////////
// Boucle sur les couples de parcelles
//////////////////////////////////////////////////////////

  int ires=0, iloop = 0, code, c, d;

  /*  On y va */
  if (filenamer != NULL)
    {
      fpr = fopen (filenamer, openr);
      if (!fpr)
	{
	  sprintf (errmess, "cannot open result file %s\n", filenamer);
	  return (ecrmess (CALI_ERFIC1, moi, errmess));
	}

      if (strncmp (openr, "w", 1) == 0)
	{
	  // nouveau fichier

	  fprintf (fpr, "npoly:\t%d\tinput-file:\t%s\tnfunc:\t%d\tmethod:\t",
		   npoly, filenamei, nfunc);
	  if (grid == True)
	    {
	      fprintf (fpr, "grid\n");
	    }
	  else
	    {
	      fprintf (fpr, "cubature\n");
	    }
	}


    }				// fin  if (filenamer != "")

  switch (cas)
    {
    case 2:
      {
// calcul sur toutes les paires de parcelles
//      printf ("Results for all the pairs of parcels:\n");

// Les r�sultats sont sym�triques: on ne balaie pas 2 fois,
// chaque couple
	for (c = 0; c < npoly; c++)
	  {
	    if ((code = gereBoucle (iloop, c, 0, (c + 1), numPoly, nomPoly,
				    fpr, pinput, poutput,
				    methode, pfunction,
				    a, Poly, ni, area, bary,
				    tempstotal,
				    narg, targ,
				    reslignes, ires, sortie)) != OK)
	      {
		if (ERR_POLY == 0)
		  continue;
		else
		  return (code);
	      }
	  }			/* fin boucle sur les polys c */


	break;
      }				/* fin cas=2 */



    case 5:
      {
// calcul sur des couples choisis
	for (ip = 0; ip < nsend; ip++)
	  {

	    /* Chercher l'indice du polygone */
	    if ((c = getIndexPoly (npoly, send[ip], numPoly)) < 0)
	      {
		printf ("\nPolygon %d not found\n-------------------\n",
			send[ip]);
		sprintf (errmess, "polygon %d not found\n", send[ip]);
		return (ecrmess (CALI_ERDIAG1, moi, errmess));
	      }
	    /* Chercher l'indice du polygone */
	    if ((d = getIndexPoly (npoly, target[ip], numPoly)) < 0)
	      {
		printf ("\nPolygon %d not found\n-------------------\n",
			target[ip]);
		printf ("Polygon %d not found\n", target[ip]);
		sprintf (errmess, "polygon %d not found\n", target[ip]);
		return (ecrmess (CALI_ERDIAG1, moi, errmess));
	      }

	    if ((code = gereBoucle (iloop, c, d, d, numPoly, nomPoly,
				    fpr, pinput, poutput,
				    methode, pfunction,
				    a, Poly, ni, area, bary,
				    tempstotal,
				    narg, targ,
				    reslignes, ires, sortie)) != OK)
	      {
		if (ERR_POLY == 0)
		  continue;
		else
		  return (code);
	      }




	  }			/* fin boucle sur ip */

	break;
      }				/* fin cas 5 */


    default:
      sprintf (errmess, "bad case %d, must be 2 or 5\n", cas);
      return (ecrmess (CALI_ERDIAG3, moi, errmess));

    }				/* fin switch */

  if (poutput == ALL) 
    {
      printf
	("\n\nElapsed time: %.f seconds \n",
	 tempstotal);
    }


  ///////////////////////////////////////
  // Fermer les fichiers
  ///////////////////////////////////////

  if (fpr != NULL)
    fclose (fpr);
  return (OK);

}				// fin suite
