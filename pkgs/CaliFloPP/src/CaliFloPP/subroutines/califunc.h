#ifndef _CALIFUNC_H
#define  _CALIFUNC_H
#include "Point.h"

/////////////////////////////////////////////////////////
// Type de la fonction a integrer
/////////////////////////////////////////////////////////
typedef real (*Function) (const Point &, int narg, real targ[MAX_NARGFUNCTIONS]);
/////////////////////////////////////////////////////////
#endif
