/////////////////////////////////////////////////////////
// CLASS for the input parameters
/////////////////////////////////////////////////////////
#include "caliboolean.h"
#include "caliconfig.h"
#include "calidefs.h"
class Iparam
{
public:
// True si on veut l'ip des coordonnées lues
    // et de l'indication si les poly sont convexes ou non
    Boolean pverbose;
    // Ce qu'on veut comme impressions à l'écran:
    int poutput;
    
    // Le format du fichier des polygones et le délimitateur des valeurs
    int pinput;
    char pdelim[2];
    
    // Les fonctions de dispersion
    int nfunct;
    int ifunct[MAX_NFUNCTIONS];
    
    // Adapt
    real pabser[MAX_NFUNCTIONS], preler[MAX_NFUNCTIONS];
    long int pmaxpts[MAX_NFUNCTIONS];
///////////////////////////////////////////////
// CONSTRUCTORS
///////////////////////////////////////////////
    Iparam ()
    {
        /* Constructeur avec les valeurs par defaut */
        int i;
        this->nfunct=DEFAULT_NFUNCTIONS;
        this->pverbose=Boolean (DEFAULT_VERBOSE);
        this->pinput=DEFAULT_INPUT_FORMAT;
        this->poutput=DEFAULT_OUTPUT;
        strcpy(this->pdelim, DEFAULT_DELIM);
        for (i=0; i< MAX_NFUNCTIONS; i++) {
            this->ifunct[i]=i+1;
            this->pabser[i]=DEFAULT_ABS_ERR;
            this->preler[i]=DEFAULT_REL_ERR;
            this->pmaxpts[i]=DEFAULT_MAX_PTS;
        }
    };
///////////////////////////////////////////////
    Iparam ( Boolean verbose,  int input,
            char *delim,  int output,
            int nfunct, int *ifunct,
            real *abser,real *reler,
            long int *maxpts)
    {
        int i;
        this->pverbose=verbose;
        this->pinput=input;
        this->poutput=output;
        strcpy(this->pdelim, delim);
        this->nfunct=nfunct;
        for (i=0; i< nfunct; i++) {
            this->ifunct[i]=ifunct[i];
            this->pabser[i]=abser[i];
            this->preler[i]=reler[i];
            this->pmaxpts[i]=maxpts[i];
        }
    };
    
    ~Iparam ( )
      { 
      }
};
