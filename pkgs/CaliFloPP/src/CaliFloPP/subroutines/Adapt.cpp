/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : application de la m�thode |
|                        de cubature adaptive      |
--------------------------------------------------*/

#include "caliinclc.h"
#include "caliconfig.h"
#include "calimacros.h"
#include "calierror.h"
#include "Adapt.h"
#include "Rule.h"
#include "PileTr.h"


///////////////////////////////////////////////
// Constructeur
///////////////////////////////////////////////
/* 
Arguments:
nfun: nbre de fonctions quand on veut en estimer pls d'un coup
ntri: nbre de triangles  donn�s
reqmaxpts: nbre maximum de points d'�valuations demand�
listpolya, listpolyb: nos des sous-polys convexes auxquels
                appartiennent les ntri triangles.
reqreler, reqabser: pr�cision relative et absolue demand�es
lestriangles: les ntri triangles 
*/


Adapt::Adapt (int nfun, int ntri,
	      const long int reqmaxpts,
	      const int *listpolya, const int *listpolyb,
	      const real reqreler, const real reqabser,
	      const Triangle * lestriangles):
neval (0)
{
  int i, minpts;
  long int maxpts;

  this->nfun = nfun;
  this->ntri = ntri;
  this->epsabs = reqabser;
  this->epsrel = reqreler;


  if (reqmaxpts == 0)
    maxpts = DEFAULT_NB_PTS * ntri;
  else
    maxpts = MAX (reqmaxpts, 37 * ntri);	// doit �tre >= 37*ntri
  // Ne pas d�passer le maxi:
  maxpts = MIN (DEFAULT_MAX_PTS, maxpts);

  minpts = 37;
  this->maxnreg = 3 * ((maxpts - 37 * ntri) / (4 * 37)) + ntri;
  this->minnreg = 3 * ((minpts - 37 * ntri) / (4 * 37)) + ntri;
  if ((minpts - 37 * ntri) % (4 * 37) > 0)
    this->minnreg += 3;
  this->minnreg = MAX (ntri, this->minnreg);

  this->maxtri = this->maxnreg + 1;
  int nw = this->maxtri * (2 * nfun + 1) + MAX (32, 8 * ntri) * nfun + 1;

  this->lgpile = (nw - 1 - nfun * MAX (32, 8 * ntri)) / (2 * nfun + 1);

  /* Creation des structures de dim lgpile: */
  CREER_T2 (this->values, this->lgpile, real);
  CREER_T2 (this->errors, this->lgpile, real);
  for (i = 0; i < this->lgpile; i++)
    {
      CREER_T1 (this->values[i], nfun, real);
      CREER_T1 (this->errors[i], nfun, real);
    }
  /* lpoly: va contenir les indices des sous-polys p�res
     les ntri premiers sont ceux donn�s */
  CREER_T1 (this->lpolya, this->lgpile, int);
  CREER_T1 (this->lpolyb, this->lgpile, int);
  CREER_T1 (this->plusgrand, lgpile, real);


  /* Creation des autres structures de travail */
  /* ltri: va contenir tous les triangles;
     les ntri premiers sont ceux donn�s */
  CREER_T1 (this->ltri, this->maxtri, Triangle);
  /* Creer les structures des resultats */
  CREER_T1 (this->results, nfun, real);
  CREER_T1 (this->abserr, nfun, real);

  /* Affectation des 1iers �l�ments des piles */
  for (i = 0; i < ntri; i++)
    {
      this->lpolya[i] = listpolya[i];
      this->lpolyb[i] = listpolyb[i];
      this->ltri[i] = lestriangles[i];
    }				// fin i

}				// fin constructeur

///////////////////////////////////////////////////////
// Destructeur
///////////////////////////////////////////////////////

Adapt::~Adapt ()
{
  // D�sallocations:
  DETRU_T1 (this->abserr);
  DETRU_T1 (this->results);
  DETRU_T1 (this->plusgrand);
  DETRU_T1 (this->lpolyb);
  DETRU_T1 (this->lpolya);
  DETRU_T1 (this->ltri);
  DETRU_T2 (this->errors, this->lgpile);
  DETRU_T2 (this->values, this->lgpile);


}

//////////////////////////////////////////////////////////
// M�thodes d'acc�s
//////////////////////////////////////////////////////////
int
Adapt::GetIfail () const
{
  return (this->ifail);
}

//////////////////////////////////////////////////////////
int
Adapt::GetNregions () const
{
  return (this->nregions);
}

//////////////////////////////////////////////////////////
int
Adapt::GetMaxnreg () const
{
  return (this->maxnreg);
}



//////////////////////////////////////////////////////////
int
Adapt::GetNeval () const
{
  return (this->neval);
}

//////////////////////////////////////////////////////////
// GetResult0 renvoie le r�sultat de la 1iere fonction
//////////////////////////////////////////////////////////

real Adapt::GetResult0 () const
{
  return (this->results[0]);
}

//////////////////////////////////////////////////////////
// GetAbserr0 renvoie l'abser de la 1iere fonction
//////////////////////////////////////////////////////////

real Adapt::GetAbserr0 () const
{
  return (this->abserr[0]);	// Ici une seule fonction
}

//////////////////////////////////////////////////////////
// impression de plusgrand
//////////////////////////////////////////////////////////
void
Adapt::PrintPlusgrand () const
{
  int i;
  for (i = 0; i < this->nregions; i++)
    cout << this->plusgrand[i] << " ";
  cout << endl;
}




//////////////////////////////////////////////////////////
// Pilote des calculs 
//////////////////////////////////////////////////////////
void
Adapt::Integration (Integrand funsub, const int numbera, const int numberb)
{
  int i1, i2, i3, i4, indice, i, j;
  int pointeur, top, vacant, polyolda, polyoldb;
  Triangle ancien;
  Boolean fini;
  Point p12, p13, p23;
  Rule rule13;
  PileTr piletr (this->maxtri);	// la pile des triangles
  char moi[] = "Adapt::Integration";
/* Pour stocker les messages */
  char errmess[CHAR_MAX];



  /* **************** Initialisation. *************** */
  for (i = 0; i < this->nfun; i++)
    this->results[i] = this->abserr[i] = 0.0;
  this->ifail = this->neval = this->nregions = 0;

  /*   Appliquer la r�gle sur les ntri triangles. */
  for (i = 0; i < this->ntri; i++)
    {
      rule13.Apply (this->ltri[i], funsub, this->nfun,
		    this->lpolya[i], this->lpolyb[i],
		    this->values[i], this->errors[i], this->plusgrand[i]);


      this->neval += 37;
      this->nregions += 1;
      for (j = 0; j < this->nfun; j++)
	{
	  this->results[j] += this->values[i][j];
	  this->abserr[j] += this->errors[i][j];
	}
    }

/* Sauvegarder les resultats */
  for (i = 0; i < this->ntri; i++)
    {
      indice = i + 1;		// nbre de sous-regions
      piletr.Ajout (indice, this->plusgrand, indice);
    }


  /* A-t-on fini? */
  fini = True;
  if (this->nregions < this->minnreg)
    fini = False;
  else
    {

      for (j = 0; j < this->nfun; j++)
	{
	  if ((this->abserr[j] > this->epsrel * fabs (this->results[j])) &&
	      (this->abserr[j] > this->epsabs))
	    {
	      fini = False;
	      break;
	    }
	}			// fin j
    }				// fin else


  /* **************** Fin de l'initialisation. *************** */

/* ******************** d�but boucle ********************* */
  /* Boucler tant que l'erreur est trop grande
     et que le nombre de r�gions (nregions+3) est inf.
     au maximum de r�gions tol�r�es (maxnreg) */
  while (fini == False)
    {
      /* On cree 4 triangles mais comme le 1ier est mis 
         � l'emplacement du pr�c�dent, on en ajoute 3 en pratique.
       */
      if (this->nregions + 3 > this->maxnreg)
	{
	  this->ifail = CALI_MAXITER;
	  break;
	}

      //Rque: pointeur et top sont des indices, donc commencent � 0 (=> -1)
      pointeur = this->nregions + 3 - 1;
      top = piletr.Get (1) - 1;

      /* Verification des dimensions */
      if ((pointeur >= this->maxtri) || (top >= this->maxtri))
	{
	  sprintf (errmess,
		   "Internal error pointeur %d top=%d maxtri=%d on polygons %d,%d\n",
		   pointeur, top, maxtri, numbera, numberb);
	  ecrmess (CALI_ERINTERNAL, moi, errmess, True);
	  //     Fatal error: ecrmess se termine par     exit(1);
	}

      vacant = top;
      for (j = 0; j < this->nfun; j++)
	{
	  this->results[j] -= this->values[top][j];
	  this->abserr[j] -= this->errors[top][j];
	}

      /* Sauver les sommets du triangle top ainsi que
         les nos des polygones peres (indices des sous-polygones
         convexes dans lesquels se trouve ce triangle */
      ancien = this->ltri[top];
      polyolda = lpolya[top];
      polyoldb = lpolyb[top];

      /* Maj de la pile */
      piletr.Ote (this->nregions, this->plusgrand);

      /* Calcul des 4 nouveaux triangles */
      i1 = top;
      i2 = pointeur - 2;
      i3 = pointeur - 1;
      i4 = pointeur;
      /* Verification des dimensions */
      if ((i1 >= this->lgpile) ||
	  (i2 >= this->lgpile) ||
	  (i3 >= this->lgpile) || (i4 >= this->lgpile))
	{
	  this->ifail = CALI_MAXITER;
	  break;
	}

      /* Sauvegarde des polygones p�res */
      lpolya[i1] = lpolya[i2] = lpolya[i3] = lpolya[i4] = polyolda;
      lpolyb[i1] = lpolyb[i2] = lpolyb[i3] = lpolyb[i4] = polyoldb;

      /* Cr�ation des triangles */
      p12 = (ancien.Sommet (1) + ancien.Sommet (2)) / 2;
      p13 = (ancien.Sommet (1) + ancien.Sommet (3)) / 2;
      p23 = (ancien.Sommet (2) + ancien.Sommet (3)) / 2;
      this->ltri[i1] = Triangle (ancien.Sommet (1), p12, p13);
      this->ltri[i2] = Triangle (p12, ancien.Sommet (2), p23);
      this->ltri[i3] = Triangle (p13, p23, ancien.Sommet (3));

      this->ltri[i4] = Triangle (p23, p13, p12);




      /* Appliquer la r�gle sur les 4 triangles */
      for (i = 1; i <= 4; i++)
	{
	  if (i == 1)
	    indice = vacant;
	  else
	    indice = this->nregions + i - 1;

	  rule13.Apply (this->ltri[indice], funsub, this->nfun,
			this->lpolya[indice], this->lpolyb[indice],
			this->values[indice], this->errors[indice],
			this->plusgrand[indice]);

	  /* Ajouter les nouvelles contributions aux tableaux
	     de r�sultats et d'erreurs */
	  for (j = 0; j < this->nfun; j++)
	    {
	      this->results[j] += this->values[indice][j];
	      this->abserr[j] += this->errors[indice][j];
	    }			// fin j
	}			// fin i

      this->neval += 4 * 37;

      /* Maj de la pile */
      for (i = 0; i < 4; i++)
	{
	  // On rajoute 1 � indice car Ajout consid�re
	  // les positions � partir de 1
	  if (i == 0)
	    indice = vacant + 1;
	  else
	    indice = this->nregions + i + 1;
	  j = this->nregions + i + 1;
	  piletr.Ajout (j, this->plusgrand, indice);
	}			// fin i

      this->nregions += 4;


      /* A-t-on fini? */
      fini = True;
      if (this->nregions < this->minnreg)
	fini = False;
      else
	{

	  for (j = 0; j < this->nfun; j++)
	    {
	      if ((this->abserr[j] > this->epsrel * fabs (this->results[j]))
		  && (this->abserr[j] > this->epsabs))
		{
		  fini = False;
		  break;
		}
	    }			// fin j
	}			// fin else
    }				// fin while
/* ******************** fin boucle ********************* */

/* Cumul des r�sultats et erreurs de chaque r�gion */

  for (j = 0; j < this->nfun; j++)
    {
      this->results[j] = this->abserr[j] = 0.0;
      for (i = 0; i < this->nregions; i++)
	{
	  this->results[j] += this->values[i][j];
	  this->abserr[j] += this->errors[i][j];
	}
    }



}				// fin Integration

////////////////////////////////////////////////////
