#ifndef _UNVECTOR_H
#define _UNVECTOR_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 : classe Vecteur de real    |
--------------------------------------------------*/
#include "caliinclc.h"
#include "calireal.h"

class Vector
{

public:
  Vector (unsigned int longueur);
  Vector  operator* (const real r) const;
    Vector & operator+= (const Vector & v);
    Vector & operator= (const Vector & v);

  const real & operator[] (const int ind) const;
    real & operator[] (const int ind);

   ~Vector ();
 void   Detru ();
 int getSize() const;

private:
  int taille;
  real *x;
};
////////////////////////////////////////
inline int Vector::getSize() const {
  return(this->taille);
}

#endif
