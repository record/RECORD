#ifndef _RULE_H
#define _RULE_H
/*--------------- COPYRIGHT ------------------------
| INRA - Unit� MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Derniere mise a jour : 24 Jan 2007               |
| Role                 :  classe de la r�gle de    |
|                         cubature                 |
--------------------------------------------------*/
#include "caliinclc.h"
#include "Integrand.h"

//////////////////////////////////////////////////
class Rule
{
public:
  Rule ();

  void Apply (Triangle & triangle,
	      Integrand funsub, int nfun, int polya, int polyb,
	      real * laValeur, real * lErreur, real & plusgrand);
};

//////////////////////////////////////////////////
#endif
