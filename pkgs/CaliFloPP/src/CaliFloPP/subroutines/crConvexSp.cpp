/*--------------- COPYRIGHT ------------------------
| INRA - Unite MIA de Jouy en Josas                |
--------------------------------------------------*/

/*--------------- IDENTIFICATION PRODUIT -----------
| Role                 : creation des sous-polygones|
|                        convexes                   |
---------------------------------------------------*/
///////////////////////////////////////////////////////

#include "caliinclc.h"
#include "geom.h"
#include "calierror.h"
#include "calimacros.h"
#include "util.h"

/////////////////////////////////////////////////////////////////
// Fonction:
// Calculer l'indice de l'inverse d'une diagonale ou c�t� dans le
// tableau PolygonDiagonals
// Arguments d'entr�e:
// idiag: indice de la diagonale ou c�t�
// ndiagcot: nombre de c�t�s+diagonales
// Retour:
// l'indice de l'inverse de idiag
/////////////////////////////////////////////////////////////////
int
indexInv (int idiag, int ndiagcot)
{
  if (idiag >= ndiagcot)
    return (idiag - ndiagcot);
  else
    return (idiag + ndiagcot);
}				// fin indexInv

/////////////////////////////////////////////////////////////////
// Fonction:
// Affecter les indicateurs de convexite de la diagonale
// d'indice iladiag et de son inverse d'indice iladiaginv.
// Arguments d'entree:
// PolygonVertices: les coordonnees des sommets  anti-clockwise
// PolygonDiagonals: l'ensemble des cotes et diagonales:
// d'abord dans le sens original puis dans l'autre sens.
// ndiagcot2: le nbre d'elements dans PolygonDiagonals
// iladiag: indice dans  PolygonDiagonals de la diagonale
// a mettre a jour.
// iladiaginv: indice de son inverse.
// majfrom: Si majfrom = T, il faut mettre a jour le convexfrom;
// Sinon, c'est le convexto.
// En sortie, le convexfrom (majfrom=T) ou le convexto (majfrom=F),
//  de PolygonDiagonals[iladiag] est modifie, ainsi que
// le convexto (majfrom=T) et le convexfrom (majfrom=F) de 
// la diagonale inverse.
// Arguments de sortie:
//  vfromdte, vfromgche: les indices des diagonales
// qui partent de l'origine de la diagonale et qui sont les plus
// proches a droite et a gauche respectivement.
//  vtodte, vtogche: les indices des diagonales
// qui partent du but de la diagonale et qui sont les plus
// proches a droite et a gauche respectivement.
// Retour:
// 0 ou un code d'erreur negatif.
// Algorithme:
// Si majfrom=T: Soit (v1,v3) la diagonale iladiag.
// On recherche les diagonales les plus proches
// a droite et a gauche de la diagonale iladiag
// qui partent de v1.
// Soient vfromdte=(v1,a1) et vfromgche=(v1,a0),
//  ces 2 diagonales respectivement
// a droite et a gauche.
// L'angle qu'elles forment avec v1 est convexe
// si v1 est a gauche de (a1,a0).
// Si majfrom=F: Soit (v3,v1) la diagonale iladiag
//  et (v1,a1) et (v1,a0) idem que lorsque majfrom=T.
//  L'angle qu'elles forment avec v1 est convexe
// si v1 est a gauche de (a0,a1).
/////////////////////////////////////////////////////////////////
int
setConvexFromTo (POLYGON_STRUCT * PolygonVertices,
		 DIAGONAL_STRUCT * PolygonDiagonals,
		 int ndiagcot2, int iladiag, int iladiaginv,
		 Boolean majfrom,
		 int &vfromgche, int &vfromdte, int &vtogche, int &vtodte)
{
  DIAGONAL_STRUCT ladiag;	// La diag courante
  int idiag, iv1, iv3, ipt, iptg, iptd;
  tPointi v1, v3, a1, a0, pt;

  real acoscour, acosmind, acosming;

  Boolean gche;
  /* Pour stocker les messages */
  char moi[] = "setConvexFromTo";
  char errmess[CHAR_MAX];


  ladiag = PolygonDiagonals[iladiag];


  if (majfrom == True)
    {
      iv1 = ladiag.vfrom;
      iv3 = ladiag.vto;
    }
  else
    {
      iv3 = ladiag.vfrom;
      iv1 = ladiag.vto;
    }
  v1[XX] = PolygonVertices[iv1].xv;
  v1[YY] = PolygonVertices[iv1].yv;
  v3[XX] = PolygonVertices[iv3].xv;
  v3[YY] = PolygonVertices[iv3].yv;

  // Indices des extremites vto des diagonales a droite et a gauche
  // Les initialiser  permettra de savoir si on en a trouve.
  iptd = -1;
  iptg = -1;

  // Rechercher les extremites to des diag essentielles
  // qui partent de iv1 et qui sont les plus proches 
  // a dte et a gche de ladiag

  // 5/12/2007: Rajout de epsilon a pi, pour eviter l'egalite
  // sinon, pbe quand 2 diag essentielles sont alignees
  acosmind = acosming = M_PI + 0.01;

  for (idiag = 0; idiag < ndiagcot2; idiag++)
    {
      if ((PolygonDiagonals[idiag].exist == False) ||
	  (idiag == iladiag) || (PolygonDiagonals[idiag].vfrom != iv1))
	continue;

      // Parmi toutes les diago essentielles qui
      // partent de v1, choisir la plus proche de ladiag



      pt[XX] = PolygonVertices[PolygonDiagonals[idiag].vto].xv;
      pt[YY] = PolygonVertices[PolygonDiagonals[idiag].vto].yv;
      ipt = PolygonDiagonals[idiag].vto;


      // Voir si cette autre extremite
      //  est a dte ou gche de v1,v3 (majfrom=T) 
      // ou de (v3,v1) (majfrom=F)
      if (majfrom == True)
	{
	  gche = LeftOn (pt, v1, v3);
	}
      else
	{
	  gche = LeftOn (pt, v3, v1);
	}
      // Le calcul de l'angle permet de determiner
      // celle qui est la plus proche
      acoscour = Angle3i (v3, v1, pt);

      // La plus proche a dte:
      if ((acoscour > 0) && (acoscour < acosmind) && (gche == False))
	{
	  acosmind = acoscour;
	  a1[XX] = pt[XX];
	  a1[YY] = pt[YY];
	  iptd = ipt;
	  if (majfrom == True)
	    vfromdte = idiag;
	  else
	    vtodte = idiag;
	}


      // La plus proche a gche:
      if ((acoscour > 0) && (acoscour < acosming) && (gche == True))
	{

	  acosming = acoscour;
	  a0[XX] = pt[XX];
	  a0[YY] = pt[YY];
	  iptg = ipt;

	  // On stocke l'indice de la diag qui est le plus
	  // a gauche du vto car elle sera utile 
	  // lors de la construction des sous-polys
	  // pour le parcours des diagonales
	  if (majfrom == True)
	    vfromgche = idiag;
	  else
	    vtogche = idiag;
	}			// fin gche


    }				// fin for idiag

  // Maintenant, en a0,  il y a l'autre extremite de la diag (ou cote) la plus
  // proche a gche de v1,v3 (majfrom=T) ou de (v3,v1) (majfrom=F)
  // En a1, il y a l'autre extremite de la diag (ou cote) la plus
  // proche a dte  de v1,v3 (majfrom=T) ou de (v3,v1) (majfrom=F)


  // Verification au cas ou: Si c'est bien programme on n'y passe jamais
  if ((iptd == -1) && (iptg == -1))
    {
      sprintf (errmess,
	       "Internal error: on aurait du trouver au moins les 2 cotes adjacents\nLa diagonale iladiag va de %d a %d \n",
	       PolygonDiagonals[idiag].vfrom, PolygonDiagonals[idiag].vto);
      return (ecrmess (CALI_ERINTERNAL, moi, errmess, True));
    }




  if (majfrom == True)
    {
      if ((iptd == -1) || (iptg == -1))
	// Les 2 sommets adjacents sont du meme cote
	PolygonDiagonals[iladiag].convexfrom = False;
      else
	PolygonDiagonals[iladiag].convexfrom = LeftOn (v1, a1, a0);

    }				// fin (majfrom == True)
  else
    {
      if ((iptd == -1) || (iptg == -1))
	// Les 2 sommets adjacents sont du meme cote
	PolygonDiagonals[iladiag].convexto = False;
      else
	PolygonDiagonals[iladiag].convexto = LeftOn (v1, a0, a1);

      // Maj des convexites de la diag inverse
      PolygonDiagonals[iladiaginv].convexto =
	PolygonDiagonals[iladiag].convexfrom;

      PolygonDiagonals[iladiaginv].convexfrom =
	PolygonDiagonals[iladiag].convexto;

    }

  return (OK);
}				// fin setConvexFromTo

/////////////////////////////////////////////////////////////////
// Fonction:
// Mettre a jour les attributs dnext des diagonales
// adjacentes a la diagonale idiag.
// Programme appele quand le statut "exist" de la diagonale idiag
// a change. 
// Arguments d'entree:
// PolygonDiagonals: le tableau des cotes et diagonales
// idiag: la diagonale dont le statut a change
// vfromgche, vfromdte: les indices des diagonales qui partent
// de son vfrom a sa gauche et a sa droite
// vtogche, vtodte: les indices des diagonales qui partent
// de son vto  a sa gauche et a sa droite
/////////////////////////////////////////////////////////////////
void
majDnext (DIAGONAL_STRUCT * PolygonDiagonals,
	  int ndiagcot, int idiag,
	  int vfromgche, int vfromdte, int vtogche, int vtodte)
{

  // On est sur que le suivant est oriente dans le bon sens
  // car on prend l'arete qui part sur la gauche
  if (PolygonDiagonals[idiag].exist == False)
    {
      // La diag a ete supprimee
      PolygonDiagonals[indexInv (vtodte, ndiagcot)].dnext = vtogche;
      PolygonDiagonals[indexInv (vfromgche, ndiagcot)].dnext = vfromdte;

    }
  else
    {

      // La diag a ete rajoutee
      PolygonDiagonals[indexInv (vfromgche, ndiagcot)].dnext = idiag;
      PolygonDiagonals[idiag].dnext = vtogche;
      PolygonDiagonals[indexInv (vtodte, ndiagcot)].dnext =
	indexInv (idiag, ndiagcot);
      PolygonDiagonals[indexInv (idiag, ndiagcot)].dnext = vfromdte;


    }
}				// fin majDnext



/////////////////////////////////////////////////////////////////
// Fonction:
// Boucle de mise a jour des indicateurs de convexite, d'existence
// et l'arete suivante (dnext)
// des diagonales stockees dans PolygonDiagonals.
// Les indicateurs des diagonales inverses sont mis a jour 
// dans la foulee.
// Arguments d'entree:
// PolygonVertices: les coordonnees des sommets  anti-clockwise
// PolygonDiagonals: l'ensemble des cotes et diagonales:
// d'abord dans le sens original puis dans l'autre sens.
// Les attributs convexfrom, convexto, exist, dnext
//  sont modifies en sortie.
// nvertices: le nombre de sommets
// ndiagcot: le nbre de cotes + diagonales dans le sens original.
// ndiagcot2: le nbre d'elements dans PolygonDiagonals
// (i.e ndiagcot*2)
// Note:
// L'indicateur d'essentialite, exist, est affecte ainsi:
// exist=F si les 2 indicateurs de convexite (from et to)
// sont vrais, et exist=T si au moins un des 2 est faux.
/////////////////////////////////////////////////////////////
void
crConvexDiag (POLYGON_STRUCT * PolygonVertices,
	      DIAGONAL_STRUCT * PolygonDiagonals,
	      int nvertices, int ndiagcot, int ndiagcot2)
{
  int idiag, idiaginv, vfromgche, vfromdte, vtogche, vtodte;
  Boolean existence, convexitefrom, convexiteto;


  for (idiag = nvertices; idiag < ndiagcot; idiag++)
    {




      // Sauvegarde des indicateurs de convexite et existence
      // car on aura besoin de savoir si on les a changes
      convexitefrom = PolygonDiagonals[idiag].convexfrom;
      convexiteto = PolygonDiagonals[idiag].convexto;
      existence = PolygonDiagonals[idiag].exist;
      // Indice de la diagonale inverse
      idiaginv = ndiagcot + idiag;


      // Rajout du 13/12/2007
      vfromgche = vfromdte = vtogche = vtodte = -1;

      // Maj de la convexite from de la diag idiag et to de son inverse:
      setConvexFromTo (PolygonVertices,
		       PolygonDiagonals,
		       ndiagcot2, idiag, idiaginv, True,
		       vfromgche, vfromdte, vtogche, vtodte);
      // Maj des convexites to
      setConvexFromTo (PolygonVertices,
		       PolygonDiagonals,
		       ndiagcot2, idiag, idiaginv, False,
		       vfromgche, vfromdte, vtogche, vtodte);

      // Maj de l'indicateur exist
      if ((convexitefrom != PolygonDiagonals[idiag].convexfrom) ||
	  (convexiteto != PolygonDiagonals[idiag].convexto))
	{
	  // Si l'une au moins des convexites a change, maj de 
	  // l'exist
	  if ((PolygonDiagonals[idiag].convexfrom == True) &&
	      (PolygonDiagonals[idiag].convexto == True))
	    PolygonDiagonals[idiag].exist = False;
	  else
	    PolygonDiagonals[idiag].exist = True;
	  // Maj de l'exist de l'inverse
	  PolygonDiagonals[idiag + ndiagcot].exist =
	    PolygonDiagonals[idiag].exist;
	}			// fin Maj de l'indicateur exist

      // Si l'existence a change, maj des dnext des diagonales adjacentes



      if (existence != PolygonDiagonals[idiag].exist)
	{


	  majDnext (PolygonDiagonals, ndiagcot,
		    idiag, vfromgche, vfromdte, vtogche, vtodte);
	}

    }				// fin idiag

}				// fin crConvexDiag

////////////////////////////////////////////////////////////
// Fonction:
// Mettre a jour les indicateurs de convexite de toutes
// les diagonales.
// Arguments:
// PolygonVertices: les coordonnees des sommets anticlockwise
// PolygonDiagonals: 
// En entree, dans les elements nvertices a ndiagonals,
// il y a les diagonales calculees par Triangulate.
// C.a.d les vfrom et vto des elements nvertices a
//  nvertices+ndiagonales sont remplis.
// En sortie, dans les elements 0 a nvertices, il y a
// les cotes, et, a partir de l'element (nvertices+ndiagonals),
// la duplication des (nvertices+ndiagonals) premiers elements
// mais avec le sens vto, vfrom inverse.
// Les indicateurs de convexite et d'existence ainsi que
// l'indice de la diagonale (ou cote) qui suit (dnext) 
// sont calcules.
// nvertices: le nombre de sommets
// ndiagonals: le nombre de diagonales
// diagonalsize: taille de PolygonDiagonals
// Retour:
// OK ou un code d'erreur negatif
/////////////////////////////////////////////////////////
int
crConvexSp (POLYGON_STRUCT * PolygonVertices,
	    DIAGONAL_STRUCT * PolygonDiagonals,
	    int nvertices, int ndiagonals, int diagonalsize)
{
  int ndiagcot, ndiagcot2, ivert, idiag, idiag2;


  // Initialisations
  ndiagcot = nvertices + ndiagonals;
  // nbre total d'elements dans PolygonDiagonals:
  // celui-ci contiendra les cotes, les diagonales
  // et les memes inverses.
  ndiagcot2 = 2 * ndiagcot;

  if (ndiagcot2 >= diagonalsize)
    {
      // Pbe de taille insuffisante
      return (CALI_ERTRI3);
    }


  // Mettre les cotes dans les premiers elements de
  // PolygonDiagonals de facon a� les traiter comme 
  // des diagonales essentielles
  // c.a.d comme des bordures  de polys convexes.
  // Les cotes sont essentiels
  idiag2 = ndiagcot;
  for (ivert = 0; ivert < nvertices; ivert++)
    {
      PolygonDiagonals[ivert].convexfrom = Notknown;
      PolygonDiagonals[ivert].convexto = Notknown;
      PolygonDiagonals[ivert].exist = True;
      PolygonDiagonals[ivert].dnext = ivert + 1;
      PolygonDiagonals[ivert].vfrom = ivert;
      PolygonDiagonals[ivert].vto = (ivert + 1);
      // Les cotes inverses sont stockes dans les elements 
      // nvertices+ndiagonals suivants.
      PolygonDiagonals[idiag2].convexfrom = Notknown;
      PolygonDiagonals[idiag2].convexto = Notknown;
      PolygonDiagonals[idiag2].dnext = -1;
      PolygonDiagonals[idiag2].vfrom = PolygonDiagonals[ivert].vto;
      PolygonDiagonals[idiag2].vto = PolygonDiagonals[ivert].vfrom;
      PolygonDiagonals[idiag2++].exist = Notknown;
    }
  // Le dernier sommet est zero:
  PolygonDiagonals[nvertices - 1].vto = 0;
  PolygonDiagonals[nvertices - 1].dnext = 0;
  PolygonDiagonals[2 * nvertices + ndiagonals - 1].vfrom = 0;

  // Initialiser les indicateurs de convexite et
  // d'existence des diagonales calculees par Triangulate
  // et creer leurs inverses.
  idiag2 = ndiagcot + nvertices;
  for (idiag = nvertices; idiag < ndiagcot; idiag++)
    {
      PolygonDiagonals[idiag].convexfrom = Notknown;
      PolygonDiagonals[idiag].convexto = Notknown;
      PolygonDiagonals[idiag].exist = Notknown;
      PolygonDiagonals[idiag].dnext = -1;
      // Les dupliquer en inversant le from et le to
      PolygonDiagonals[idiag2].vfrom = PolygonDiagonals[idiag].vto;
      PolygonDiagonals[idiag2].vto = PolygonDiagonals[idiag].vfrom;
      PolygonDiagonals[idiag2].convexfrom = Notknown;
      PolygonDiagonals[idiag2].convexto = Notknown;
      PolygonDiagonals[idiag2].dnext = -1;
      PolygonDiagonals[idiag2++].exist = Notknown;
    }				// fin idiag




  // Maj les indicateurs de convexite, le exist et le dnext 
  // de toutes les diag. 
  // Ceux des diag inverses seront affectes dans la foulee.
  crConvexDiag (PolygonVertices,
		PolygonDiagonals, nvertices, ndiagcot, ndiagcot2);




  return (OK);

}				// fin crConvexSp

////////////////////////////////////

/////////////////////////////////////////////////
// Fonction:
// Trianguler un polygone en vue d'extraire les sous-polys convexes.
// Les diagonales sont stockees dans PolygonDiagonals a partir
// du nvertices ieme element (les nvertices premiers correspondront
// aux cotes du poly)
// Arguments d'entree:
// PolygonVertices: les coordonnees des sommets anti-clockwise
// vertices: la liste chainee des sommets anti-clockwise
// Modifiee en sortie
// nvertices: le nombre de sommets
// diagonalsize: taille de PolygonDiagonals
// errident: l'identification du poly courant
// (pour l'afficher quand il y a une erreur)
// Argument de sortie:
// PolygonDiagonals: alloue avant l'appel.
// En sortie, a partir de l'element d'indice nvertices,
// il y a les diagonales.
// Retour: le nombre de diagonales ou un code d'erreur negatif
/////////////////////////////////////////////////
int
Triangulate (POLYGON_STRUCT * PolygonVertices,
	     DIAGONAL_STRUCT * PolygonDiagonals,
	     tVertex vertices,
	     int nvertices, int diagonalsize, char *errident)
{
  tVertex v0, v1, v2, v3, v4;	// five consecutive vertices
  int code, idiag, ndiagonals = 0;
  int maxidiag = diagonalsize / 2;	// max index in PolygonDiagonals
  // for the diagonals
  int n = nvertices;		// number of vertices; shrinks to 3
  Boolean earfound;		// for debugging and error detection only
  /* Pour stocker les messages */
  char moi[] = "Triangulate";
  char errmess[CHAR_MAX];



  EarInit (vertices);
  // printf("\nnewpath\n");

  // Each step of outer loop removes one ear
  idiag = nvertices;		// indice de la diag courante dans PolygonDiagonals
  while (n > 3)
    {
      // Inner loop searches for an ear
      v2 = vertices;
      earfound = False;

      do
	{
	  if (v2->ear)
	    {

	      if (idiag >= maxidiag)
		{
		  sprintf (errmess,
			   "Too many diagonals (maximum  = %d)\n%s",
			   (maxidiag - MAX_VERTICES), errident);
		  return (ecrmess (CALI_ERTRI2, moi, errmess));
		}

	      earfound = True;
	      // Ear found. Fill variables
	      v3 = v2->next;
	      v4 = v3->next;
	      v1 = v2->prev;
	      v0 = v1->prev;

	      // (v1,v3) is a diagonal

	      // save from endpoint
	      PolygonDiagonals[idiag].vfrom = v1->vnum;
	      // save to endpoint
	      PolygonDiagonals[idiag].vto = v3->vnum;

	      idiag++;
	      ndiagonals++;	// increment number of diagonals

	      // Update earity of diagonal endpoints
	      v1->ear = Diagonal (v0, v3, vertices);
	      v3->ear = Diagonal (v1, v4, vertices);

	      // Cut off the ear v2
	      v1->next = v3;
	      v3->prev = v1;

	      vertices = v3;	// In case the head was v2

	      n--;

	      break;		// out of inner loop; resume outer loop

	    }			// end if ear found

	  v2 = v2->next;
	}
      while (v2 != vertices);


      if (!earfound)
	{
	  sprintf (errmess,
		   "%%Error in Triangulate:  No ear found.  (Are coordinates clockwise?)\n%s",
		   errident);
	  return (ecrmess (CALI_ERTRI1, moi, errmess));
	}

    }				// end outer while loop



  // printf("closepath stroke\n\n");

// Mettre a jour les convexites de toutes les diagonales
  if ((code = crConvexSp (PolygonVertices, PolygonDiagonals,
			  nvertices, ndiagonals, diagonalsize)) < 0)
    {
      sprintf (errmess,
	       "%%Error in Triangulate: not enough memory size. (MAX_VERTICES great enough?)\n%s",
	       errident);
      return (ecrmess (code, moi, errmess));
    }
  else
    return ndiagonals;
}				// fin Triangulate

/////////////////////////////////////////////////////
// Fonction:
// Renvoyer le no de la diag essentielle qui part de iladiag
// et qui est la plus a gche de son vfrom, vto
/////////////////////////////////////////////////////////
int
chercheDiagSuiv (int iladiag,
		 int ndiagcot,
		 POLYGON_STRUCT * PolygonVertices,
		 DIAGONAL_STRUCT * PolygonDiagonals)
{
  int idiag, vfrom, vto, leto;
  int retour = -1;
  real acoscour, acosming;
  Boolean gche;
  tPointi ptfrom, ptto, ptvto;

  acosming = M_PI;
  vfrom = PolygonDiagonals[iladiag].vfrom;
  vto = PolygonDiagonals[iladiag].vto;
  // Les points qui correspondent aux extremit�s de la diag:
  ptfrom[XX] = PolygonVertices[vfrom].xv;
  ptfrom[YY] = PolygonVertices[vfrom].yv;
  ptto[XX] = PolygonVertices[vto].xv;
  ptto[YY] = PolygonVertices[vto].yv;

  for (idiag = 0; idiag < (2 * ndiagcot); idiag++)
    {
      // On saute la diag elle-m�me, son inverse,
      // celles qui ne sont pas essentielles,
      // et celles qui ne partent pas de son vto
      if ((idiag == iladiag) ||
	  (idiag == indexInv (iladiag, ndiagcot)) ||
	  (PolygonDiagonals[idiag].exist == False) ||
	  (PolygonDiagonals[idiag].vfrom != PolygonDiagonals[iladiag].vto))
	continue;

      leto = PolygonDiagonals[idiag].vto;

      ptvto[XX] = PolygonVertices[leto].xv;
      ptvto[YY] = PolygonVertices[leto].yv;
      gche = LeftOn (ptfrom, ptto, ptvto);

      acoscour = Angle3i (ptvto, ptto, ptfrom);
      // La plus proche a gche:
      if ((acoscour <= acosming) && (gche == True))
	{
	  retour = idiag;
	  acosming = acoscour;
	}
    }				// fin idiag


  return (retour);
}				// fin chercheDiagSuiv



/////////////////////////////////////////////////////
// Fonction:
//  Creation des sous-polygones convexes d'un polygone donne.
// Arguments d'entree:
// verbose=T si on veut afficher les diagonales et les
// sous-polys convexes crees
// PolygonVertices: les coordonnees des sommets  anti-clockwise
// PolygonDiagonals: l'ensemble des cotes et diagonales:
// d'abord orientes dans le sens origine puis dans l'autre sens.
// Les indicateurs exist sont modifies en sortie.
// nvertices: le nombre de sommets,
//  ndiagonals: le nombre de diagonales
// Arguments de sortie:
// poly: les sous-polygones convexes anti-clockwise.
// ns: le nombre de sommets de chacun des sous-polygones convexes
// Retour:
// le nombre de sous-polygones convexes ou code d'erreur negatif.
// Algorithme:
// A partir d'une diagonale essentielle, on parcourt 
// toutes ses suivantes jusqu'a revenir au point de depart.
// Les diagonales incluses dans le sous-poly sont marquees
// comme inexistantes au fur et a mesure.
////////////////////////////////////////////////////////////
int
makePolyLeft (Boolean verbose,
	      POLYGON_STRUCT * PolygonVertices,
	      DIAGONAL_STRUCT * PolygonDiagonals,
	      int nvertices, int ndiagonals, tPolygoni * poly, int ns[])
{

  int depart, ndiagcot, idiag, idiagstart, np, nsommets, isom;
  /* Pour stocker les messages */
  char moi[] = "makePolyLeft";
  char errmess[CHAR_MAX];


  np = 0;			// no du sous-poly courant; en sortie: nbre de sous-polys

  ndiagcot = nvertices + ndiagonals;	// nbre de cotes+diago (dans le sens original)

  idiagstart = 0;		// la diagonale de depart du poly courant

  // Chercher la 1iere diag essentielle (ou cote) parmi celles
  // non inversees




  while (idiagstart < ndiagcot)
    {
      if (PolygonDiagonals[idiagstart].exist != True)
	{
	  idiagstart++;
	  continue;
	}
      // Debut d'un sous-poly
      if (verbose == True)
	printf ("%%%dth polygon\n", np + 1);
      nsommets = 0;
      idiag = idiagstart;
      depart = PolygonDiagonals[idiag].vfrom;
      // Enregistrer le depart:
      poly[np][nsommets][XX] = PolygonVertices[depart].xv;
      poly[np][nsommets++][YY] = PolygonVertices[depart].yv;
      if (verbose == True)
	printf (" %d ", depart);

      do
	{
	  isom = PolygonDiagonals[idiag].vto;
	  if (verbose == True)
	    printf ("- %d ", isom);

	  if (nsommets >= MAX_VERTICES)
	    {
	      return (CALI_ERTRI3);
	    }

	  poly[np][nsommets][XX] = PolygonVertices[isom].xv;
	  poly[np][nsommets++][YY] = PolygonVertices[isom].yv;
	  PolygonDiagonals[idiag].exist = False;

	  // Un next pas attribu�
	  // (17/02/2007: cela arrive quand pls diag successives
	  // sont align�es)
	  if (PolygonDiagonals[idiag].dnext == -1)
	    {
	      // Chercher parmi les diag essentielles
	      // celle la plus a gche qui part du vto
	      PolygonDiagonals[idiag].dnext =
		chercheDiagSuiv (idiag, ndiagcot,
				 PolygonVertices, PolygonDiagonals);

	      if (PolygonDiagonals[idiag].dnext == -1)
		{
		  sprintf (errmess,
			   "Internal error: un next de %d from %d a %d pas connu\n",
			   idiag,
			   PolygonDiagonals[idiag].vfrom,
			   PolygonDiagonals[idiag].vto);
		  return (ecrmess (CALI_ERINTERNAL, moi, errmess, False));
		}
	    }			// fin de Un next pas attribu�

	  idiag = PolygonDiagonals[idiag].dnext;
	}
      while (PolygonDiagonals[idiag].vto != depart);
      // Invalider la derniere diagonale parcourue
      PolygonDiagonals[idiag].exist = False;
      // fin du poly
      if (verbose == True)
	// printf ("; %d vertices \n", nsommets);
	printf ("\n");
      // Erreur si le souspoly a moins de 3 sommets:
      if (nsommets < 3)
	return (CALI_ERTRI4);
      ns[np++] = nsommets;
      idiagstart++;
    }				//  fin (idiag < ndiag)

  return (np);

}				// fin makePolyLeft


/////////////////////////////////////////////////////
// Fonction:
// Supprimer les sommets alignes
// dans les sous-polys convexes
// Entrees-sorties: 
// ns: le nbre de sommets des sous-polys convexes
// poly: leurs coordonnees
/////////////////////////////////////////////////////
int
supVertices (Boolean verbose, int np, tPolygoni * poly, int ns[])
{
  // Oter les sommets alignes
  int ipoly, isom, j, k, ij;
  tPointd aa, bb, cc;
  real angle;
  Boolean supprimer;

  for (ipoly = 0; ipoly < np; ipoly++)
    {
      isom = 0;
      while (isom < ns[ipoly])
	{
	  bb[XX] = real (poly[ipoly][isom][XX]);
	  bb[YY] = real (poly[ipoly][isom][YY]);
	  j = isom + 1;
	  if (j == ns[ipoly])
	    j = 0;
	  k = j + 1;
	  if (k == ns[ipoly])
	    k = 0;
	  cc[XX] = real (poly[ipoly][j][XX]);
	  cc[YY] = real (poly[ipoly][j][YY]);
	  aa[XX] = real (poly[ipoly][k][XX]);
	  aa[YY] = real (poly[ipoly][k][YY]);

	  angle = Angle3d (bb, cc, aa);

	  supprimer = False;

	  if ((angle >= (M_PI - ANGLEPREC)) && (angle <= (M_PI + ANGLEPREC)))
	    {
	      // Sommets alignes: on ote le sommet du milieu:c
	      if (verbose == True)
		{
		  printf
		    ("The %dth vertice (%g %g) removed in polygon %d:\n  the points (%g %g), (%g %g), (%g %g) are aligned\n",
		     j + 1, cc[XX], cc[YY], (ipoly + 1), bb[XX], bb[YY],
		     cc[XX], cc[YY], aa[XX], aa[YY]);
		}
	      supprimer = True;
	    }



	  if (supprimer == True)
	    {
	      // On decale les sommets a partir de celui d'indice j

	      //  si j est le dernier, , il n'y a rien a decaler
	      if (j < (ns[ipoly] - 1))
		{
		  for (ij = j; ij < (ns[ipoly] - 1); ij++)
		    {
		      poly[ipoly][ij][XX] = poly[ipoly][ij + 1][XX];
		      poly[ipoly][ij][YY] = poly[ipoly][ij + 1][YY];
		    }
		}
	      ns[ipoly] = ns[ipoly] - 1;
	      // Erreur si le souspoly a moins de 3 sommets:
	      if (ns[ipoly] < 3)
		return (CALI_ERTRI4);
	    }			// fin (supprimer== True)
	  else
	    isom++;
	}			// fin while


    }				// fin ipoly
  return (OK);
}				// fin supVertices


/////////////////////////////////////////////////////
// Fonction:
// Programme maitre de la creation des sous-polygones convexes
// d'un polygone donne.
// Programme appele apres Triangulate() par readPoly
// Arguments d'entree:
// PolygonVertices: les coordonnees des sommets
// PolygonDiagonals: l'ensemble des cotes et diagonales:
// d'abord dans le sens anti-clockwise puis dans l'autre sens.
// Les indicateurs d'existence sont tous Faux en sortie.
// nvertices: le nombre de sommets
// ndiagonals: le nombre de diagonales
// verbose=T si on veut afficher les diagonales et les
// sous-polys convexes crees
// errident: l'identification du poly courant
// (pour l'afficher quand il y a une erreur)
// Arguments de sortie:
// poly: les sous-polygones convexes
// ns: le nombre de sommets de chacun des sous-polygones convexes
// Retour:
// le nombre de sous-polygones convexes ou code d'erreur negatif.
/////////////////////////////////////////////////////////////////

int
createSubPoly (tPolygoni * poly, int ns[],
	       POLYGON_STRUCT * PolygonVertices,
	       DIAGONAL_STRUCT * PolygonDiagonals,
	       int nvertices, int ndiagonals, Boolean verbose, char *errident)
{
  //

  int code, i, np, ndiagcot;

/* Pour les messages */
  char moi[] = "createSubPoly";
  char errmess[CHAR_MAX];

  // Ecrire les diagonales essentielles:
  if (verbose == True)
    {
      ndiagcot = nvertices + ndiagonals;
      printf ("Essential diagonals:\n");
      for (i = nvertices; i < ndiagcot; i++)
	{
	  if (PolygonDiagonals[i].exist == True)
	    {
	      printf ("%d - %d\n", PolygonDiagonals[i].vfrom,
		      PolygonDiagonals[i].vto);
	    }
	}
    }				// fin verbose


  // Creation des sous-polys:
  if ((np = makePolyLeft (verbose, PolygonVertices, PolygonDiagonals,
			  nvertices, ndiagonals, poly, ns)) < 0)
    // Erreur pbe de taille ou il y a un souspoly avec moins de 3 sommets:
    {

      if (verbose == 1)
	{
	  printf ("Cannot split polygon into convex subpolygons\n");

	}

      sprintf (errmess,
	       "Cannot split polygon into convex subpolygons\n%s", errident);
      return (ecrmess (np, moi, errmess));
    }				// fin erreur





  // Supprimer les sommets alignes 

  if ((code = supVertices (verbose, np, poly, ns)) < 0)
    {

      if (verbose == 1)
	{
	  printf ("Cannot split polygon into convex subpolygons\n");

	}

      sprintf (errmess,
	       "Cannot split polygon into convex subpolygons\n%s", errident);
      // AJOUT 10/01/2008
      np = -1;			// poly erronne
      return (ecrmess (np, moi, errmess));
    }				// fin erreur

  // fin Supprimer les sommets alignes 

  return np;
}

				// end of createSubPoly
