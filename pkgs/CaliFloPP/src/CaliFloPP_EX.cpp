/**
 * @file src/CaliFloPP_EX.cpp
 * @author Eric Casellas (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2014 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/devs/Executive.hpp>

#include <vle/utils/Tools.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>

#include <vle/value/Set.hpp>
#include <vle/value/Map.hpp>

#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>

#include "Iparam.h"
#include "Ipoly.h"
#include "util.h"
#include "litpoly.h"
#include "cubature.h"

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vp = vle::vpz;
namespace vu = vle::utils;


namespace  CaliFloPP {

    class CaliFloPP_EX : public vd::Executive
    {
    public:
        CaliFloPP_EX(const vd::ExecutiveInit& model, const vd::InitEventList& events)
             : vd::Executive(model, events)
        {
            std::cout << "[CaliFloPP_EX]\tConstructor start" << std::endl;

            getVPZParameters(events);

            checkVPZParametersValidity();

            callCaliFloPP();

            writeVPZConditions();

            dumpVPZ();

            std::cout << "[CaliFloPP_EX]\tConstructor end" << std::endl;
        }

        
        virtual ~CaliFloPP_EX() {DETRU_T2 (sortie, reslignes);}


        virtual vd::Time init(const vd::Time& /*time*/)
        { return vd::infinity; }


    private:

        struct FnSelector
        {
            int ifunct[MAX_NFUNCTIONS];
            real abser[MAX_NFUNCTIONS];
            real reler[MAX_NFUNCTIONS];
            long int maxpts[MAX_NFUNCTIONS];
            std::vector<int> nargu;
            std::vector<std::vector<real> > args;
        };

        //paramètres VPZ
        double mDbgLog;
        std::string mDataPackage;
        std::string mPolyFile;
        std::string mCondCaliOut;
        int mInputFormat;
        bool mTxtOutput;
        bool mVerbose;
        std::string mDelim;
        int mNCouples;
        int mCouples[MAX_NPAIRS][2];
        FnSelector fnSelector;

        //Variables locales
        int nfunct;
        int ifunct[MAX_NFUNCTIONS];
        int npoly;
        int ncouples;
        int reslignes;
        real **sortie;

        //Dump du vpz modifié par le modèle
        void dumpVPZ()
        {
            if (mDbgLog >= 1.) {
                std::cout << "\t[CaliFloPP_EX]\tDumping file : output_CaliFloPP_EX.vpz" << std::endl;
                std::ofstream file("output_CaliFloPP_EX.vpz");
                dump(file, "dump");
            }
        }

        //Lecture configuration de l'Executive (paramètres VPZ)
        void getVPZParameters (const vd::InitEventList& events)
        {
            mDbgLog = (events.exist("DbgLog")) ? vv::toDouble(events.get("DbgLog")) : 0.;

            mTxtOutput = (events.exist("TxtOutput")) ? vv::toBoolean(events.get("TxtOutput")) : false;

            mVerbose = (events.exist("Verbose")) ? vv::toBoolean(events.get("Verbose")) : mTxtOutput;

            mDataPackage = (events.exist("PkgName")) ? vv::toString(events.get("PkgName")) : "CaliFloPP";
            mPolyFile = vv::toString(events.get("PolyFile"));

            mCondCaliOut = (events.exist("CondCaliOut")) ? vv::toString(events.get("CondCaliOut")) : "cond_CaliOut";

            mInputFormat = (events.exist("InputFormat")) ? vv::toInteger(events.get("InputFormat")) : 2;

            mDelim = (events.exist("Delim")) ? vv::toString(events.get("Delim")) : "\t";
            replaceAll(mDelim, "\\t", "\t");

            const vv::Set& couplesSelect = events.getSet("Couples");
            for (unsigned int index = 0; index < couplesSelect.size(); ++index) {
                const vv::Set& couplepair = couplesSelect.getSet(index);
                for (unsigned int index_pair = 0; index_pair < 2; ++index_pair) {
                    mCouples[index][index_pair] = couplepair.getInt(index_pair);
                }
            }
            mNCouples = (events.exist("NCouples")) ? vv::toInteger(events.get("NCouples")) : couplesSelect.size();

            const vv::Set& fnSelect = events.getSet("FnSelector");
            nfunct = fnSelect.size();
            for (int index = 0; index < nfunct; ++index) {
                const vv::Map& tab(fnSelect.getMap(index));
                fnSelector.ifunct[index] = toInteger(tab["ifunct"]);
                fnSelector.nargu.push_back(toInteger(tab["nargu"]));
                fnSelector.abser[index] = tab.exist("abser") ? toDouble(tab["abser"]) : DEFAULT_ABS_ERR;
                fnSelector.reler[index] = tab.exist("reler") ? toDouble(tab["reler"]) : DEFAULT_REL_ERR;
                fnSelector.maxpts[index] = tab.exist("maxpts") ? toInteger(tab["maxpts"]) : DEFAULT_MAX_PTS;
                const vv::Set& targs = tab.getSet("args");
                std::vector<real> args_tmp;
                for (int index_args = 0; index_args < fnSelector.nargu[index]; ++index_args) {
                    args_tmp.push_back(targs.getDouble(index_args));
                }
                fnSelector.args.push_back(args_tmp);
            }

        }

        //Vérification de la validité de la configuration en fonction du format attendu
        void checkVPZParametersValidity ()
        {
            /////////////////////////////////////////////////
            // Quelques vérifications a oter quand OK
            /////////////////////////////////////////////////
            if (nfunct > MAX_NFUNCTIONS) {
                std::stringstream ss;
                ss << "Le nombre de fonctions excède le maximum " << MAX_NFUNCTIONS <<". Augmenter MAX_NFUNCTIONS dans le fichier calidefs.h" << std::endl;
                throw vu::ModellingError(ss.str());
            }

        }

        //Modifications du simulateur (édition de conditions expérimentales existantes)
        void writeVPZConditions()
        {
            vp::Conditions& cond_l = conditions(); //référence aux conditions du VPZ
            vp::Condition& cond_CaliOut = cond_l.get(mCondCaliOut); //référence à la condition toto
            cond_CaliOut.setValueToPort("npoly", vv::Integer::create(npoly)); //modification de la valeur du port npoly de la condition toto
            cond_CaliOut.setValueToPort("ncouples", vv::Integer::create(ncouples)); //modification de la valeur du port npoly de la condition toto
            vv::Set* cond_couple = new vv::Set();
            for (int i = 0; i < reslignes; i++) {
                vv::Map* mymap = new vv::Map();
                mymap->add("index.poly.1", vv::Double::create(sortie[i][0]));
                mymap->add("index.poly.2", vv::Double::create(sortie[i][1]));
                mymap->add("aire.poly.1", vv::Double::create(sortie[i][2]));
                mymap->add("aire.poly.2", vv::Double::create(sortie[i][3]));
                mymap->add("nfunct", vv::Integer::create(nfunct));
                vv::Set* cond_flux = new vv::Set();
                vv::Set* cond_ifunct = new vv::Set();
                for (int j = 0; j < nfunct; j++) {
                    cond_flux->add(vv::Double::create(sortie[i][j + 4]));
                    cond_ifunct->add(vv::Integer::create(ifunct[j]));
                }
            mymap->add("ifunct", (*cond_ifunct).clone());
            mymap->add("flux", (*cond_flux).clone());
            cond_couple->add((*mymap).clone());
            }
            cond_CaliOut.setValueToPort("couples", (*cond_couple).clone());
        }

        //Appel de CaliFloPP
        void callCaliFloPP()
        {
            setlocale(LC_NUMERIC,"C"); // to be sure to correctly read the parameter files 

            /////////////////////////////////////////////////
            // Affecter des valeurs aux paramètres de califlopp
            /////////////////////////////////////////////////

            Boolean verbose = Boolean(mVerbose); // Impressions intermédiaires
            int input = mInputFormat; // Format du fichier des polygones
            char delim[2]; // Séparateur des valeurs sur le fichier des polygones
            strncpy(delim, mDelim.c_str(), 2); // Le séparateur ne peut etre que de taille 2
            int output = mTxtOutput ? ALL : NOTHING; // Sorties voulues a l'écran

            real abser[MAX_NFUNCTIONS];
            real reler[MAX_NFUNCTIONS];
            long int maxpts[MAX_NFUNCTIONS];

            memcpy(ifunct, fnSelector.ifunct, sizeof(fnSelector.ifunct));
            memcpy(abser, fnSelector.abser, sizeof(fnSelector.abser));
            memcpy(reler, fnSelector.reler, sizeof(fnSelector.reler));
            memcpy(maxpts, fnSelector.maxpts, sizeof(fnSelector.maxpts));


            /////////////////////////////////////////////////
            // Creer la structure qui contient les parametres fixes
            /////////////////////////////////////////////////
            Iparam param = Iparam (verbose, input, delim, output,
                                   nfunct, ifunct, abser, reler, maxpts);
if (mDbgLog>0.) printParam(param);


            /////////////////////////////////////////////////
            // Lire le nombre de polys
            /////////////////////////////////////////////////

            char lu[MAX_LINE_POLY];
            char moi[] = "litpoly";
            char errlect[] = "Error in reading";

            vu::Package pack(context(), mDataPackage);
            char* filenamepoly = new char[pack.getDataFile(mPolyFile).size()+1];
            strcpy (filenamepoly, pack.getDataFile(mPolyFile).c_str());
            std::cout << "Using polygon input file : " << filenamepoly << std::endl;

            FILE *fpi;
            fpi = fopen (filenamepoly, "r");
            if (!fpi) {
                std::stringstream ss;
                ss << moi << "(" << CALI_ERFIC1 << ")" << std::endl << "Cannot open polygons file " << filenamepoly << std::endl;
                throw vu::ModellingError(ss.str());
            }

            // Lire avec fgets ce fichier, car il a un nbre va. de données
            // sur chaque ligne et il ne faut pas mélanger les fscanf et fgets
            if (fgets (lu, MAX_LINE_POLY, fpi) == NULL) {
                std::stringstream ss;
                ss << moi << "(" << CALI_ERLECT << ")" << std::endl << errlect << std::endl;
                throw vu::ModellingError(ss.str());
            }

            npoly = atoi(lu);
if (mDbgLog>0.) std::cout << "npoly:" << npoly << std::endl;


            /////////////////////////////////////////////////
            // Creer la structure qui va contenir les polys
            /////////////////////////////////////////////////
            Ipoly polys = Ipoly (npoly, filenamepoly);
            const Ipoly *lespolys = &polys;
if (mDbgLog>0.) printPolys(lespolys, 5, 5);

            /////////////////////////////////////////////////
            // Lire les polys
            /////////////////////////////////////////////////
            int err = litpoly (fpi, &param, lespolys);
if (mDbgLog>0.) printPolys(lespolys, 5, 5);

            /////////////////////////////////////////////////
            // Determiner les parcelles voulues
            /////////////////////////////////////////////////

            ncouples = mNCouples;
            int couples[MAX_NPAIRS][2];    // pour stocker les nos des couples voulus */
            memcpy(couples, mCouples, sizeof(mCouples));

            /////////////////////////////////////////////////
            // Définir les traitements initiaux des especes
            /////////////////////////////////////////////////
            /* Ce seront les arguments supplémentaires de la fonction  de dispersion.
            */
            int nargu[nfunct];
            real targs[nfunct][MAX_NARGFUNCTIONS];

            for (int i=0;i<nfunct;i++) {
                nargu[i] = fnSelector.nargu[i];
                for (int j=0;j<nargu[i];j++) {
                    targs[i][j] = fnSelector.args[i][j];
                }
            }

if (mDbgLog>0.) printArgs(targs, nargu, nfunct);

            /* Si les fonctions  de dispersion n'ont pas d'arguments supplémentaires: affecter 0 à tous les elts de nargu et targs est ignoré */

            /////////////////////////////////////////////////
            // Le fichier resultat
            /////////////////////////////////////////////////
            /* si on veut un fichier resultat (6/1/2014)
            char *filenamer= new char[sizeof("RES")];
            sprintf (filenamer, "RES");
            sinon            : */
            char * filenamer =NULL;      

            /* mode d'ouverture du fichier resultat */
            char openr = 'w'; // 'a' si append         


            //////////////////////////////////////////////////////////
            // Allouer la sortie
            //////////////////////////////////////////////////////////
            reslignes = 0;
            sortie = NULL;

            /* si on veut les résultats en retour : */
            reslignes = calcnres (npoly, ncouples, couples);
if (mDbgLog>0.) std::cout << "reslignes:" << reslignes << std::endl;

            CREER_T2 (sortie, reslignes, real);
            for (int i = 0; i < reslignes; i++) {
                CREER_T1 (sortie[i], (4 + nfunct), real);
            }

            /////////////////////////////////////////////////
            // Lancer les calculs
            /////////////////////////////////////////////////
//if (mDbgLog>0.) printPolys(lespolys, 5, 5);
            err = cubature (lespolys, &param, ncouples, couples, nargu, targs,
                            filenamer, openr, reslignes, sortie);


            if (mTxtOutput) {
                /////////////////////////////////////////////////
                // Ecrire les sorties 
                /////////////////////////////////////////////////
                if (reslignes > 0) {
                    printf ("\n\n\n****** VALEURS RETOURNEES ******\n");
                    for (int i = 0; i < reslignes; i++) {
                        printf ("\n POLYGONE 1: %g; ", sortie[i][0]);
                        printf ("POLYGONE 2: %g\n", sortie[i][1]);
                        printf ("aire poly1: %g; ", sortie[i][2]);
                        printf ("aire poly2: %g\n", sortie[i][3]);
                        for (int j = 0; j < nfunct; j++) {
                            printf ("Espece: %d (fonction %d) \n", j + 1, ifunct[j]);
                            printf ("Flux total: %g; ", sortie[i][j + 4]);
                            printf ("par unité de surface du poly1: %g; ", (sortie[i][j + 4] / sortie[i][2]));
                            printf ("par unité de surface du poly2: %g\n", (sortie[i][j + 4] / sortie[i][3]));
                        }
                    } // fin i
                } // fin reslignes
            }
            delete []filenamepoly;
            if (filenamer != NULL)
                delete []filenamer;

        }






    //Affichage de la valeurs des paramètres (Iparam)
    void printParam(const Iparam param)
    {
        std::cout << "printParam" << std::endl;
        std::cout << "\tpverbose:" << param.pverbose << std::endl;
        std::cout << "\tpoutput:" << param.poutput << std::endl;
        std::cout << "\tpinput:" << param.pinput << std::endl;
        std::cout << "\tpdelim[2]:\"" << param.pdelim  << "\""<< std::endl;
        std::cout << "\tnfunct:" << param.nfunct << std::endl;
        for (int i=0;i<param.nfunct;i++) {
            std::cout << "\tindex:" << i << std::endl;
            std::cout << "\t\tifunct:" << param.ifunct[i] << std::endl;
            std::cout << "\t\tpabser:" << param.pabser[i] << std::endl;
            std::cout << "\t\tpreler:" << param.preler[i] << std::endl;
            std::cout << "\t\tpmaxpts:" << param.pmaxpts[i] << std::endl;
        }
    }

    //Affichage des polygones (Ipoly)
    void printPolys(const Ipoly* polys, unsigned int npol, unsigned int nsom)
    {
        std::cout << "printPolys" << std::endl;
        std::cout << "\tnpoly:" << (*polys).npoly << std::endl;
        std::cout << "\tfilenamei:" << (*polys).filenamei << std::endl;
        for (unsigned int i=0;i<npol;i++) {
            std::cout << "\ti:" << i << std::endl;
            std::cout << "\t\tnumPoly:" << (*polys).numPoly[i] << std::endl;
            std::cout << "\t\tnomPoly:" << (*polys).nomPoly[i] << std::endl;
            std::cout << "\t\ta:" << (*polys).a[i] << std::endl;
            std::cout << "\t\tarea:" << (*polys).area[i] << std::endl;
            std::cout << "\t\tbary:";
            for (int j=0;j<DIM;j++) std::cout << (*polys).bary[i][j] << ", ";
            std::cout << std::endl;
            std::cout << "\t\tni:";
            for (unsigned int j=0;j<nsom;j++) std::cout << (*polys).ni[i][j] << ", ";
            std::cout << std::endl;
        }
    }

    //Affichage des arguments pour chaque fonction de dispertion
    void printArgs(const real args[][MAX_NARGFUNCTIONS], int m[], unsigned int n)
    {
        std::cout << "printArgs:" << std::endl;
        for (unsigned int i=0;i<n;i++) {
            std::cout << "\tf" << i << " (" << m[i] << ")";
            if (m[i]>0) {
                std::cout << args[i][0];
                for (int j=1;j<m[i];j++) {
                    std::cout << ", " << args[i][j];
                }
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }


    void replaceAll(std::string& str, const std::string& from, const std::string& to) {
        if(from.empty())
            return;
        size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != std::string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
        }
    }


    };
}
DECLARE_EXECUTIVE(CaliFloPP::CaliFloPP_EX);

