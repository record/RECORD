
/*
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems.
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2013 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2013 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2013 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and
 * contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/version.hpp>
#include <iostream>
#include <thread>
#include <vle/vle.hpp>
#include <vle/utils/Rand.hpp>
#include <vle/utils/Filesystem.hpp>
#include <vle/utils/Context.hpp>
#include <vle/utils/unit-test.hpp>
#include <record/eigen/Stats.hpp>


struct F
{
#if VLE_VERSION < 20010
    vle::Init a;
#endif
    vle::utils::Path current_path;

    F()
    {
        current_path = vle::utils::Path::temp_directory_path();
        current_path /= vle::utils::Path::unique_path("vle-%%%%-%%%%-%%%%");
        current_path.create_directory();

        /* We need to ensure each file really installed. */
        std::this_thread::sleep_for(std::chrono::milliseconds(5));

        if (not current_path.is_directory())
            throw std::runtime_error("Fails to found temporary directory");

#ifdef _WIN32
        ::_putenv((vle::fmt("VLE_HOME=%1%")
                   % current_path.string()).str().c_str());
#else
        ::setenv("VLE_HOME", current_path.string().c_str(), 1);
#endif

        vle::utils::Path::current_path(current_path);
        std::cout << "test start in " << current_path.string() << '\n';

        auto ctx = vle::utils::make_context();
        ctx->write_settings();
    }

    ~F()
    {
        std::cout << "test finish in " << current_path.string() << '\n';
    }
};


void test_cast()
{
    using namespace Eigen;


    vle::value::Table t(2,3);
    double v=0;
    for (unsigned int c=0; c<t.width(); c++) {
        for (unsigned int r=0; r<t.height(); r++) {
            t(c,r)=v;
            v++;
        }
    }
    std::cout << "dbg " << t << "\n";
    MatrixXd tt = record::eigen::Stats::toEigenMat(t).transpose();
    std::cout << "dbg " << tt << "\n";

    std::cout << "dbg " << t << "\n";
}

void test_std()
{
// in R
// > std(c(1,2,3,6,4,5))
// [1] 1.870829

 using namespace Eigen;
 VectorXd x(6);
 x << 1, 2, 3, 4, 5, 6;
 double d = record::eigen::Stats::std(x);
 std::cout << "test_std " << d << "\n";
 EnsuresApproximatelyEqual(d, 1.870829, 10e-5);
}

void test_var()
{
// in R
// > var(c(1,2,3,6,4,5))
// [1] 3.5

 using namespace Eigen;
 VectorXd x(6);
 x << 1, 2, 3, 4, 5, 6;
 double d = record::eigen::Stats::var(x);
 std::cout << "test_var " << d << "\n";
 EnsuresApproximatelyEqual(d, 3.5, 10e-5);
}

void test_subMatrix()
{
    using namespace Eigen;

    MatrixXd m(3,3);
    m << -1, 0.2, 8, 2, 5, 0.6, 0.5, 2, 4;

    {
        MatrixXd s1 =  record::eigen::Stats::subMatrix_noti_noti(m, 1);
        EnsuresEqual(s1.cols(), 2);
        EnsuresEqual(s1.rows(), 2);

        double v1 = (double) s1(0,1);
        EnsuresApproximatelyEqual(v1, 8, 10e-5);
    }


    {
        VectorXd s1 =  record::eigen::Stats::subMatrix_i_noti(m, 1);
        EnsuresEqual(s1.size(), 2);
        double v1 = (double) s1(1);
        EnsuresApproximatelyEqual(v1, 0.6, 10e-5);
    }
}

void test_def_pos()
{

    //def.pos(matrix(c(1,2,2,2), nrow=2))

    using namespace Eigen;



    MatrixXd m(2,2);
    m << 1, 2, 2, 2;

    MatrixXd defpos = record::eigen::Stats::def_pos(m);
    double r = defpos(0,0);
    EnsuresApproximatelyEqual(r, 1.570143, 10e-4);
    r = defpos(0,1);
    EnsuresApproximatelyEqual(r, 1.554846, 10e-4);
    r = defpos(1,0);
    EnsuresApproximatelyEqual(r, 1.554846, 10e-4);
    r = defpos(1,1);
    EnsuresApproximatelyEqual(r, 2.347566, 10e-4);


}

void test_multivariate_density()
{

    using namespace Eigen;
    vle::utils::Rand rng;
    rng.seed(1239);


    VectorXd mu(2);
    mu << 1 ,1;
    MatrixXd sigma(2,2);
    sigma << 1.0, 0.3,
            0.3, 1.0;
    VectorXd mubis(2);
    mubis << 4 ,4;
    MatrixXd sigmabis(2,2);
    sigmabis << 1.0, -0.3,
            -0.3, 1.0;

    unsigned int n = 50;
    MatrixXd normData(2*n,2);
    normData << record::eigen::Stats::rmnorm(n, mu, sigma, rng),
            record::eigen::Stats::rmnorm(n, mubis, sigmabis, rng);
    VectorXd min(2);
    min << normData.col(0).array().minCoeff(),
            normData.col(1).array().minCoeff();
    VectorXd max(2);
    max << normData.col(0).array().maxCoeff(),
            normData.col(1).array().maxCoeff();


    std::cout << " normData \n" << normData <<"\n";


    unsigned int grid = 151;
    MatrixXd densityMultivar = MatrixXd::Zero(grid, grid);
    for (unsigned int r=0; r<grid; r++) {
        for (unsigned int c=0; c<grid; c++) {
            VectorXd x(2);
            x << (((double) r * (max(0)-min(0)))/(double)grid)+min(0), 
                    (((double) c * (max(1)-min(1)))/(double)grid)+min(1) ;
            densityMultivar(r,c) = record::eigen::Stats::kernelDensity(x, normData);
        }
    }
    std::cout << " kernel density \n" << densityMultivar <<"\n";
}


void test_dmnorm()
{

//    In R pkg mnormt
//    > x = -2
//    > y <- 2*x+10
//    > z <- x+cos(y)
//    > mu <- c(1,12,2)
//    > Sigma <- matrix(c(1,2,0,2,5,0.5,0,0.5,3), 3, 3)
//    > dmnorm(c(x,y,z), mu, Sigma)
//    7.926462e-05

    using namespace Eigen;

    double x = -2;
    double y = 2*x+10;
    double z = x+cos(y);

    VectorXd xyz(3);
    xyz << x, y ,z;
    VectorXd mu(3);
    mu << 1, 12, 2;
    MatrixXd Sigma(3, 3);
    Sigma << 1, 2, 0,
            2, 5, 0.5,
            0, 0.5, 3;

    double r;
    r = record::eigen::Stats::dmnorm(xyz, mu, Sigma);
    EnsuresApproximatelyEqual(r, 7.926462e-05, 10e-5);
    std::cout << "test_dmnorm ok \n";
}

void test_pmnorm_indep()
{
//     In R pkg mnormt
//     > library(mnormt)
//     > pvalues = pnorm(q = c(0.5,0.6), mean = c(0,1), sd=sqrt(c(1,2)))
//     > p = pvalues[1] * pvalues[2] ; print(p)
//     [1] 0.268736
//     > pbis = pmnorm(x = c(0.5,0.6), mean = c(0,1), varcov=diag(c(1,2))); print(pbis)
//     [1] 0.268736
//     ==> meme valeurs

    using namespace Eigen;

    VectorXd z(2);
    z << 0.5, 0.6;
    VectorXd mu(2);
    mu << 0, 1;
    MatrixXd sigma(2, 2);
    sigma << 1, 0, 0, 2;
    double r = record::eigen::Stats::pmnorm_indep(z, mu, sigma);
    EnsuresApproximatelyEqual(r, 0.268736, 10e-5);
}


void test_rmnorm()
{
    using namespace Eigen;

//     In R pkg mnormt
//     > library(mnormt)
//     > set.seed(1236)
//     > X=  rmnorm(n=1000, mean=c(1,0), varcov= matrix(c(1,0.2,0.2,1), nrow=2, byrow=T)); 
//     > plot(X[,1], X[,2])
//     > apply(X, MARGIN=2, mean)
//     [1]  0.96569840 0.03382583
//     > apply(X, MARGIN=2, var)
//     [1] 0.9881305 1.0583848
//     > cov(X[,1], X[,2])
//     [1] 0.2490633
 

    vle::utils::Rand mrand(1234);

    VectorXd mu(2);
    mu << 1, 0;
    MatrixXd sigma(2, 2);
    sigma << 1, 0.2, 0.2, 1;

    MatrixXd X = record::eigen::Stats::rmnorm(1000, mu,
                   sigma, mrand);

    VectorXd smu = X.colwise().mean();
    double r = smu(0);
    EnsuresApproximatelyEqual(r, 0.982979, 10e-2);//1
    r = smu(1);
    EnsuresApproximatelyEqual(r, -0.0117363, 10e-2);//0
    r = record::eigen::Stats::var(X.col(0));
    EnsuresApproximatelyEqual(r, 0.950852, 0.2);//1
    r = record::eigen::Stats::var(X.col(1));
    EnsuresApproximatelyEqual(r, 1.03626, 0.2);//1
    r = record::eigen::Stats::cov(X.col(0), X.col(1));
    EnsuresApproximatelyEqual(r, 0.180164, 0.2);//0.2
}

void test_rtmnorm()
{

    using namespace Eigen;

// > library(tmvtnorm)
// > set.seed(6369)
// > X = rtmvnorm(n=100000, mean=c(0,0), sigma=matrix(c(1.0,0.6,0.6,1.0), nrow=2),
//                lower=c(-0.1,-1), upper=c(2.5,3.0), algorithm="gibbs",
//                start.value = c(0,0), burn.in.sample=20000);
// > par(mfrow=c(1,3)); plot(X); hist(X[,1]); hist(X[,2])
// > apply(X, MARGIN=2, mean)
// [1] 0.7274779 0.5131802
// > apply(X, MARGIN=2, var)
// [1] 0.3365233 0.6049052
// > cov(X[,1], X[,2])
// [1] 0.1710277



    vle::utils::Rand mrand(1234);
    
    VectorXd mean(2);
    VectorXd min(2);
    VectorXd max(2);
    MatrixXd cov(2,2);
    mean << 0,0;
    min << -0.1, -1;
    max << 2.5, 3;
    cov << 1, 0.6, 0.6, 1;

    MatrixXd X = record::eigen::Stats::rtmnorm_gibbs(100000, mean, min,
            max, cov, mean, 20000, mrand);

    VectorXd smu = X.colwise().mean();
    double r = smu(0);
    EnsuresApproximatelyEqual(r, 0.72266, 10e-2);
    r = smu(1);
    EnsuresApproximatelyEqual(r, 0.505724, 10e-2);
    r = record::eigen::Stats::var(X.col(0));
    EnsuresApproximatelyEqual(r, 0.337544, 0.2);
    r = record::eigen::Stats::var(X.col(1));
    EnsuresApproximatelyEqual(r, 0.608417, 0.2);
    r = record::eigen::Stats::cov(X.col(0), X.col(1));
    EnsuresApproximatelyEqual(r, 0.171328, 0.2);
}


void test_rmcsn()
{

    using namespace Eigen;

// > source("/pub/src/weathergen/WACS/R/wacs.CSNfunctions.R")
// > source("/pub/src/weathergen/WACS/R/wacs.utils.R")
// > library(tmvtnorm)
// > set.seed(1234);
// > X = rmcsn(10000, Mu=c(0,0), Sigma=matrix(c(1,0.6,0.6,1),nrow=2), 
//             D=matrix(c(-10,0,0,10), nrow=2), Nu=c(0,0), Delta=matrix(c(1,0,0,1), nrow=2))
// > par(mfrow=c(1,3)); plot(X); hist(X[,1]); hist(X[,2])
// > apply(X, MARGIN=2, mean)
// [1] -0.5343422  0.5367735
// > apply(X, MARGIN=2, var)
// [1] 0.1935824 0.1981408
// > cov(X[,1], X[,2])
// [1] 0.03497205

    vle::utils::Rand mrand(1234);

    VectorXd Mu(2);
    Mu <<  0,0;
    MatrixXd Sigma(2,2);
    Sigma << 1.0,0.6,0.6,1.0;
    MatrixXd D(2,2);
    D << -10,0,0,10;
    VectorXd Nu(2);
    Nu <<  0,0;
    MatrixXd Delta(2,2);
    Delta << 1,0,0,1;
    
    MatrixXd X = record::eigen::Stats::rmcsn(10000, Mu, Sigma, D, Nu, Delta, mrand);

    VectorXd smu = X.colwise().mean();
    double r = smu(0);
    EnsuresApproximatelyEqual(r, -0.541596, 10e-2);
    r = smu(1);
    EnsuresApproximatelyEqual(r, 0.527708, 10e-2);
    r = record::eigen::Stats::var(X.col(0));
    EnsuresApproximatelyEqual(r, 0.20126, 0.2);
    r = record::eigen::Stats::var(X.col(1));
    EnsuresApproximatelyEqual(r, 0.197418, 0.2);
    r = record::eigen::Stats::cov(X.col(0), X.col(1));
    EnsuresApproximatelyEqual(r, 0.0337383, 0.2);
}



void test_dmcsnstar()
{
//    test based on dmcsnstar using WACSgen in R

    using namespace Eigen;


    VectorXd mu(4);
    mu << 1.1782576, -0.4987161, 1.1499834, 0.7956978;
    MatrixXd cov(4, 4);
    cov << 1.3961425, -0.0861974, -0.1555092, -0.1466791,
           -0.0861974, 0.4815183, 0.2286828, -0.3733830,
           -0.1555092, 0.2286828, 1.0658719, 0.4586755,
           -0.1466791, -0.3733830, 0.4586755, 1.4442475;

    VectorXd skew(4);
    skew << -0.762900, 0.426550, -0.899937, -0.000060;

    VectorXd z(4);
    z << 1.1782576, -0.4987161, 1.1499834, 0.7956978;

    double r = record::eigen::Stats::dmcsnstar(z, mu, cov, skew);
    EnsuresApproximatelyEqual(r, 0.03765009, 10e-4);
}

void test_rmcsnstar()
{

    using namespace Eigen;


// > source("/pub/src/weathergen/WACS/R/wacs.CSNfunctions.R")
// > source("/pub/src/weathergen/WACS/R/wacs.utils.R")
// > library(tmvtnorm)
// > set.seed(1234);
// > X = rmcsnstar(10000, Mu=c(0,0), Sigma=matrix(c(1,0.5,0.5,1),nrow=2), Skew=c(-0.9,0.9));
// > par(mfrow=c(1,3)); plot(X); hist(X[,1]); hist(X[,2])
// > apply(X, MARGIN=2, mean)
// [1] -0.5079076  0.5082143
// > apply(X, MARGIN=2, var)
// [1] 0.4818808 0.4770093
// > cov(X[,1], X[,2])
// [1] 0.2381556


   vle::utils::Rand mrand(1234);

    VectorXd Mu(2);
    Mu << 0,0;
    MatrixXd Sigma(2,2);
    Sigma << 1.0,0.5,0.5,1.0;
    VectorXd Skew(2);
    Skew << -0.9, 0.9;

    MatrixXd X = record::eigen::Stats::rmcsnstar(10000, Mu, Sigma, Skew, mrand);

    VectorXd smu = X.colwise().mean();
    double r = smu(0);
    EnsuresApproximatelyEqual(r, -0.500674, 10e-2);
    r = smu(1);
    EnsuresApproximatelyEqual(r, 0.497371, 10e-2);
    r = record::eigen::Stats::var(X.col(0));
    EnsuresApproximatelyEqual(r, 0.483683, 0.2);
    r = record::eigen::Stats::var(X.col(1));
    EnsuresApproximatelyEqual(r, 0.482761, 0.2);
    r = record::eigen::Stats::cov(X.col(0), X.col(1));
    EnsuresApproximatelyEqual(r, 0.246805, 0.2);
}



void test_rmcsn_cond()
{
    using namespace Eigen;
//
//NOTE: sqrtm has to me modified for this test with : if (nrow(M) == 1) return(sqrt(M[1,1]));
//
// > source("/pub/src/weathergen/WACS/R/wacs.CSNfunctions.R")
// > source("/pub/src/weathergen/WACS/R/wacs.utils.R")
// > library(tmvtnorm)
// > set.seed(1234);
// > X = rmcsn.cond(10000, y.cond=-2, sel.cond=1, Mu=c(0,0), Sigma=matrix(c(1,0.6,0.6,1),nrow=2), 
//             D=matrix(c(-10,0,0,10), nrow=2), Nu=c(0,0), Delta=matrix(c(1,0,0,1), nrow=2))
// > hist(X[,1]);
// > mean(X[,1])
// [1] 0.3314752
// > var(X[,1])
// [1] 0.1031935
//

   vle::utils::Rand mrand(1234);

    VectorXd y_cond(1);
    y_cond << -2;
    VectorXd Mu(2);
    Mu <<  0,0;
    MatrixXd Sigma(2,2);
    Sigma << 1.0,0.6,0.6,1.0;
    MatrixXd D(2,2);
    D << -10,0,0,10;
    VectorXd Nu(2);
    Nu <<  0,0;
    MatrixXd Delta(2,2);
    Delta << 1,0,0,1;

    MatrixXd X = record::eigen::Stats::rmcsn_cond(10000, y_cond, Mu, Sigma, D, Nu, Delta, mrand);
    
    double r = record::eigen::Stats::mean(X.col(0));
    EnsuresApproximatelyEqual(r, 0.328571, 10e-2);
    r = record::eigen::Stats::var(X.col(0));
    EnsuresApproximatelyEqual(r, 0.103334, 10e-2);

}

int main()
{
    F fixture;

    test_cast();
    test_std();
    test_var();
    test_subMatrix();
    test_def_pos();
    test_multivariate_density();
    test_dmnorm();
    test_pmnorm_indep();
    test_rmnorm();
    test_rtmnorm();
    test_rmcsn();
    test_dmcsnstar();
    test_rmcsnstar();
    test_rmcsn_cond();

    return unit_test::report_errors();
}
