/**
 * @file test/test.cpp
 * @author The VLE Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2013 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_MAIN
#define BOOST_AUTO_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_2CV
#include <boost/test/unit_test.hpp>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <tester/TesterSimulation.hpp>
#include <string>
#include <iostream>


namespace vz = vle::vpz;
namespace vu = vle::utils;
namespace vm = vle::manager;
namespace va = vle::value;

BOOST_AUTO_TEST_CASE(test_2CV_parcelle)
{
    std::cout << " test_2CV_parcelle " << std::endl;

    double v;

    tester::TesterSimulation tsim("2CV");
    const va::Map& views = tsim.simulates("2CV-parcelle.vpz");

    //check the number of views
    BOOST_REQUIRE_EQUAL(views.size(),9);
    //check view LAI
    {
        const va::Matrix& lai = views.getMatrix("vueLAI");
        BOOST_REQUIRE_EQUAL(lai.columns(),5);
        BOOST_REQUIRE_EQUAL(lai.rows(),352);

        //check ALAI line 197
        v = tsim.getViewElt(lai,"Top model,2CV,CropFull:CropLAI",
                "ALAI", 198).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 4.160459, 10e-5);
        //check FracSen line 239
        v = tsim.getViewElt(lai,"Top model,2CV,CropFull:CropLAI",
                "FracSen", 240).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 0.1378907, 10e-5);
        //check LAI line 260
        v = tsim.getViewElt(lai,"Top model,2CV,CropFull:CropLAI",
                "LAI", 261).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 5.790568, 10e-5);
        //check lplant line 299
        v = tsim.getViewElt(lai,"Top model,2CV,CropFull:CropLAI",
                "lplant", 300).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 0.5805587, 10e-5);
    }



    //check vueAGB
    {
        const va::Matrix& abg = views.getMatrix("vueAGB");
        BOOST_REQUIRE_EQUAL(abg.columns(),6);
        BOOST_REQUIRE_EQUAL(abg.rows(),352);

        //check AGBiomass line 205
        v = tsim.getViewElt(abg,"Top model,2CV,CropFull:CropAGB",
                "AGBiomass", 206).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 927.222617, 10e-5);
        //check HI line 233
        v = tsim.getViewElt(abg,"Top model,2CV,CropFull:CropAGB",
                "HI", 234).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 0.09, 10e-5);
        //check HIpot line 287
        v = tsim.getViewElt(abg,"Top model,2CV,CropFull:CropAGB",
                "HIpot", 288).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 0.506732, 10e-5);
        //check Yield line 294
        v = tsim.getViewElt(abg,"Top model,2CV,CropFull:CropAGB",
                "Yield", 295).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 1208.667558, 10e-5);
        //check dayCount line 299
        v = tsim.getViewElt(abg,"Top model,2CV,CropFull:CropAGB",
                "dayCount", 300).toDouble().value();
        BOOST_REQUIRE_CLOSE(v, 26, 10e-5);

    }
}



BOOST_AUTO_TEST_CASE(test_2CV_decision)
{
    std::cout << " test_2CV_decision " << std::endl;

    std::string v;

    tester::TesterSimulation tsim("2CV");
    const va::Map& views = tsim.simulates("2CV-decision.vpz");

    //check the number of views
    BOOST_REQUIRE_EQUAL(views.size(),2);

    const va::Matrix& dec = views.getMatrix("vueDecision");
    BOOST_REQUIRE_EQUAL(dec.columns(),22);
    BOOST_REQUIRE_EQUAL(dec.rows(),352);

    //check .Date line 110
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision",".Date", 111).toString().value());
    BOOST_REQUIRE_EQUAL(v, "1961-04-21");
    //check periodeSeche line 110
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","periodeSeche", 111).toString().value());
    BOOST_REQUIRE_EQUAL(v,"periodeSeche");

    //check Recolte line 249, WARNING change because of different PRNG
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","Recolte", 250).toString().value());
    BOOST_REQUIRE_EQUAL(v, "Recolte:int-wait");
    //check Recolte line 250, WARNING change because of different PRNG
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","Recolte", 251).toString().value());
    BOOST_REQUIRE_EQUAL(v, "Recolte:int-done");


    //check finRecolte line 280, WARNING change because of different PRNG
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","finRecolte", 281).toString().value());
    BOOST_REQUIRE_EQUAL(v, "finRecolte");
    //check solPortant line 169
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","solPortant", 170).toString().value());
    BOOST_REQUIRE_EQUAL(v,"solPortant");
    //check Irrigation_10 line 341, WARNING change because of different PRNG
    v.assign(tsim.getViewElt(dec,"2CV_decision:Decision","Irrigation_10", 342).toString().value());
    BOOST_REQUIRE_EQUAL(v, "Irrigation_10:ext-failed");

}


BOOST_AUTO_TEST_CASE(test_2CV_multi_parcellaire)
{
    std::cout << " test_2CV_multi_parcellaire " << std::endl;

    tester::TesterSimulation tsim("2CV");
    const va::Map& views = tsim.simulates("2CV-multi-parcellaire.vpz");

    //check the number of views
    BOOST_REQUIRE_EQUAL(views.size(),9);

    const va::Matrix& swcb = views.getMatrix("vueSWCB");
    BOOST_REQUIRE_EQUAL(swcb.columns(),73);
    BOOST_REQUIRE_EQUAL(swcb.rows(),352);

    //check S4 line 350
    double v = tsim.getViewElt(swcb,"Top model,2CV_2,SoilFull:SoilSWCB",
            "S4", 351).toDouble().value();
    BOOST_REQUIRE_CLOSE(v, 323.313799, 10e-5);
}

