#/**
#  * @file planADTG.txt
#  * @author ...
#  * ...
#  */

## Predicates
predicates {
   predicate {
      id = "plotAccesibility";
      type = "GPred";
      parameter {
         _opLeftType = "Var";
         _opLeft = "days3rain";
         _op = "<=";
         _opRightType = "Val";
         _opRight = 10;
      }
   }
   predicate {
      id = "maturityReached";
      type = "GPred";
      parameter {
         _opLeftType = "Var";
         _opLeft = "beginnningMaturity";
         _op = ">";
         _opRightType = "Val";
         _opRight = 0.;
      }
   }
   predicate {
      id = "dryPeriod";
      type = "GPred";
      parameter {
         _opLeftType = "Var";
         _opLeft = "days4rain";
         _op = "<=";
         _opRightType = "Val";
         _opRight = 10;
      }
   }
   predicate {
      id = "waterAvailable";
      type = "GPred";
      parameter {
         _opLeftType = "Var";
         _opLeft = "WaterStock";
         _op = ">=";
         _opRightType = "Val";
         _opRight = 30;
      }
   }
}

## Rules
rules {
   rule {
      id = "ruleSowing";
      predicates = "plotAccesibility";
   }
   rule {
      id = "ruleHarvesting";
      predicates = "plotAccesibility", "maturityReached";
   }
   rule {
      id = "ruleIrrigation";
      predicates = "dryPeriod", "waterAvailable";
   }
}

## Activities
activities {
   activity {
      id = "Sowing";
      temporal {
            minstart = 2437395; # 1961-4-5
            maxfinish = 2437461; #1961-6-10
        }
      output = "GOut";
      update = "GUpdate";
      rules = "ruleSowing";
      parameter {
            _update_Done_Sowing = 1;
      }
   }
   activity {
      id = "harvesting";
      output = "GOut";
      update = "GUpdate";
      rules = "ruleHarvesting";
      parameter {
            _update_Done_Harvesting = 1;
      }
   }
   activity {
      id = "Irrigation";
      temporal {
            minstart = 2437466; # 1961-6-15
            maxfinish = 2437639; #1961-12-5
        }
      output = "GOut";
      update = "GUpdate";
      rules = "ruleIrrigation";
      parameter {
            _update_Done_irrigationAmount = 30;
            maxIter = 12;
            timeLag = 9;
      }
   }
}

## Precedences
