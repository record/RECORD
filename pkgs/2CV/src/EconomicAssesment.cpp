// @@tagdynamic@@
// @@tagdepends: vle.extension.fsa @@endtagdepends

#include <iostream>
#include <vle/extension/fsa/Statechart.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace ve = vle::extension;

namespace record {
namespace cv {

enum EcoState {BEFORE_HARVEST, AFTER_HARVEST};

class EconomicAssesment: public ve::fsa::Statechart {
public:
    EconomicAssesment(const vd::DynamicsInit& init, const vd::InitEventList& events) :
        ve::fsa::Statechart(init, events), tmp_irrigation(0), tmp_harvest(0),
                mYield(0), mDateHarvest(""), mTotalWaterConsumed(0),
        mTotalWaterCost(0), mMargin(0), mNbIrrigations(0),
        begin_date()
    {
        begin_date = vle::utils::DateTime::toJulianDayNumber(
            events.getString("begin_date"));

        states(this) << BEFORE_HARVEST << AFTER_HARVEST;

        transition(this, BEFORE_HARVEST, AFTER_HARVEST)
                << guard(&EconomicAssesment::harvest);

        eventInState(this, "Irrigation", &EconomicAssesment::inIrrigation)
                >> BEFORE_HARVEST; 
        eventInState(this, "Harvesting", &EconomicAssesment::inHarvesting)
                >> BEFORE_HARVEST;

        eventInState(this, "Yield", &EconomicAssesment::inYield)
                >> BEFORE_HARVEST;

        initialState(BEFORE_HARVEST);

        mWaterCost = vv::toDouble(events.get("waterCost"));
        mSellingPrice = vv::toDouble(events.get("sellingPrice"));
    }

    virtual ~EconomicAssesment(){}

    bool harvest(const vd::Time& /* time */)
    { return !mDateHarvest.empty(); }

    void inIrrigation(const vd::Time& /*time*/, const vd::ExternalEvent& event)
    {
        tmp_irrigation = event.attributes()->toMap().getDouble("value");
        mTotalWaterConsumed += tmp_irrigation;
        mNbIrrigations++;
        tmp_irrigation = 0;
    }

    void inHarvesting(const vd::Time& time, const vd::ExternalEvent& event)
    {
        tmp_harvest = event.attributes()->toMap().getDouble("value");
        if (tmp_harvest == 1.0) {
            mDateHarvest = vle::utils::DateTime::toJulianDayNumber(begin_date + time);
        }
    }

    void inYield(const vd::Time& /* time */, const vd::ExternalEvent& event)
    {
        tmp_yield = event.attributes()->toDouble().value();
        mYield = tmp_yield;
        mTotalWaterCost = mTotalWaterConsumed * mWaterCost;
        mMargin = ((mYield * mSellingPrice) / 100.0) - mTotalWaterCost;
    }

    virtual std::unique_ptr<vv::Value> observation(const vd::ObservationEvent& event) const
    {
        if (event.onPort("NbIrrigations")) {
            return vv::Integer::create(mNbIrrigations);
        } else if (event.onPort("Margin")) {
            return vv::Double::create(mMargin);
        } else if (event.onPort("TotalWaterCost")) {
            return vv::Double::create(mTotalWaterCost);
        } else if (event.onPort("TotalWaterConsumed")) {
            return vv::Double::create(mTotalWaterConsumed);
        } else if (event.onPort("Yield")) {
            return vv::Double::create(mYield);
        } else if (event.onPort("DateHarvest")) {
            if (mDateHarvest == "") {
                return vv::String::create("NoHarvest");
            } else {
                return vv::String::create(mDateHarvest);
            }
        } else {
            return 0;
        }
    }

private:
    //variables locales temporaires (utilisee pour les valeurs des evenement d'entree)
    /**
     * @brief quantité d'irrigation reçue. [Irrigation] <==> mm
     */
    double tmp_irrigation;
    /**
     * @brief déclenchement de la recolte. [Harvesting] <==> 0/1 - bool
     */
    double tmp_harvest;
    /**
     * @brief production de la culture. [Yield] <==> g/m^2 <==> 10^(-2) tonne / ha
     */
    double tmp_yield;

    //variables des evenements de sortie
    /**
     * @brief rendement. [Yield] <==> g/m^2 <==> 10^(-2) tonne / ha
     */
    double mYield;
    /**
     * @brief Date de recolte
     */
    std::string mDateHarvest;
    /**
     * @brief Eau consomée. [Irrigation] <==> mm  <==> (1 mm = 10^(-3) m^3 / m^2) <==> 1 l/m^2 <==> 10^4 l/ha
     */
    double mTotalWaterConsumed;
    /**
     * @brief cout de l'eau d'irrigation. [mTotalWaterConsumed]*[mWaterCost] <==> euros
     */
    double mTotalWaterCost;
    /**
     * @brief Profit. [mTotalWaterCost] <==> [mYield]*[mSellingPrice]/100 <==> euros
     */
    double mMargin;
    /**
     * @brief nombre d'irrigation effectuées.
     */
    int mNbIrrigations;

    //parametres lus dans le vpz
    /**
     * @brief cout de l'eau d'irrigation. euros/[mTotalWaterConsumed] <==> euros/mm
     */
    double mWaterCost;
    /**
     * @brief prix de vente de la culture. euros/[mYield]*100 <==> euros/(tonne/ha) <==> euros * ha / tonne
     */
    double mSellingPrice;
    /**
     * @brief the begin date to manage human date acording to the RECORD Policy
     */
    int begin_date;

};
}
}
DECLARE_DYNAMICS(record::cv::EconomicAssesment);
