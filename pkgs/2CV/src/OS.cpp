// @@tagdynamicTODO@@
// @@tagdepends: vle.extension.fsa @@endtagdepends

#include <vle/extension/fsa/Statechart.hpp>// correspond a l'extension DEVS utilise ici
#include <vle/extension/DifferenceEquation.hpp>// correspond a l'extension DEVS utilise ici
#include <vle/DiscreteTime.hpp>// inclus ici car nous utilisons les Var comme evenement d'entree
#include <vle/utils/DateTime.hpp> //pour recuperer la date a partir du moteur de simulation de VLE
#include <vle/utils/i18n.hpp>
#include <iostream>

// raccourcis de nommage des namespaces frequement utilises
namespace vdt = vle::discrete_time;
namespace vd = vle::devs;
namespace ve = vle::extension;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {
using namespace vle::temporal_values;

class OS: public ve::fsa::Statechart {
public:
    enum State { IDLE, PROCESS };// liste des differents etat possibles de l'automate

    OS(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::fsa::Statechart(atom, events), mPort(), mActivity(),
                mIrrigationAmount(0)
    {
        //definition des etats possibles de l'automate
        states(this) << IDLE << PROCESS;

        //Definition des transition entre les etats
        // "guard" defini la fonction de condition de declenchement de changement d'etat
        // "action" defini une action a mener dans le cas d'une transition
        // "send" defini un evenement sur un port de sortie dans le cas d'une transition
        transition(this, IDLE, PROCESS)
            << guard(&OS::isNotEmpty)
            << send(&OS::out);

        transition(this, PROCESS, IDLE)
            << action(&OS::clear);

        //defini la reaction aux evenements d'entree 
        eventInState(this, "Sowing", &OS::in)
            >> IDLE;
        eventInState(this, "Irrigation", &OS::in)
            >> IDLE;
        eventInState(this, "Harvesting", &OS::in)
            >> IDLE;

        //etat initial de l'automate
        initialState(IDLE);
    }

    virtual ~OS() {}

private:
    bool isNotEmpty(const vd::Time& /*time*/)
    { return not mPort.empty(); }

    void out(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    {
        if (mPort == "Sowing") {
            output << (ve::DifferenceEquation::Var("Sowing") = 1.0);

            vd::ExternalEvent* order = new vd::ExternalEvent("ack");
            order->putAttribute("name", new vv::String(mActivity));
            order->putAttribute("value", new vv::String("done"));
            output.push_back(order);
        } else if (mPort == "Harvesting") {
            output << (ve::DifferenceEquation::Var("Harvesting") = 1.0);

            vd::ExternalEvent* order = new vd::ExternalEvent("ack");
            order->putAttribute("name", new vv::String(mActivity));
            order->putAttribute("value", new vv::String("done"));
            output.push_back(order);
        } else if (mPort == "Irrigation") {
            output << (ve::DifferenceEquation::Var("Irrigation")
                    = mIrrigationAmount);

            vd::ExternalEvent* order = new vd::ExternalEvent("ack");
            order->putAttribute("name", new vv::String(mActivity));
            order->putAttribute("value", new vv::String("done"));
            output.push_back(order);
        } else {
            throw "Error";
        }
    }

    void clear(const vd::Time& /*time*/)
    { mPort.clear(); }

    void in(const vd::Time& /*time*/, const vd::ExternalEvent* evt)
    {
        mPort.assign(evt->getPortName());
        mActivity.assign(evt->getStringAttributeValue("activity"));

        if (mPort == "Irrigation") {
            mIrrigationAmount = evt->getDoubleAttributeValue("value");
        }
    }

    std::string mPort;
    std::string mActivity;
    double mIrrigationAmount;
};

}
} //namespace

DECLARE_DYNAMICS(record::cv::OS)
