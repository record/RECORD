// @@tagdynamic@@
// @@tagdepends: vle.discrete-time.decision, vle.discrete-time, vle.extension.decision @@endtagdepends

#include <vle/extension/Decision.hpp>
#include <vle/DiscreteTime.hpp>
#include <vle/AgentDT.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>

#include <sstream>
#include <numeric>
#include <iterator>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <CropDvtStage.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace ved = vle::extension::decision;
namespace vdd = vle::discrete_time::decision;
namespace vu = vle::utils;

namespace record {
namespace cv {
using namespace vle::discrete_time;

class DecAgent: public vdd::AgentDT {
public:
    DecAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts) :
        vdd::AgentDT(mdl, evts), mDateLastIrrigation(0.0)
    {
        irrigationAmount = evts.getDouble("irrigationAmount");
        waterAmountForPlotAccessibility = evts.getDouble("waterAmountForPlotAccessibility");
        quantiteEaudryPeriod = evts.getDouble("waterAmountForDryPeriod");
        nbDaysBetweenIrrigations = evts.getInt("nbDaysBetweenIrrigations");

        addFacts(this) += F("DvtStage", &DecAgent::DvtStageFact);

        addPredicates(this) += P("dryPeriod", &DecAgent::dryPeriod),
                P("maturityReached", &DecAgent::maturityReached),
                P("waterAvailable", &DecAgent::waterAvailable),
                P("plotAccesibility",&DecAgent::plotAccesibility),
                P("endSowing", &DecAgent::endSowing),
                P("endHarvest", &DecAgent::endHarvest),
                P("notHarvested",&DecAgent::notHarvested);

        addOutputFunctions(this) += O("sowing", &DecAgent::sowing),
                O("harvest",&DecAgent::harvest),
                O("irrigation", &DecAgent::irrigation);

        addAcknowledgeFunctions(this) += A("ackIrrigation", &DecAgent::ackIrrigation);

        //State Variables
        NbIrrigation.init(this, "NbIrrigation", evts);
        NbIrrigation.init_value(1.0);
        BeginningMaturity.init(this, "BeginningMaturity", evts);
        WaterStock.init(this, "WaterStock", evts);
        WaterStock.init_value(evts.getDouble("waterStock"));
        //Variables facts
        DvtStage.init(this,"DvtStage", evts);
        DvtStage.init_value(BARE_SOIL);
        Rain.init(this,"Rain", evts);
        Rain.history_size(5);

        //Initalisation du plan
        vu::ContextPtr ctx = vle::utils::make_context();
        vu::Package pack(ctx, "2CV");

        std::string filePath = pack.getDataFile("DecAgentPlan.txt");

        std::ifstream fileStream(filePath.c_str());
        KnowledgeBase::plan().fill(fileStream);
    }

    virtual ~DecAgent() {}

    void compute(const vd::Time& t)
    {
        AgentDT::compute(t);
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Predicats
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    bool dryPeriodIP() const
    {
        double acc = 0;
        for (double j=0; j< 5; j++) {
            acc += Rain(-j);
        }
        return acc <= waterAmountForPlotAccessibility;
    }
    /**
     * @brief predicate dryPeriod
     * (rain summed over the last 5 days < waterAmountForPlotAccessibility)
     */
    bool dryPeriod(const std::string& /* activity */,
                      const std::string& /* rule */,
                      const ved::PredicateParameters& /* param */) const
    {
        return dryPeriodIP();
    }

    bool maturityReachedIP() const
    { return (BeginningMaturity() > 0); }
    /**
     * @brief predicate maturityReached
     * (variable BeginningMaturity has been set)
     */
    bool maturityReached(const std::string& /* activity */,
                          const std::string& /* rule */,
                          const ved::PredicateParameters& /* param */) const
    { return maturityReachedIP(); }

    bool endHarvestIP() const
    { return current_date == BeginningMaturity() + 30; }
    /**
     * @brief predicate endHarvest
     * (maturity date outdated since 30 days)
     */
    bool endHarvest(const std::string& /* activity */,
                    const std::string& /* rule */,
                    const ved::PredicateParameters& /* param */) const
    { return endHarvestIP();}

    bool waterAvailableIP() const
    { return WaterStock() >= irrigationAmount; }

    /**
     * @brief predicat waterAvailable
     * (water stock is enough for at least one irrigation)
     */
    bool waterAvailable(const std::string& /* activity */,
                       const std::string& /* rule */,
                       const ved::PredicateParameters& /* param */) const
    { return waterAvailableIP();}
    bool plotAccesibilityIP() const
    {
        double acc = 0;
        for (double j=0; j< 3; j++) {
            acc += Rain(-j);
        }
        return acc <= waterAmountForPlotAccessibility;
    }
    /**
     * @brief predicate plotAccesibility
     * (rain summed over the last 3 days < waterAmountForPlotAccessibility)
     */
    bool plotAccesibility(const std::string& /* activity */,
                    const std::string& /* rule */,
                    const ved::PredicateParameters& /* param */) const
    {
        return plotAccesibilityIP();

    }
    bool endSowingIP() const
    { return activity("Sowing").finish() == current_date; }
    /**
     * @brief predicate endSowing
     * (we are currently at the date of the finish parameter of the activity).
     */
    bool endSowing(const std::string& /* activity */,
                  const std::string& /* rule */,
                  const ved::PredicateParameters& /* param */) const
    { return endSowingIP();}

    /**
     * @brief predicat notHarvested
     * (true if harvest has not been performed yet).
     */
    bool notHarvested(const std::string& /* activity */,
                             const std::string& /* rule */,
                             const ved::PredicateParameters& /* param */) const
    { return !activity("Harvest").isInDoneState(); }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Facts
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @brief update of fact DvtStage
     */
    void DvtStageFact(const vv::Value& /*value*/)
    {
        if (DvtStage() == MATURITY && DvtStage(-1) != MATURITY) {
            BeginningMaturity = current_date;
        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Output functions
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @brief activation of sowing
     */
    void sowing(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            {
                output.emplace_back("Sowing");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("activity",name);
                attrs.addDouble("value",1.0);
            }
            {
                output.emplace_back("ack");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("value","done");
            }
        }
    }

    /**
     * @brief activation of harvest
     */
    void harvest(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            {
                output.emplace_back("Harvesting");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("activity",name);
                attrs.addDouble("value",1.0);
            }
            {
                output.emplace_back("ack");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("value","done");
            }
        }
    }

    /**
     * @brief activation of one irrigation
     */
    void irrigation(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            {
                output.emplace_back("Irrigation");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("activity",name);
                attrs.addDouble("value",std::min(irrigationAmount, WaterStock()));
            }
            {
                output.emplace_back("ack");
                vv::Map& attrs = output.back().addMap();
                attrs.addString("name",name);
                attrs.addString("value","done");
            }
        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Acknowledge functions
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    void ackIrrigation(const std::string&
                       /*activityname*/,
                       const ved::Activity& /*activity*/)
    {
        NbIrrigation = NbIrrigation() + 1;
        std::string name(irrigationActivity(NbIrrigation()));
        std::string prec(irrigationActivity(NbIrrigation() - 1));
        mDateLastIrrigation = current_date;
        WaterStock = WaterStock(-1) - irrigationAmount;

        if (waterAvailableIP() and DvtStage() != BARE_SOIL and NbIrrigation() <= 12) {
            ved::Activity& a = addActivity(name,
                    mDateLastIrrigation + nbDaysBetweenIrrigations, 2437641.0); //correspond au debut de simulation + 340 jours
            a.addRule("ruleIrrigation", rule("ruleIrrigation"));
            a.addAcknowledgeFunction(ack("ackIrrigation"));
            a.addOutputFunction(out("irrigation"));
        }
    }

private:

    //Agent parameters
    double irrigationAmount;
    double waterAmountForPlotAccessibility;
    double quantiteEaudryPeriod;
    int nbDaysBetweenIrrigations;

    //storage variable
    vd::Time mDateLastIrrigation;

    Var NbIrrigation;
    Var BeginningMaturity;
    Var WaterStock;
    Var DvtStage;
    Var Rain;

    /**
     * @brief get an activity
     * @param name, name of the activity
     * @return activity of name 'name'
     */
    const ved::Activity& activity(const std::string& name) const
    { return activities().get(name)->second; }
    /**
     * @brief get a rule from knowledge base
     * @param rule, name of the rule
     * @return rule of name 'rule'
     */
    const ved::Rule& rule(const std::string& rule)
    { return KnowledgeBase::rules().get(rule); }

    /**
     * @brief get ack function
     * @param ack, name of the function
     * @return ack function of name 'ack'
     */
    const ved::Activity::AckFct& ack(const std::string& ack)
    { return KnowledgeBase::acknowledgeFunctions().get(ack)->second; }

    /**
     * @brief get an output function
     * @param out, name of the function
     * @return output function of name 'out'
     */
    const ved::Activity::OutFct& out(const std::string& out)
    { return KnowledgeBase::outputFunctions().get(out)->second; }

    /**
     * @brief tells if the activity is a supllementary irrigation activity
     * (other than the first one)
     * @param act, name of activity
     * @return true if the activity is a supplementary activity
     */
    bool isSupplementaryIrrigation(const std::string& act) const
    { return (act.size() >= 11 and act.compare(0, 10, "Irrigation") == 0); }

    /**
     * @brief get the name of the activity corresponding to a number
     * @param nbr, number of irrigation activity
     * @return name of the irrigation activity corresponding to the
     *         'nbr' th irrigation activity
     */
    std::string irrigationActivity(int nbr) const
    {
        std::stringstream ret;
        ret << "Irrigation_";
        ret << vle::utils::format("%1$02d", nbr);
        return ret.str();

    }

    /**
     * @brief DEVS observation function
     */
    std::unique_ptr<vv::Value> observation(const vle::devs::ObservationEvent& event) const
    {
        std::string port_name = event.getPortName();
        if (port_name == "Harvest" or port_name == ".Sowing" or port_name
                == "Irrigation_01" or isSupplementaryIrrigation(port_name)) {

            if (port_name == ".Sowing")
                port_name = "Sowing";
            std::string obs = port_name + ":";

            if (activities().exist(port_name)) {
                const ved::Activity& act = activities().get(port_name)->second;
                std::string interval = "ext";
                if (event.getTime() >= act.start() and event.getTime()
                        <= act.finish()) {
                    interval = "int";
                }
                obs = port_name + ":";
                obs = obs + interval;
                switch (act.state()) {
                    case ved::Activity::WAIT:
                        obs = obs + "-wait";
                        break;
                    case ved::Activity::STARTED:
                        obs = obs + "-started";
                        break;
                    case ved::Activity::FF:
                        obs = obs + "-started-ff";
                        break;
                    case ved::Activity::DONE:
                        obs = obs + "-done";
                        break;
                    case ved::Activity::FAILED:
                        obs = obs + "-failed";
                        break;
                }

            }
            return vv::String::create(obs);
        } else if (port_name == ".Date") {
            return vv::String::create(
                    vu::DateTime::toJulianDayNumber((long) event.getTime()));
        } else if (event.onPort("waterAvailable")) {
            if (waterAvailableIP()) {
                return vv::String::create("waterAvailable");
            } else {
                return vv::String::create("false");
            }
        } else if (event.onPort("endHarvest")) {
            if (endHarvestIP()) {
                return vv::String::create("endHarvest");
            } else {
                return vv::String::create("false");
            }
        } else if (event.onPort("endSowing")) {
            if (endSowingIP()) {
                return vv::String::create("endSowing");
            } else {
                return vv::String::create("false");
            }
        } else if (event.onPort("maturityReached")) {
            if (maturityReachedIP()) {
                return vv::String::create("maturityReached");
            } else {
                return vv::String::create("false");
            }
        } else if (event.onPort("dryPeriod")) {
            if (dryPeriodIP()) {
                return vv::String::create("dryPeriod");
            } else {
                return vv::String::create("false");
            }
        } else if (event.onPort("plotAccesibility")) {
            if (plotAccesibilityIP()) {
                return vv::String::create("plotAccesibility");
            } else {
                return vv::String::create("false");
            }
        }
        return vdd::AgentDT::observation(event);
    }

};

}
} // namespaces

DECLARE_DYNAMICS(record::cv::DecAgent)
