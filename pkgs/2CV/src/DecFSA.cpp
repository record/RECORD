// @@tagdynamic@@
// @@tagdepends: vle.extension.fsa @@endtagdepends

#ifndef DECISION_DecFSA_CPP
#define DECISION_DecFSA_CPP

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/value/Set.hpp>
#include <algorithm>
#include <iostream>

// Raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace ve = vle::extension;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

enum DecState {INIT, DEC_SOWING, DEC_IRRIG, DEC_HARVEST};// liste des differents etat possibles de l'automate

class DecFSA: public ve::fsa::Statechart {
public:
    DecFSA(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::fsa::Statechart(atom, events), IrrigationAmount(0), Rain(0)
    {
        // Lecture des valeurs de parametres dans les conditions du vpz
        IrrigationAmount = events.getDouble("IrrigationAmount");
        const vv::Set& irrDays = events.getSet("IrrigationDays");
        sowingDay = events.getInt("SowingDay");
        harvestingDay = events.getInt("HarvestingDay");

        //initialisation de la taille du vecteur irrigationDays
        for (unsigned int i = 0; i < irrDays.size(); i++) {
            irrigationDays.push_back(irrDays.getInt(i));
        }
        std::sort(irrigationDays.begin(), irrigationDays.end());
        nextIrrigationIndex = 0;

        //definition des etats possibles de l'automate
        states(this) << INIT << DEC_SOWING << DEC_IRRIG << DEC_HARVEST;

        //Definition des transition entre les etats
        // "when" defini la fonction determinant la date a laquelle se fera la transition
        // "action" defini une action a mener dans le cas d'une transition
        // "send" defini un evenement sur un port de sortie dans le cas d'une transition
        transition(this, INIT, DEC_SOWING)
            << action(&DecFSA::initDecFSA);
        transition(this, DEC_SOWING, DEC_IRRIG)
            << when(&DecFSA::dateSowing)
            << send(&DecFSA::out_sowing);
        transition(this, DEC_IRRIG, DEC_IRRIG)
            << when(&DecFSA::dateNextIrrigation)
            << action(&DecFSA::increaseIrrigation)
            << send(&DecFSA::out_irrig);
        transition(this, DEC_IRRIG, DEC_HARVEST)
            << when(&DecFSA::dateHarvesting)
            << send(&DecFSA::out_harvesting);

        //etat initial de l'automate
        initialState(INIT);
    }

    virtual ~DecFSA() {}

    //actions
    void initDecFSA(const vd::Time& julianDay)
    { beginDay = julianDay; }

    void increaseIrrigation(const vd::Time& /*time*/)
    { nextIrrigationIndex++; }

    //when predicates
    vd::Time dateSowing(const vd::Time& /*time*/)
    { return beginDay + sowingDay; }

    vd::Time dateNextIrrigation(const vd::Time& /*time*/)
    {
        if (nextIrrigationIndex < irrigationDays.size()) {
            return beginDay + irrigationDays[nextIrrigationIndex];
        } else {
            return vd::infinity;
        }
    }

    vd::Time dateHarvesting(const vd::Time& /*time*/)
    { return beginDay + harvestingDay; }

    //send
    void out_sowing(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    {
        output.emplace_back("Sowing");
        vv::Map& attrs = output.back().addMap();
        attrs.addString("name", "Sowing");
        attrs.addDouble("value", 1.0);
    }

    void out_harvesting(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    {
        output.emplace_back("Harvesting");
        vv::Map& attrs = output.back().addMap();
        attrs.addString("name", "Harvesting");
        attrs.addDouble("value", 1.0);
    }

    void out_irrig(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    {
        output.emplace_back("Irrigation");
        vv::Map& attrs = output.back().addMap();
        attrs.addString("name", "Irrigation");
        attrs.addDouble("value", IrrigationAmount);
    }

    // Genere un evenement d'observation : calcul l'etat courant du modele a un temps donne et pour un port de sortie specifique
    virtual std::unique_ptr<vv::Value> observation(const vd::ObservationEvent& /*event*/) const
    { return vv::Integer::create(currentState()); }

private:
    double IrrigationAmount;
    double Rain;

    unsigned int nextIrrigationIndex;
    vd::Time beginDay;

    std::vector<int> irrigationDays;
    int sowingDay;
    int harvestingDay;

};

}
}//namespaces

DECLARE_DYNAMICS(record::cv::DecFSA);
#endif
