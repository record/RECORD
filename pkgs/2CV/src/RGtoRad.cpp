/**
  * @file RGtoRad.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace CV {

class RGtoRad : public DiscreteTimeDyn
{
public:
RGtoRad(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    RG.init(this, "RG", evts);
    Rad.init(this, "Rad", evts);


}

virtual ~RGtoRad()
{}

void compute(const vle::devs::Time& /*t*/)
{
Rad = RG() /100.0;
}

    Var RG;
    Var Rad;

};

} // namespace CV
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::CV::RGtoRad)

