// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>// correspond a l'extension DEVS utilise ici

// raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {
using namespace vle::discrete_time;

class SoilET: public DiscreteTimeDyn {
public:
    SoilET(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {

        // Lecture des valeurs de parametres dans les conditions du vpz
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        xtinc = (events.exist("xtinc")) ? vv::toDouble(events.get("xtinc")) : 0.7;
        p1Evap = (events.exist("p1Evap")) ? vv::toDouble(events.get("p1Evap")) : 0.075;
        p2Evap = (events.exist("p2Evap")) ? vv::toDouble(events.get("p2Evap")) : 1.2;
        p3Evap = (events.exist("p3Evap")) ? vv::toDouble(events.get("p3Evap")) : 0.3;
        pke = (events.exist("pke")) ? vv::toDouble(events.get("pke")) : 1.25;
        r1Tran = (events.exist("r1Tran")) ? vv::toDouble(events.get("r1Tran")) : 0.4;
        r2Tran = (events.exist("r2Tran")) ? vv::toDouble(events.get("r2Tran")) : 0.6;

        // Variables d'etat gerees par ce composant
        ATPT.init(this,"ATPT", events);
        AT.init(this,"AT", events);
        AT2.init(this,"AT2", events);
        AT3.init(this,"AT3", events);
        PT.init(this,"PT", events);
        AE.init(this,"AE", events);
        AE1.init(this,"AE1", events);
        AE2.init(this,"AE2", events);
        PE.init(this,"PE", events);

        // Variables  gerees par un autre  composant
        ETP.init(this,"ETP", events);
        ALAI.init(this,"ALAI", events);
        FAW1.init(this,"FAW1", events);
        FAW2.init(this,"FAW2", events);
        FAW3.init(this,"FAW3", events);
        TH1.init(this,"TH1", events);
        TH2.init(this,"TH2", events);
        TH3.init(this,"TH3", events);
        Q1.init(this,"Q1", events);
        Q2.init(this,"Q2", events);
        Q3.init(this,"Q3", events);
        Qx2.init(this,"Qx2", events);
        Qx3.init(this,"Qx3", events);

        //Initialisation WARNING
        double root_depth = 0.0;
        TH1.init_value(20.0);
        TH2.init_value(280.0);
        TH3.init_value(std::max(0., root_depth - (TH1() + TH2())));
    }

    virtual ~SoilET() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        PE = ETP() * std::exp(-xtinc * ALAI());
        PT = ETP() - PE();

        if (FAW1(-1) > 0) {
            AE = PE() * (p1Evap + p2Evap * exp(-p3Evap / FAW1(-1)));
        } else {
            AE = 0;
        }

        double f1 = FAW1(-1) * (1 - std::exp(-pke * TH1(-1)));
        double f2 = FAW2(-1) * (std::exp(-pke * TH1(-1)) - std::exp(
                -pke * TH2(-1)));
        if (f1 + f2 > 0) {

            AE1 = std::min(Q1(-1), AE() * f1 / (f1 + f2));
            AE2 = std::min(Q2(-1), AE() * f2 / (f1 + f2));
        } else {
            AE1 = 0;
            AE2 = 0;
        }

        double FAW23 = 0;

        if (Qx2(-1) + Qx3(-1) > 0) {
            FAW23 = (Q2(-1) + Q3(-1)) / (Qx2(-1) + Qx3(-1));
        }

        AT = std::min(Q2(-1) + Q3(-1), PT() * reduc(FAW23, r1Tran, r2Tran));

        if (PT() > 0) {
            ATPT = AT() / PT();
        } else {
            ATPT = 1;
        }

        double tempAT2 = std::min(Q2(-1), AT() * TH2(-1) / (TH2(-1) + TH3(-1)));
        double tempAT3 = std::min(Q3(-1), AT() * TH3(-1) / (TH2(-1) + TH3(-1)));

        if (Q2(-1) < AT() * TH2(-1) / (TH2(-1) + TH3(-1))) {
            AT3 = tempAT3 + AT() * TH2(-1) / (TH2(-1) + TH3(-1)) - Q2(-1);
        } else {
            AT3 = tempAT3;
        }

        if (Q3(-1) < AT() * TH3(-1) / (TH2(-1) + TH3(-1))) {
            AT2 = tempAT2 + AT() * TH3(-1) / (TH2(-1) + TH3(-1)) - Q3(-1);
        } else {
            AT2 = tempAT2;
        }
    }

private:
    //Variables d'etat
    Var ATPT;
    Var AT;
    Var AT2;
    Var AT3;
    Var PT;
    Var AE;
    Var AE1;
    Var AE2;
    Var PE;

    //Entrées
    Var ETP;
    Var ALAI;
    Var FAW1;
    Var FAW2;
    Var FAW3;
    Var TH1;
    Var TH2;
    Var TH3;
    Var Q1;
    Var Q2;
    Var Q3;
    Var Qx2;
    Var Qx3;

    //Parametres du modele
    double xtinc;
    double p1Evap;
    double p2Evap;
    double p3Evap;
    double pke;
    double r1Tran;
    double r2Tran;

    //fonction locale
    double reduc(double x, double p1, double p2)
    {
        if (x < p2 - p1) {
            return 0;
        } else {
            if (x > p2) {
                return 1;
            } else {
                return (p1 - p2 + x) / p1;
            }
        }
    }

};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::SoilET); // balise specifique VLE

