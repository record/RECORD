// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>


namespace record {
namespace cv {

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vdt = vle::discrete_time;

using namespace vle::discrete_time;

class SoilSurfaceIncomingWater: public DiscreteTimeDyn {
public:
    SoilSurfaceIncomingWater(const vd::DynamicsInit& atom,
            const vd::InitEventList& events) :
                DiscreteTimeDyn(atom, events)
    {
        // Lecture des parametres
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        fracRunOff = (events.exist("fracRunOff")) ? vv::toDouble(events.get("fracRunOff")) : 0;

        // Variables d'etat gerees par ce composant
        Irrigation.init(this, "Irrigation", events);
        Infiltration.init(this, "Infiltration", events);
        IncomingRunOff.init(this, "IncomingRunOff", events);
        RunOff.init(this, "RunOff", events);

        // Variables  gerees par un autre  composant
        Rain.init(this, "Rain", events);
    }

    virtual ~SoilSurfaceIncomingWater() {}

    virtual void compute(const vd::Time& /*julianDay*/)
    {
        //variables qui peuvent être perturbées
        Irrigation = 0.0;
        IncomingRunOff = 0.0;
        //calcul des autres variables d'etat
        RunOff = (Rain() + Irrigation() + IncomingRunOff()) * fracRunOff;
        Infiltration = (Rain() + Irrigation() + IncomingRunOff()) * (1 - fracRunOff);
    }

private:
    //Variables d'etat
    Var Irrigation;
    Var Infiltration;
    Var IncomingRunOff;
    Var RunOff;

    //Entrées
    Var Rain;

    //Parametres du modele
    double fracRunOff;
};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::SoilSurfaceIncomingWater); // balise specifique VLE


