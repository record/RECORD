// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>// correspond a l'extension DEVS utilise ici


// raccourcis de nommage des namespaces frequement utilises

namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

using namespace vle::discrete_time;

class SoilSWCB: public DiscreteTimeDyn {
public:
    SoilSWCB(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {

        // Lecture des parametres
        soilDepth = vv::toDouble(events.get("soilDepth"));
        FC12 = vv::toDouble(events.get("FC12"));
        FC34 = vv::toDouble(events.get("FC34"));
        WP12 = vv::toDouble(events.get("WP12"));
        WP34 = vv::toDouble(events.get("WP34"));

        // Variables d'etat gerees par ce composant
        FAW1.init(this,"FAW1", events);
        FAW2.init(this,"FAW2", events);
        FAW3.init(this,"FAW3", events);
        FAW4.init(this,"FAW4", events);

        TH1.init(this,"TH1", events);
        TH2.init(this,"TH2", events);
        TH3.init(this,"TH3", events);
        TH4.init(this,"TH4", events);

        Q1.init(this,"Q1", events);
        Q2.init(this,"Q2", events);
        Q3.init(this,"Q3", events);
        Q4.init(this,"Q4", events);

        Qx1.init(this,"Qx1", events);
        Qx2.init(this,"Qx2", events);
        Qx3.init(this,"Qx3", events);
        Qx4.init(this,"Qx4", events);

        S1.init(this,"S1", events);
        S2.init(this,"S2", events);
        S3.init(this,"S3", events);
        S4.init(this,"S4", events);

        Drainage1.init(this,"Drainage1", events);
        Drainage2.init(this,"Drainage2", events);
        Drainage3.init(this,"Drainage3", events);
        Drainage4.init(this,"Drainage4", events);

        FAW12.init(this,"FAW12", events);
        FAW123.init(this,"FAW123", events);

        // Variables  gerees par un autre  composant
        RootDepth.init(this,"RootDepth", events);
        Infiltration.init(this,"Infiltration", events);
        AE1.init(this,"AE1", events);
        AE2.init(this,"AE2", events);
        AT2.init(this,"AT2", events);
        AT3.init(this,"AT3", events);


        FAW1.init_value(0.0);
        FAW2.init_value(0.0);
        FAW3.init_value(0.0);
        FAW4.init_value(0.0);

        TH1.init_value(20.0);
        TH2.init_value(280.0);
        TH3.init_value(std::max(0., RootDepth() - (TH1() + TH2())));
        TH4.init_value(std::max(0., soilDepth - (TH1() + TH2() + TH3())));

        Q1.init_value(0.0);
        Q2.init_value(0.0);
        Q3.init_value(0.0);
        Q4.init_value(0.0);

        Qx1.init_value((FC12 - WP12) * TH1());
        Qx2.init_value((FC12 - WP12) * TH2());
        Qx3.init_value((FC34 - WP34) * TH3());
        Qx4.init_value((FC34 - WP34) * TH4());

        S1.init_value(WP12 * TH1());
        S2.init_value(WP12 * TH2());
        S3.init_value(WP34 * TH3());
        S4.init_value(WP34 * TH4());

        Drainage1.init_value(0.0);
        Drainage2.init_value(0.0);
        Drainage3.init_value(0.0);
        Drainage4.init_value(0.0);

        FAW12.init_value(0.0);
        FAW123.init_value(0.0);
    }

    virtual ~SoilSWCB() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        TH1 = 20;
        TH2 = 280;
        TH3 = std::max(0., RootDepth() - (TH1() + TH2()));
        TH4 = std::max(0., soilDepth - (TH1() + TH2() + TH3()));

        double S3temp = S3(-1);
        double S4temp = S4(-1);
        if (TH4() < TH4(-1)) {
            S4temp = S4(-1) * TH4() / TH4(-1);
            S3temp = S3(-1) + S4(-1) - S4temp;
        }
        if (TH4() > TH4(-1)) {
            S3temp = S3(-1) * TH3() / TH3(-1);
            S4temp = S4(-1) + S3(-1) - S3temp;
        }

        Q1 = std::max(0., S1(-1) - WP12 * TH1());
        Q2 = std::max(0., S2(-1) - WP12 * TH2());
        Q3 = std::max(0., S3temp - WP34 * TH3());
        Q4 = std::max(0., S4temp - WP34 * TH4());

        Qx1 = (FC12 - WP12) * TH1();
        Qx2 = (FC12 - WP12) * TH2();
        Qx3 = (FC34 - WP34) * TH3();
        Qx4 = (FC34 - WP34) * TH4();

        FAW1 = Q1() / Qx1();
        FAW2 = Q2() / Qx2();
        if (Qx3() > 0) {
            FAW3 = Q3() / Qx3();
        } else {
            FAW3 = 0;
        }
        if (Qx4() > 0) {
            FAW4 = Q4() / Qx4();
        } else {
            FAW4 = 0;
        }

        double Stemp;
        Stemp = S1(-1) + Infiltration() - AE1();
        Drainage1 = std::max(0., Stemp - (FC12 * TH1()));
        S1 = std::max(0., Stemp - Drainage1());

        Stemp = S2(-1) + Drainage1() - AE2() - AT2();
        Drainage2 = std::max(0., Stemp - (FC12 * TH2()));
        S2 = std::max(0., Stemp - Drainage2());

        Stemp = S3temp + Drainage2() - AT3();
        Drainage3 = std::max(0., Stemp - (FC34 * TH3()));
        S3 = std::max(0., Stemp - Drainage3());

        Stemp = S4temp + Drainage3();
        Drainage4 = std::max(0., Stemp - (FC34 * TH4()));
        S4 = std::max(0., Stemp - Drainage4());

        FAW12 = (Q1() + Q2()) / (Qx1() + Qx2());
        FAW123 = (Q1() + Q2() + Q3()) / (Qx1() + Qx2() + Qx3());
    }

private:
    //Variables d'etat
    Var FAW1;
    Var FAW2;
    Var FAW3;
    Var FAW4;

    Var TH1;
    Var TH2;
    Var TH3;
    Var TH4;

    Var Q1;
    Var Q2;
    Var Q3;
    Var Q4;

    Var Qx1;
    Var Qx2;
    Var Qx3;
    Var Qx4;

    Var S1;
    Var S2;
    Var S3;
    Var S4;

    Var Drainage1;
    Var Drainage2;
    Var Drainage3;
    Var Drainage4;

    Var FAW12;
    Var FAW123;

    //Entrées
    Var RootDepth;
    Var Infiltration;
    Var AE1;
    Var AE2;
    Var AT2;
    Var AT3;

    //Parametres du modele
    double soilDepth;
    double FC12;
    double FC34;
    double WP12;
    double WP34;

};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::SoilSWCB); // balise specifique VLE

