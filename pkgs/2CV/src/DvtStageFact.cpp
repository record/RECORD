/**
  * @file DvtStageFact.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace deCV {

class DvtStageFact : public DiscreteTimeDyn
{
public:
DvtStageFact(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    DvtStage.init(this, "DvtStage", evts);
    BeginningMaturity.init(this, "BeginningMaturity", evts);


}

virtual ~DvtStageFact()
{}

void compute(const vle::devs::Time& t)
{
if (DvtStage() == MATURITY && DvtStage(-1) != MATURITY) {
   BeginningMaturity = t;
}

}

    Var DvtStage;
    Var BeginningMaturity;
enum State {
    SOWING, /* 0. */
    EMERGENCE, /* 1. */
    MAX_LAI, /* 2. */
    FLOWERING, /* 3. */
    CRITICAL_GRAIN_ABORTION, /* 4. */
    LEAF_SENESCENCE, /* 5. */
    MATURITY, /* 6. */
    BARE_SOIL /* 7. */
};
};

} // namespace 2CV
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::deCV::DvtStageFact)
