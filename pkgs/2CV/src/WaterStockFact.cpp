/**
  * @file WaterStockFact.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace deCV {

class WaterStockFact : public DiscreteTimeDyn
{
public:
WaterStockFact(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    irrigationAmount.init(this, "irrigationAmount", evts);
    WaterStock.init(this, "WaterStock", evts);


}

virtual ~WaterStockFact()
{}

void compute(const vle::devs::Time& t)
{
WaterStock = WaterStock(-1) - irrigationAmount();
}

    Var irrigationAmount;
    Var WaterStock;

};

} // namespace 2CV
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::deCV::WaterStockFact)
