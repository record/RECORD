// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <CropDvtStage.hpp>
#include <vle/utils/DateTime.hpp>
#include <iomanip>

namespace vd = vle::devs;
namespace vv = vle::value;

namespace record {
namespace cv {
using namespace vle::discrete_time;

/**
 * @brief Croissance du front racinaire basé sur le temps thermique.
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 * #include <CropDvtStage.hpp>
 *
 * Le modèle calcul à chaque pas de temps la variable d'état :
 * - RootDepth : profondeur du front racinaire. (mm)
 *
 * A partir des données d'entrée :
 * - TT : temps thermique (°C.j)
 * - DvtStage : le stade phenologique de la plante (cf CropDvtStage.hpp) (-)
 *
 * Et des paramètres :
 * - soilDepth : la profondeur du sol. (mm)
 * - maxDepth : la profondeur maximal atteignable par les racines. (mm)
 * - rateDepth : la croissance journaliere maximale du front racinaire (mm/j)
 * - iniDepth : la profondeur initiale à l'emergence (mm)
 */
class CropRoot: public DiscreteTimeDyn {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Var) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    CropRoot(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {
        // Lecture des valeurs de parametres dans les conditions du vpz
        soilDepth = vv::toDouble(events.get("soilDepth"));
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        maxDepth = (events.exist("maxDepth")) ? vv::toDouble(events.get("maxDepth")) : 1300;
        rateDepth = (events.exist("rateDepth")) ? vv::toDouble(events.get("rateDepth")) : 1.63;
        iniDepth = (events.exist("iniDepth")) ? vv::toDouble(events.get("iniDepth")) : 100;

        RootDepth.init(this,"RootDepth", events);

        // Variables gerees par un autre composant
        TT.init(this,"TT", events);
        DvtStage.init(this,"DvtStage", events);
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropRoot() {};

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/)
    {
        if (DvtStage() == BARE_SOIL) {
            RootDepth = 0.0;
        } else {
            double dRootDepth = 0;
            switch ((int) DvtStage()) {
                case SOWING:
                    dRootDepth = 0;
                    RootDepth = 0;
                    break;
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                    if (DvtStage(-1) == SOWING) {
                        dRootDepth = iniDepth;
                    } else {
                        dRootDepth = std::min(
                                std::min(soilDepth - RootDepth(-1),
                                        maxDepth - RootDepth(-1)),
                                rateDepth * (TT() - TT(-1)));
                    }
                    RootDepth = RootDepth(-1) + dRootDepth;
                    break;
            }
        }
    }

private:
    //Variables d'etat
    /**
     * @brief profondeur du front racinaire. (mm)
     */
    Var RootDepth;

    //Entrées
    /**
     * @brief temps thermique (°C.j)
     */
    Var TT;
    /**
     * @brief le stade phenologique de la plante (cf CropDvtStage.hpp) (-)
     */
    Var DvtStage;

    //Parametres du modele
    /**
     * @brief la profondeur du sol. (mm)
     */
    double soilDepth;
    /**
     * @brief la profondeur maximal atteignable par les racines. (mm)
     */
    double maxDepth;
    /**
     * @brief la croissance journaliere maximale du front racinaire (mm/j)
     */
    double rateDepth;
    /**
     * @brief la profondeur initiale à l'emergence (mm)
     */
    double iniDepth;

};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::CropRoot); // balise specifique VLE
