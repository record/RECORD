// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>// correspond a l'extension DEVS utilise ici

// raccourcis de nommage des namespaces frequement utilises

namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {
using namespace vle::discrete_time;

class SoilRunOff: public DiscreteTimeDyn {
public:
    SoilRunOff(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {

        // Lecture des parametres
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        fracRunOff = (events.exist("fracRunOff")) ? vv::toDouble(events.get("fracRunOff")) : 0.1;

        // Variables d'etat gerees par ce composant
        RunOff.init(this,"RunOff", events);
        Infiltration.init(this,"Infiltration", events);

        // Variables  gerees par un autre  composant
        Rain.init(this,"Rain", events);
        Irrigation.init(this,"Irrigation", events);
        IncomingRunOff.init(this,"IncomingRunOff", events);
    }

    virtual ~SoilRunOff() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        RunOff = (Rain() + Irrigation() + IncomingRunOff()) * fracRunOff;
        Infiltration = (Rain() + Irrigation() + IncomingRunOff()) * (1 - fracRunOff);
    }

private:
    //Variables d'etat
    Var RunOff;
    Var Infiltration;

    //Entrées
    Var Rain;
    Var Irrigation;
    Var IncomingRunOff;

    //Parametres du modele
    double fracRunOff;

};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::SoilRunOff); // balise specifique VLE

