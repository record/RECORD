/**
  * @file EconomicAssesmentDT.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>
#include <iostream>
namespace vd = vle::devs;

namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace deCV {

class EconomicAssesmentDT : public DiscreteTimeDyn
{
public:
EconomicAssesmentDT(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    nbIrrigations.init(this, "nbIrrigations", evts);
    Margin.init(this, "Margin", evts);
    TotalWaterCost.init(this, "TotalWaterCost", evts);
    TotalWaterConsumed.init(this, "TotalWaterConsumed", evts);
    DateHarvest.init(this, "DateHarvest", evts);
    Harvesting.init(this, "Harvesting", evts);
    Irrigation.init(this, "Irrigation", evts);
    Yield.init(this, "Yield", evts);
    WaterCost.init(this, "WaterCost", evts);
    SellingPrice.init(this, "SellingPrice", evts);
    YieldIN.init(this, "YieldIN", evts);


}

virtual ~EconomicAssesmentDT()
{}

void compute(const vle::devs::Time& t)
{
if (DateHarvest() == 0 && Harvesting() == 0) {
    nbIrrigations = nbIrrigations(-1) + (Irrigation() != 0);
   TotalWaterConsumed = TotalWaterConsumed(-1) + Irrigation();
   TotalWaterCost = TotalWaterConsumed() * WaterCost();
   Yield = YieldIN();
   Margin = ((Yield() * SellingPrice()) / 100.0) - TotalWaterCost();
}
if (Harvesting() != 0) {
   DateHarvest = t;
}
}

    Var nbIrrigations;
    Var Margin;
    Var TotalWaterCost;
    Var TotalWaterConsumed;
    Var DateHarvest;
    Var Harvesting;
    Var Irrigation;
    Var Yield;
    Var WaterCost;
    Var SellingPrice;
    Var YieldIN;

};

} // namespace 2CV
} // namespace discrete_time
} // namespace vle

DECLARE_DYNAMICS(vle::discrete_time::deCV::EconomicAssesmentDT)
