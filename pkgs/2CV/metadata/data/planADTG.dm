<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata version="1.x" author="meto">
 <dataPlugin package="vle.discrete-time.decision" name="Plan Activities"/>
 <dataModel outputsType="discrete-time" package="2CV" conf="planADTG"/>
 <definition>
  <activities>
   <activity timeLag="1" value="-220" y="-483" minstart="1961-4-5" x="-1425" name="Sowing" maxfinish="1961-6-10" maxiter="1">
    <outputParams>
     <outputParam value="1" name="Sowing"/>
    </outputParams>
    <rulesAssigment>
     <rule name="ruleSowing"/>
    </rulesAssigment>
   </activity>
  <activity timeLag="1" value="-26" y="-414" minstart="" x="-1344" name="harvesting" maxfinish="" maxiter="1">
    <outputParams>
     <outputParam value="1" name="Harvesting"/>
    </outputParams>
    <rulesAssigment>
     <rule name="ruleHarvesting"/>
    </rulesAssigment>
   </activity>
  <activity timeLag="1" value="-164" y="-331" minstart="1961-6-15" x="-1234" timelag="9" name="Irrigation" maxfinish="1961-12-5" maxiter="12">
    <outputParams>
     <outputParam value="30" name="irrigationAmount"/>
    </outputParams>
    <rulesAssigment>
     <rule name="ruleIrrigation"/>
    </rulesAssigment>
   </activity>
  </activities>
  <predicates>
   <predicate operator="&lt;=" leftValue="days3rain" leftType="Var" rightValue="10" name="plotAccesibility" rightType="Val"/>
  <predicate operator=">" leftValue="beginnningMaturity" leftType="Var" rightValue="0." name="maturityReached" rightType="Val"/>
  <predicate operator="&lt;=" leftValue="days4rain" leftType="Var" rightValue="10" name="dryPeriod" rightType="Val"/>
   <predicate operator=">=" leftValue="WaterStock" leftType="Var" rightValue="30" name="waterAvailable" rightType="Val"/>
  </predicates>
  <rules>
   <rule name="ruleSowing">
    <predicate name="plotAccesibility"/>
   </rule>
  <rule name="ruleHarvesting">
    <predicate name="plotAccesibility"/>
    <predicate name="maturityReached"/>
   </rule>
  <rule name="ruleIrrigation">
    <predicate name="dryPeriod"/>
    <predicate name="waterAvailable"/>
   </rule>
  </rules>
  <precedences/>
 </definition>
 <configuration>
  <dynamic library="agentDTG" package="vle.discrete-time.decision" name="dynagentDTG"/>
  <observable name="obsplanADTG">
   <port name="Knowledgebase"/>
   <port name="Activities"/>
   <port name="Activity_Sowing"/>
   <port name="Activity(state)_Sowing"/>
   <port name="Activity(ressources)_Sowing"/>
   
   <port name="days3rain"/>
  <port name="Activity_harvesting"/>
   <port name="Activity(state)_harvesting"/>
   <port name="Activity(ressources)_harvesting"/>
   
   <port name="beginnningMaturity"/>
  <port name="Activity_Irrigation"/>
   <port name="Activity(state)_Irrigation"/>
   <port name="Activity(ressources)_Irrigation"/>
   
   <port name="WaterStock"/>
   <port name="days4rain"/>
  <port name="Sowing"/>
   <port name="irrigationAmount"/>
   <port name="Harvesting"/>
  </observable>
  <condition name="condplanADTG">
   <port name="dyn_allow">
    <boolean>1</boolean>
   </port>
   
   <port name="autoAck">
    <boolean>1</boolean>
   </port>
   <port name="PlansLocation">
    <string>2CV</string>
   </port>
   <port name="Rotation">
    <map>
     <key name="">
      <set>
       <integer>1</integer>
       <set>
        <integer>1</integer>
        <string>planADTG</string>
       </set>
      </set>
     </key>
    </map>
   </port>
  </condition>
  <in>
   <port name="days3rain"/>
  <port name="beginnningMaturity"/>
  <port name="WaterStock"/>
   <port name="days4rain"/>
  </in>
  <out>
   <port name="Sowing"/>
  <port name="Harvesting"/>
  <port name="irrigationAmount"/>
  </out>
 </configuration>
</vle_project_metadata>
