/**
 * @file src/BYDV.hpp
 * @author J.Soudais - INRA
 */

/*
 * Copyright (C) 2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef APHIDS_HPP  
#define APHIDS_HPP

#include <vle/extension/DifferenceEquation.hpp>
#include <fstream>
using namespace std;
using namespace vle;

namespace aphids { 

class APHIDS : public vle::extension::DifferenceEquation::Multiple
{
public:
    APHIDS(const vle::devs::DynamicsInit& atom,
      const devs::InitEventList& events) :
     vle::extension::DifferenceEquation::Multiple(atom, events)
  {
    // Variables d'etat gerees par ce composant "APHIDS"
    HONEY=createVar("HONEY");
    RSAP=createVar("RSAP");
    RFAPH=createVar("RFAPH");
    
    // Variables  gerees par un autre  composant que "APHIDS"   
    // "WHEATPEST_MS"
    DVS = createSync("DVS"); // cette variable est synchrone
    
    //"RUN"
    PAPH = createSync("PAPH"); // cette variable est synchrone
    
    // Lecture des parametres
    AHONEY = vle::value::toDouble(events.get("AHONEY"));
    BHONEY = vle::value::toDouble(events.get("BHONEY"));
    CHONEY = vle::value::toDouble(events.get("CHONEY"));
        
       
  }
    virtual ~APHIDS() {}; 
    
    virtual void compute(const vle::devs::Time&  time ) ;
    virtual void initValue(const vle::devs::Time&  time);

private:
    //Variables d'etat
    Var HONEY; /**< State variable Honeydew weight depending on DVS (g)*/
    Var RSAP; /**< State variable Rate of RUE reduction due to assimilates diversion by aphids depending on DVS (-)*/
    Var RFAPH; /**< State variable Rate of RUE reduction due to honeydew of aphids depending on DVS (-)*/
    
    //Variables  gerees par un autre  composant que "APHIDS"
    Sync DVS; /**< Synchron variable Stage development (-)*/

    //"RUN"
    Sync PAPH; /**< Synchron variable PAPH (0/1)*/
        
    //Parametres du modele
	double AHONEY; /**< Honeydew deposition rate,  (-)*/
    double BHONEY; /**< Coefficient to calculate the rate of reduction of RUE due to honeydew,  (g-1.m-2)*/
    double CHONEY; /**< Maximum rate of reduction of RUE due to honeydew,  (-)*/
    
    //Les fichiers de points d'observation fournis en param�tre
    ifstream APHNBTf; /**< External file; */
    ifstream SFWAPTf; /**< External file; */
    ifstream RRSAPTf; /**< External file; */
    string line;
    
    //Variables d'entr�e du mod�le
        int APHNBTn; /**<Number of observation points, Number of aphids per m2 at different times */
        double *APHNBTx  ; /** <Observations "Number of aphids per m2":  time vector */
        double *APHNBTy; /** <Observations "Number of aphids per m2":  number vector */

        int SFWAPTn; /**<Number of observation points, Specific Fresh Weight of Aphid, fresh biomass of an individual aphid (g) at different DVS (wheat development state) */
        double *SFWAPTx; /** <Observations "Specific Fresh Weight of Aphid, fresh biomass of an individual aphid,(g) at different DVS (wheat development state)  ":  time vector */
        double *SFWAPTy; /** <Observations "Specific Fresh Weight of Aphid, fresh biomass of an individual aphid,(g) at different DVS (wheat development state) ":  number vector */

        int RRSAPTn; /**<Number of observation points, " Daily relative feeding rate of aphids, at different DVS (wheat development state),  (g.g-1.d-1)": Number vector */
        double *RRSAPTx; /** <Observations "Daily relative feeding rate of aphids, at different DVS (wheat development state), at different DVS (wheat development state)  (g.g-1.d-1)"  */
        double *RRSAPTy; /** <Observations " Daily relative feeding rate of aphids, at DVSRRSAPT0,  (g.g-1.d-1) , at different DVS (wheat development state) */


    
};
	
}  // fin du namespace

DECLARE_NAMED_DYNAMICS(APHIDS, aphids::APHIDS); // balise specifique VLE
#endif
