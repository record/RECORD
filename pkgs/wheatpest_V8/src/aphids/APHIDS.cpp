/**
 * @file src/BYDV.cpp
 * @author  J.Soudais -INRA
 */

/*
 * Copyright (C)  2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <aphids/APHIDS.hpp>

// Pour la fonction interpolation de type linéaire 
#include <math.h> 
#include </usr/include/gsl/gsl_interp.h> 


//using namespace vle;
using namespace std;
namespace aphids {

void APHIDS::compute(const vle::devs::Time& time) 
{
      //le nb de pucerons
      gsl_interp *APHNBTinterpol = gsl_interp_alloc (gsl_interp_linear,APHNBTn);
      gsl_interp_init(APHNBTinterpol, APHNBTx, APHNBTy, APHNBTn);
      gsl_interp_accel * accelerator =  gsl_interp_accel_alloc();
      
      //le poids de pucerons
      gsl_interp *SFWAPTinterpol = gsl_interp_alloc (gsl_interp_linear,SFWAPTn);
      gsl_interp_init(SFWAPTinterpol, SFWAPTx, SFWAPTy, SFWAPTn);
      
      //le nb de pucerons
      gsl_interp *RRSAPTinterpol = gsl_interp_alloc (gsl_interp_linear,RRSAPTn);
      gsl_interp_init(RRSAPTinterpol, RRSAPTx, RRSAPTy, RRSAPTn);

   double APHNBT;
      double valTime = time.getValue();
      APHNBT = gsl_interp_eval(APHNBTinterpol, APHNBTx, APHNBTy, valTime, accelerator);
      //std::cout << "APHNBT :  " << time  << "   " << APHNBT << std::endl;
 
   // variable de calcul APHNB
   double APHNB;
   APHNB = PAPH()*APHNBT;
     //std::cout << "PAPH:  " << PAPH() << std::endl;

   // variable de calcul: SFWAPT
   double SFWAP;
   SFWAP = gsl_interp_eval(SFWAPTinterpol, SFWAPTx, SFWAPTy, DVS(), accelerator);
      //std::cout << "SFWAP :  " << DVS()  << "   " << SFWAP << std::endl;
   
   // variable de calcul APHFW
   double APHFW;
   APHFW = APHNB*SFWAP;
   
   // variable de calcul: RRSAP
   double RRSAP;
  RRSAP = gsl_interp_eval(RRSAPTinterpol, RRSAPTx, RRSAPTy, DVS(), accelerator);
  //std::cout << "RRSAP :  " << DVS()  << "   " << RRSAP << std::endl;

   // variable détat RSAP
   RSAP = RRSAP*APHFW;


   // variable de calcul RHONEY
   double RHONEY;
   RHONEY=AHONEY*RSAP();

   // variable d'état HONEY
   HONEY=HONEY(-1)+RHONEY;
 
   // variable d'état RFAPH
   if(1.0-(HONEY()*BHONEY)>CHONEY) RFAPH = 1.0-(HONEY()*BHONEY);
   else RFAPH = CHONEY;  
                   
}
void APHIDS::initValue(const vle::devs::Time& time )
{
    init(HONEY,0.0);	
    init(RSAP,0.0);	
    init(RFAPH,0.0);

     // LECTURE  DES FICHIERS EXTERNES 
     // !! je n'ai pas réussi à utiliser GSL et la bibliothèque STL pour sa partie vecteur, d'où tableau par pointeur.
     // Vecteurs de points pour interpolation sur le nb de pucerons
     // Ouverture du fichier de points d'observation
     APHNBTf.open(getPackageDataFile("APHNBT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     APHNBTn=0; //nb de points, soit nb de lignes du fichier
     if (APHNBTf.good()) {
       while (getline(APHNBTf,line)) ++APHNBTn; // il faudra rajouter un test sur ligne != espace²²
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
     APHNBTx = new double[APHNBTn];
     APHNBTy = new double[APHNBTn];
     APHNBTf.close(); 

     APHNBTf.open(getPackageDataFile("APHNBT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     if (APHNBTf.good()) {
       for  (int i=0; i<APHNBTn; i++) {
         getline (APHNBTf, line);
         stringstream ss(line);
         ss >> APHNBTx[i];
         ss >> APHNBTy[i];
         //std::cout << "Lecture APHNBT : " << APHNBTx[i] << " "<<APHNBTy[i] << std::endl;
      }
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
     
     // Ouverture du fichier de points d'observation
     SFWAPTf.open(getPackageDataFile("SFWAPT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     SFWAPTn=0; //nb de points, soit nb de lignes du fichier
     if (SFWAPTf.good()) {
       while (getline(SFWAPTf,line)) ++SFWAPTn;
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
     SFWAPTx = new double[SFWAPTn];
     SFWAPTy = new double[SFWAPTn];
     SFWAPTf.close(); 

     SFWAPTf.open(getPackageDataFile("SFWAPT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     if (SFWAPTf.good()) {
       for  ( int i=0; i<SFWAPTn; i++) {
         getline (SFWAPTf, line);
         stringstream ss(line);
         ss >> SFWAPTx[i];
         ss >> SFWAPTy[i];
         //std::cout << "Lecture SFWAPT : " << SFWAPTx[i] << "-"<<SFWAPTy[i] << std::endl;
      }
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
     
     // Ouverture du fichier de points d'observation
     RRSAPTf.open(getPackageDataFile("RRSAPT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     RRSAPTn=0; //nb de points, soit nb de lignes du fichier
     if (RRSAPTf.good()) {
       while (getline(RRSAPTf,line)) ++RRSAPTn;
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
     RRSAPTx = new double[RRSAPTn];
     RRSAPTy = new double[RRSAPTn];
     RRSAPTf.close(); 

     RRSAPTf.open(getPackageDataFile("RRSAPT.txt").c_str()); // 1 ligne par observation, 1ère colonne x, 2ème colonne y
     if (RRSAPTf.good()) {
       for  (int i=0; i<RRSAPTn; i++) {
         getline (RRSAPTf, line);
         stringstream ss(line);
         ss >> RRSAPTx[i];
         ss >> RRSAPTy[i];
         //std::cout << "Lecture RRSAPT : " << RRSAPTx[i] << "-"<<RRSAPTy[i] << std::endl;
      }
     } else { 
     std::cout << "Fichier n'existe pas \n " << std::endl ;};
   



      //initialisation pour le nb de pucerons
      gsl_interp *APHNBTinterpol = gsl_interp_alloc (gsl_interp_linear,APHNBTn);
      gsl_interp_init(APHNBTinterpol, APHNBTx, APHNBTy, APHNBTn);
      gsl_interp_accel * accelerator =  gsl_interp_accel_alloc();
      
      //initialisation pour le poids de pucerons
      gsl_interp *SFWAPTinterpol = gsl_interp_alloc (gsl_interp_linear,SFWAPTn);
      gsl_interp_init(SFWAPTinterpol, SFWAPTx, SFWAPTy, SFWAPTn);
      
      //initialisation pour le nb de pucerons
      gsl_interp *RRSAPTinterpol = gsl_interp_alloc (gsl_interp_linear,RRSAPTn);
      gsl_interp_init(RRSAPTinterpol, RRSAPTx, RRSAPTy, RRSAPTn);

      } 

}
