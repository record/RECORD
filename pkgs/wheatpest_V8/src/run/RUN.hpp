/**
 * @file src/RUN.hpp
 * @author J.Soudais - INRA
 */

/*
 * Copyright (C) 2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RUN_HPP  
#define RUN_HPP

#include <vle/extension/DifferenceEquation.hpp>
using namespace std;
using namespace vle;

namespace run { 

class RUN : public vle::extension::DifferenceEquation::Multiple
{
	
public:
    RUN(const vle::devs::DynamicsInit& atom,
      const devs::InitEventList& events) :
     vle::extension::DifferenceEquation::Multiple(atom, events)
  {
    // Variables d'etat gerees par ce composant "RUN"
    PAPH=createVar("PAPH");
    PBYDV=createVar("PBYDV");
    PWEED=createVar("PWEED");
    PTA=createVar("PTA");
    PSTB=createVar("PSTB");
    PLR=createVar("PLR");
    PFHB=createVar("PFHB");
    PPM=createVar("PPM");
    PES=createVar("PES");
    PSR=createVar("PSR");
    PSNB=createVar("PSNB");
    PSES=createVar("PSES");
    PBFR=createVar("PBFR");
    
        
    // Lecture des variables d'entree
    PAPHR = vle::value::toDouble(events.get("PAPHR")); 
    PBYDVR = vle::value::toDouble(events.get("PBYDVR"));
    PWEEDR = vle::value::toDouble(events.get("PWEEDR"));
    PTAR = vle::value::toDouble(events.get("PTAR"));
    PSTBR = vle::value::toDouble(events.get("PSTBR")); 
    PLRR = vle::value::toDouble(events.get("PLRR"));
    PFHBR = vle::value::toDouble(events.get("PFHBR")); 
    PPMR = vle::value::toDouble(events.get("PPMR"));
    PESR = vle::value::toDouble(events.get("PESR"));
    PSRR = vle::value::toDouble(events.get("PSRR"));
    PSNBR = vle::value::toDouble(events.get("PSNBR")); 
    PSESR = vle::value::toDouble(events.get("PSESR")); 
    PBFRR = vle::value::toDouble(events.get("PBFRR")); 
  }
    virtual ~RUN() {}; 
    
    virtual void compute(const vle::devs::Time&  time );
    virtual void initValue(const vle::devs::Time&  time);

private:
    //Variables d'etat
    Var PAPH; /**< State variable for aphid Sitobion avenae (0/1)*/ 
    Var PBYDV; /**< State variable for BYDV (0/1)*/
    Var PWEED; /**< State variable for weeds (0/1)*/
    Var PTA; /**< State variable for Take All (0/1)*/
    Var PSTB; /**< State variable for Septoria tritici blotch (0/1)*/
    Var PLR; /**< State variable for Leaf rust (0/1)*/
    Var PFHB; /**< State variable for Fusarium head blight (0/1)*/
    Var PPM; /**< State variable for Powdery mildew (0/1)*/
    Var PES; /**< State variable for Eyespot (0/1)*/
    Var PSR; /**< State variable for Stripe rust (0/1)*/
    Var PSNB; /**< State variable for Septoria nodorum blotch (0/1)*/
    Var PSES; /**< State variable for Sharp eyespot (0/1)*/
    Var PBFR; /**< State variable for Brown foot rot (0/1)*/    
  
    //Variables d'entree du modele
    double PAPHR; /**< Parametre to activate(=1) or desactivate (=0) aphid Sitobion avenae module, (-)*/ 
    double PBYDVR; /**< Parametre to activate(=1) or desactivate (=0) BYDV module, (-)*/ 
    double PWEEDR; /**< Parametre to activate(=1) or desactivate (=0) weeds module, (-)*/ 
    double PTAR; /**< Parametre to activate(=1) or desactivate (=0) Take All module, (-)*/  
    double PSTBR; /**< Parametre to activate(=1) or desactivate (=0) Septoria tritici blotch module, (-)*/ 
    double PLRR; /**< Parametre to activate(=1) or desactivate (=0) Leaf rust module, (-)*/  
    double PFHBR; /**< Parametre to activate(=1) or desactivate (=0) Fusarium head blight module, (-)*/ 
    double PPMR; /**< Parametre to activate(=1) or desactivate (=0) Powdery mildew module, (-)*/  
    double PESR; /**< Parametre to activate(=1) or desactivate (=0) Eyespot module, (-)*/  
    double PSRR; /**< Parametre to activate(=1) or desactivate (=0) Stripe rust module, (-)*/  
    double PSNBR; /**< Parametre to activate(=1) or desactivate (=0) Septoria nodorum blotch module, (-)*/ 
    double PSESR; /**< Parametre to activate(=1) or desactivate (=0) Sharp eyespot module, (-)*/ 
    double PBFRR; /**< Parametre to activate(=1) or desactivate (=0) Brown foot rot module, (-)*/        

    };
	
}  // fin du namespace

DECLARE_NAMED_DYNAMICS(RUN, run::RUN); // balise specifique VLE

#endif
