/**
 * @file src/WHEATPEST.cpp
 * @author  J.Soudais -INRA
 */

/*
 * Copyright (C)  2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <wheatpest/WHEATPEST.hpp>

//using namespace vle;
using namespace std;

namespace wheatpest {


void WHEATPEST::compute(const vle::devs::Time&  time ) 
{
	// Variable de calcul DTEMP (�C)
   double DTEMP;
   if ( ((Tmin() + Tmax())/2)<= TBASE) DTEMP = 0.0; // rajouter commentaire lie � la constante ou la definir en tant que parametre
   if ( ((Tmin() + Tmax())/2)> TBASE)  DTEMP = ((Tmin() + Tmax())/2); 

   // variable d'etat: STEMP
   STEMP = STEMP(-1)+DTEMP;

   // variable d'etat: DVS
   if (STEMP()<=TTFLOW) DVS = STEMP()/TTFLOW;
   if (STEMP()>TTFLOW && STEMP()<=TTMAT) DVS = DVS1+((STEMP()-TTFLOW)/(TTMAT-TTFLOW));
   if (STEMP()>TTMAT && STEMP()<=TTEND) DVS = DVS2+((STEMP()-TTMAT)/(TTEND-TTMAT))/10.0;
    
   // Variables de calcul SLA
   double SLA;
   if (DVS()==DVSSLAT0) SLA = SLAT0;
   if (DVS()>DVSSLAT0 && DVS()<DVSSLAT1) SLA = SLAT0+(SLAT1-SLAT0)*DVS();
   if (DVS()==DVSSLAT1) SLA = SLAT1;
   if (DVS()>DVSSLAT1 && DVS()<DVSSLAT2) SLA = (2*SLAT1+SLAT2)+(SLAT2-SLAT1)*DVS();
   if (DVS()>=DVSSLAT2) SLA = SLAT2;
   
   // variable d'etat: LAI
   //LAI = SLA*LEAFW(-1)*RSTB()*RLR()*RSNB()*RSR()*RPM();
   LAI = SLA*LEAFW(-1);

   // Variables de calcul RUE
   double RUE;
   if (DVS()<=DVSRUET1) RUE = RUET1;
   if (DVS()>DVSRUET1 && DVS()<DVSRUET2) RUE = ((RUET2-RUET1)/(DVSRUET2-DVSRUET1))*DVS()+RUET1-DVSRUET1*((RUET2-RUET1)/(DVSRUET2-DVSRUET1));
   if (DVS()>=DVSRUET2) RUE = RUET2;
   
   // Variables de calcul
   /* A v�rifier MRFSNDV est il une var calcul�e ici ou issue d'un autre mod�le?
   double MRFSNDV; 
   if (RFSNDV()<=0.) MRFSNDV = 0.;
   if (RFSNDV()>0.) MRFSNDV = RFSNDV();
  
   double MRFSTDV; 
   if (RFSTDV()<=0.) MRFSTDV = 0.;
   if (RFSTDV()>0.) MRFSTDV = RFSTDV();
   
   double MRFSR; 
   if (RSR()<=0.) MRFSR = 0.;
   if (RSR()>0.) MRFSR = RSR();
   
   double MRFLR; 
   if (RLR()<=0.) MRFLR = 0.;
   if (RLR()>0.) MRFLR = RLR();
  */
      
   // Variable de calcul RG ()
   double RG;
   // Modif pour ne tenir compte que des aphids RG = Rg()*(RUE*1.-exp(-K*LAI()))*(RFAPH()*RFBYDV()*RFWEED()*RFTA()*RFES()*RFSES()*RFBFR())*MRFSNDV*MRFSTDV*MRFSR*MRFLR-RSAP();
   RG = Rg()*(RUE*1.-exp(-K*LAI()))*(RFAPH(-1))-RSAP(-1);
   
   // Variable d'�tat RGATT ()
   RGATT = Rg()*(RUE*1.-exp(-K*LAI()));
   
   // Variable de calcul CPS
   double CPS;
   if (DVS()==DVSCPST0) CPS = CPST0;
   if (DVS()>DVSCPST0 && DVS()<DVSCPST1) CPS = ((CPST1-CPST0)/(DVSCPST1-DVSCPST0))*DVS()+CPST1-DVSCPST0*((CPST1-CPST0)/(DVSCPST0 - DVSCPST1));
   if (DVS()>=DVSCPST1 && DVS()<DVSCPST2) CPS = ((CPST2-CPST1)/(DVSCPST1 - DVSCPST2))*DVS()+CPST2-DVSCPST1*((CPST2-CPST1)/((DVSCPST1 - DVSCPST2)));
   if (DVS()>=DVSCPST2 && DVS()<DVSCPST3) CPS = ((CPST3-CPST2)/(DVSCPST2 - DVSCPST3))*DVS()+CPST3-DVSCPST2*((CPST3-CPST2)/((DVSCPST2 - DVSCPST3)));
   if (DVS()>=DVSCPST3 && DVS()<DVSCPST4) CPS = ((CPST4-CPST3)/(DVSCPST3 - DVSCPST4))*DVS()+CPST4-DVSCPST3*((CPST4-CPST3)/((DVSCPST3 - DVSCPST4)));
   if (DVS()>=DVSCPST4 && DVS()<DVSCPST5) CPS = ((CPST5-CPST4)/(DVSCPST4 - DVSCPST5))*DVS()+CPST5-DVSCPST4*((CPST5-CPST4)/((DVSCPST4 - DVSCPST5)));
   if (DVS()>=DVSCPST5 && DVS()<DVSCPST6) CPS = ((CPST6-CPST5)/(DVSCPST5 - DVSCPST6))*DVS()+CPST6-DVSCPST5*((CPST6-CPST5)/((DVSCPST5 - DVSCPST6)));
   if (DVS()>=DVSCPST6) CPS = ((CPST7-CPST6)/(DVSCPST6 - DVSCPST7))*DVS()+CPST7-DVSCPST6*((CPST7-CPST6)/(DVSCPST6 - DVSCPST7));

   // Variable de calcul CPL
   double CPL;
   if (DVS()<=DVSCPLT0) CPL = 1.-CPS;
   if (DVS()>DVSCPLT0) CPL = CPLT0;
   
   // Variable de calcul CPR
   double CPR;
   if (DVS()==DVSCPRT0) CPR = CPRT0;
   if (DVS()>DVSCPRT0 && DVS()<DVSCPRT1) CPR = ((CPRT1-CPRT0)/(0.1-DVS0))*DVS()+CPRT1-DVS0*((CPRT1-CPRT0)/(0.1-DVS0));
   if (DVS()>=DVSCPRT1 && DVS()<DVSCPRT2) CPR = ((CPRT2-CPRT1)/(DVSCPRT2-DVSCPRT1))*DVS()+CPRT2-DVSCPRT1*((CPRT2-CPRT1)/(DVSCPRT2-DVSCPRT1));
   if (DVS()>=DVSCPRT2 && DVS()<DVSCPRT3) CPR = ((CPRT3-CPRT2)/(DVSCPRT3-DVSCPRT2))*DVS()+CPRT3-DVSCPRT2*((CPRT3-CPRT2)/(DVSCPRT3-DVSCPRT2));
   if (DVS()>=DVSCPRT3 && DVS()<DVSCPRT4) CPR = ((CPRT4-CPRT3)/(DVSCPRT4-DVSCPRT3))*DVS()+CPRT4-DVSCPRT3*((CPRT4-CPRT3)/(DVSCPRT4-DVSCPRT3));
   if (DVS()>=DVSCPRT4 && DVS()<DVSCPRT5) CPR = ((CPRT5-CPRT4)/(DVSCPRT5-DVSCPRT4))*DVS()+CPRT5-DVSCPRT4*((CPRT5-CPRT4)/(DVSCPRT5-DVSCPRT4));
   if (DVS()>=DVSCPRT5 && DVS()<DVSCPRT6) CPR = ((CPRT6-CPRT5)/(DVSCPRT6-DVSCPRT5))*DVS()+CPRT6-DVSCPRT5*((CPRT6-CPRT5)/(DVSCPRT6-DVSCPRT5));
   if (DVS()>=DVSCPRT6 && DVS()<DVSCPRT7) CPR = ((CPRT7-CPRT6)/(DVSCPRT7-DVSCPRT6))*DVS()+CPRT7-DVSCPRT6*((CPRT7-CPRT6)/(DVSCPRT7-DVSCPRT6)); 
   if (DVS()>=DVSCPRT7 && DVS()<DVSCPRT8) CPR = ((CPRT8-CPRT7)/(DVSCPRT8-DVSCPRT7))*DVS()+CPRT8-DVSCPRT7*((CPRT8-CPRT7)/(DVSCPRT8-DVSCPRT7));
   if (DVS()>=DVSCPRT8 && DVS()<DVSCPRT9) CPR = ((CPRT9-CPRT8)/(DVSCPRT9-DVSCPRT8))*DVS()+CPRT9-DVSCPRT8*((CPRT9-CPRT8)/(DVSCPRT9-DVSCPRT8));
   if (DVS()>=DVSCPRT9 && DVS()<DVSCPRT10) CPR = ((CPRT10-CPRT9)/(DVSCPRT10-DVSCPRT9))*DVS()+CPRT10-DVSCPRT9*((CPRT10-CPRT9)/(DVSCPRT10-DVSCPRT9));
   if (DVS()>=DVSCPRT10 && DVS()<DVSCPRT11) CPR = ((CPRT11-CPRT10)/(DVSCPRT11-DVSCPRT10))*DVS()+CPRT11-DVSCPRT10*((CPRT11-CPRT10)/(DVSCPRT11-DVSCPRT10));
   if (DVS()>=DVSCPRT11 && DVS()<DVSCPRT12) CPR = ((CPRT12-CPRT11)/(DVSCPRT12-DVSCPRT11))*DVS()+CPRT12-DVSCPRT11*((CPRT12-CPRT11)/(DVSCPRT12-DVSCPRT11));
   if (DVS()>=DVSCPRT12) CPR = CPRT12;
   
   // Variable de calcul RRSENL
   double RRSENL;
   if (DVS()==DVSRRSENT0) RRSENL = RRSENT0;
   if (DVS()>DVSRRSENT0 && DVS()<DVSRRSENT1) RRSENL = ((RRSENT1-RRSENT0)/(DVSRRSENT1-DVSRRSENT0))*DVS()+RRSENT0-DVSRRSENT0*((RRSENT1-RRSENT0)/(DVSRRSENT1-DVSRRSENT0));
   if (DVS()>=DVSRRSENT1 && DVS()<DVSRRSENT2) RRSENL = ((RRSENT2-RRSENT1)/(DVSRRSENT2-DVSRRSENT1))*DVS()+RRSENT1-DVSRRSENT1*((RRSENT2-RRSENT1)/(DVSRRSENT2-DVSRRSENT1));
   if (DVS()>=DVSRRSENT2 && DVS()<DVSRRSENT3) RRSENL = ((RRSENT3-RRSENT2)/(DVSRRSENT3-DVSRRSENT2))*DVS()+RRSENT2-DVSRRSENT2*((RRSENT3-RRSENT2)/(DVSRRSENT3-DVSRRSENT2));
   if (DVS()>=DVSRRSENT3 && DVS()<DVSRRSENT4) RRSENL = ((RRSENT4-RRSENT3)/(DVSRRSENT4-DVSRRSENT3))*DVS()+RRSENT3-DVSRRSENT3*((RRSENT4-RRSENT3)/(DVSRRSENT4-DVSRRSENT3));
   if (DVS()>=DVSRRSENT4 && DVS()<DVSRRSENT5) RRSENL = ((RRSENT5-RRSENT4)/(DVSRRSENT5-DVSRRSENT4))*DVS()+RRSENT4-DVSRRSENT4*((RRSENT5-RRSENT4)/(DVSRRSENT5-DVSRRSENT4));

   	// Variable de calcul RSENL (g.m-2)
   double RSENL;
   RSENL = LEAFW(-1)*RRSENL;

   	// Variable de calcul RLEAFW (-)
   double RLEAFW;
   RLEAFW = (CPL*(1.-CPR)*RG)-RSENL;
       
   // variable d'etat: LEAFW (Dry weight of leaves)
   LEAFW = LEAFW(-1)+RLEAFW;

   // Variable de calcul RMSTW (-)
   double RMSTW;
   RMSTW = (CPS*(1.-CPR)*RG);
    
   // Variable d'�tat MAXST (-)
   MAXST = MAXST(-1)+RMSTW;
    
   // Variable de calcul DDIST (-)
   double DDIST;
   DDIST = FRDIST*MAXST()*RDEV2*DTEMP;
        
   // Variable de calcul RDIST (-)
   double RDIST;
   if (DVS()<=DVS1) RDIST = RDIST0;
   if (DVS()>DVS1) RDIST = DDIST;
    
   // Variable de calcul RSTW (-)
   double RSTW;
   RSTW = (CPS*(1.-CPR)*RG)-RDIST;
            
   // variable d'etat: STEMW (Dry weight of stems)
   STEMW = STEMW(-1)+RSTW;

      
   // Variable de calcul CPE (-)
   double CPE;
   CPE = 1.-CPL-CPS;   
   
   // Variable de calcul REARW (-)
   double REARW;
   //REARW = (((CPE*(1.-CPR)*RG)+RDIST)*(1.-RFFHB()))-(RES3()*EARW(-1));
   REARW = ((CPE*(1.-CPR)*RG)+RDIST);
    
   // variable d'etat: EARW (Dry weight of ears)
   EARW = EARW(-1)+REARW;
    
   // Variable de calcul RROOTW (-)
   double RROOTW;
   RROOTW = CPR*RG;

   // variable d'etat: ROOTW (Dry weight of roots)
   ROOTW = ROOTW(-1)+RROOTW;
 
   // variable d'etat: YIELD (YIELD)
   if (DVS()<=DVS3) YIELD = YIELD(-1);
   if (DVS()>DVS3) YIELD=EARW()*AYIELD;     
        
}

void WHEATPEST::initValue(const vle::devs::Time& time )
{
    init(STEMP,0.0);
    init(DVS,0.0);
    init(LAI,0.0);
    init(LEAFW,10.0);
    init(STEMW,6.0);
    init(EARW,10.0);
    init(ROOTW,5.0);
    init(YIELD,0.0);
    init(MAXST,6.0);
    init(RGATT,0.0);       
}	

}

