Requirements
============

vle      0.8.0 http://vle.univ-littoral.fr
glibmm   2.4.0 http://www.gtkmm.org
libxml++ 2.6.0 http://libxmlplusplus.sourceforge.net
boost    1.34  http://www.boost.org
cmake    2.6.0 http://www.cmake.org


VLE Installation
================

Just run the good command:

$ vle --package=2CV configure
$ vle --package=2CV build
