<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata author="RT" version="1.x">
  <dataModel conf="auzeville_95_12" package="WACSgen"/>
  <configuration>
    <dynamic name="dyn_WACSgen" library="WACSgen" package="WACSgen"/>
    <observable name="obs_WACSgen">
      <port name="ETPP">
      </port>
      <port name="RG">
      </port>
      <port name="rain">
      </port>
      <port name="tmax">
      </port>
      <port name="tmin">
      </port>
      <port name="V">
      </port>
      <port name="dayOfYear">
      </port>
      <port name="season">
      </port>
      <port name="dayOfMonth">
      </port>
      <port name="year">
      </port>
      <port name="month">
      </port>
      <port name="WT">
      </port>
      <port name="current_date">
      </port>
      <port name="Ntry">
      </port>
      <port name="Nhelp">
      </port>
    </observable>
    <condition name="cond_WACSgen">
      <port name="PkgName">
        <string>WACSgen</string>
      </port>
      <port name="parametersFile">
        <string>auzeville_95_12.txt</string>
      </port>
      <port name="seed">
         <integer>89525</integer>
      </port>
      <port name="begin_date">
         <string>2014-01-01</string>
      </port>
   </condition>
   <out>
      <port name="ETPP"/>
      <port name="tmin"/>
      <port name="tmax"/>
      <port name="rain"/>
      <port name="RG"/>
      <port name="V"/>
   </out>
  </configuration>
</vle_project_metadata>
