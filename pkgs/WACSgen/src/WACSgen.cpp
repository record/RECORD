 /* This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems.
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2013 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2013 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2013 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

// @@tagdynamic@@
// @@tagdepends: ext.Eigen, record.eigen, vle.discrete-time @@endtagdepends



#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cmath>

#include <vle/value/Double.hpp>
#include <vle/devs/Dynamics.hpp>
#include <vle/utils/Exception.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/Rand.hpp>
#include <vle/utils/DateTime.hpp>

#include <vle/DiscreteTime.hpp>

#include <record/eigen/Stats.hpp>

using namespace Eigen;
namespace vd = vle::devs;
namespace vv = vle::value;


namespace wacsgen {

using namespace vle::discrete_time;
using namespace record::eigen;

struct WJ
{
    VectorXd loc;
    MatrixXd cov;
    VectorXd skew;
    VectorXd rho;
    bool misDry;

    inline bool isDry() const
    {
        return misDry;
    }

    inline bool isWet() const
    {
        return not isDry();
    }

    inline unsigned int getNbVariables() const
    {
        return loc.size();
    }

    friend std::ostream& operator<<(std::ostream& o, const WJ& wj)
    {
        o << "location: \n" << wj.loc << "\n";
        o << "cov: \n" << wj.cov << "\n";
        o << "skew: \n" << wj.skew << "\n";
        o << "rho: \n" << wj.rho << "\n";
        return o;
    }


};

struct Season
{
    unsigned int begin_day;
    unsigned int begin_month;
    unsigned int numbDays;
    MatrixXd transM;
    std::vector<WJ> wjs;

    unsigned int nbDryWJs() const
    {
        std::vector<WJ>::const_iterator itb = wjs.begin();
        std::vector<WJ>::const_iterator ite = wjs.end();
        unsigned int nbDrys = 0;
        for (; itb!=ite; itb++) {
            if (itb->isDry()) {
                nbDrys++;
            }
        }
        return nbDrys;
    }

    unsigned int nbWetWJs() const
    {
        return wjs.size() - nbDryWJs();
    }
    
    void print() const
    {
       std::cout << " begin_day: " << begin_day << "\n";
       std::cout << " begin_month: " << begin_month << "\n";
    }


};


struct WACSgenParameters
{
    std::vector<std::string> wacs_var_names;
    std::vector<std::string> data_var_names;
    MatrixXd vbounds; //4 var 2 val(min and max);
    MatrixXd trend_central;
    MatrixXd trend_deviation;
    std::string rain_model;
    MatrixXd rain_par;
    std::vector<Season> seasons;

    WACSgenParameters()
    {
      rain_model.assign("Gamma");
      //TODO in parameters
    }   


    ~WACSgenParameters() {}

    void print(const WJ& wj)
    {
      std::cout << " isDry: " << wj.misDry << "\n";
      std::cout << " loc: " << wj.loc << "\n";
      std::cout << " rho: " << wj.rho << "\n";
      std::cout << " skew: " << wj.skew << "\n";
      std::cout << " cov: " << wj.cov << "\n";
    }
    int colIndex(const std::string& wacs_var) 
    {
      for (int i =0; i< (int) wacs_var_names.size(); i++) {
        if (wacs_var_names[i] == wacs_var) {
          return i; 
        }
      }
      return -1;
    }

    bool parse(const std::string& pathFile);
};


void print(const WACSgenParameters& p)
{
  std::cout << "wacs_var_names:";
  for (auto s : p.wacs_var_names) {
    std::cout << ","<<s;
  }
  std::cout << "\n";
  std::cout << "data_var_names:";
  for (auto s : p.data_var_names) {
    std::cout << ","<<s;
  }
  std::cout << "\n";
  std::cout << "bounds:\n" << p.vbounds;
  std::cout << "\n";
  //std::cout << "trend_central:\n" << p.trend_central;
  std::cout << "\n";
}

bool
error(std::ifstream& fileStream, const std::string& line, const WACSgenParameters& p)
{
  std::cout << "--error:\n--" << line << "--\n";
  std::cout << "--p:\n";
  print(p);
  std::cout << "\n";
  fileStream.close();
  return false;
}


bool
read_blank_line(std::ifstream& fileStream, std::string& line)
{
   std::getline (fileStream,line);
   return line == "";
}

bool
read_header(const std::string& expected,
              std::ifstream& fileStream, std::string& line,
              bool skipEmptyLines=false)
{

   std::getline (fileStream,line);
   if (skipEmptyLines) {
       while (line.empty()) {
           std::getline (fileStream,line);
       }
   }
   return line == expected;

}

bool
read_matrix(std::ifstream& fileStream,
   std::string& line,
   MatrixXd& mat,
   std::vector<std::string>& colnames,
   std::vector<std::string>& rownames)
{
   //read col names
   mat.resize(0,0);
   colnames.clear();
   rownames.clear();
   std::vector<std::string> toks;

   std::getline (fileStream,line);
   vle::utils::tokenize(line, colnames," ", true);
   bool inMatrix = true;
   while (inMatrix) {
       std::getline (fileStream,line);
       if (line == "") {
           inMatrix = false;
           break;
       }
       mat.conservativeResize(mat.rows()+1, colnames.size());
       toks.clear();
       vle::utils::tokenize(line, toks," ", true);
       if (toks.size() != colnames.size()+1) {
           return false;
       }
       rownames.push_back(toks[0]);
       for (unsigned int j=1; j<toks.size();j++) {
           mat(mat.rows()-1,j-1) = vle::utils::to<double>(toks[j]);
       }
   }
   return true;
}

bool
read_vector(std::ifstream& fileStream,
        std::string& line,
        std::vector<std::string>& vect)
{
    std::getline (fileStream,line);
    vect.clear();
    vle::utils::tokenize(line, vect," ", true);
    vect.erase(vect.begin());
    return true;
}

bool
read_vector_double(std::ifstream& fileStream,
        std::string& line,
        VectorXd& vect)
{
    std::getline (fileStream,line);
    std::vector<std::string> vect_str;
    vle::utils::tokenize(line, vect_str," ", true);
    vect_str.erase(vect_str.begin());
    vect.resize(vect_str.size());
    for (unsigned int i=0; i<vect_str.size();i++) {
        vect[i] = vle::utils::to<double>(vect_str[i]);
    }
    return true;
}


bool
read_bounds_columns(std::ifstream& fileStream,
   std::string& line,
   std::vector<std::string>& wacs_var_names)
{

   std::getline (fileStream,line);
   std::vector<std::string> toks;
   vle::utils::tokenize(line, wacs_var_names," ", true);
   wacs_var_names.push_back("year");
   wacs_var_names.push_back("month");
   wacs_var_names.push_back("day");
   return true;
}

bool
read_mapping(std::ifstream& fileStream,
                  std::string& line,
                  std::vector<std::string>& data_var_names,
                  const std::vector<std::string>& wacs_var_names)
{
  data_var_names.resize(wacs_var_names.size());
  for (unsigned int i =0; i<wacs_var_names.size(); i++) {
    std::getline (fileStream,line);
    std::vector<std::string> toks;
    vle::utils::tokenize(line, toks," ", true);
    for (unsigned int j=0; j<wacs_var_names.size(); j++) {
       if (toks[2] == wacs_var_names[j]) {
          data_var_names[j] = toks[1];
       }
    }
  }
  return true;
}

bool
WACSgenParameters::parse(const std::string& pathFile)
{
    std::string line;
    std::ifstream fileStream(pathFile.c_str());
    std::vector<std::string> rownames;
    std::vector<std::string> colnames;
    std::vector<std::string> vect;


    if (fileStream.is_open()) {
      /*while ( std::getline (fileStream,line) ) {
        std::cout << line << '\n';
      }*/

      if (not read_header("$bounds",fileStream, line)) {
         return error(fileStream, line, *this);
      }
      if (not read_matrix(fileStream, line, vbounds,
              wacs_var_names, rownames)) {
         return error(fileStream, line, *this);
      }
      wacs_var_names.push_back("year");
      wacs_var_names.push_back("month");
      wacs_var_names.push_back("day");
      if (not read_header("$mapping",fileStream, line)) {
         return error(fileStream, line, *this);
      }
      if (not read_header("  names_data wacs_names",fileStream, line)) {
         return error(fileStream, line, *this);
      }
      if (not read_mapping(fileStream, line, data_var_names, wacs_var_names)) {
         return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
         return error(fileStream, line, *this);
      }
      if (not read_header("$Trend", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$Trend$Param", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("[1] \"0.7\" \"L2\" ", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$Trend$Central", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_matrix(fileStream, line, trend_central,
              colnames, rownames)) {
          return error(fileStream, line, *this);
      }
      if (trend_central.rows() != 365) {
          return error(fileStream, " trend_central not ok" , *this);
      }
      for (unsigned int i=0; i<colnames.size(); i++) {
          if (colnames[i] != wacs_var_names[i+1]) {
              return error(fileStream, " trend_central not ok(2)" , *this);
          }
      }
      if (not read_header("$Trend$Deviation", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_matrix(fileStream, line, trend_deviation,
              colnames, rownames)) {
          return error(fileStream, line, *this);
      }
      if (trend_deviation.rows() != 365) {
          return error(fileStream, " trend_deviation not ok" , *this);
      }
      for (unsigned int i=0; i<colnames.size(); i++) {
          if (colnames[i] != wacs_var_names[i+1]) {
              return error(fileStream, " trend_deviation not ok(2)" , *this);
          }
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$Rain", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$Rain$RainModel", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("[1] \"Gamma\"", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$Rain$RainPar", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_matrix(fileStream, line, rain_par,
              colnames, rownames)) {
          return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$seasons", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      MatrixXd season_limits;
      if (not read_matrix(fileStream, line, season_limits,
              colnames, rownames)) {
          return error(fileStream, line, *this);
      }
      if (colnames[0] !="month" or colnames[1] !="day") {
          return error(fileStream, " season limits not ok" , *this);
      }
      seasons.resize(season_limits.rows());
      for (unsigned int i=0; i<seasons.size(); i++) {
          seasons[i].begin_day = (unsigned int) season_limits(i, 1);
          seasons[i].begin_month = (unsigned int) season_limits(i, 0);
      }
      if (not read_header("$Trange", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("[1] FALSE", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_header("$varnames", fileStream, line)) {
          return error(fileStream, line, *this);
      }
      if (not read_vector(fileStream, line, vect)) {
          return error(fileStream, line, *this);
      }
      if (not read_blank_line(fileStream, line)) {
          return error(fileStream, line, *this);
      }
      for (int s = 1; s <= (int) seasons.size(); s++) {
          Season& season = seasons[s-1];
          if (not read_header(vle::utils::format("$Season_%i", s),
                  fileStream, line, true)) {
              return error(fileStream, line, *this);
          }
          if (not read_header(vle::utils::format("$Season_%i$NumbDays", s),
                  fileStream, line, true)) {
              return error(fileStream, line, *this);
          }
          if (not read_vector(fileStream, line, vect)) {
              return error(fileStream, line, *this);
          }
          if (not read_header(vle::utils::format("$Season_%i$NumbWT", s),
                  fileStream, line, true)) {
              return error(fileStream, line, *this);
          }
          if (not read_vector(fileStream, line, vect)) {
              return error(fileStream, line, *this);
          }
          if (vect.size() != 2) {
              return error(fileStream, " numwet_dry not ok", *this);
          }
          int nb_dry = vle::utils::to<int>(vect[0]);
          int nb_wet = vle::utils::to<int>(vect[1]);
          season.wjs.resize(nb_dry+nb_wet);
          int wj=0;
          for (; wj < nb_dry; wj++) {
              season.wjs[wj].misDry = true;
          }
          for (; wj < nb_dry+nb_wet; wj++) {
              season.wjs[wj].misDry = false;
          }
          if (not read_header(vle::utils::format("$Season_%i$TransM", s),
                  fileStream, line, true)) {
              return error(fileStream, line, *this);
          }
          if (not read_matrix(fileStream, line, season.transM,
                  colnames, rownames)) {
              return error(fileStream, line, *this);
          }

          for (wj = 1; wj <= (int) season.wjs.size(); wj++) {
              WJ& weather_state = season.wjs[wj-1];
              if (not read_header(vle::utils::format("$Season_%i$W%i", s, wj),
                      fileStream, line, true)) {
                  return error(fileStream, line, *this);
              }
              if (not read_header(vle::utils::format("$Season_%i$W%i$loc", s, wj),
                      fileStream, line, true)) {
                  return error(fileStream, line, *this);
              }
              if (not read_vector_double(fileStream, line, weather_state.loc)) {
                  return error(fileStream, line, *this);
              }
              if (not read_header(vle::utils::format("$Season_%i$W%i$cov", s, wj),
                      fileStream, line, true)) {
                  return error(fileStream, line, *this);
              }
              if (not read_matrix(fileStream, line, weather_state.cov,
                      colnames, rownames)) {
                  return error(fileStream, line, *this);
              }
              if (not read_header(vle::utils::format("$Season_%i$W%i$skew", s, wj),
                      fileStream, line, true)) {
                  return error(fileStream, line, *this);
              }
              if (not read_vector_double(fileStream, line, weather_state.skew)) {
                  return error(fileStream, line, *this);
              }
              if (not read_header(vle::utils::format("$Season_%i$W%i$rho", s, wj),
                      fileStream, line, true)) {
                  return error(fileStream, line, *this);
              }
              if (not read_vector_double(fileStream, line, weather_state.rho)) {
                  return error(fileStream, line, *this);
              }
              //print(weather_state);
          }
      }
    }
    return true;
    //boost::replace_all(fileString, "\r\n", "");
    //boost::replace_all(fileString, "\n", "");
}


struct oneDaySimul
{
    oneDaySimul(): WT(999)
    {
    };
    unsigned int WT;
    void copyFrom(const oneDaySimul& other)
    {
        WT = other.WT;
        Xresid = other.Xresid;
        Xsimul = other.Xsimul;
    }
    VectorXd Xresid;
    VectorXd Xsimul;
};

/***
 * The dynamics
 */

class WACSgenSimulation : public DiscreteTimeDyn
{


    enum DYN_STATE {OUTPUT, COMPUTE};

public:
    DYN_STATE mstate;
    oneDaySimul mprevDay;
    oneDaySimul mnewDay;
    vle::utils::Rand mrand;
    WACSgenParameters mparams;
    int current_date;
    std::vector<Var> mvars;
    unsigned int Ntry;
    unsigned int Nhelp;
    bool REJECT;

    /**
     * @brief Transforms Gaussian scores into rain values
     * @param y, Gaussian score
     * @param s, season index
     * @return a transformed value (eg from a gamma distribution)
     */
    double transform_rain(double y, unsigned int s)
    {
        if (mparams.rain_model == "Gamma") {
            return mrand.gamma_quantile(
                    mparams.rain_par(s,1), mparams.rain_par(s,0),//WARNING scale and shape are switched
                    mrand.normal_cdf(0,1,y));
//            boost::math::gamma_distribution<> my_gamma(
//                    mparams.rain_par(s,1), mparams.rain_par(s,0));
//            boost::math::normal_distribution<> my_norm(0, 1);
//            return boost::math::quantile(my_gamma,
//                    boost::math::cdf(my_norm, y));
        }
        return 0;
    }

    /**
     * @brief finds the most likely weather state for a given vector
     * @param resid the vector of resisduals
     * @param Season_par parameters for a given season
     * @param the previous state was a wet state
     * @return the most likely weather type
     */
    unsigned int map_wt(const VectorXd& resid,const Season& Season_par,
            bool wet)
    {
        //Number of weather state in season s
        unsigned int Nwt = Season_par.transM.cols();
        // computes the density associated to each weather state in season S;
        // If rain status is not similar, density is set to -999
        VectorXd d(Nwt);
        for (unsigned int i=0; i < Nwt; i++) {
            d(i) = -999;
        }

        //finds the stationary distribution of the Markov Chain
        EigenSolver<MatrixXd> eigensolver(Season_par.transM.transpose());
        ArrayXXd p_st =
                eigensolver.pseudoEigenvectors().col(0).array().abs();
        p_st = p_st / p_st.sum();
        std::vector<WJ>::const_iterator itb = Season_par.wjs.begin();
        std::vector<WJ>::const_iterator ite = Season_par.wjs.end();

        for (unsigned int wt=0; itb != ite; itb++, wt++) {
            const WJ& wj = *itb;
            if ((wj.isWet() and wet) || (wj.isDry() and not wet)) {
                const VectorXd& mu = wj.loc;
                const MatrixXd& sigma  = wj.cov;
                VectorXd skew(wj.skew);
                for (unsigned int i = 0 ; i< skew.cols() ; i ++) {
                    double sign = 0;
                    if (skew(i) < 0) {
                        sign = -1;
                    } else {
                        sign = 1;
                    }
                    skew(i) = sign * std::min(0.99, std::abs((double) skew(i)));
                    if (!wet) {
                        d(wt) = Stats::dmcsnstar(
                                  Stats::subVector_noti(resid, 0),
                                  mu, sigma, skew) * p_st(wt);
                    } else {
                        d(wt) = Stats::dmcsnstar(resid, mu, sigma, skew) * p_st(wt);
                    }
                }
            }
        }
        double d_max = -9999;
        unsigned int d_argmax = 0;
        for (unsigned int i =0; i < d.rows(); i++) {
            if (d(i) > d_max) {
                d_max = d(i);
                d_argmax = i;
            }
        }
        return d_argmax;
    }

    /**
     * @brief Performes a transition of a Markov Chain
     *
     * @param oldWT, previous markov chain state
     * @param transM, markov transition matrix
     * @return new markov chain state
     */
    unsigned int rMarkov(unsigned int oldWT, const MatrixXd& transM)
    {
	VectorXd prob = transM.row(oldWT);
        return  Stats::sample_int(prob, mrand);
    }

    /**
     * @brief get the season index
     * @param julian day:  eg 2200548
     * @return the season index : 0 <= seasonInd < season.size()
     */
    unsigned int season(double julianDay) const
    {


        unsigned int dom = vle::utils::DateTime::dayOfMonth(julianDay);
        unsigned int month = vle::utils::DateTime::month(julianDay);
        unsigned int Ns   =  mparams.seasons.size();
        for (unsigned int s = 0; s < Ns; s++) {
            const Season&  season = mparams.seasons[s];

            if (month < season.begin_month or
                (month ==  season.begin_month and dom < season.begin_day)) {
                return s;
            }
        }
        return 0;
    }

    /**
     * @brief tells if a vector in in bounds
     * @param X: vector of simulation
     * @return true if X is in bounds
     */
    bool testvariables(const VectorXd& X) 
    {
        unsigned int Nv = X.rows();
        for (unsigned int v=0; v < Nv; v++){
            if ((X(v) < mparams.vbounds(0,v)) or 
                (X(v) > mparams.vbounds(1,v))) {
                return false;
            }
        }
        if (X[mparams.colIndex("tmin")] > X[mparams.colIndex("tmax")]) {
            return false;
        }
        return true;
    }

    /**
     * @brief gives a vector into bounds
     * @param X: initial vector of simulation
     * @return a vector into bounds
     */
    VectorXd boundvariables(const VectorXd& X) 
    {
        unsigned int Nv = X.rows();
        VectorXd res = X;
        for (unsigned int v=0; v < Nv; v++){
            if (X(v) < mparams.vbounds(0,v)) {
              res[v] = mparams.vbounds(0,v);
            } else  if (X(v) > mparams.vbounds(1,v)) {
              res[v] = mparams.vbounds(1,v);
            } 
        }
        if (res[mparams.colIndex("tmin")] > res[mparams.colIndex("tmax")]) {
            res[mparams.colIndex("tmax")] = res[mparams.colIndex("tmin")];
        }
        return res;
    }

    /**
     * @brief Constructor
     */
    WACSgenSimulation(const vd::DynamicsInit& init,
            const vd::InitEventList& events)
        : DiscreteTimeDyn(init, events), mstate(OUTPUT), mprevDay(),
          mnewDay(), mrand(), mparams(), current_date()
    {
        if (events.exist("seed")) {
           if (events.get("seed")->isInteger()) {
             mrand.seed(events.getInt("seed"));
           } else if (events.get("seed")->isDouble()) {
             mrand.seed((int) events.getDouble("seed"));
           }
        }
        if (!(events.exist("PkgName") && events.get("PkgName")->isString())) {
            throw vu::FileError(vle::utils::format(" [%s] PkgName condition missing",
                    getModelName().c_str()));
        }
        if (!(events.exist("parametersFile") &&
              events.get("parametersFile")->isString())) {
            throw vu::FileError(
                    vle::utils::format("[%s] parametersFile condition missing",
                                getModelName().c_str()));
        }
        std::string pathFile = getPackageDataFile(
                events.getString("parametersFile"));
        mparams.parse(pathFile);
        if (!(events.exist("begin_date") &&
                events.get("begin_date")->isString())) {
            throw vu::FileError(
                    vle::utils::format("[%s] begin_date condition missing",
                            getModelName().c_str()));
        }
        current_date = vle::utils::DateTime::toJulianDayNumber(
                events.getString("begin_date"));



        //###############################################
        //# Initialization
        //###############################################

         //# Sets the number of variables (including rain)
        unsigned int Nv   =  1 + mparams.trend_central.cols();
        unsigned int jd  = vle::utils::DateTime::dayOfYear(current_date);
        if (vle::utils::DateTime::isLeapYear(current_date) and jd > 60) {
           jd = jd-1;
        }
        //# Creating the vector of season number : SEASON
        mnewDay.Xsimul.resize(Nv);
        mnewDay.Xresid.resize(Nv);

        //###############################################
        //# Simulating of first day
        //###############################################

        // Simulating weather type for day 1
        unsigned int currSeason = season(current_date);
        const Season& season_par = mparams.seasons[currSeason];

        EigenSolver<MatrixXd> eigensolver(season_par.transM.transpose());
        ArrayXXd limit_prob =
                eigensolver.pseudoEigenvectors().col(0).array().abs();//TODO take the most important eigen value
        limit_prob = limit_prob / limit_prob.sum();
        mnewDay.WT = record::eigen::Stats::sample_int(VectorXd(limit_prob),
                mrand);

        // Setting residual parameters for day 1
        const WJ& wj = season_par.wjs[mnewDay.WT];
        //mparams.print(wj);

        bool WET = wj.isWet();
        unsigned int Nvv = wj.getNbVariables();

        const VectorXd& mu = wj.loc;
        const MatrixXd& sigma  = wj.cov;
        const VectorXd& skew  = wj.skew;
        const VectorXd& rho  = wj.rho;

        VectorXd MU(2*mu.rows());
        MU  << mu, mu;
        VectorXd SKEW(2*skew.rows());
        SKEW  << skew, skew;
        MatrixXd SS = MatrixXd::Zero(2*Nvv,2*Nvv);
	
	MatrixXd sigma_sqrt = Stats::sqrtm(sigma);

        SS.block(0,0,Nvv,Nvv) << sigma;
        SS.block(Nvv,Nvv,Nvv,Nvv) << sigma;

        SS.block(0,Nvv,Nvv,Nvv) << (sigma_sqrt * rho.asDiagonal() * sigma_sqrt);
        SS.block(Nvv,0,Nvv,Nvv) << SS.block(0,Nvv,Nvv,Nvv).transpose();

        //Simulating residuals for day 1
        bool COMP = false;
	Ntry =0;
        while (not COMP) {
            Ntry++; 
            VectorXd XtildeTmp = Stats::rmcsnstar(1, MU, SS, SKEW,
                    mrand).block(0,0,1,Nvv).transpose();

            if (!WET) {
                mnewDay.Xresid << 0, XtildeTmp;
            } else {
                mnewDay.Xresid << XtildeTmp;
            }
            for (unsigned int v=0;  v < Nv; v++){
                if (v == 0) {//rain
                    if (!WET) {
                        mnewDay.Xsimul(v) = 0;
                    } else {
                        mnewDay.Xsimul(v) = transform_rain(
                                (double) mnewDay.Xresid(0), currSeason);
                    }
                } else {
                    mnewDay.Xsimul(v) = mnewDay.Xresid(v)
                            * mparams.trend_deviation(jd-1, v-1) +
                            mparams.trend_central(jd-1, v-1);
                }
            }
            COMP = testvariables(mnewDay.Xsimul);
        }
        //Strore previous Day
        mprevDay.copyFrom(mnewDay);

        //initialize vars
        for (auto var : mparams.wacs_var_names) {
           mvars.push_back(Var());
           mvars[mvars.size()-1].init(this, var, events);
           if (var != "year" and var != "month" and var != "day") {
             mvars[mvars.size()-1].init_value(
                             mnewDay.Xsimul[mparams.colIndex(var)]);
           }
        }
        Nhelp = 0;
        REJECT = false;
    }



    virtual ~WACSgenSimulation()
    {
    }

    virtual void compute(const vd::Time& /*time*/) override
    {


           //##################################
            //#
            //# Loop on all days:
            //
            //#  1/ sample a new weather type (calls function rMarkov)
            //#  2/ conditionnaly to this weather type, sample a new vector of variables
            //#     (calls function rvariables)
            //#  3/ test wether these variables are within vbounds (calls function testvariables);
            //#     if yes OK; if not go to 2 (uses a while loop)
            //#
            //#  newWT  is a boolean; if TRUE must sample a weather type
            //#  KRITER is a boolean; if TRUE the simulated variables are OK
            //#
            //#
            //##################################

	    current_date ++;

            //# Sets the number of variables (including rain)
            unsigned int Nv   =  1 + mparams.trend_central.cols();
            //Nhelp = 0
            unsigned int jd  = vle::utils::DateTime::dayOfYear(current_date);
            if (vle::utils::DateTime::isLeapYear(current_date) and jd > 60) {
              jd = jd-1;
            }
            unsigned int currSeason  = season(current_date);


            //Loading parameters of previous day
            unsigned int oldSeason = season(current_date-1);
            unsigned int oldWT = mprevDay.WT;

            const Season& season_par_old = mparams.seasons[oldSeason];


            bool WET_old = season_par_old.wjs[oldWT].isWet();

            // Some preliminary work if season changes
            const Season* season_par = &season_par_old;
            if ( (oldSeason != currSeason) &&
                    (season_par->nbDryWJs() + season_par->nbWetWJs() > 2) ) {
                season_par = &mparams.seasons[currSeason];
                // finding the closest WT of previous day in new season
                oldWT = map_wt(mprevDay.Xresid,*season_par, WET_old);
            }

            bool vdry_old  = not WET_old;
            const VectorXd& mu_old    = season_par->wjs[oldWT].loc;
            const MatrixXd& sigma_old    = season_par->wjs[oldWT].cov;
            const VectorXd& skew_old    = season_par->wjs[oldWT].skew;
            const VectorXd& rho_old    = season_par->wjs[oldWT].rho;
            unsigned int Nvv_old = Nv - vdry_old;

            bool COMP = false;
	    Ntry = 0;
            VectorXd Xtilde_new_save;
            VectorXd Xtest_save;
            VectorXd Xtest = VectorXd::Zero(Nv-1);
            while(not COMP){
                Ntry = Ntry+1;

                // Simulating weather type for new day
                unsigned int WT_j = rMarkov(oldWT,season_par->transM);


                mnewDay.WT = WT_j;

                // new parameters
                const WJ& wt_new  = season_par->wjs[WT_j];
                bool WET_new   = wt_new.isWet();
                bool vdry_new  = not WET_new;
                const VectorXd& mu_new    = wt_new.loc;
                const MatrixXd& sigma_new    = wt_new.cov;
                const VectorXd& skew_new    = wt_new.skew;
                const VectorXd& rho_new    = wt_new.rho;
                unsigned int Nvv_new = Nv - vdry_new;


                // building the covariance matrix of (X_t, X_(t+1))
                MatrixXd SS = MatrixXd::Zero((Nvv_old+Nvv_new), (Nvv_old+Nvv_new));
                SS.topLeftCorner(Nvv_old,Nvv_old) << sigma_old;
                SS.bottomRightCorner(Nvv_new,Nvv_new) << sigma_new;

                VectorXd rho = VectorXd::Zero(std::min(Nvv_old, Nvv_new));
		MatrixXd rho_mat = MatrixXd::Zero(Nvv_old, Nvv_new);
                if (WET_new == WET_old) {
                  rho = Stats::zip_max(rho_old, rho_new);
                  rho_mat = rho.asDiagonal();
                } else if (WET_old and !WET_new) {
                  rho = Stats::zip_max(Stats::subVector_noti(rho_old,0), 
                                       rho_new);
                  rho_mat.bottomRightCorner(Nvv_old-1,Nvv_new) << MatrixXd(rho.asDiagonal()); 	
                } else if (!WET_old and WET_new) {
	          rho = Stats::zip_max(rho_old, 
                             	       Stats::subVector_noti(rho_new,0));
                  rho_mat.bottomRightCorner(Nvv_old,Nvv_new-1) << MatrixXd(rho.asDiagonal()); 	
                }

                MatrixXd SSdiag = Stats::sqrtm(sigma_old)* rho_mat * Stats::sqrtm(sigma_new);

                SS.topRightCorner(Nvv_old,Nvv_new) << SSdiag;

                SS.bottomLeftCorner(Nvv_new, Nvv_old) << SSdiag.transpose();


                VectorXd MU(mu_old.size()+mu_new.size()); 
                MU  << mu_old, mu_new;
                VectorXd SKEW(skew_old.size() + skew_new.size());
                SKEW << skew_old,skew_new;
                MatrixXd SKEW_diag = SKEW.asDiagonal();
                MatrixXd D = SKEW_diag * Stats::sqrtm(SS).inverse();

               MatrixXd Delta = MatrixXd(VectorXd::Ones(Nvv_old+Nvv_new).asDiagonal()).array() -
                        SKEW_diag.array().pow(2.0);


                //simulating residuals, conditional on the residuals
                //of previous day
                VectorXd Xtilde_old  = mprevDay.Xresid;
                if (!WET_old) {
                    Xtilde_old = Xtilde_old.tail(Xtilde_old.size()-1);
                }
                VectorXd Nu = VectorXd::Zero(Nvv_old+Nvv_new);
                VectorXd Xtilde_new = Stats::rmcsn_cond(1,Xtilde_old,
                        MU,SS,D,Nu,Delta,mrand).transpose();
		if (Ntry > 50){
                   Nhelp = Nhelp + 1;
                   Xtilde_new = Stats::rmcsnstar(1,MU,SS,SKEW, mrand).row(1).segment(
                            Nvv_old,Nvv_new);;
                }

                if (!WET_new){
                    VectorXd Xtilde_new_bis(Xtilde_new.size()+1);
                    Xtilde_new_bis << -999, Xtilde_new;
                    Xtilde_new = Xtilde_new_bis;
                }
                double minRes = Xtilde_new.segment(1, Xtilde_new.size()-1).array().minCoeff();
                double maxRes = Xtilde_new.segment(1, Xtilde_new.size()-1).array().maxCoeff();
                if (maxRes > 3.5 or minRes < -3.5) {
                    COMP =false;
                } else {
                    mnewDay.Xresid << Xtilde_new;
                    for (unsigned int v=0;  v < Nv; v++){
                      if (v == 0) {//rain
                        if (!WET_new) {
                          mnewDay.Xsimul(v) = 0;
                        } else {
                          mnewDay.Xsimul(v) = transform_rain(
                                (double) mnewDay.Xresid(0), currSeason);
                        }
                      } else {
                        mnewDay.Xsimul(v) = mnewDay.Xresid(v)
                            * mparams.trend_deviation(jd-1, v-1) +
                            mparams.trend_central(jd-1, v-1);
                     }
                   }
                   if (REJECT) {
                     COMP = testvariables(mnewDay.Xsimul);
                   } else {
                     COMP =true;
                   } 
                }
            }
            if (not REJECT) {
              mnewDay.Xsimul = boundvariables(mnewDay.Xsimul);
             }

            //Strore previous Day
            mprevDay.copyFrom(mnewDay);

            //initialize vars
            for (auto var : mparams.wacs_var_names) {
              if (var != "year" and var != "month" and var != "day") {
               mvars[mparams.colIndex(var)] = mnewDay.Xsimul[mparams.colIndex(var)];
              }
            }
    }


    virtual std::unique_ptr<vle::value::Value> observation(
                      const vd::ObservationEvent& event) const override
    {
        if (event.onPort("Ntry")) {
            return vle::value::Double::create(Ntry);
        }
        if (event.onPort("Nhelp")) {
            return vle::value::Double::create(Nhelp);
        }
        if (event.onPort("year")) {
            return vle::value::Double::create(
                    vle::utils::DateTime::year(current_date));
        }
        if (event.onPort("month")) {
            return vle::value::Double::create(
                    vle::utils::DateTime::month(current_date));
        }
        if (event.onPort("dayOfMonth")) {
            return vle::value::Double::create(
                    vle::utils::DateTime::dayOfMonth(current_date));
        }
        if (event.onPort("dayOfYear")) {
            return vle::value::Double::create(
                    vle::utils::DateTime::dayOfYear(current_date));
        }
        if (event.onPort("current_date")) {
            return vle::value::Double::create(current_date);
        }
        if (event.onPort("current_date_str")) {
            return vle::value::String::create(
                   vle::utils::DateTime::toJulianDay(current_date));
        }
        if (event.onPort("season")) {
            return vle::value::Double::create(season(current_date));
        }
        if (event.onPort("WT")) {
            return vle::value::Double::create(mnewDay.WT);
        }
        return DiscreteTimeDyn::observation(event);
    }

};

} // namespace wacsgen

DECLARE_DYNAMICS(wacsgen::WACSgenSimulation)

