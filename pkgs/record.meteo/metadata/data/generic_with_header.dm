<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata author="PC" version="1.x">
  <dataModel conf="generic_with_header" package="record.meteo"/>
  <configuration>
    <dynamic package="record.meteo" library="MeteoReader" name="dyn_generic_with_header"/>
    <observable name="obs_generic_with_header">
      <port name="Tmin">
      </port>
      <port name="Tmax">
      </port>
      <port name="ETP">
      </port>
      <port name="Pluie">
      </port>
    </observable>
    <condition name="cond_generic_with_header">
      <port name="PkgName">
	<string>record.meteo</string>
      </port>
      <port name="meteo_file">
	<string>generic_with_header.txt</string>
      </port>
      <port name="meteo_type">
	<string>generic_with_header</string>
      </port>
    </condition>
    <in/>
    <out>
      <port name="Tmin"/>
      <port name="Tmax"/>
      <port name="ETP"/>
      <port name="Pluie"/>
    </out>
  </configuration>
</vle_project_metadata>
