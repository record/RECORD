<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata author="PC" version="1.x">
  <dataModel conf="agroclim" package="record.meteo"/>
  <configuration>
    <dynamic package="record.meteo" library="MeteoReader" name="dyn_agroclim"/>
    <observable name="obs_agroclim">
      <port name="I1N">
      </port>
      <port name="I1X">
      </port>
      <port name="I5N">
      </port>
    </observable>
    <condition name="cond_agroclim">
      <port name="PkgName">
	<string>record.meteo</string>
      </port>
      <port name="meteo_file">
	<string>agroclim.csv</string>
      </port>
      <port name="meteo_type">
	<string>agroclim</string>
      </port>
    </condition>
    <in/>
    <out>
      <port name="I1N"/>
      <port name="I1X"/>
      <port name="I5N"/>
    </out>
  </configuration>
</vle_project_metadata>
