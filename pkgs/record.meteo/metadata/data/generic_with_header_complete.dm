<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata author="PC" version="1.x">
  <dataModel conf="generic_with_header_complete" package="record.meteo"/>
  <configuration>
    <dynamic package="record.meteo" library="MeteoReader" name="dyn_generic_with_header"/>
    <observable name="obs_generic_with_header_complete">
      <port name="Tmin">
      </port>
      <port name="Tmax">
      </port>
      <port name="ETP">
      </port>
      <port name="Rain">
      </port>
    </observable>
    <condition name="cond_generic_with_header_complete">
      <port name="PkgName">
	<string>record.meteo</string>
      </port>
      <port name="meteo_file">
	<string>Blagnac_87-97.txt</string>
      </port>
      <port name="meteo_type">
	<string>generic_with_header</string>
      </port>
      <port name="year_column">
	<string>annee</string>
      </port>
      <port name="month_column">
	<string>mois</string>
      </port>
      <port name="day_column">
	<string>jour</string>
      </port>
      <port name="column_separator">
	<string> </string>
      </port>
      <port name="begin_date">
	<string>1988-1-1</string>
      </port>
    </condition>
    <in/>
    <out>
      <port name="Tmin"/>
      <port name="Tmax"/>
      <port name="ETP"/>
      <port name="Rain"/>
    </out>
  </configuration>
</vle_project_metadata>
