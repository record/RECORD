Package: record.meteo
Version: 0.1.0
Depends:
Build-Depends: vle.discrete-time, vle.reader
Conflicts:
Maintainer: Ronan Trépos <rtrepos@toulouse.inra.fr>
Description: Meteo Files readers
 .
Tags: record utils meteo reader
Url: http://recordb.toulouse.inra.fr/distributions/2.0
Size: 0
MD5sum: xxxx
