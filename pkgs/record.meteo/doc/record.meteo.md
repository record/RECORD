# Package record.meteo for vle-2.0.0

<img src="../../../formation/figures/record_logo.png" alt="Record Team" style="width: 200px;"/>

The [**record.meteo**](http://recordb.toulouse.inra.fr/distributions/2.0/record.meteo.tar.bz2) package provide a single generic model called **MeteoReader** and three prepared configurations in order to facilitate the use of the model.
Three sample input files are also provided in the */data* folder.

Tests and a sample simulator can be found in the [**record.meteo_test**](http://recordb.toulouse.inra.fr/distributions/2.0/record.meteo_test.tar.bz2) companion package

# Table of Contents
1. [Package dependencies](#p1)
2. [Atomic model MeteoReader](#p2)
    1. [Configuring a MeteoReader Model](#p2.1)
        1. [Dynamics settings](#p2.1.1)
        2. [Parameters settings](#p2.1.2)
        3. [Input settings](#p2.1.3)
        3. [Output settings](#p2.1.4)
        4. [Observation settings](#p2.1.5)
        5. [Available configurations](#p2.1.6)
    2. [Details](#p2.2)
        1. [meteo_type specifications](#p2.2.1)
            1. [agroclim](#p2.2.1.1)
            2. [generic_with_header](#p2.2.1.2)
            3. [drias_ascii](#p2.2.1.3)
        2. [Input file format restrictions](#p2.2.2)

---

## Package dependencies <a name="p1"></a>

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)
* [vle.reader](http://www.vle-project.org/pub/2.0/vle.reader.tar.bz2)

---

## Atomic model MeteoReader <a name="p2"></a>

The **MeteoReader** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) enable to load a dataset table from a text file (csv[^1], tsv[^2] or fwf[^3] file for example).
Each column of the table corresponds to a possible variable of the model. Each row of the table corresponds to the value of the variables for a given time step.
For each output ports defined (with name corresponding to the available columns names), the model will output values gathered from the dataset.
The **MeteoReader** can try to guess as much as possible the format of the file, but optionnal settings are available to adjust, if necessary, the definition of the format of the file.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#MeteoReader_panel'>Show/Hide Model Details</button>
<div id="MeteoReader_panel" class="collapse">

### Configuring a MeteoReader Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : record.meteo
* **name** : MeteoReader

#### Parameters settings <a name="p2.1.2"></a>

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim|generic_with_header|drias_ascii) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|
| **year_column** | string| [] |Name of the column defining the year of the date.<br>Default value is "AN".|
| **month_column** | string| [] |Name of the column defining the month of the date.<br>Default value is "MOIS".|
| **day_column** | string| [] |Name of the column defining the day of the date.<br>Default value is "JOUR".|
| **column_separator** | string | [] |Character used as column separator in the **meteo_file** file.<br>Default value is ";" if **meteo_type**=agroclim, or "\t" if **meteo_type**=generic_with_header.|


#### Input settings <a name="p2.1.3"></a>

No input ports have to be defined.

#### Output settings <a name="p2.1.4"></a>

Output ports have to be defined according to the column names available inside the dataset.
In order to work properly, the name of the port has to match a column name (case sensitive).
Wrong port names will provide a constant value (0 by default).
All columns do not have to be defined as an output port.

#### Observation settings <a name="p2.1.5"></a>

Same rule applies as for output ports, observable ports have to be defined according to the column names available inside the dataset.
The name of the port has to match a column name (case sensitive).

Two additionnal observation ports are available *current_date* and *current_date_str*, providing the date of the current time-step in double or string format respectivly.

#### Available configurations <a name="p2.1.6"></a>

Three configurations are available, they all use sample input files stored inside the **record.meteo** package :

* **record.meteo/agroclim**: an example of a MeteoReader configuration handling a file at the format of the Agrocim database.
* **record.meteo/generic_with_header**: an example of a MeteoReader configuration handling a custom file where only minimal settings are involved.
* **record.meteo/generic_with_header_complete**: an example of a MeteoReader configuration handling a custom file where all settings are involved.

### Details <a name="p2.2"></a>

#### meteo_type specifications <a name="p2.2.1"></a>
##### agroclim <a name="p2.2.1.1"></a>

The file format expected when using **meteo_type** *agroclim* is the one used by the [climatik web portal](https://intranet.inra.fr/climatik/do/welcome) (require LDAP account).
__Remark :__ The first line of data is expected right after the first line starting with the string "NUM_POSTE"

Sample file :
```
Nom de la demande:test
Pas de temps:Journalier
Date de début:01/01/2008
Date de fin:31/05/2009
Série:31035001

ETP:EVAPOTRANSPIRATION MESUREE (MILLIMETRES)
TM:TEMPERATURE MOYENNE (DEGRES CELSIUS)

NUM_POSTE;AN;MOIS;JOUR;ETP;TM;
31035002;2008;1;1;10;7;
31035002;2008;1;2;8.5;5;
...

```

##### generic_with_header <a name="p2.2.1.2"></a>

The file format expected when using **meteo_type** *generic_with_header* have a header in the first line with column names and the first line of data is expected right after it.

Sample file :
```
AN;MOIS;JOUR;ETP;TM;
2008;1;1;10;7;
2008;1;2;8.5;5;
...

```

##### drias_ascii <a name="p2.2.1.3"></a>

The file format expected when using **meteo_type** *drias_ascii* has a header inside which the format can be found. Only ascii DRIAS file with date stored in 3 columns are managed.

Sample file :
```
# -----------------------------------------------------------------------------------------------------------
# Origine : DRIAS-CLIMAT - http://www.drias-climat.fr/commande  (produit par 'nc2txt2nc_drias2014 1.0.3 du 23.01.2017')
# Date d'extraction : 01/02/2018 08:06:32 UTC
# -----------------------------------------------------------------------------------------------------------
# Production : CNRM2014
# Modele : ALADIN
# Scenario : Periode de reference (ref)
# Donnees CORRIGEES
# -----------------------------------------------------------------------------------------------------------
# Periode : 01/01/2005 - 31/12/2005
# -----------------------------------------------------------------------------------------------------------
...
# -----------------------------------------------------------------------------------------------------------
# Format de la ligne :
#   "Date  tasmin tasmax rstr huss rsds rlds sfcwind"
# avec :
...
# -----------------------------------------------------------------------------------------------------------
# Conditions d'utilisation :
# http://www.drias-climat.fr/accompagnement/conditionsEDC
# -----------------------------------------------------------------------------------------------------------
2005;01;01;2.7;8.8;0.00;4.94;42.24240;297.810;1.17
2005;01;02;3.3;9.0;7.29;5.73;7.19794;334.117;5.57
2005;01;03;2.6;6.9;1.01;4.90;46.67690;290.880;3.44
...
```

#### Input file format restrictions <a name="p2.2.2"></a>

When using calendar dates the file must contain all data in ascending chronological order without any missing values.

All values of the dataset are expected as numerical values using `.` for decimal separator.
</div>


[^1]:
Comma separated value.
[^2]:
Tab separated value.
[^3]:
Fixed width value.