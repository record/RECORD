/**
 * @file src/Carbone.cpp
 * @author The Record Development Team
 */

/*
 * Copyright (C) 2009-2017 The Record Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
using namespace vle::discrete_time;

namespace carboneR {
class Carbone : public DiscreteTimeDyn
{

public:
    Carbone(const vle::devs::DynamicsInit& model,
	    const vle::devs::InitEventList& events) :
    DiscreteTimeDyn(model, events)
	{
            // Lecture des paramètres du modèle
	    b = vv::toDouble(events.get("b"));
            EpsZtm1= vv::toDouble(events.get("EpsZtm1"));
	    EpsRtm1 = vv::toDouble(events.get("EpsRtm1"));
	    U0 = vv::toDouble(events.get("U0"));
	    Z0 = vv::toDouble(events.get("Z0"));
	    R0 = vv::toDouble(events.get("R0"));
            // Variable d'état du systeme
            Z.init(this,"Z", events);
        
            // Autres 
	    R.init(this,"R", events); // Paramètre inconnu, taux de décomposition du C du sol
	    U.init(this,"U", events); // Quantité de C ajoutée au sol pour l'année t en kg . ha-1

            initValue(0.);
	}


// -----------------------------
    virtual ~Carbone() { }


// -----------------------------
    virtual void compute(const vle::devs::Time& /* time */)
	{
            // Comment est calculé R 
             double tempR = log(R0) + EpsRtm1;
             R= exp(tempR);
            //Dynamqie de Z
            Z = Z(-1) -R()*Z(-1) +   b * U(-1) +  EpsZtm1;
	}


// -----------------------------
    void initValue(const vle::devs::Time& /*time*/)
	{
            // Lecture des valeurs initiales de U R Z
            Z= Z0;
            R= R0;
            U= U0;
	}

private:
    double Z0;
    double R0;
    double U0;
    double EpsZtm1;
    double EpsRtm1;
    double b;

    Var Z;
    Var U;
    Var R;

};
} // namespace carboneR

DECLARE_DYNAMICS(carboneR::Carbone);
