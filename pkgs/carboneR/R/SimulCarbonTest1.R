####Implementation of the particle filter with a soil carbon model

set.seed(1)
source("ImpSamplingR.R")
source("ModelCarbon.R")

######Number of particles####
N<-10000
#############################

##Number of years of simulations
Q<-31

##Data (yearly observation of soil carbon (kg/ha)
Obs<-c(15730.73, 14810.49, 17347.48, 17707.61, 16897.79, 15574.29, 16785.51, 15805.75, 15798.69, 16771.28, 14911.78, 15088.59,
14926.66, 15498.13, 14345.42, 15035.19, 16125.55, 15984.13, 14489.86, 15551.67, 13392.08, 14905.99, 15353.07, 14004.30, 14145.95,
12832.96, 14229.82, 12038.80, 12736.38, 12391.17, 12328.56)
VAR.O<-500000

##Definition of two matrices including all simulations
Z.mat.ini<-matrix(nrow=N, ncol=Q)
R.mat.ini<-matrix(nrow=N, ncol=Q)
Z.mat.upd<-matrix(nrow=N, ncol=Q)
R.mat.upd<-matrix(nrow=N, ncol=Q)

##Initial soil carbon value (kg/ha)
MU.Z.0<-16000
VAR.Z.0<-20000

##Initial soil carbon yearly decomposition rate (per year)
MU.R.0<-0.02
VAR.R.0<-0.001

##Generation of N particles of Z and R at t=0
Z.par<-MU.Z.0 + rnorm(N, mean=0, sd=sqrt(VAR.Z.0))
lR.par<-log(MU.R.0) + rnorm(N, mean=0, sd=sqrt(abs(log(MU.R.0))*VAR.R.0))
R.par<-exp(lR.par)

Z.mat.ini[,1]<-Z.par
Z.mat.ini[,1]<-Z.par
R.mat.ini[,1]<-R.par
Z.mat.upd[,1]<-Z.par
R.mat.upd[,1]<-R.par

##Simulations during Q-1 years

#for (q in 2:Q) {

	for (i in 1:N) {

	#X.i.ini.tm1<-c(Z.mat.ini[i,q-1],R.mat.ini[i,q-1])
	#X.i.ini.tm1<-c(Z.mat.ini[i,q-1],R.mat.ini[i,q-1])
 	#X.i.upd.tm1<-c(Z.mat.upd[i,q-1],R.mat.upd[i,q-1])
	U.tm1<-2000
	b<-0.2
	#Noise for Z
	Eps.Z.tm1<-rnorm(1, mean=0, sd=sqrt(VAR.Z.0))
	#Noise for R
	Eps.R.tm1<-rnorm(1, mean=0, sd=sqrt(abs(log(MU.R.0))*VAR.R.0))
	
	##Initial model simulations (no correction)
        #Chgt RECORD	X.i.t<-ModelCarbon(X.i.ini.tm1, U.tm1, b, Eps.Z.tm1, Eps.R.tm1)
        library(rvle)
        ModelCarbonR <- rvle.open("Carbone.vpz")
        rvle.condition_set_real(ModelCarbonR, "condZ", "Q",  Q) #  duration codée en 100
        rvle.condition_set_real(ModelCarbonR, "condZ", "b",  b) 
        rvle.condition_set_real(ModelCarbonR, "condZ", "Z0",Z.par[i]  ) 
        rvle.condition_set_real(ModelCarbonR, "condZ", "R0",R.par[i]  ) 
        rvle.condition_set_real(ModelCarbonR, "condZ", "U0",  U.tm1) 
        X.i.t <- rvle.run(ModelCarbonR)
 
	Z.mat.ini[i,q]<-X.i.t[1]
	R.mat.ini[i,q]<-X.i.t[2]

	X.i.t<-ModelCarbon(X.i.upd.tm1, U.tm1, b, Eps.Z.tm1, Eps.R.tm1)
	Z.mat.upd[i,q]<-X.i.t[1]
	R.mat.upd[i,q]<-X.i.t[2]

	}

	##Updated model simulations using the particle filter
	Y<-Obs[q]
	A<-matrix(0,nrow=1, ncol=2)
	A[1,1]<-1
	MU<-0
	S<-VAR.O
	W<-rep(1/N, N)
	X.t<-rbind(Z.mat.upd[,q],R.mat.upd[,q])
	Xupd.t<-ImpSamplingR(Y,X.t,S,A,MU,W)
	Z.mat.upd[,q]<-Xupd.t[1,]
	R.mat.upd[,q]<-Xupd.t[2,]
		}

###Computation of the mean values over the N particles for Q years

Z.mean.ini<-apply(Z.mat.ini, 2, mean)
R.mean.ini<-apply(R.mat.ini, 2, mean)
Z.mean.upd<-apply(Z.mat.upd, 2, mean)
R.mean.upd<-apply(R.mat.upd, 2, mean)

par(mfrow=c(1,2))

plot(1:Q, Z.mean.ini, xlab="Year", ylab="Soil C (kg/year)", type="l", lwd=3, lty=3, ylim=c(10000, 20000))
lines(1:Q, Z.mean.upd, lwd=2, lty=1)
points(1:Q, Obs, pch=21)
plot(1:Q, R.mean.ini, xlab="Year", ylab="Soil C decomposition rate (per year)", type="l", lwd=3, lty=3, ylim=c(0.015, 0.05))
lines(1:Q, R.mean.upd, lwd=2, lty=1)

X11()
par(mfrow=c(2,2))
hist(Z.mat.ini[,31], main="Initial", xlab="Soil C (kg/ha)")
hist(Z.mat.upd[,31], main="Final", xlab="Soil C (kg/ha)")
hist(R.mat.ini[,31], main=" ", xlab="Soil C decomposition rate")
hist(R.mat.upd[,31], main=" ", xlab="Soil C decomposition rate")




