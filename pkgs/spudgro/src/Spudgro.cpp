/**
 * @file src/Spudgro.cpp
 * @author Toky-(The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

#include <vector>
#include <iostream>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;
using namespace std;

namespace These {

    class Spudgro: public DiscreteTimeDyn
    {
    public:
        Spudgro(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
            DiscreteTimeDyn(atom, events)
        {
            // Variables d'etat gerees par ce composant "Spudgro
            MS.init(this,"MS", events);
            LAI.init(this,"LAI", events);
            LEAF.init(this,"LEAF", events);
            dLEAF.init(this,"dLEAF", events);
            STEM.init(this,"STEM", events);
            ROOT.init(this,"ROOT", events);
            TUBER.init(this,"TUBER", events);
            AGE.init(this,"AGE", events);
            P_ASSIMres.init(this,"P_ASSIMres", events);
            // A compléter
            death.init(this,"death", events);

            // Variables  gerees par un autre  composant que "Spudgro ici "METEO"
             // Temperature minimal du jour (°C), cette variable est synchrone
            Tmin.init(this,"Tmin", events);
            getOptions().syncs.insert(std::make_pair("Tmin", 1));
            // Temperature maximal du jour (°C), cette variable est synchrone
            Tmax.init(this,"Tmax", events);
            getOptions().syncs.insert(std::make_pair("Tmax", 1));
            // Rayonnement global du jour (MJ), cette variable est synchrone
            Rg.init(this,"Rg", events);
            getOptions().syncs.insert(std::make_pair("Rg", 1));
            // Stress hydrique, cette variable est synchrone
            SWP.init(this,"SWP", events);
            getOptions().syncs.insert(std::make_pair("SWP", 1));

            // Lecture des parametres
            rowspacing = vv::toDouble(events.get("rowspacing"));
            plantspacing = vv::toDouble(events.get("plantspacing"));
            PNCGR = vv::toDouble(events.get("PNCGR"));
            K1 = vv::toDouble(events.get("K1"));
            K2 = vv::toDouble(events.get("K2"));
            K3 = vv::toDouble(events.get("K3"));
            K4 = vv::toDouble(events.get("K4"));
            K5 = vv::toDouble(events.get("K5"));
            K6 = vv::toDouble(events.get("K6"));
            seedpc = vv::toDouble(events.get("seedpc"));
            debsimul = vv::toDouble(events.get("debsimul"));
            finrealoc = vv::toDouble(events.get("finrealoc"));
            leafalocinit = vv::toDouble(events.get("leafalocinit"));
            stemalocinit = vv::toDouble(events.get("stemalocinit"));
            rootalocinit = vv::toDouble(events.get("rootalocinit"));
            inidleaf = vv::toDouble(events.get("inidleaf"));
            inituber = vv::toDouble(events.get("inituber"));
            reconverMS = vv::toDouble(events.get("reconverMS"));
            
            { // detect begin time
                std::string  begin_date;
                if (events.get("begin_date")->isDouble()) {
                    begin_time = events.getDouble("begin_date");
                    begin_date = vle::utils::DateTime::toJulianDay(begin_time);
                } else if (events.get("begin_date")->isString()) {
                    begin_date = events.getString("begin_date");
                    begin_time = vle::utils::DateTime::toJulianDay(begin_date);
                }
            }

            initValue(begin_time);
        }


        virtual ~Spudgro() {};


        virtual void compute(const vd::Time& time)
        {
            updateAGES(time+begin_time);

        // CALCUL DE L'ACCROISSEMENT DE MS PAR UNITE DE SURFACE: MS=f(PTRi, RG, PNCGR, MSRF, Pday)
        //========================================================================================
            // Variable de calcul PTmin
            double PTmin;
            if ( Tmin() < 7. ) 
                   PTmin = 0.;
            else if ( (Tmin() >= 7. ) && ( Tmin() < 21.))  
                PTmin = 10. * (1. - ( (pow((Tmin() - 21.),2) / 196.))) ;
            else if ( (Tmin() >= 21.) && ( Tmin() <= 30.)) 
                PTmin = 10. * (1. - ( (pow((Tmin() - 21.),2) / 81.))) ;
            else
                PTmin = 0.;

            // Variable de calcul PTmax
            double PTmax;
            if ( Tmax() < 7. ) 
                   PTmax = 0.;
            else if ( (Tmax() >= 7. ) && ( Tmax() < 21.))  
                   PTmax = 10. * (1. - ( (pow((Tmax() - 21.),2) / 196.))) ;
            else if ( (Tmax() >= 21.) && ( Tmax() <= 30.)) 
                PTmax = 10. * (1. - ( (pow((Tmax() - 21.),2) / 81.)));
            else
                PTmax = 0.;

            // variable de calcul Pday
            double Pday;
            Pday = 1./24. * (5. * PTmin + 8. * (2. * PTmin + PTmax ) / 3. + 8. * (2. * PTmax + PTmin ) / 3. + 3. * PTmax);

            // variable d'etat :  AGE  (âge physiologique de la plante en d° jour)
            AGE = AGE(-1) + Pday;

            // variable de calcul nbplant // nombre de pieds par unité de surface (en m-²)
            double nbplant;
            nbplant = (100. / rowspacing) * (100./ plantspacing) / 10000.;

            //  variable d'etat: LAI (m² m-²)(indice de surface foliaire(leaf area index))
            LAI = (LEAF(-1) * 230. + 0.5 * STEM(-1) * 3.14) * nbplant / 10000.;


            // variable de calcul MSRF (facteur de réduction dû au stress hydrique, f(SWP). Cette variable est nécessaire pour le calcul de l'élaboration de MS matière sèche par jour
            double MSRF;
            if ( SWP() < -5.) 
                MSRF = 0.;
            else if ( (SWP() >= -5.) && (SWP() <= -1.)) 
                MSRF = 0.25 * SWP() + 1.25;
            else
                MSRF = 1.;

            // variable de calcul PTRi (-) proportion de rayonnement intercepté, f(LAI). Cette variable est nécessaire pour le calcul de l'elaboration de MS matière sèche par jour
            double PTRi;
            PTRi = max(0., 1. - exp ( -0.5 * LAI(-1) ));

            // variable d'etat: MS (g m-² j-1): quantite de matieres seches elaborees par jour, f(PTRi, RG, PNCGR, MSRF, Pday)
            MS =    (PTRi * Rg() * PNCGR * MSRF * Pday )/ nbplant;

            // variable d'etat P_ASSIM (gm-²), quantité de MS accumulées et prêtes à être reparties vers les divers organes
            double ASSIM;
            //double P_ASSIM;
            ASSIM = P_ASSIMres(-1) * (1.-  K6 * MSRF); //ne peut pas utiliser P_ASSIMres() avant de l'avoir calculé
            ASSIM = ASSIM + MS();
            P_ASSIMres = ASSIM;

            // variable de calcul des équations "Michaelis - Menten" nécessaires pour la répartition des MS pour chaque organe
            double dL;
            dL = K2 * (K1 / (K1 + STEM(-1) + ROOT(-1) + TUBER(-1))) ;
            double dS;
            dS = K3 * (STEM(-1) / (STEM(-1) + K1 * (1 + TUBER(-1)))) ;
            double dR;
            dR = K4 * (ROOT(-1) / (ROOT(-1) + K1 * (1 + TUBER(-1)))) ;
            double dT;
            dT = K5 * (TUBER(-1) / (TUBER(-1) + K1)) ;

            // variable de calcul des taux d'accroissement de MS par  jour pour chaque organe
            dLEAF = (dL / (dL + dS + dR + dT)) *  ASSIM * K6 * MSRF ; // accroissement de biomasse dans les feuilles par unité de temps et de surface (g m-² j-1)
            double dSTEM;
            dSTEM = (dS  / (dL + dS + dR + dT)) *  ASSIM * K6 * MSRF  ; // accroissement de biomasse dans les tiges par unité de temps et de surface (g m-² j-1)
            double dROOT;
            dROOT = (dR / (dL + dS + dR + dT)) *  ASSIM * K6 * MSRF  ; // accroissement de biomasse dans les racines par unité de temps et de surface (g m-² j-1)
            double dTUBER;
            dTUBER = (dT / (dL + dS + dR + dT)) *  ASSIM * K6 * MSRF  ; // accroissement de biomasse dans les tubercules par unité de temps et de surface (g m-² j-1)

            // Calcul de la sénéscence du jour
            double deathleaf=0.;

            int ind = vle::utils::DateTime::dayOfYear(time+begin_time); // A verifier!!!

            int j = (indlastsen - ind); // indlastsen: indice donnant le jour d'apparition des feuilles qui ont été les dernières sénéscentes à ce pas de temps de simulation
            // j: pour remonter dans l'historique et déterminer les feuilles candidates à la sénescence
            if ((AGE() - AGES.at(AGES.size()+j)) > inidleaf) 
            {
                deathleaf = dLEAFS.at(dLEAFS.size()+j);
                indlastsen= indlastsen + 1;
            }
            death=deathleaf;


            // Calcul en fonction du stade physiologique de la plante

            int jour = vle::utils::DateTime::dayOfYear(time+begin_time); // A verifier!!!

            if  (AGE() < finrealoc) 
            {
                if  ( jour < debsimul)
                    LEAF = 0.;
                else if (jour == debsimul)
                    LEAF = seedpc * reconverMS * leafalocinit;
                else
                    LEAF = max(0.,LEAF(-1) + dLEAF() -deathleaf  + seedpc * 0.6 * leafalocinit *  ((AGE()-AGE(-1))/finrealoc) );  
            }
            else 
            {
                if ( AGE() > finrealoc && AGE(-1) < finrealoc) 
                    LEAF =  max(0.,LEAF(-1) + dLEAF() -deathleaf  + seedpc * 0.6 * leafalocinit *  ((finrealoc -AGE(-1))/finrealoc) );                      
                else 
                    LEAF = max(0.,LEAF(-1) + dLEAF()  -deathleaf );  
            }


            // variable d'etat STEM (g m-²)quantité de matière sèche des tiges par unité de surface de sol
            // Calcul en fonction du stade physiologique de la plante
            if  (AGE() < finrealoc) 
            {
                if  ( jour < debsimul)
                    STEM = 0.;
                else if (jour == debsimul)
                    STEM = seedpc * reconverMS * stemalocinit;
                else
                    STEM = max(0.,STEM(-1) + dSTEM  + (seedpc * 0.6 * stemalocinit) * ( (AGE()-AGE(-1)) / finrealoc)); 
            } 
            else 
            {
                if ( AGE() > finrealoc && AGE(-1) < finrealoc) 
                    STEM = max(0.,STEM(-1) + dSTEM  + (seedpc * 0.6 * stemalocinit) * ( (finrealoc-AGE(-1)) / finrealoc));
                else
                    STEM= max(0., STEM(-1) + dSTEM ) ; 
            }


            // variable d'etat ROOT (g m-²)quantité de matière sèche des racines par unité de surface de sol
            if  (AGE() < finrealoc) 
            {
                if  ( jour < debsimul)
                    ROOT = 0.;
                else if (jour == debsimul)
                    ROOT = seedpc * reconverMS * rootalocinit;
                else
                    ROOT = max(0.,ROOT(-1) + dROOT  + seedpc * 0.6 * rootalocinit * ( (AGE()-AGE(-1)) / finrealoc));  
             } 
             else 
                ROOT= max(0., ROOT(-1) + dROOT ) ;
                

            // variable d'etat TUBER (g m-²)quantité de matière sèche des tubercules par unité de surface de sol
            if  (AGE() >= finrealoc) 
            {
                if ((AGE() >= inituber) && (AGE(-1) < inituber)) 
                    TUBER = 0.5;
                else
                    TUBER = TUBER(-1) + dTUBER + 0.5 * deathleaf;
            } 
            else 
                TUBER =0.;
        }


        void initValue(const vd::Time&  time )
        {
            //Variables d'etat
            MS = 0.;
            LAI = 0.1;
            LEAF = (seedpc * reconverMS * leafalocinit);
            dLEAF = 0.;
            STEM = (seedpc * reconverMS * stemalocinit);
            ROOT = (seedpc * reconverMS * rootalocinit);
            TUBER = 0.;
            AGE = 0.;
            P_ASSIMres = 0.;
            death = 0.;
            //autres
            nbj = 0;
            indlastsen=vle::utils::DateTime::dayOfYear(time);
            bdebsimul = false;
            lastUpdateLocalMem = 0.;
        }


    private:
        //Variables d'etat
        Var MS; /**< Variable d'etat: Qauntité de matières sèches élaborées par jour */
        Var LAI; /**< Variable d'etat: Leaf area index, utile pour calculer PTRi (rayonnment intercepté)*/
        Var LEAF; /**< Variable d'etat */
        Var dLEAF; /**< Variable d'etat */
        Var STEM; /**< Variable d'etat */
        Var ROOT; /**< Variable d'etat */
        Var TUBER; /**< Variable d'etat */
        Var AGE; /**< Variable d'etat */
        Var P_ASSIMres; /**< Variable d'etat */
        // A completer
        Var death;

        Var Tmin;
        Var Tmax;
        Var Rg;
        Var SWP;
        // A completer

        //Parametres du modele lus dans le vpz
        double rowspacing; /**< Parametre: distance entre les rangs, Unite: m*/
        double plantspacing; /**< Parametre: distance entre les palntes, Unite: m*/
        double PNCGR; /**< Parametre: taux d'accroissement potentiel de la plante,utile pour le calcul de MS, Unite: g MJ-1 °C-1 j-1*/
        double K1; /**< Parametre: constante de repartition globale de Michaelis-Menten*/
        double K2; /**< Parametre: constante de repartition de Michaelis-Menten pour les feuilles*/
        double K3; /**< Parametre: constante de repartition de Michaelis-Menten pour les tiges*/
        double K4; /**< Parametre: constante de repartition de Michaelis-Menten pour les racines*/
        double K5; /**< Parametre: constante de repartition de Michaelis-Menten pour les tubercules*/
        double K6; /**< Parametre: taux d'utilisation de journalier de la matiere seche accumilee ASSIM, Unite: j+1*/
        double seedpc; /**< Parametre: quantité initiale de matiere contenu dans les palnts, Unite: g*/
        double debsimul; /**<Parametre: debut de simulation correspondant au jour julien du 50 % d'emmergence mais en age physiologique, Unite: °Cj*/
        double finrealoc; /**<Parametre: age physiologique de la fin de reallocation de la matiere contenue dans le plant, Unite: °Cj*/
        double leafalocinit; /**<Parametre: coefficient de repartition de la MS dans les feuilles à partir de la source (plant) au début de la simulation*/
        double stemalocinit; /**<Parametre: coefficient de repartition de la MS dans les tiges à partir de la source (plant) au début de la simulation*/
        double rootalocinit; /**<Parametre: coefficient de repartition de la MS dans les racines à partir de la source (plant) au début de la simulation*/
        double inidleaf; /**<Parametre: âge physiologique correspondant au début de la sénéscence des feuilles, Unité: °Cj*/
        double inituber; /**<Parametre: âge physiologique correspondant à l'initiation à la tubérisation, Unité: °Cj*/
        double reconverMS;

        // variables locales
        int indlastsen; /**<indice donnant le jour d'apparition des feuilles, dernièrement mortes. Initialisé à 0. */
        int nbj;/**<  nbj (période d'allocation des matières en âge physiologique) */
        int jdebsimul;/**<  n° du jour où on atteint dbsimul */
        bool bdebsimul;
        double begin_time;
        
        std::vector<double> AGES; //vecteur local de l'historique de AGE
        std::vector<double> dLEAFS; //vecteur local de l'historique de dLeafS
        double lastUpdateLocalMem; //pour verifier que l'on ne stock pas deux fois la même valeur en cas de perturb du modèle

        void updateAGES(const vd::Time& time)
        {
            if (lastUpdateLocalMem != vle::utils::DateTime::dayOfYear(time) )
            {
                lastUpdateLocalMem = vle::utils::DateTime::dayOfYear(time);
                AGES.push_back(AGE(-1));
                dLEAFS.push_back(dLEAF(-1));
            }
        }

    };
}
DECLARE_DYNAMICS(These::Spudgro); // balise specifique VLE

