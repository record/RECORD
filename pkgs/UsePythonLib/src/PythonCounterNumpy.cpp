/**
 * @file src/PythonCounterNumpy.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>
#include <sstream>
#include <Python.h>
//#include <dlfcn.h>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace vle::discrete_time;

namespace record {

class PythonCounterNumpy: public DiscreteTimeDyn {
public:

    PythonCounterNumpy(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events),mCtx(vu::make_context())
    {
        counter.init(this,"counter", events);
        counter.init_value(0.5);

        step = (events.exist("step")) ? vv::toDouble(events.get("step")) : 2.;
//        Py_Initialize(); //initializes the Python interpreter
//    	dlopen("/usr/lib/python2.7/dist-packages/numpy/core/multiarray.so",  RTLD_NOW | RTLD_GLOBAL);

    }

    virtual ~PythonCounterNumpy() {};

    virtual void compute(const vd::Time& /*time*/)
    {

        Py_Initialize(); //initializes the Python interpreter

        //run a simple python code from string
        PyRun_SimpleString("from time import time,ctime\n"
                           "print 'Today is',ctime(time())\n");

//    	PyImport_ImportModule("numpy");
//    	PyRun_SimpleString("import sys; sys.path.append('/usr/lib/python2.7/dist-packages/numpy')");

//    	PyRun_SimpleString("import sys;	sys.path.append('/usr/lib/pyshared/python2.7/numpy')");


//        PyRun_SimpleString("import numpy as np; import pprint;pprint.pprint(np.zeros(10))");


        //run a custom function contained in personal .py module
        double mytmp_1[] = {counter(-1)};
        std::vector<double> fnargs_1 (mytmp_1, mytmp_1 + sizeof(mytmp_1) / sizeof(double) );
        counter = runPythonFunction_double("my.Python.src", "myPythonFunctionsNumpy", "counterinc", fnargs_1);

        std::cout << "counter:" << counter() <<std::endl;

    }

private:
    Var counter;
    double step;
    vu::ContextPtr mCtx;
    
    double runPythonFunction_double (std::string moduleFolder, std::string moduleName, std::string functionName, std::vector<double> functionArguments, bool outCall = false)
    {
        double output; //define output variable
        PyObject *pName, *pModule, *pFunc, *pArgs, *pValue;
        Py_Initialize(); //initializes the Python interpreter
        vu::Package mPack(mCtx);
        mPack.select("UsePythonLib");
        std::string addModulePath = "import sys; sys.path.append('" + mPack.getSrcDir() + "/" + moduleFolder + "/" + "')";
        PyRun_SimpleString(addModulePath.c_str()); //define where the .py file is to be found
        pName = PyString_FromString(moduleName.c_str());
        pModule = PyImport_Import(pName); //import python module
        Py_DECREF(pName); //deallocation function 
        if (pModule != NULL) {
            pFunc = PyObject_GetAttrString(pModule, functionName.c_str());//get function reference
            if (pFunc && PyCallable_Check(pFunc)) {
                //prepare function arguments 
                pArgs = PyTuple_New(functionArguments.size());
                for (unsigned int i = 0; i < functionArguments.size(); i++) {
                    pValue = PyFloat_FromDouble(functionArguments[i]);
                    if (!pValue) {
                        Py_DECREF(pArgs);
                        Py_DECREF(pModule);
                        PyErr_Print();
                        std::stringstream ss;
                        ss << "Cannot convert argument n°" << i << std::endl;
                        throw vu::ModellingError(ss.str());
                    }
                    PyTuple_SetItem(pArgs, i, pValue);
                }
                pValue = PyObject_CallObject(pFunc, pArgs);//call function with arguments
                Py_DECREF(pArgs);
                if (pValue != NULL) {
                	if (outCall)
                		std::cout << "Result of call: " << PyFloat_AsDouble(pValue) << std::endl;
                    output = PyFloat_AsDouble(pValue);//get result back in C++ type
                    Py_DECREF(pValue);
                } else {
                    Py_DECREF(pFunc);
                    Py_DECREF(pModule);
                    PyErr_Print();
                    std::stringstream ss;
                    ss << "Call failed" << std::endl;
                    throw vu::ModellingError(ss.str());
                }
            } else {
                PyErr_Print();
                std::stringstream ss;
                ss << "Cannot find function " << functionName << std::endl;
                throw vu::ModellingError(ss.str());
            }
        } else {
            PyErr_Print();
            std::stringstream ss;
            ss << "Failed to load module " << moduleName << std::endl;
            throw vu::ModellingError(ss.str());
        }
        return output;
    }

};
}//namespaces
DECLARE_DYNAMICS(record::PythonCounterNumpy); // balise specifique VLE

