/**
 * @file src/PythonCounter.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/DiscreteTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>
#include <sstream>
#include <iostream>
#include <Python.h>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace vle::discrete_time;

namespace record {

class PythonCounter: public DiscreteTimeDyn {
public:

    PythonCounter(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events),mCtx(vu::make_context())
    {
        counter.init(this,"counter", events);
        pcounter.init(this,"pcounter", events);
        
        counter.init_value(0.5);
        pcounter.init_value(0.5);

        step = (events.exist("step")) ? vv::toDouble(events.get("step")) : 2.;
    }

    virtual ~PythonCounter() {};

    virtual void compute(const vd::Time& /*time*/)
    {

        //run a simple python code from string
        Py_Initialize(); //initializes the Python interpreter
        PyRun_SimpleString("from time import time,ctime\n"
                           "print 'Today is',ctime(time())\n");
        Py_Finalize(); //close interpreter

        //run a custom function contained in personal .py module
        double mytmp_1[] = {counter(-1)};
        std::vector<double> fnargs_1 (mytmp_1, mytmp_1 + sizeof(mytmp_1) / sizeof(double) );
        counter = runPythonFunction_double("my.Python.src", "myPythonFunctions", "counterinc", fnargs_1);

        //run another custom function contained in personal .py module
        double mytmp_2[] = {pcounter(-1), step};
        std::vector<double> fnargs_2 (mytmp_2, mytmp_2 + sizeof(mytmp_2) / sizeof(double) );
        pcounter = runPythonFunction_double("my.Python.src", "myPythonFunctions", "counterincp", fnargs_2);
        
        std::cout << "counter:" << counter() << "\tpcounter:" << pcounter()<<std::endl;
    }

private:
    Var counter;
    Var pcounter;
    double step;
    vu::ContextPtr mCtx;
    
    double runPythonFunction_double (std::string moduleFolder, std::string moduleName, std::string functionName, std::vector<double> functionArguments, bool outCall = false)
    {
        double output; //define output variable
        PyObject *pName, *pModule, *pFunc, *pArgs, *pValue;
        Py_Initialize(); //initializes the Python interpreter
        vu::Package mPack(mCtx);
        mPack.select("UsePythonLib");
        std::string addModulePath = "import sys; sys.path.append('" + mPack.getSrcDir() + "/" + moduleFolder + "/" + "')";
        PyRun_SimpleString(addModulePath.c_str()); //define where the .py file is to be found
        pName = PyString_FromString(moduleName.c_str());
        pModule = PyImport_Import(pName); //import python module
        Py_DECREF(pName); //deallocation function 
        if (pModule != NULL) {
            pFunc = PyObject_GetAttrString(pModule, functionName.c_str());//get function reference
            if (pFunc && PyCallable_Check(pFunc)) {
                //prepare function arguments 
                pArgs = PyTuple_New(functionArguments.size());
                for (unsigned int i = 0; i < functionArguments.size(); i++) {
                    pValue = PyFloat_FromDouble(functionArguments[i]);
                    if (!pValue) {
                        Py_DECREF(pArgs);
                        Py_DECREF(pModule);
                        PyErr_Print();
                        std::stringstream ss;
                        ss << "Cannot convert argument n°" << i << std::endl;
                        throw vu::ModellingError(ss.str());
                    }
                    PyTuple_SetItem(pArgs, i, pValue);
                }
                pValue = PyObject_CallObject(pFunc, pArgs);//call function with arguments
                Py_DECREF(pArgs);
                if (pValue != NULL) {
                	if (outCall)
                		std::cout << "Result of call: " << PyFloat_AsDouble(pValue) << std::endl;
                    output = PyFloat_AsDouble(pValue);//get result back in C++ type
                    Py_DECREF(pValue);
                } else {
                    Py_DECREF(pFunc);
                    Py_DECREF(pModule);
                    PyErr_Print();
                    std::stringstream ss;
                    ss << "Call failed" << std::endl;
                    throw vu::ModellingError(ss.str());
                }
            } else {
                PyErr_Print();
                std::stringstream ss;
                ss << "Cannot find function " << functionName << std::endl;
                throw vu::ModellingError(ss.str());
            }
        } else {
            PyErr_Print();
            std::stringstream ss;
            ss << "Failed to load module " << moduleName << std::endl;
            throw vu::ModellingError(ss.str());
        }
        Py_Finalize(); //close python interpreter
        return output;
    }

};
}//namespaces
DECLARE_DYNAMICS(record::PythonCounter); // balise specifique VLE

