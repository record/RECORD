/*
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems.
 * http://www.vle-project.org
 *
 * Copyright (c) 2003-2014 Gauthier Quesnel <quesnel@users.sourceforge.net>
 * Copyright (c) 2003-2014 ULCO http://www.univ-littoral.fr
 * Copyright (c) 2007-2014 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * @@tagdynamic@@
 * @@tagdepends: record.eigen, ext.Eigen @@endtagdepends
 */


#include <iostream>
#include <ostream>

#include <vle/value/Value.hpp>
#include <vle/value/Map.hpp>
#include <vle/value/Set.hpp>
#include <vle/utils/Rand.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Context.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/devs/Dynamics.hpp>
#include <vle/manager/Manager.hpp>

#undef NDEBUG
#undef EIGEN_NO_DEBUG
#include <Eigen/Eigen>
#include <record/eigen/Stats.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
namespace vm = vle::manager;

namespace record {

template <class EIGEN_OBJ>
std::string toString(const EIGEN_OBJ& vect)
{
    std::stringstream ss;
    ss << "\n--\n";
    ss << vect;
    ss << "\n--\n";
    return ss.str();
}


class EnKF : public vd::Dynamics
{
public:

    struct VarToAssimilate
    {
        VarToAssimilate():
            name(), forcing(), output()
        {
        }

        VarToAssimilate(const VarToAssimilate& v):
            name(v.name), forcing(v.forcing), output(v.output)
        {
        }
        std::string name;
        std::string forcing;
        std::string output;
    };

    //embedded model
    std::unique_ptr<vle::vpz::Vpz> m_model;
    //total time of simulation
    double m_total_duration;
    //simulation manager
    std::unique_ptr<vm::Manager> m_simulator;
    //state variables to assimilate
    std::vector<VarToAssimilate> m_vars;
    //id of observed variable
    std::string m_varObs;

    //number of particles
    unsigned int m_N;
    //number of variables to assimilate
    unsigned int m_Nv;
    //number of variables observed (such as H == 1)
    unsigned int m_Nvo;
    // H: Vector Nv, observation operator filled with either 1 or 0. Nvo
    //    is defined as the nb of element in H that equals 1
    Eigen::VectorXd m_H;
    //error observation matrix (of size Nvo * Nvo)
    Eigen::MatrixXd m_R;
    //covairance matrix (of size Nv * Nv)
    Eigen::MatrixXd m_Be;
    //matrix Nv * N (state of all particles)
    Eigen::MatrixXd m_Mb;
    //Kalman gain (Nvo * Nvo)
    Eigen::MatrixXd m_K;
    //inovation vector of size N
    Eigen::VectorXd m_iv;
    //initalization map for the simulator
    std::unique_ptr<vv::Map> m_initSimulator;
    std::unique_ptr<vv::Map> m_simRes;
    std::unique_ptr<vv::Map> m_simWithoutAssimilation;
    std::unique_ptr<vv::Matrix> m_observations;

    /**
     * @brief EnKF constructor
     * @note: events should contain:
     *
     * - vpz
     *
     * - package
     *
     * - manager_config: config of the manager (parallel_option, etc..)
     *
     *
     * - nb_particles : the number of particles to simulate
     *
     * - bounds: a matrix of nb inputs columns and 4 lines (name, def, min, max)
     *        eg.: CONFIG_Sol.Hcc - CONFIG_Sol.Hpf
     *             21.5           - 10
     *             15.05          - 7
     *             27.95          - 13
     *
     * - variables: variables to assimilate, matrix with Nv columns (number of
     *        variables to assimilate) and 3 lines (id, forcing, output)
     *        eg.: (id)      - "LAI"
     *             (forcing) - "forcing_values.forcing_LAI"
     *             (output)  - "viewLAI/sunflo,sunflo_bio:croissance_plante.LAI"
     *
     * - outputs : variables that are not assimilated but that
     *        are output anyway (optionnal)
     *        eg.: (id)          - "Yield"
     *             (integration) - "last"
     *             (path)        - "viewStatic/sunflo:production.Yield"
     *
     * - R: observation error matrix Nvo * Nvo
     *
     * - observations: matrix with 2 columns. First colunm is the time of
     *          observations (in julianDay) and second column refers to the
     *          the id of a var to assimilate.
     *          eg.:  time    -  Y
     *                2456718 - 5.2
     *                2456800 - 10.3
     */

    EnKF(const vd::DynamicsInit& init, const vd::InitEventList& evts)
    : vd::Dynamics(init, evts), m_model(), m_total_duration(0),
      m_simulator(nullptr), m_vars(), m_varObs(), m_N(0), m_Nv(0),
      m_Nvo(0), m_H(), m_R(), m_Be(), m_Mb(), m_K(), m_iv(),
      m_initSimulator(new vv::Map()), m_simRes(nullptr),
      m_simWithoutAssimilation(nullptr), m_observations()
    {

        //initialize embedded model
        std::string vpzFile = "";
        if (evts.exist("package")) {
            vle::utils::Package pkg(context(), evts.getString("package"));
            vpzFile = pkg.getExpFile(evts.getString("vpz"),
                    vle::utils::PKG_BINARY);
        } else {
            vpzFile = evts.getString("vpz");
        }
        m_model.reset(new vle::vpz::Vpz(vpzFile));

        //init m_manager
        m_simulator.reset(new vm::Manager(context(),
                evts.getMap("manager_config")));
        m_initSimulator->addString("vpz", vpzFile);

        //initalize particles
        m_N = evts.getInt("nb_particles");
        const vv::Matrix& bounds = evts.getMatrix("bounds");
        vu::Rand& mrand = m_simulator->random_number_generator();
        for (unsigned int i =0; i<bounds.columns();i++){
            std::string input_name = "input_";
            input_name += bounds.getString(i, 0);
            double min = bounds.getDouble(i, 2);
            double max = bounds.getDouble(i, 3);
            vv::Tuple& initI = m_initSimulator->addTuple(input_name, m_N, -999);
            for (unsigned int p=0; p<m_N; p++) {
                initI[p] = mrand.getDouble(min ,max);
            }
        }

        //initialize variables
        const vv::Matrix& variables = evts.getMatrix("variables");
        for (unsigned int v =0; v<variables.columns();v++){
            VarToAssimilate va;
            va.name = variables.getString(v, 0);
            va.forcing = variables.getString(v, 1);
            va.output = variables.getString(v, 2);
            m_vars.push_back(va);
        }

        if (evts.exist("outputs") and evts.get("outputs")->isMatrix()) {
            const vv::Matrix& outputs = evts.getMatrix("outputs");
            std::string key;
            for (unsigned int v =0; v<outputs.columns();v++){
                key = "output_";
                key += outputs.getString(v, 0);
                std::unique_ptr<vv::Map> outputv(new  vv::Map());
                outputv->addString("integration", outputs.getString(v, 1));
                outputv->addString("path", outputs.getString(v, 2));
                m_initSimulator->add(key, std::move(outputv));
            }
        }


        //init R
        if (evts.get("R")->isMatrix()) {
            m_R = record::eigen::Stats::toEigenMat(evts.getMatrix("R"));
        } else {
            m_R.resize(1,1);
            m_R << evts.getDouble("R");
        }
        //initialize observations
        m_observations.reset(new vv::Matrix(evts.getMatrix("observations")));

        if (m_observations->columns() != 2) {
            throw vle::utils::ArgError(" Error EnKF in observations");
        }
        {//check observations
            double  obs_time = -999;
            for (unsigned int o=1; o<m_observations->rows();o++) {
                if (obs_time > m_observations->getDouble(0,o)) {
                    throw vu::ArgError(" Error EnKF in observations "
                            "not increasing");
                }
                obs_time = m_observations->getDouble(0,o);
            }
        }
        //get the name of the observation
        m_varObs = m_observations->getString(1,0);

        //initialize total simulation duration
        m_total_duration = m_model->project().experiment().duration();
    }

    virtual ~EnKF()
    {
    }

    virtual vd::Time init(vd::Time /*time*/) override
    {
        //ENKF initializations
        //initialize Nv, H, Nvo
        m_Nv = m_vars.size();
        m_H.resize(m_Nv);
        for (unsigned int v=0; v<m_Nv; v++) {
            if (m_vars[v].name == m_varObs) {
                m_H[v] = 1.0;
            } else {
                m_H[v] = 0;
            }
        }
        m_Nvo = (m_H.array() >= 1.0).count();

        //some checks
        if (m_Nvo != 1) {
            throw vle::utils::ArgError(
                    "Error EnKF cannot handle multiple obs");
        }
        if (m_R.rows() != m_R.cols() or m_R.rows() != m_Nvo) {
            throw vle::utils::ArgError(vu::format(
                    " Error EnKF R.rows[%lu] != Nvo[%u]", m_R.rows(), m_Nvo));
        }

        //initialize forcing events (to NULL) and output declaration to
        //the simulator and check obs in increasing times
        for (unsigned int v=0; v<m_Nv; v++) {
            const VarToAssimilate& varV = m_vars[v];
            std::unique_ptr<vv::Set> forcingEventsInOne(new vv::Set());
            for (unsigned int p=0; p<m_N;p++) {
                vv::Set& forcingEvents = forcingEventsInOne->addSet();
                for (unsigned int o=1; o<m_observations->rows();o++) {
                    vv::Map& forcingEv = forcingEvents.addMap();
                    forcingEv.addDouble("time", -999);
                    forcingEv.addDouble("value", 0);
                    forcingEv.addBoolean("before_output", true);
                }
            }

            std::string key = "input_";
            key += varV.forcing;
            m_initSimulator->add(key, std::move(forcingEventsInOne));
            key = "output_";
            key += varV.name;
            std::unique_ptr<vv::Map> outputv(new  vv::Map());
            outputv->addString("path",varV.output);
            outputv->addString("integration","all");
            m_initSimulator->add(key, std::move(outputv));
        }
        return 0;


    }

    virtual void output(vd::Time /*time*/,
            vd::ExternalEventList& /*output*/) const override
    {
    }

    virtual vd::Time timeAdvance() const override
    {
        return vd::infinity;
    }

    virtual void internalTransition(vd::Time /*time*/) override
    {
        context()->notice("[EnKF] simulation without assimilation\n");
        //simulate without assimilation
        m_initSimulator->addDouble("propagate_simulation_engine.duration",
                m_total_duration);
        m_simRes.reset(nullptr);
        vle::manager::Error err;
        m_simWithoutAssimilation = m_simulator->runPlan(*m_initSimulator, err);


        //reset integration to last
        for (unsigned int v=0; v<m_Nv; v++) {
            const VarToAssimilate& varV = m_vars[v];
            std::string key = "output_";
            key += varV.name;
            vv::Map& outputv = m_initSimulator->getMap(key);
            outputv.get("integration")->toString().set("last");
        }

        //Begin assimilation
        m_Be.resize(m_Nv,m_Nv);
        m_Mb.resize(m_Nv,m_N);
        m_iv.resize(m_N);

        context()->notice("[EnKF] start assimilation\n");
        for (unsigned int o=1; o< m_observations->rows(); o++) {
            //set duration to next and simulate
            double obs_time = m_observations->getDouble(0,o);
            m_initSimulator->get("propagate_simulation_engine.duration")
                                                    ->toDouble().set(obs_time);
            context()->notice("[EnKF] relative observation time: %f\n",
                    obs_time);
            //run simulation
            m_simRes.reset(nullptr);
            m_simRes = m_simulator->runPlan(*m_initSimulator, err);
            if (err.code != 0) {
                throw vle::utils::ArgError(vu::format(" Error EnKF during "
                        "simulation: %s", err.message.c_str()));
            }

            //get simulated state (background state)
            for (unsigned int v=0; v<m_Nv; v++) {
                const vv::Table& tble =
                        m_simRes->getTable(m_vars[v].name);
                for (unsigned int p =0; p<m_N;p++) {
                    m_Mb(v,p) = tble.get(p,0);
                }
            }

            //compute Be
            Eigen::MatrixXd M_bt = m_Mb.transpose();
            m_Be = record::eigen::Stats::covariance(M_bt);

            if (context()->get_log_priority() >= 5) {
                context()->notice("[EnKF] observation %i/%i\n",
                        (int) o, (int) m_observations->rows());
                context()->notice("[EnKF] cov matrix Be:");
                for (const VarToAssimilate& v : m_vars) {
                    context()->notice("%s,",v.name.c_str());
                }
                context()->notice("%s", toString(m_Be).c_str());
            }

            //compute Kalman gain (Ht should be H and H, Ht)
            MatrixXd K = m_Be*m_H*(m_H.transpose()*m_Be*m_H + m_R).inverse();

            context()->notice("[EnKF] K %s", toString(K).c_str());

            //update each member
            for (unsigned int p =0; p<m_N;p++) {
                //compute innovation vector
                double y = m_observations->getDouble(1,o);
                m_iv[p] = y - (m_H.transpose() * m_Mb.col(p));

                //compute analysis state
                m_Mb.col(p) = m_Mb.col(p) + K * m_iv[p];

                //Set analysis state
                for (unsigned int v=0; v<m_Nv; v++) {
                    std::string key = "input_";
                    key += m_vars[v].forcing;
                    vv::Map& forcingEv =
                            m_initSimulator->getSet(key).getSet(p).getMap(o-1);
                    forcingEv.get("time")->toDouble().set(obs_time);
                    forcingEv.get("value")->toDouble().set(m_Mb(v,p));
                }
            }
            context()->notice("[EnKF] mean iv: %s",
                    toString(m_iv.mean()).c_str());

        }

        //reset integration to last
        for (unsigned int v=0; v<m_Nv; v++) {
            std::string key = "output_";
            key += m_vars[v].name;
            vv::Map& outputv = m_initSimulator->getMap(key);
            outputv.get("integration")->toString().set("all");
        }

        //simulate
        m_initSimulator->get("propagate_simulation_engine.duration")
                                                ->toDouble().set(m_total_duration);
        m_simRes.reset(nullptr);
        m_simRes = m_simulator->runPlan(*m_initSimulator, err);
    }

    virtual void externalTransition(const vd::ExternalEventList& /*event*/,
            vd::Time /*time*/) override
    {
    }

    virtual void confluentTransitions(vd::Time time,
            const vd::ExternalEventList& events) override
    {
        internalTransition(time);
        externalTransition(events, time);
    }

    virtual std::unique_ptr<vv::Value> observation(
            const vd::ObservationEvent& event) const override
    {
        if (event.onPort("sim_with_assimilation")) {
            return m_simRes->clone();
        }
        if (event.onPort("sim_without_assimilation")) {
            return m_simWithoutAssimilation->clone();
        }
        if (event.onPort("all_forcing_events")) {
            std::unique_ptr<vv::Map>  res(new vv::Map());
            for (unsigned int i=0; i<m_vars.size(); i++) {
                const VarToAssimilate& v = m_vars[i];
                std::string key = "input_";
                key += v.forcing;
                res->add(v.name, m_initSimulator->getSet(key).clone());
            }
            return std::move(res);
        }
        if (event.onPort("all_members")) {
            std::unique_ptr<vv::Map> res(new vv::Map());
            std::string in_cond;
            std::string in_port;
            vv::Map::const_iterator itb = m_initSimulator->begin();
            vv::Map::const_iterator ite = m_initSimulator->end();
            for (; itb != ite; itb++) {
                const std::string& conf = itb->first;
                if (vm::Manager::parseInput(conf, in_cond, in_port)) {
                    std::string input = in_cond;
                    input += ".";
                    input += in_port;
                    res->add(input, itb->second->clone());
                }
            }
            return std::move(res);
        }

        //to get forcing events, not very usefull
        std::string prefix = "forcing_";
        if (not event.getPortName().compare(0, prefix.size(), prefix)) {
            std::string varname = event.getPortName().substr(prefix.size(),
                    event.getPortName().size());
            for (unsigned int i=0; i<m_vars.size(); i++) {
                const VarToAssimilate& v = m_vars[i];
                if (v.name == varname) {
                    std::string key = "input_";
                    key += v.forcing;
                    return m_initSimulator->getSet(key).clone();
                }
            }
        }

        return nullptr;
    }

    virtual void finish()
    {
    }




};

} // namespace

DECLARE_DYNAMICS(record::EnKF)
