
###
## TODO
###
rvleExp.enkf = function(rvleHandle=NULL, file_expe=NULL, varsToAssimilate=NULL, 
                        observations=NULL, R=NULL, NbSims=NULL)
{
  
  #read bounds
  bounds = rvleExp.parseExpe(file_expe, rvleHandle, typeReturn='bounds');
  
  #add additional inputs and outputs
  for (v in names(varsToAssimilate)){
    #add outputputs for forcing variables
    rvlePlan.output(rvleHandle, id=v, path=varsToAssimilate[[v]]$path,
                    integration="all");
    #add inputs for forcing variables
    rvlePlan.define(rvleHandle, varsToAssimilate[[v]]$cond, 
                    varsToAssimilate[[v]]$port)
    rvlePlan.propagate(rvleHandle, varsToAssimilate[[v]]$cond, 
                       varsToAssimilate[[v]]$port, NULL)
  }
  
  #save the embedded model
  file_emb =tempfile(pattern="vleEnKF_", fileext=".vpz"); 
  rvle.save(rvlePlan.embedded(mod), file_emb);
  
  #open the EnKF embedding model
  enkf_mod = rvle.open(file="EnKF.vpz", pkg="EnKF");

  #set var_Y settings
  for (v in names(varsToAssimilate)) {
    varPort = paste("var", sep="_", v);
    rvle.addPort(enkf_mod, "enkf", varPort);
    config = NULL;
    config$forcing =  paste(sep=".",varsToAssimilate[[v]]$cond, 
                            varsToAssimilate[[v]]$port);
    config$output = varsToAssimilate[[v]]$path;
    class(config) <- "VleMAP";
    rvle.setValueCondition(enkf_mod, cond="enkf", port=varPort, config);
  }
  
  #set input_X settings
  for (u in names(bounds)){
    inputu = paste(sep="", "input_",u)
    rvle.addPort(enkf_mod, "enkf", inputu);
    rvle.setValueCondition(enkf_mod, cond="enkf",  port=inputu, 
                           runif(NbSims, min=bounds["min", u], 
                                 max=bounds["max", u]));  
  }
  
  #add other settings for EnKF
  rvle.setValueCondition(enkf_mod, cond="enkf", port="vpz", file_emb);
  rvle.addPort(enkf_mod, "enkf", "observations");
  rvle.setValueCondition(enkf_mod, cond="enkf", port="observations", observations);
  rvle.addPort(enkf_mod, "enkf", "R");
  rvle.setValueCondition(enkf_mod, cond="enkf", port="R", R);

  return(rvle.run(enkf_mod));
}

#'
#' Explore the results of a data assimilation experiment
#' 
#' @export
#' 
#' @param res, a structure resulting from rvleExp.enkf
#' @param what, either
#'  - 'begin_time': begin time of simulation for all members
#'  - 'var_list': the list of variables assimilated
#'  - 'input_list': the list of uncertain inputs (TODO)
#'  - 'number_of_members' : nb of members
#'  - 'input_values': the input values of uncertain input for a given member 
#'                    (or all members if member is NULL)
#'  - 'forcing_events': get forcing events for given variable (can be NULL) 
#'                      and member (can be NULL)
#'  - 'without_assimil': simulation results without assimilation
#'  - 'with_assimil': simulation results with assimilation
#' @param var, identification of an assimilated variable (maybe NULL)
#' @param member, identification of the member (maybe NULL)
#' @param input, identification of an uncertain input (maybe NULL)
#' @param relativeDate, if true get results with the relativeDate
#' @param reformat , if true (pretty) reformating is performed on results
#' 
#' @return a structure containing specified data.
#' 
#' @author Ronan Trépos MIA-T, INRA
#' 
#' @note
#' 
#' Nothing
#' 
#' @examples
#' 
#' #TODO
#' 

rvleExp.exploreEnKF = function(res, what, var=NULL, member=NULL, input=NULL, 
        relativeDate=TRUE, reformat=FALSE)
{
  resReq = NULL;
  if (what == "forcing_events") {
    resReq = res$"top:EnKF.all_forcing_events"[[1]];
    if (! is.null(var)) {
      resReq=resReq[[var]];
      if (!is.null(member)) {
        resReq=resReq[[member]];
      }
    } else if (! is.null(member)) {
      resReq = lapply(1:length(resReq), function(varI) {
        varIm = lapply(member, function(m){
          resReq[[varI]][[m]]
        });
      });
      names(resReq) = names(res$"top:EnKF.all_forcing_events"[[1]]);
    }
  } else if (what =="with_assimil" ){
    resReq = res$"top:EnKF.sim_with_assimilation"[[1]];
    if (!is.null(var)) {
      resReq = resReq[[var]];
      if (!is.null(member)) {
        resReq = resReq[,member]
      }
    } else {
      if (!is.null(member)) {
        resReq = lapply(resReq, function(x){x[,member]})
      }
    }
  } else if (what =="without_assimil" ){
    resReq = res$"top:EnKF.sim_without_assimilation"[[1]]; 
    if (!is.null(var)) {
      resReq = resReq[[var]];
      if (!is.null(member)) {
        resReq = resReq[,member]
      }
    } 
  } else if (what =="var_list" ){
    resReq = names(res$"top:EnKF.all_forcing_events"[[1]]);
  } else if (what =="input_list" ){
    nbMembers = rvleExp.exploreEnKF(res, what="number_of_members");
    resReq = NULL;
    namesCond = names(res$"top:EnKF.all_members"[[1]]);
    for (condPort in 1:length(res$"top:EnKF.all_members"[[1]])){
      if (length(res$"top:EnKF.all_members"[[1]][[condPort]]) == nbMembers) {
        #if inputs length is not nbMembers , it is removed
        if (is.numeric(res$"top:EnKF.all_members"[[1]][[condPort]][[1]])) {
          #if inputs has not numreic values (eg forcing_events), it is removed
          resReq = c(resReq,namesCond[condPort]);
        }
      }
    }
  } else if (what == "number_of_members") {
    nbCondPort = length(res$"top:EnKF.all_members"[[1]])
    resReq = max(unlist(lapply(1:nbCondPort, function(c){
      return(length(res$"top:EnKF.all_members"[[1]][[c]]));
    })))
  } else if (what == "input_values") {
    resReq = res$"top:EnKF.all_members"[[1]];
    if (!is.null(input)){
      resReq = resReq[[input]];
      if (!is.null(member)) {
        resReq = resReq[member];
      }
    } else if (!is.null(member)){
      resReq2 = NULL; 
      for (i in 1:length(resReq)){
        resReq2[[names(resReq)[i]]] = as.list(resReq[[names(resReq)[i]]][[member]]);
      }
      resReq = resReq2;
    }
  } else if(what == "begin_time") {
    resReq = res$"top:EnKF.begin_time";
  }
  
  ##reformat
  if (reformat) {
    if (what == "forcing_events") {
      if (! is.null(var)) {
        if (!is.null(member)) {
          vecTime = unlist(lapply(resReq[[1]], function(t){t$time}));
          resReq = lapply(resReq, function(member){
            unlist(lapply(1:length(vecTime),
                          function(t){member[[t]]$value}))});
          resReq = as.data.frame(matrix(unlist(resReq),
                                        ncol=length(resReq)));
          resReq$time = vecTime;
        } else {
          resReq = matrix(unlist(resReq), nrow=length(resReq), byrow=T);
          colnames(resReq) = c("before_output", "time", var);
          resReq = as.data.frame(resReq);
          resReq$before_output = NULL;
        }
        if (relativeDate) {
          begin_time = res$"top:EnKF.begin_time";
          resReq$time = resReq$time - begin_time;
        }
      }
    }
  }
  return (resReq);
}
