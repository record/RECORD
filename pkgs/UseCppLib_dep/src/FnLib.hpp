
#ifndef FnLib_H
#define FnLib_H
namespace FnLib {
    double getLAI(const double Lai, const double RUElai, const double RG);

    double getLAIref(const double& Lai, const double& RUElai, const double& RG);

    double getLAIpoin(const double Lai, const double* RUElai, const double* RG);

    void updateLAIref(double& Lai, const double& RUElai, const double& RG);
}
#endif
