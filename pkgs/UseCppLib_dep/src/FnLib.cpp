#include "FnLib.hpp"

namespace FnLib {

    double getLAI(const double Lai, const double RUElai, const double RG) {
        return Lai + RG * RUElai;
    }

    double getLAIref(const double& Lai, const double& RUElai, const double& RG) {
        return Lai + RG * RUElai;
    }

    double getLAIpoin(const double Lai, const double* RUElai, const double* RG) {
        return Lai + *RG * *RUElai;
    }

    void updateLAIref(double& Lai, const double& RUElai, const double& RG) {
        Lai = Lai + RG * RUElai;
    }

}
