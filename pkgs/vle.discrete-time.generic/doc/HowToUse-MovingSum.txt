= Description =

Le modele MovingSum du paquet vle.discrete-time.generic est un modele qui calcul des sommes
glissante sur une fenetre de n pas de temps.
Le parametre n est configurable.


= Port d'entree/sortie =

 - entree : X, nombre et noms libres au choix de l'utilisateur
 - sortie : MovingSum_X, nombre et noms correspondant aux ports d'entree

= Parametres =

 - n : nombre de pas de temps a prendre en consideration dans la fenetre

= Details =

Ce modele utilise l'extension vle::discrete_time::DiscreteTimeDyn.
Le parametre n permet de fixer la taille de la fenetre temporelle a
prendre en consideration dans le calcul de la Somme. Si ce parametre
n'est pas renseigne dans le vpz il prend par defaut une valeur de 1.
A l'initialisation n'ayant pas l'historique necessaire pour calculer
les moyennes, le modele prends comme hypothese que tous les jours
precedant le debut de simulation ont des valeurs de 0.

= Exemple =

voir dans le paquet vle.discrete-time.generic_test exp/MovingSum.vpz

= Source =

voir src/MovingSum.cpp
