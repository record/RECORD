
# Package vle.discrete-time.generic for vle-2.0.0

<img src="../../../formation/figures/record_logo.png" alt="Record Team" style="width: 200px;"/>

The [**vle.discrete-time.generic**](http://recordb.toulouse.inra.fr/distributions/2.0/vle.discrete-time.generic.tar.bz2) package provide several generic models (computing simple math functions) using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) and their corresponding configuration in order to facilitate their usage.  
The global description of output trajectories of these models is based on a function of intput trajectories.
We can thus distinguish 4 categories of models regarding their input/output handling behaviour :  

* *Generative models* : do not require any input to generate output data.
* *Aggregate Variable models* : functions aggregating several input variables values for each time step.
* *Aggregate Times models* : functions aggregating a single input variable values for different time steps.
* *Identity models* : functions based on a single input value.

List of available models :  

* [**Average**](#Average) : Compute for each time-step the average value of all received inputs for this time-step. (*Aggregate Variable model*)
* [**Constant**](#Constant) : Provide a constant value for each time-step of the simulation. (*Generative model*)
* [**Identity**](#Identity) : Provide for each time-step an output using the value of the corresponding input. (*Identity model*)
* [**MovingAverage**](#MovingAverage) : Compute a moving average (using a time window) of each input it receives. (*Aggregate Times model*)
* [**MovingSum**](#MovingSum) : Compute a moving sum (using a time window) of each input it receives. (*Aggregate Times model*)
* [**Product**](#Product) : Compute for each time-step the product value of all received inputs for this time-step. (*Aggregate Variable model*)
* [**StepDates**](#StepDates) : A generic step function or piecewise linear function based on time and dates. (*Generative model*)
* [**StepFun**](#StepFun) : A generic step function or piecewise linear function based on an input value. (*Identity model*)
* [**Sum**](#Sum) : Compute for each time-step the sum value of all received inputs for this time-step. (*Aggregate Variable model*)
* [**WeightedProduct**](#WeightedProduct) : Compute for each time-step the weighted product value of all received inputs for this time-step. (*Aggregate Variable model*)
* [**WeightedSum**](#WeightedSum) : Compute for each time-step the weighted sum value of all received inputs for this time-step. (*Aggregate Variable model*)
* [**Interpolate**](#Interpolate) : A generic interpolation function based on time and value pairs. (*Generative model*)

Tests and sample simulators can be found in the [**vle.discrete-time.generic_test**](http://recordb.toulouse.inra.fr/distributions/2.0/vle.discrete-time.generic_test.tar.bz2) companion package 

---

## Package dependencies

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)

--- 




## Atomic model Average <a name="Average"></a>

*Aggregate Variable model*  

The **Average** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute the average value of all inputs received for each time-step.  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Average_panel'>Show/Hide Model Details</button>
<div id="Average_panel" class="collapse">

### Configuring an Average Model

#### Dynamics settings

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Average

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

No model-specific parameters are available.

#### Input settings

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with no dynamic detection at run-time.

#### Output settings

A single output port **Mean**.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/Average** is available and define no inputs.

#### Details

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :-------: | :-------: | :-------: | :-------: |
| $\frac{1}{m}\sum_{j=1}^m v_1^j$ | $\frac{1}{m}\sum_{j=1}^m v_2^j$ | $\ldots$ | $\frac{1}{m}\sum_{j=1}^m v_z^j$ |


</div>

--- 





## Atomic model Constant <a name="Constant"></a>

*Generative model*

The **Constant** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) provide a constant value for each time-step of the simulation..  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Constant_panel'>Show/Hide Model Details</button>
<div id="Constant_panel" class="collapse">

### Configuring a Constant Model

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Constant

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

* $init\_value\_XXs$ (double) : for any user defined output port XXs, the initial value to use as constant value (default 0.).

#### Input settings 

No input is required.  
The value provided by this model can be a never changing initial value, but it can also be changed during the simulation with an input event (using an input port corresponding to the output port). One can use an input port corresponding to the user defined output port in order to change the constant value during the simulation.

#### Output settings 

Any user defined output port detected is used.  
All output ports are detected in the model constructor with no dynamic detection at run-time.

#### Observation settings

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/Constant** is available and define a single output *a* with initial value of *1*


#### Details

Input trajectory :  

None  

Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $c$ | $c$ | $\ldots$ | $c$ |


The Constant model can be used to develop a complex model in an incremental way.  
Consider for example the development a model DiscreteTime DT that depends on a external variable, that is not yet available.  
The constant model C provides, during the implementation time period, the inputs necessary to the dynamic of DT .

</div>

--- 



## Atomic model Identity <a name="Identity"></a>

*Identity model*

The **Identity** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) provide for each time-step an output using the value of the corresponding input.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Identity_panel'>Show/Hide Model Details</button>
<div id="Identity_panel" class="collapse">

### Configuring an Identity Model

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Identity

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

No model-specific parameters are available.

#### Input settings 

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with no dynamic detection at run-time.

#### Output settings 

Same as inputs.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/Identity** is available and define a single output *a*.

### Details 

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1$ | $v_2$ | $\ldots$ | $v_z$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1$ | $v_2$ | $\ldots$ | $v_z$ |


The Identity model can be used to observe state variables of models built by an executive.   
For example, consider an executive model that instantiates, at time $t_1$, a model M and replaces, at $t_2$ , M by another model M' providing the same outputs ; outputs of both M and M' are coupled to inputs of an Identity model Id.  

>  M &rarr; Id between $t_1$ and $t_2$  
>  M' &rarr; Id after $t_2$  

The Identity model allows the observation of the state variable of both M and M', seen as the same state variable.

</div>

--- 




## Atomic model MovingAverage <a name="MovingAverage"></a>

*Aggregate Times model*

The **MovingAverage** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute a moving average (using a user defined time window) of each input it receives.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#MovingAverage_panel'>Show/Hide Model Details</button>
<div id="MovingAverage_panel" class="collapse">

### Configuring a MovingAverage Model 

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : MovingAverage

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

* $n$ (integer) : number of time-step to take into account in the average (default 1).
* $m$ (integer) : number of time-step to wait before taking into account thecurrent input value (default 0).

#### Input settings 

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with no dynamic detection at run-time.

#### Output settings 

For any XXs user-defined input port there is a corresponding output port MovingAverage_XXs.

#### Observation settings

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/MovingAverage** is available and define a single input *a* and its corresponding output MovingAverage_a, and using n=1 & m=0.

### Details 

Input trajectory :  

| $\Delta$ | $2 * \Delta$| $3 * \Delta$| $4 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: | :--: | :--: |
| $v_1$ | $v_2$ | $v_3$ | $v_4$ | $\ldots$ | $v_z$ |


Output trajectory : (for $m = 1$, $n = 3$)

| $\Delta$ | $2 * \Delta$| $3 * \Delta$| $4 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :------: | :------: | :------: | :------: | :------: | :------: |
| $0$ | $\frac{v_1}{n}$ | $\frac{v_1 + v_2}{n}$ | $\frac{v_1 + v_2 + v_3}{n}$ | $\ldots$ | $\frac{1}{n}\sum\limits_{i=z-m-n}^{z-m} v_i$ |


Use cases :  

* The MovingAverage model can be used to provide the classical moving average (with $m=0$) of the sub sequence of $v_i$ from $t-n$ to $t$ where $t$ is the current time.
* It can also simulates a delay (with $n=1$). The output value is then $v_{t-m}$ where $t$ is the current time.

__Remark :__ It can accept multiple inputs and outputs, but a single shared {n, m} parameter pair.

</div>

---




## Atomic model MovingSum <a name="MovingSum"></a>

*Aggregate Times model*

The **MovingSum** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute a moving sum (using a user defined time window) of each input it receives.

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#MovingSum_panel'>Show/Hide Model Details</button>
<div id="MovingSum_panel" class="collapse">

### Configuring a MovingSum Model 

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : MovingSum

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

* $n$ (integer) : number of time-step to take into account in the sum (default 1).

#### Input settings 

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with no dynamic detection at run-time.

#### Output settings 

For any XXs user-defined input port there is a corresponding output port MovingSum_XXs.

#### Observation settings

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/MovingSum** is available and define a single input *a* and its corresponding output MovingSum_a, and using n=1.

### Details 

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : (for $n = 3$)

| $\Delta$ | $2 * \Delta$| $3 * \Delta$| $4 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :------: | :------: | :------: | :------: | :------: | :------: |
| $v_1^1$ | $v_1^1 + v_2^1$ | $v_1^1 + v_2^1+ v_3^1$ | $v_2^1 + v_3^1 + v_4^1$ | $\ldots$ | $\sum\limits_{i=z-n}^{z} v_i^1$ |
| $v_1^2$ | $v_1^2 + v_2^2$ | $v_1^2 + v_2^2+ v_3^2$ | $v_2^2 + v_3^2 + v_4^2$ | $\ldots$ | $\sum\limits_{i=z-n}^{z} v_i^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_1^m + v_2^m$ | $v_1^m + v_2^m+ v_3^m$ | $v_2^m + v_3^m + v_4^m$ | $\ldots$ | $\sum\limits_{i=z-n}^{z} v_i^m$ |


Use cases :  

* The MovingSum model can be used to provide the classical moving sum of the sub sequence of $v_i$ from $t − n$ to $t$ where $t$ is the current time..

__Remark :__ It can accept multiple inputs and outputs, but a single shared n parameter.
</div>

---




## Atomic model Product <a name="Product"></a>

*Aggregate Variable model*  

The **Product** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute the product value of all inputs received for each time-step.  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Product_panel'>Show/Hide Model Details</button>
<div id="Product_panel" class="collapse">

### Configuring an Product Model

#### Dynamics settings

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Product

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

No model-specific parameters are available.

#### Input settings

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with possible dynamic detection at run-time.

#### Output settings

A single output port **Product**.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/Product** is available and define no inputs.

#### Details

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $\prod_{j=1}^m v_1^j$ | $\prod_{j=1}^m v_2^j$ | $\ldots$ | $\prod_{j=1}^m v_z^j$ |


</div>

--- 




## Atomic model StepDates <a name="StepDates"></a>

The **StepDates** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) is a generic step function or piecewise linear function based on time and day of the year. 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#StepDates_panel'>Show/Hide Model Details</button>
<div id="StepDates_panel" class="collapse">

### Configuring a StepDates Model 

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : StepDates

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| :--: | :----------------------: | :--: | ---------------------------------- |
| **DefaultValue** | double |[] | The default value used before the first user-defined Step threshold value (default 0) |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double |[x] | Date of simulation start |
| **Steps** | set < map < Threshold(string MM/JJ) ; Slope(double) ; Value(double) > > | [] | All user-defined steps (see details section) |

#### Input settings 

No input is required.

#### Output settings 

A single output port **out**.

#### Observation settings 

Same as outputs.  

Two additionnal observation ports are available *current_date* and *current_date_str*, providing the date of the current time-step in double or string format respectivly.

#### Available configurations 

A single configuration **vle.discrete-time.generic/StepDates** is available and use 2 steps.

### Details 

The **Steps** parameter is a collection of an arbitrary number of user-defined *Step* (corresponding to a linear function until next *Step*). (order doesn't matter, they will be re-ordered automatically and any redudant **Threshold** value will be ignored)  
Each of these user-defined parameter *Step* is a triplet {**Threshold** string (MM/JJ), **Value** double, **Slope** double (default 0)}


Input trajectory :  

None  

Output trajectory :  
for  
Steps =  
>  S1={Threshold=t1 ; Value=v1 ; Slope=s1},  
>  S2={Threshold=t2 ; Value=v2 ; Slope=s2}  

with $t1<t2$

| $time < t1$ | $t1 < time < t2$ | $time > t2$ |
| :--: | :--: |  :--: |
| $DefaultValue$ | $v1+s1*(time-t1)$ | $v2+s2*(time-t2)$ |



</div>




## Atomic model StepFun <a name="StepFun"></a>

The **StepFun** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) is a generic step function or piecewise linear function based on a single input value. 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#StepFun_panel'>Show/Hide Model Details</button>
<div id="StepFun_panel" class="collapse">

### Configuring a StepFun Model 

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : StepFun

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| :--: | :----------------------: | :--: | ---------------------------------- |
| **DefaultValue** | double |[] | The default value used before the first user-defined Step threshold value (default 0) |
| **ComputeSlope** | boolean | [] | Automatic slope computation (default false) (see details section) |
| **Steps** | set < map < Threshold(double) ; Slope(double) ; Value(double) > > | [] | All user-defined steps (see details section) |

#### Input settings 

A single input port **in**.

#### Output settings 

A single output port **out**.

#### Observation settings 

Same as outputs.  

#### Available configurations 

A single configuration **vle.discrete-time.generic/StepFun** is available and use 2 steps.

### Details 

The **Steps** parameter is a collection of an arbitrary number of user-defined *Step* (corresponding to a linear function until next *Step*). (order doesn't matter, they will be re-ordered automatically and any redudant **Threshold** value will be ignored)  
Each of these user-defined parameter *Step* is a triplet {**Threshold** double, **Value** double, **Slope** double (default 0)}

If the option **ComputeSlope** is activated then all Slopes defined in **Steps** will be ignored and replaced with an automatically computed slope producing a continuous function.  
$Slope_i = \frac{Value_{i+1} - Value_i}{Threshold_{i+1} - Threshold_i}$


Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1$ | $v_2$ | $\ldots$ | $v_z$ |

Output trajectory :  
for  
Steps =  
>  S1={Threshold=t1 ; Value=V1 ; Slope=s1},  
>  S2={Threshold=t2 ; Value=V2 ; Slope=s2}  

with $t1<t2$  
and $v_1<t1$, $t1<v_2<t2$, $t2<v_z$


| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: |  :--: |
| $DefaultValue$ | $V1+s1*(v_2-t1)$ | $V2+s2*(v_z-t2)$ |



</div>





## Atomic model Sum <a name="Sum"></a>

*Aggregate Variable model*  

The **Sum** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute the sum value of all inputs received for each time-step.  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Sum_panel'>Show/Hide Model Details</button>
<div id="Sum_panel" class="collapse">

### Configuring an Sum Model

#### Dynamics settings

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Sum

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

No model-specific parameters are available.

#### Input settings

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with possible dynamic detection at run-time.

#### Output settings

A single output port **Sum**.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/Sum** is available and define no inputs.

#### Details

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $\sum_{j=1}^m v_1^j$ | $\sum_{j=1}^m v_2^j$ | $\ldots$ | $\sum_{j=1}^m v_z^j$ |


</div>

--- 







## Atomic model WeightedProduct <a name="WeightedProduct"></a>

*Aggregate Variable model*  

The **WeightedProduct** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute the weighted product value of all inputs received for each time-step.  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#WeightedProduct_panel'>Show/Hide Model Details</button>
<div id="WeightedProduct_panel" class="collapse">

### Configuring an WeightedProduct Model

#### Dynamics settings

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : WeightedProduct

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

* $XXs$ (double) : weight for XXs input variable

#### Input settings

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with possible dynamic detection at run-time (but associated weight parameter must be already defined).

#### Output settings

A single output port **WeightedProduct**.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/WeightedProduct** is available and define no inputs.

#### Details

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :-------: | :-------: | :-------: | :-------: |
| $\prod_{j=1}^m w_j*v_1^i$ | $\prod_{j=1}^m w_j*v_2^i$ | $\ldots$ | $\prod_{j=1}^m w_j*v_z^i$ |


</div>

--- 





## Atomic model WeightedSum <a name="WeightedSum"></a>

*Aggregate Variable model*  

The **WeightedSum** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) compute the weighted sum value of all inputs received for each time-step.  

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#WeightedSum_panel'>Show/Hide Model Details</button>
<div id="WeightedSum_panel" class="collapse">

### Configuring an WeightedSum Model

#### Dynamics settings

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : WeightedSum

#### Parameters settings

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

* $XXs$ (double) : weight for XXs input variable

#### Input settings

Any user defined input port detected is set with a value of 1 for sync parameter.  
All input ports are detected in the model constructor with possible dynamic detection at run-time (but associated weight parameter must be already defined).

#### Output settings

A single output port **WeightedSum**.

#### Observation settings 

Same as outputs.

#### Available configurations 

A single configuration **vle.discrete-time.generic/WeightedSum** is available and define no inputs.

#### Details

Input trajectory :  

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $v_1^1$ | $v_2^1$ | $\ldots$ | $v_z^1$ |
| $v_1^2$ | $v_2^2$ | $\ldots$ | $v_z^2$ |
| $\ldots$ | $\ldots$ | $\ldots$ | $\ldots$ |
| $v_1^m$ | $v_2^m$ | $\ldots$ | $v_z^m$ |


Output trajectory : 

| $\Delta$ | $2 * \Delta$ | $\ldots$ | $z * \Delta$ |
| :--: | :--: | :--: | :--: |
| $\sum_{j=1}^m w_j*v_1^i$ | $\sum_{j=1}^m w_j*v_2^i$ | $\ldots$ | $\sum_{j=1}^m w_j*v_z^i$ |


</div>

--- 




## Atomic model Interpolate <a name="Interpolate"></a>

The **Interpolate** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) is a generic interpolation function based on time and value pairs. 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#Interpolate_panel'>Show/Hide Model Details</button>
<div id="Interpolate_panel" class="collapse">

### Configuring a Interpolate Model 

#### Dynamics settings 

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : Interpolate

#### Parameters settings 

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

Any user defined output port detected expect a parameter which name is the output port name with a "_params" suffix.
Below a "XX"" output port name is taken as example

| Parameter name | Type | Is mandatory? | Description |
| :--: | :----------------------: | :--: | ---------------------------------- |
| **XX_params** | map< t0=double, pairs=set<map<date=int, XX=double>>> | [] | All user-defined steps (see details section) |

#### Input settings 

No input is required.

#### Output settings 

Any user defined output port detected is used.
All output ports are detected in the model constructor with no dynamic detection at run-time.

#### Observation settings 

Same as outputs.  

#### Available configurations 

A single configuration **vle.discrete-time.generic/Interpolate** is available and use 3 pairs.

### Details 

The first part **t0** is an initial time padding to be applied to each of the following **pairs** element. 

The second part **pairs**, can have any number *n* of elements for which each is composed of a **date** (in number of day of simulation) and a value for **XX**. Remark : this part being a *map* using **date** as its keys, this implies the unicity of the defined dates. 

We consider all the **date~i~** (with i &isin; [1,n]) values with their associated **XX~i~** in an increasing order. 

Then for each time-step **t** the following rule is used : 

> $$XX(t)=\left\{
  \begin{array}{@{}ll@{}}
    XX_{i}, & \text{if}\ t-t0=date_{i} \\
    XX_{i} + ( t -t0 - date_{i}) * \frac{XX_{i+1} - XX_{i}}{date_{i+1} - date_{i}}, & \text{else if}\ date_{i}<t-t0 <date_{i+1} \\
    XX_{n}, & \text{otherwise} (t-t0 > date_{n})
  \end{array}\right.
\label{eq:XX_interpolation}$$




</div>












## Atomic model YY <a name="p2"></a>

Atomic model short description.
must include the class it inherit from (DEVS or extension)

for example :

The **YY** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate $\ldots$ based on$\ldots$ 

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#YY_panel'>Show/Hide Model Details</button>
<div id="YY_panel" class="collapse">

### Configuring a YY Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : vle.discrete-time.generic
* **name** : YY

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

for example :  
See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| :--: | :--: | :--: | :--: |
| **PkgName** | string |[] | Name of the package where the data file is stored.<br>If not provided the **meteo_file** will be used as an absolute path or a relative path to the running environment|
| **meteo_file** | string |[x] | Name of the file to read.<br>(file expected inside the ***PkgName**/data* folder)<br>This can be a relative path from the data folder if it has subfolders|
| **meteo_type** | string (agroclim or generic_with_header) |[x] | Type of file the model has to read.<br>(see details section for information on the different types). |
| **begin_date** | string (YYYY-MM-DD)<br>or<br>double| [] | Enables to start the reading of data at the specified date.<br>(If not provided datas are read from the begining of the data set)|

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files

### Details <a name="p2.2"></a>

Additionnal detailled information on the model usage and/or parameters interactions.

</div>

