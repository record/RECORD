\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{listings}
\lstset{language=C++}
\usepackage{hyperref}
\usepackage{amsthm}
\usepackage{makeidx}


\theoremstyle{remark}
\newtheorem{tutorial}{Tutorial }

\pdfinfo{
   /Author (RECORD Team - Ronan Trpos, Eric Casellas)
   /Title  (vle.discrete-time.generic)
   /Subject (RECORD VLE)
   /Keywords (RECORD VLE)
}


\author{RECORD Team \texorpdfstring{\\}{} (Ronan Tr\'epos, Eric Casellas)}
\title{{\tt vle.discrete-time.generic} : a VLE package that provides simple atomics models.}

%%%%%%%%%%%%
\makeindex
\begin{document}


\maketitle

\tableofcontents

%%%%%%%%%%%%
\section{Introduction}
\label{sec:introduction}

The package {\tt vle.discrete-time.generic} provides simple atomic models for either:
\begin{itemize}
\item computing simple functions such as weighted sums.
\end{itemize}

The global description of output trajectories of these models is based on
a function of intput trajectories.

%%%%%%%%%%%%
\section{Constant}
\label{sec:constant}
%%%%%%%%%%%%
\underline{Categories:}
Generative models\index{Generative models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $c$ : the constant value
  \item $\Delta$ : the time step of the output
\end{itemize}
\underline{Input trajectory:}\\[\baselineskip]
None
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$c$        & $c$          & \ldots & $c$ \\
\end{tabular}
\\[\baselineskip]
\underline{Perturbations:}
The value of $c$ can be modified during simulation by using the input port
corresponding to the output port.
\\[\baselineskip]
\underline{Use cases:}
\begin{itemize}
  \item The Constant model can be used to develop a complex model
  in an incremental way. Consider for example the development a model
  DiscreteTime $DT$ that depends on a external variable, that is not yet
  available. The constant model $C$ provides, during the implementation
  time period, the inputs necessary to the dynamic of $DT$.
\begin{displaymath}
    C \to DT
\end{displaymath}
\end{itemize}

\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.

%%%%%%%%%%%%
\section{Identity}
\label{sec:identity}
%%%%%%%%%%%%
\underline{Categories:}
Identity models\index{Identity models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the input
\end{itemize}
\underline{Input trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1$      & $v_2$        & \ldots & $v_z$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1$      & $v_2$        & \ldots & $v_z$ \\
\end{tabular}
\\[\baselineskip]
\underline{Use cases:}
\begin{itemize}
  \item The Identity model can be used to observe state variables of
  models built by an executive. For example, consider an executive model
  that instantiates, at time $t_1$, a model $M$ and replaces, at $t_2$, $M$ by
  another model $M'$ providing the same outputs ; outputs of both $M$ and $M'$
  are coupled to inputs of an Identity model $Id$.
\begin{displaymath}
\left\{ \begin{array}{cc}
 \textrm{ between $t_1$ and $t_2$} & M \to Id \\
 \textrm{ after $t_2$}             & M' \to Id
\end{array} \right.
\end{displaymath}
  The Identity model allows the observation of
  the state variable of both $M$ and $M'$, seen as the
  same state variable.
\end{itemize}
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.

%%%%%%%%%%%%
\section{Sum}
\label{sec:sum}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Variable models\index{Aggregate Variable models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the output
\end{itemize}
\underline{Input trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$           & $2*\Delta$         & \ldots & $z*\Delta$ \\ \hline
$\sum_{j=1}^m v_1^j$ & $\sum_{j=1}^m v_2^j$ & \ldots & $\sum_{j=1}^m v_z^j$ \\
\end{tabular}
\\[\baselineskip]
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.


%%%%%%%%%%%%
\section{Average}
\label{sec:average}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Variable models\index{Aggregate Variable models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the output
\end{itemize}
\underline{Input trajectory:}
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$                      & $2*\Delta$                    & \ldots & $z*\Delta$ \\ \hline
$\frac{1}{m}\sum_{j=1}^m v_1^j$ & $\frac{1}{m}\sum_{j=1}^m v_2^j$ & \ldots &$\frac{1}{m}\sum_{j=1}^m v_z^j$\\
\end{tabular}
\\[\baselineskip]
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.

%%%%%%%%%%%%
\section{Product}
\label{sec:product}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Variable models\index{Aggregate Variable models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the output
\end{itemize}
\underline{Input trajectory:}
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$            & $2*\Delta$          & \ldots & $z*\Delta$ \\ \hline
$\prod_{j=1}^m v_1^j$ & $\prod_{j=1}^m v_2^j$ & \ldots & $\prod_{j=1}^m v_z^j$ \\
\end{tabular}
\\[\baselineskip]
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.

%%%%%%%%%%%%
\section{WeightedSum}
\label{sec:weightedsum}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Variable models\index{Aggregate Variable models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
\item $\Delta$ : the time step of the output
\item $w_i$ : weight for all input variables
\end{itemize}
\underline{Input trajectory:}
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$           & $2*\Delta$         & \ldots & $z*\Delta$ \\ \hline
$\sum_{j=1}^m w_j*v_1^j$ & $\sum_{j=1}^m w_j*v_2^j$ & \ldots & $\sum_{j=1}^m w_j*v_z^j$\\
\end{tabular}
\\[\baselineskip]
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn..

%%%%%%%%%%%%
\section{WeightedProduct}
\label{sec:weightedproduct}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Variable models\index{Aggregate Variable models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
\item $\Delta$ : the time step of the output
\item $w_i$ : weight for all input variables
\end{itemize}
\underline{Input trajectory:} \\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:} \\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$           & $2*\Delta$         & \ldots & $z*\Delta$ \\ \hline
$\prod_{j=1}^m w_j*v_1^i$ & $\prod_{j=1}^m w_j*v_2^i$ & \ldots & $\prod_{j=1}^m w_j*v_z^i$\\
\end{tabular}
\\[\baselineskip]
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.


%%%%%%%%%%%%
\section{MovingAverage}
\label{sec:movingaverage}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Times models\index{Aggregate Times models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the output
  \item $n$ : number of time-step to take into account in the average
  \item $m$ : number of time-step to wait before taking into account the
  current input value (default 0).
\end{itemize}
\underline{Input trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c|c|c}
$\Delta$ & $2*\Delta$ & $3*\Delta$ & $4*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1$      & $v_2$        & $v_3$        & $v_4$        & \ldots & $v_z$        \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:} (for $m=1$, $n=3$)\\[\baselineskip]
\begin{tabular}{c|c|c|c|c|c}
$\Delta$ & $2*\Delta$ & $3*\Delta$            & $4*\Delta$                 & \ldots & $z*\Delta$   \\ \hline
0        & 0          & $\frac{v_1 + v_2}{n}$ & $\frac{v_1 + v_2 + v_3}{n}$ & \ldots & $\frac{1}{n}\sum\limits_{i=z-m-n}^{z-m} v_i$ \\
\end{tabular}
\\[\baselineskip]
\underline{Use cases:}
\begin{itemize}
  \item  The MovingAverage model can be used to provide the classical
  moving average (with $m=0$) of the sub sequence of $v_i$ from $t-n$ to $t$
  where $t$ is the current time.
  \item It can also simulates a delay (with $n=1$). The output value
  is then $v_{t-m}$ where $t$ is the current time.
\end{itemize}
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.
%%%%%%%%%%%%

\section{MovingSum}
\label{sec:movingsum}
%%%%%%%%%%%%
\underline{Categories:}
Aggregate Times models\index{Aggregate Times models}
\\[\baselineskip]
\underline{Parameters:}
\begin{itemize}
  \item $\Delta$ : the time step of the output
  \item $n$ : number of time-step to take into account in the sum
\end{itemize}
\underline{Input trajectory:}\\[\baselineskip]
\begin{tabular}{c|c|c|c}
$\Delta$ & $2*\Delta$ & \ldots & $z*\Delta$ \\ \hline
$v_1^1$    & $v_2^1$      & \ldots & $v_z^1$ \\
$v_1^2$    & $v_2^2$      & \ldots & $v_z^2$ \\
\ldots     & \ldots       & \ldots & \ldots \\
$v_1^m$    & $v_2^m$      & \ldots & $v_z^m$ \\
\end{tabular}
\\[\baselineskip]
\underline{Output trajectory:} (for $n=3$)\\[\baselineskip]
\begin{tabular}{c|c|c|c|c|c}
$\Delta$  & $2*\Delta$      & $3*\Delta$             & $4*\Delta$              & \ldots & $z*\Delta$   \\ \hline
$v_1^1$    & $v_1^1 + v_2^1$ & $v_1^1 + v_2^1 + v_3^1$  & $v_2^1 + v_3^1 + v_4^1$  & \ldots & $\sum\limits_{i=z-n}^{z} v_i^1$\\
$v_1^2$    & $v_1^2 + v_2^2$ & $v_1^2 + v_2^2 + v_3^2$  & $v_2^2 + v_3^2 + v_4^2$  & \ldots & $\sum\limits_{i=z-n}^{z} v_i^2$\\
\ldots     & \ldots         & \ldots                  & \ldots                 & \ldots & \ldots\\
$v_1^m$    & $v_1^m + v_2^m$ & $v_1^m + v_2^m + v_3^m$  & $v_2^m + v_3^m + v_4^m$  & \ldots & $\sum\limits_{i=z-n}^{z} v_i^m$\\
\end{tabular}
\\[\baselineskip]
\underline{Use cases:}
\begin{itemize}
  \item  The MovingSum model can be used to provide the classical
  moving sum of the sub sequence of $v_i$ from $t-n$ to $t$
  where $t$ is the current time.\end{itemize}
\underline{Miscellaneous:}
Based on vle::discrete\_time::DiscreteTimeDyn.. It can accept multiple inputs and outputs, but a single n parameter.


\printindex

\end{document}


