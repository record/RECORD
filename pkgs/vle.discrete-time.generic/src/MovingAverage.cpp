/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>
#include <algorithm>

namespace vle {
namespace discrete_time {
namespace generic {

class MovingAverage : public DiscreteTimeDyn
{
public:
    MovingAverage(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
        n = (events.exist("n")) ? vle::value::toInteger(events.get("n")) : 1;
        m = (events.exist("m")) ? vle::value::toInteger(events.get("m")) : 0;

        n = (n>1) ? n : 1;
        m = (m>0) ? m : 0;

        
        vle::vpz::ConnectionList::const_iterator itb = getModel().getInputPortList().begin();
        vle::vpz::ConnectionList::const_iterator ite = getModel().getInputPortList().end();
        for (; itb != ite; itb++) { //loop over input port names
            Var* v = new Var();
            v->init(this, itb->first, events);
            getOptions().syncs.insert(std::make_pair(itb->first, 1)); // force sync for all inputs
            inputs.insert( std::pair< std::string, Var*>(itb->first,v) );

            Var* v2 = new Var();
            v2->init(this, "MovingAverage_"+itb->first, events);
            means.insert( std::pair< std::string, Var*>(itb->first,v2) );
            
            double buffer_init = (events.exist("init_value_MovingAverage_"+itb->first)) ? vle::value::toDouble(events.get("init_value_MovingAverage_"+itb->first)) : 0.;
            
            mBuffer[itb->first] = std::deque< double >(n + m, buffer_init); // set history size for local buffer
        }
    }

    virtual ~MovingAverage()
    {
        std::map < std::string, Var*>::iterator itb = inputs.begin();
        std::map < std::string, Var*>::iterator ite = inputs.end();
        for (; itb!= ite; itb++) {
            delete itb->second;
        }
        std::map < std::string, Var*>::iterator itb2 = means.begin();
        std::map < std::string, Var*>::iterator ite2 = means.end();
        for (; itb2!= ite2; itb2++) {
            delete itb2->second;
        }
    }

    void compute(const vle::devs::Time& /*t*/)
    {
        for (std::map < std::string, Var*>::iterator it = means.begin(); it != means.end(); ++it) {
            mBuffer[it->first].push_front((*inputs[it->first])()); //add new daily value
            *it->second =  (*it->second)(-1) - mBuffer[it->first][n + m]/n + mBuffer[it->first][m]/n; //compute current moving average
            mBuffer[it->first].pop_back(); //delete last daily value
        }
    }

private:
    std::map < std::string, Var*> inputs;
    std::map < std::string, Var*> means;
    std::map < std::string, std::deque< double > > mBuffer;
    
    int m;
    int n;

};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::MovingAverage)

