/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>
//#include <iostream>


namespace vle {
namespace discrete_time {
namespace generic {

class StepFun : public DiscreteTimeDyn
{
public:
    StepFun(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
//        std::cout << "\t[StepFun] Constructor" << std::endl;
        
        DefaultValue = events.exist("DefaultValue") ? events.getDouble("DefaultValue") : 0.;
//        std::cout << "\t\tDefaultValue="  << DefaultValue << std::endl;
        ComputeSlope = events.exist("ComputeSlope") ? events.getBoolean("ComputeSlope") : false;
//        std::cout << "\t\tComputeSlope="  << ComputeSlope << std::endl;

        Steps.clear();
        if (events.exist("Steps")) {
            const vle::value::Set& step_set = events.getSet("Steps");
            for (unsigned int index = 0; index < step_set.size(); ++index) {
                const vle::value::Map& tab(step_set.getMap(index));
                double theshold = toDouble(tab["Threshold"]);
                double value = toDouble(tab["Value"]);
                double slope = 0.;
                if (tab.exist("Slope")) {
                    slope = toDouble(tab["Slope"]);
                }
                std::vector< double > vals;
                vals.push_back(value);
                vals.push_back(slope);
                if ( Steps.find(theshold) == Steps.end() ) {
                    Steps.insert(std::make_pair(theshold,vals));
                } else {
//                    std::cout << "\t\tThreshold already defined:"  << theshold << "\t Value ignored: " << value << std::endl;
                }
            }
            
/*            std::cout << "\t\tThreshold/Value pairs found:" << std::endl;
            for (std::map< double, std::vector< double > >::const_iterator it = Steps.begin(); it != Steps.end(); ++it) {
                std::cout << "\t\t" << it->first << "\t/\t" << (it->second)[0] << " - " << (it->second)[1] <<std::endl;
            }
*/
            if (ComputeSlope) {
                for (std::map< double, std::vector< double > >::iterator it = std::next(Steps.begin()); it != Steps.end(); ++it) {
                (std::prev(it)->second)[1] = ((it->second)[0] - (std::prev(it)->second)[0]) / ((it->first) - (std::prev(it)->first));
                (it->second)[1] = 0.;
            }

            }

        } else {
//            std::cout << "\t\tNo steps found" << std::endl;
        }

        in.init(this, "in", events);
        getOptions().syncs.insert(std::make_pair("in", 1)); // force sync for in input
        out.init(this, "out", events);
    }

    virtual ~StepFun()
    {
    }

    void compute(const vle::devs::Time& /*t*/)
    {
        double tmp_out = DefaultValue;
        
//        std::cout << t << "\t[StepFun] Compute" << std::endl;
        
        for (std::map< double, std::vector< double > >::const_iterator it = Steps.begin(); it != Steps.end(); ++it) { // les valeurs des clés de map sont déjà en ordre croissant
            if (in() >= it->first) {
                tmp_out = (it->second)[0] + (in()-it->first)*(it->second)[1];
            }
        }
        out = tmp_out;
//        std::cout << "\t\tin=" << in() << "\tout=" << out() << std::endl;
    }

    Var in;
    Var out;
    
private:

    double DefaultValue;
    bool ComputeSlope;
    std::map< double, std::vector< double > > Steps;

};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::StepFun)

