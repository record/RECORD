/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>


namespace vle {
namespace discrete_time {
namespace generic {

class Constant : public DiscreteTimeDyn
{
public:
    Constant(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
        vle::vpz::ConnectionList::const_iterator itb = getModel().getOutputPortList().begin();
        vle::vpz::ConnectionList::const_iterator ite = getModel().getOutputPortList().end();
        for (; itb != ite; itb++) { //loop over output port names
            Var* v = new Var();
            v->init(this, itb->first, events);
            outputs.push_back(v);
        }
    }

    virtual ~Constant()
    {
        std::vector<Var*>::iterator itb = outputs.begin();
        std::vector<Var*>::iterator ite = outputs.end();
        for (; itb!= ite; itb++) {
            delete *itb;
        }
    }

    void compute(const vle::devs::Time& /*t*/)
    { }

    std::vector<Var*> outputs;
};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::Constant)

