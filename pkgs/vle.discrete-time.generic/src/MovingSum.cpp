/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>
#include <algorithm>
#include <numeric>


namespace vle {
namespace discrete_time {
namespace generic {

class MovingSum : public DiscreteTimeDyn
{
public:
    MovingSum(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
        int n = (events.exist("n")) ? vle::value::toInteger(events.get("n")) : 1;
        
        vle::vpz::ConnectionList::const_iterator itb = getModel().getInputPortList().begin();
        vle::vpz::ConnectionList::const_iterator ite = getModel().getInputPortList().end();
        for (; itb != ite; itb++) { //loop over input port names
            Var* v = new Var();
            v->init(this, itb->first, events);
            getOptions().syncs.insert(std::make_pair(itb->first, 1)); // force sync for all inputs
            inputs.insert( std::pair< std::string, Var*>(itb->first,v) );

            Var* v2 = new Var();
            v2->init(this, "MovingSum_"+itb->first, events);
            sums.insert( std::pair< std::string, Var*>(itb->first,v2) );
            
            mBuffer[itb->first] = std::vector< double >(n, 0); // set history size for local buffer
        }
    }

    virtual ~MovingSum()
    {
        std::map < std::string, Var*>::iterator itb = inputs.begin();
        std::map < std::string, Var*>::iterator ite = inputs.end();
        for (; itb!= ite; itb++) {
            delete itb->second;
        }
        std::map < std::string, Var*>::iterator itb2 = sums.begin();
        std::map < std::string, Var*>::iterator ite2 = sums.end();
        for (; itb2!= ite2; itb2++) {
            delete itb2->second;
        }
    }

    void compute(const vle::devs::Time& /*t*/)
    {
        for (std::map < std::string, Var*>::iterator it = sums.begin();
             it != sums.end(); ++it) {

            std::rotate(mBuffer[it->first].rbegin(),
                        mBuffer[it->first].rbegin() + 1,
                        mBuffer[it->first].rend()); //put last daily value at begin

            mBuffer[it->first][0] = (*inputs[it->first])(); //overwrite last daily value with new daily value

            *it->second = std::accumulate(mBuffer[it->first].begin(),
                                         mBuffer[it->first].end(),
                                         0.0); //compute current moving sum
        }
    }

    std::map < std::string, Var*> inputs;
    std::map < std::string, Var*> sums;
    std::map < std::string, std::vector< double > > mBuffer;
};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::MovingSum)

