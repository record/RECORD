/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>
#include <vle/utils/Tools.hpp>

namespace vd = vle::devs;
namespace vu = vle::utils;
namespace vv = vle::value;

namespace vle {
namespace discrete_time {
namespace generic {

class Interpolate : public DiscreteTimeDyn
{
public:
    Interpolate(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events)
    {
        std::string ModelName = getModel().getCompleteName();
        vle::vpz::ConnectionList::const_iterator itb = getModel().getOutputPortList().begin();
        vle::vpz::ConnectionList::const_iterator ite = getModel().getOutputPortList().end();
        for (; itb != ite; itb++) { //loop over output port names
            std::string Varname = itb->first;
            Varnames.push_back(Varname);
            std::string ParamName = Varname + "_params";
            // declare Vars
            Var* v = new Var();
            v->init(this, Varname, events);
            outputs[Varname] = v;
            // read pair parameters
            double t0;
            std::map<int, std::map<std::string, double>> pair_param;
            readPairs(events, ParamName, Varname, t0, pair_param, ModelName);
            t0_parameters[Varname] = t0;
            pair_parameters[Varname] = pair_param;
        }

    }

    virtual ~Interpolate() {}

    void compute(const vle::devs::Time& t)
    {
        std::vector<std::string>::const_iterator it;
        for ( it = Varnames.begin(); it != Varnames.end(); it++ )
        {
            *outputs[*it] = interpolation(pair_parameters[*it], t0_parameters[*it], t);
        }
    }

    std::vector<std::string> Varnames;
    std::map<std::string, Var*> outputs;
    std::map<std::string, std::map<int, std::map<std::string, double>> > pair_parameters;
    std::map<std::string, double> t0_parameters;
    
    void readPairs(const vd::InitEventList& evts, const std::string ParamName, const std::string KeyName, double &t0, std::map<int, std::map<std::string, double>> &pairs, const std::string ModelName) {
        const vv::Map& pairs_params(evts.getMap(ParamName));
        
        if (!pairs_params.exist("t0")) {//check mandatory key t0
            throw vu::ModellingError(
                vu::format("[%s] missing mandatory key 't0' in parameter '%s' \n",
                        ModelName.c_str(), ParamName.c_str()));
        }
        if (!pairs_params.exist("pairs")) {//check mandatory key pairs
            throw vu::ModellingError(
                vu::format("[%s] missing mandatory key 'pairs' in parameter '%s' \n",
                        ModelName.c_str(), ParamName.c_str()));
        }
        
        t0 = toInteger(pairs_params["t0"]);
        const vv::Set& pairs_set = pairs_params.getSet("pairs");
        for (unsigned int index = 0; index < pairs_set.size(); ++index) {
            const vv::Map& tab(pairs_set.getMap(index));
            if (!tab.exist("date")) {//check mandatory key date
                throw vu::ModellingError(
                vu::format("[%s] missing mandatory key 'date' in the %d th pair element inside parameter '%s' \n",
                        ModelName.c_str(), (index+1), ParamName.c_str()));
            }
            if (!tab.exist(KeyName)) {//check mandatory key KeyName
                throw vu::ModellingError(
                vu::format("[%s] missing mandatory key '%s' in the %d th pair element inside parameter '%s' \n",
                        ModelName.c_str(), KeyName.c_str(), (index+1), ParamName.c_str()));
            }
            
            int date = toInteger(tab["date"]);
            double value = toDouble(tab[KeyName]);
            double slope = 0.;
            std::map<std::string, double> vals;
            
            if ( pairs.find(date) == pairs.end() ) {
                vals["slope"] = slope;
                vals["value"] = value;
                pairs.insert(std::make_pair(date,vals));
            } else {
              std::cout << "\t\tdate already defined:"  << date << "\t Value ignored: " << value << std::endl;
            }
        }
        if ( pairs.find(0) == pairs.end() ) {
            std::map<std::string, double> vals;
            vals["slope"] = 0.;
            vals["value"] = 0.;
            pairs.insert(std::make_pair(0,vals));
        }

        std::map<int, std::map<std::string, double>>::iterator it_end = pairs.end();
        --it_end;

        for (std::map<int, std::map<std::string, double>>::iterator it = pairs.begin(); it != it_end; ++it) {
            std::map<int, std::map<std::string, double>>::iterator it_next = it;
            ++it_next;
            std::map<std::string, double> tmp = it->second;
            std::map<std::string, double> tmp2 = (it_next)->second;
            tmp["slope"] = (tmp2["value"] - tmp["value"])/((it_next)->first - it->first);
            it->second = tmp;
        }
    }


    // 2 keys are expected in the pairs input : 'value' and 'slope' both of type double
    double interpolation(std::map<int, std::map<std::string, double>> pairs, double t0, const vle::devs::Time& t) {
        double rel_t = t - t0;
        double tmp_val=(pairs.begin()->second)["value"];

        for (std::map<int, std::map<std::string, double>>::const_iterator it = pairs.begin(); (rel_t >= it->first) & (it != pairs.end()) ; ++it) {
            std::map<std::string, double> tmp = it->second;
            if (rel_t==it->first) {
                tmp_val = tmp["value"];
            } else {
                tmp_val = tmp["slope"] * (rel_t-it->first) + tmp["value"];
            }
        }

        return(tmp_val);
    }
};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::Interpolate)

