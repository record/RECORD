/*
 * Copyright (c) 2014-2017 INRA http://www.inra.fr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends


#include <vle/DiscreteTime.hpp>
#include <vle/devs/Dynamics.hpp>
#include <vle/utils/DateTime.hpp> //vu::DateTime::toJulianDay
//#include <iostream> // std::cout
//#include <iomanip> // std::setprecision
#include <sstream> // std::stringstream


namespace vle {
namespace discrete_time {
namespace generic {

class StepDates : public DiscreteTimeDyn
{
public:
    StepDates(const vle::devs::DynamicsInit& init, const vle::devs::InitEventList& events)
        : DiscreteTimeDyn(init, events), nb_compute(0)
    {
//        std::cout << "\t[StepDates] Constructor" << std::endl;
        
        DefaultValue = events.exist("DefaultValue") ? events.getDouble("DefaultValue") : 0.;
//        std::cout << "\t\tDefaultValue="  << DefaultValue << std::endl;

        if (events.get("begin_date")->isDouble()) {
            begin_time = events.getDouble("begin_date");
            begin_date = vle::utils::DateTime::toJulianDay(begin_time);
        } else if (events.get("begin_date")->isString()) {
            begin_date = events.getString("begin_date");
            begin_time = vle::utils::DateTime::toJulianDay(begin_date);
        }
//        std::cout << std::setprecision(10)<< "******\nbegin_time:" << begin_time << std::endl;
//        std::cout << "begin_date:" << begin_date << std::endl;

        Steps.clear();
        if (events.exist("Steps")) {
            const vle::value::Set& step_set = events.getSet("Steps");
            for (unsigned int index = 0; index < step_set.size(); ++index) {
                const vle::value::Map& tab(step_set.getMap(index));
                std::string theshold = toString(tab["Threshold"]);
                double value = toDouble(tab["Value"]);
                double slope = 0.;
                if (tab.exist("Slope")) {
                    slope = toDouble(tab["Slope"]);
                }
                std::vector< double > vals;
                vals.push_back(value);
                vals.push_back(slope);
                if ( Steps.find(theshold) == Steps.end() ) {
                    Steps.insert(std::make_pair(theshold,vals));
                } else {
//                    std::cout << "\t\tThreshold already defined:"  << theshold << "\t Value ignored: " << value << std::endl;
                }
            }
            
/*            std::cout << "\t\tThreshold/Value-Slope pairs found:" << std::endl;
            for (std::map< std::string, std::vector< double > >::const_iterator it = Steps.begin(); it != Steps.end(); ++it) {
                std::cout << "\t\t" << it->first << "\t/\t" << (it->second)[0] << " - " << (it->second)[1] <<std::endl;
            }
*/
        } else {
//            std::cout << "\t\tNo steps found" << std::endl;
        }

        out.init(this, "out", events);
    }

    virtual ~StepDates()
    {
    }

    void compute(const vle::devs::Time& t)
    {
        nb_compute++;
        double tmp_out = DefaultValue;
        double currentDOY = vle::utils::DateTime::dayOfYear(t+begin_time);
        int current_year = vle::utils::DateTime::year(t+begin_time);
        
//        std::cout << vle::utils::DateTime::toJulianDayNumber(t+begin_time) << " - " << currentDOY << "\t[StepDates] Compute" << std::endl;

        for (std::map< std::string, std::vector< double > >::const_iterator it = Steps.begin(); it != Steps.end(); ++it) { // les valeurs des clés de map sont déjà en ordre croissant
            std::stringstream date;
            date << current_year << "/" << it->first;
            double theshold = vle::utils::DateTime::dayOfYear(vle::utils::DateTime::toJulianDayNumber(date.str()));

            if (currentDOY >= theshold) {
                tmp_out = (it->second)[0] + (currentDOY-theshold)*(it->second)[1];
            }
        }
        out = tmp_out;
//        std::cout << "\t\tout=" << out() << std::endl;
    }

    std::unique_ptr<vle::value::Value> observation(
        const vle::devs::ObservationEvent& event) const override
    {
        if (event.onPort("current_date_str")) {
            if (begin_time == std::numeric_limits<double>::infinity()) {
                return vle::value::String::create("no_begin_date");
            }
            return vle::value::String::create(vle::utils::DateTime::toJulianDayNumber(
                    (unsigned long) (begin_time+nb_compute)));
        }
        if (event.onPort("current_date")) {
            return vle::value::Double::create(begin_time+ nb_compute);
        }

        return DiscreteTimeDyn::observation(event);
    }


    Var out;
    
private:

    double DefaultValue;
    bool ComputeSlope;
    std::map< std::string, std::vector< double > > Steps;
    std::string  begin_date;
    double begin_time;
    double nb_compute;

};

}}}

DECLARE_DYNAMICS(vle::discrete_time::generic::StepDates)

