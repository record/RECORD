Description
===========

This package provide 2 simulators that can be running together.

They can simulate a plitted 2CV model.

On the "server side" simulator you get the plant.
On the "client side" simulator you getthe soil and the climat.

In both side you will find an executive managing dt models.

In a certain way yhe client side does command as many plants as he want.
The server side does serve ase many plant simulation as needed.

Exchange of values of variables are made at each time step.

Requirements
============

* vle      2.0  http://www.vle-project.org
* boost    1.49 http://www.boost.org
* cmake    2.8  http://www.cmake.org


Installation
============
```
$ vle --package=record.mipp configure
$ vle --package=record.mipp build
```

More Documentation
==================

[MIPP_2CV_CropFull](https://forgemia.inra.fr/record/RECORD/blob/master/pkgs/record.mipp/doc/MIPP_2CV_CropFull_Server.md)