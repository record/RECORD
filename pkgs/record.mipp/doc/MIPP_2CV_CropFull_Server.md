# 2CV CropFull submodel definition

According to the native 2CV package this is what we get:

| Item Name | Atomic Model Name | Type | Description | Unit |
| ------ | ------ | ------ | ------ | ------ |
| ALAI | CropLai |  Output Variable | LAI_actif  | m²/m² |
| RootDepth | CropRoot |  Output Variable | profondeur du front racinaire | mm |
| Tmin | CropTT | Input Variable | la température minimale journalière | °C |
| Tmax | CropTT | Input Variable | la température maximale journalière | °C |
| Sowing | CropTT | Input Variable | jour du semis, (jour j) ?  1 : 0 | - |
| Harvesting | CropTT | Input Variable | jour de la récolte, (jour j) ?  1 : 0 | - |
| Rad | CropAGB | Input Variable | la radiation solaire journalière totale | MJ/m² |
| ATPT | CropAGB |Input Variable | le rapport transpiration réelle / potentielle utilisé comme indice de stress | - |
| dens | CropLai | parameter | la densité de semis  | plante/m² |
| p1logi | CropLai | parameter | paramètre de l'équation logistique du LAI | - |
| p2logi | CropLai | parameter | paramètre de l'équation logistique du LAI | (°C.j)^-1 |
| lai0 | CropLai | parameter | la valeur du LAI à l'emergtence | m²/m² |
| r1sf | CropLai | parameter | effet du rapport ATPT sur la croissance du LAI | - |
| 2sf | CropLai | parameter | effet du rapport ATPT sur la croissance du LAI | - |
| p1sen | CropLai | parameter | parametre de la fraction de LAI senescent | - |
| p2sen | CropLai | parameter | parametre de la fraction de LAI senescent | - |
| soilDepth | CropRoot | parameter | la profondeur du sol. | mm |
| maxDepth | CropRoot | parameter | la profondeur maximal atteignable par les racines. | mm |
| rateDepth | CropRoot | parameter | la croissance journaliere maximale du front racinaire | mm/j |
| iniDepth | CropRoot | parameter | la profondeur initiale à l'emergence | mm |
| tt_Sowing_Emergence | CropTT | parameter | le temps thermique entre les stades 0 & 1 | °C.j |
| tt_Emergence_MaxLAI | CropTT | parameter | le temps thermique entre les stades 1 & 2 | °C.j |
| tt_MaxLAI_Flowering | CropTT | parameter | le temps thermique entre les stades 2 & 3 | °C.j |
| tt_Flowering_GrainAbort | CropTT | parameter | le temps thermique entre les stades 3 & 4 | °C.j |
| tt_GrainAbort_LeafSen | CropTT | parameter | le temps thermique entre les stades 4 & 5 | °C.j |
| tt_LeafSen_Maturity | CropTT | parameter | le temps thermique entre les stades 5 & 6 | °C.j |
| xtinc | CropAGB | parameter | le coefficient d'extinction de la lumière à travers les feuilles | - |
| Rue1 | CropAGB | parameter | RUE avant debut de senescence rapide des feuilles. | g/MJ |
| Rue2 | CropAGB | parameter | RUE après debut de senescence rapide des feuilles. | g/MJ |
| r1Rue | CropAGB | parameter | effet du rapport ATPT sur la croissance en biomasse | - |
| r2Rue | CropAGB | parameter | effet du rapport ATPT sur la croissance en biomasse | - |
| hiMax | CropAGB | parameter | indice de recolte maximal | - |
| r1hi | CropAGB | parameter | effet du rapport ATPT sur l'indice de recolte | - |
| r2hi | CropAGB | parameter | effet du rapport ATPT sur l'indice de recolte | - |
| rateHI | CropAGB | parameter | croissance journaliere maximale du HI| /j |

(default Values of parameters can be found inside the multiClientINOUTCropFull.cpp)

# Client & Server simulators usage.

A file is necessary to instanciate the the socket */tmp/MIPP/vle/vle_socket*

Two simulators are available. One does provide the server side, and can be lauched this way :
~~~
vle -P record.mipp build multi_2CV-parcelle-single-server_INOUT.vpz
~~~
The other one does provide a client side, in order to provide an OpenFluid like client to test the server, and can be launched this way :
~~~
vle -P record.mipp build multi_2CV-parcelle-single-client_INOUT.vpz
~~~
Of course the server should be launched first.

So far when running, only 2 crop plots are simulated, you can ask for more by editing multi_2CV-parcelle-single-client_INOUT.vpz parameters:
* *unitIDS* : a set of crop plots identifiers(integers)
* *[parameterName]@[unitID]* : a double value tos set a crop plot in particular.

Remark: The duration of the client as to be the duration of the server - 1.
This is due to the usage of the discretime Executive Class, that enable to provide a "simple" to code server.

# Installation & vle package dependencies

## To install
~~~
vle -P record.mipp clean rclean configure build
~~~
## Dependencies
* The 2CV Package (https://forgemia.inra.fr/record/RECORD)
* vle.discrete-time (https://github.com/vle-forge/packages)
* vle.extension.fsa (https://github.com/vle-forge/packages)
* vle.extension.decision (https://github.com/vle-forge/packages)
* vle.discrete-time.decision (https://github.com/vle-forge/packages)

# vle version

The vle version used to produce the packae is the 2.1.

# TO DO
* providing automatised test
* better managment of the duration, for example by sending the duration inside the init Message
* even more genericity to manage Messages.
* if necessary limiting vle package dependencies
* if necessary providing a 2.0
