// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/discrete-time/DiscreteTimeExec.hpp>
#include <iostream>
#include <zmq.hpp>
#include "CommunicationInterfaceCropFull.hpp"

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;

namespace zmq_test {
using namespace vle::discrete_time;

class multiServerINOUTCropFull: public DiscreteTimeExec {
public:

    multiServerINOUTCropFull(const vd::ExecutiveInit& atom, const vd::InitEventList& events) :
        DiscreteTimeExec(atom, events), m_duration(0),
        m_Context(1), m_Socket(m_Context, ZMQ_REP)
    {
        m_duration = vv::toDouble(events.get("duration"));

        m_Socket.bind(EndPoint);

        InitMessage message;
        std::string refStrTime;
        receive(message);

        m_unitsCount = message.nbUnits;

        for (const std::pair<unsigned int,Param_t>& unit : message.parameters)
        {
             m_UnitIDs.push_back(unit.first);
        }

        vz::Condition& condAGB(conditions().get("condAGB"));
        vz::Condition& condLAI(conditions().get("condLAI"));
        vz::Condition& condRoot(conditions().get("condRoot"));
        vz::Condition& condSoilCommon(conditions().get("condSoilCommon"));
        vz::Condition& condTT(conditions().get("condTT"));

        for (const std::pair<unsigned int,Param_t>& unit : message.parameters)
        {
            /* Parameters AGB*/
            condAGB.setValueToPort("xtinc", vv::Double::create(unit.second.xtinc));
            condAGB.setValueToPort("Rue1", vv::Double::create(unit.second.Rue1));
            condAGB.setValueToPort("Rue2", vv::Double::create(unit.second.Rue2));
            condAGB.setValueToPort("r1Rue", vv::Double::create(unit.second.r1Rue));
            condAGB.setValueToPort("r2Rue", vv::Double::create(unit.second.r2Rue));
            condAGB.setValueToPort("hiMax", vv::Double::create(unit.second.hiMax));
            condAGB.setValueToPort("r1hi", vv::Double::create(unit.second.r1hi));
            condAGB.setValueToPort("r2hi", vv::Double::create(unit.second.r2hi));
            condAGB.setValueToPort("rateHI", vv::Double::create(unit.second.rateHI));

            /* Parameters LAI*/
            condLAI.setValueToPort("dens", vv::Double::create(unit.second.dens));
            condLAI.setValueToPort("p1logi", vv::Double::create(unit.second.p1logi));
            condLAI.setValueToPort("p2logi", vv::Double::create(unit.second.p2logi));
            condLAI.setValueToPort("lai0", vv::Double::create(unit.second.lai0));
            condLAI.setValueToPort("r1sf", vv::Double::create(unit.second.r1sf));
            condLAI.setValueToPort("r2sf", vv::Double::create(unit.second.r2sf));
            condLAI.setValueToPort("p1sen", vv::Double::create(unit.second.p1sen));
            condLAI.setValueToPort("p2sen", vv::Double::create(unit.second.p2sen));

            /* Parameters Root*/
            condSoilCommon.setValueToPort("soilDepth",
                                          vv::Double::create(unit.second.soilDepth));
            condRoot.setValueToPort("maxDepth",
                                    vv::Double::create(unit.second.maxDepth));
            condRoot.setValueToPort("rateDepth",
                                    vv::Double::create(unit.second.rateDepth));
            condRoot.setValueToPort("iniDepth",
                                    vv::Double::create(unit.second.iniDepth));

            /* Parameters TT*/
            condTT.setValueToPort("tt_Sowing_Emergence",
                                  vv::Double::create(unit.second.tt_Sowing_Emergence ));
            condTT.setValueToPort("tt_Emergence_MaxLAI",
                                  vv::Double::create(unit.second.tt_Emergence_MaxLAI));
            condTT.setValueToPort("tt_MaxLAI_Flowering ",
                                  vv::Double::create(unit.second.tt_MaxLAI_Flowering ));
            condTT.setValueToPort("tt_Flowering_GrainAbort ",
                                  vv::Double::create(unit.second.tt_Flowering_GrainAbort ));
            condTT.setValueToPort("tt_GrainAbort_LeafSen",
                                  vv::Double::create(unit.second.tt_GrainAbort_LeafSen));
            condTT.setValueToPort("tt_LeafSen_Maturity",
                                  vv::Double::create(unit.second.tt_LeafSen_Maturity));

            std::string modelName = "U" + std::to_string(unit.first);
            const vz::BaseModel *model = createModelFromClass("CropFull", modelName);

            //Linking everything
            for (auto in : model->getOutputPortList())
            {
                std::string inputPort = in.first + "@U" + std::to_string(unit.first);
                addInputPort(getModelName(), inputPort);
                addConnection(modelName, in.first, getModelName(), inputPort);

                Var* v = new Var();
                v->init(this, inputPort, events);
                inputs.push_back(v);
            }

            for (auto out : model->getInputPortList())
            {
                std::string outputPort = out.first + "@U" + std::to_string(unit.first);
                addOutputPort(getModelName(), outputPort);
                addConnection(getModelName(), outputPort, modelName, out.first);

                Var* v = new Var();
                v->init(this, outputPort, events);
                outputs.push_back(v);

            }
        }

        send(std::string("OK"));
    }

    virtual ~multiServerINOUTCropFull() {}

    virtual void compute(const vd::Time& time)
    {
        if (not firstCompute()) {
            for (unsigned int id : m_UnitIDs) {
                std::string variableNameValue;
                Variables::const_iterator itv;
                VarMono* v;

                variableNameValue = "ALAI@U" + std::to_string(id);
                itv = getVariables().find(variableNameValue);
                v = (VarMono*) itv->second;
                m_MessageCropFull.outputs[id].ALAI = v->getVal(time, 0.0);

                variableNameValue = "RootDepth@U" + std::to_string(id);
                itv = getVariables().find(variableNameValue);
                v = (VarMono*) itv->second;
                m_MessageCropFull.outputs[id].RootDepth = v->getVal(time, 0.0);
            }

            send(m_MessageCropFull);
        }

        std::cout << time << std::endl;

        if (time < m_duration) {
            RunMessage message;
            receive(message);

            for (const std::pair<unsigned int, Input_t>& in : message.inputs)
            {
                std::string variableName;
                Variables::const_iterator itv;
                VarMono* v;

                variableName = "ATPT@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.ATPT);

                variableName = "Harvesting@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.Harvesting);

                variableName = "Rad@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.Rad);

                variableName = "Sowing@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.Sowing);

                variableName = "Tmax@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.Tmax);

                variableName = "Tmin@U" + std::to_string(in.first);
                itv = getVariables().find(variableName);
                v = (VarMono*) itv->second;
                v->update(time, in.second.Tmin);
            }
        }
    }


private:
    double m_duration;

    unsigned int m_unitsCount;

    std::vector<unsigned int> m_UnitIDs;

    std::vector<Var*> inputs;
    std::vector<Var*> outputs;

    template<typename T>
    void send(const T& message)
    {
        msgpack::sbuffer buffer;
        msgpack::pack(&buffer, message);

        zmq::message_t zmqMessage(buffer.size());

        memcpy(zmqMessage.data(), buffer.data(), buffer.size());
        m_Socket.send(zmqMessage);
    }

    template<typename T>
    void receive(T& message)
    {
        zmq::message_t zmqMessage;
        m_Socket.recv(&zmqMessage);

        msgpack::sbuffer buffer;
        buffer.write(static_cast<const char*>(zmqMessage.data()),
                     zmqMessage.size());

        // deserialize it
        msgpack::unpacked unpackedMessage;
        msgpack::unpack(unpackedMessage, buffer.data(), buffer.size());
        unpackedMessage.get().convert(message);
    }

    RunMessage m_MessageCropFull;

    zmq::context_t m_Context;
    zmq::socket_t m_Socket;

};
}
DECLARE_EXECUTIVE(zmq_test::multiServerINOUTCropFull);
