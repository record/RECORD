// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/discrete-time/DiscreteTimeExec.hpp>
#include <iostream>
#include <zmq.hpp>
#include "CommunicationInterfaceCropFull.hpp"

namespace vd = vle::devs;
namespace vz = vle::vpz;
namespace vv = vle::value;

namespace zmq_test {
using namespace vle::discrete_time;

class multiClientINOUTCropFull: public DiscreteTimeExec {
public:

    multiClientINOUTCropFull(const vd::ExecutiveInit& atom, const vd::InitEventList& events) :
        DiscreteTimeExec(atom, events), m_Context(1), m_Socket(m_Context, ZMQ_REQ)
    {
        m_Socket.connect(EndPoint);

        const vle::value::Set& unitIDS = events.getSet("unitIDS");

        for (auto&& id : unitIDS) {
            m_UnitIDs.push_back(toInteger(id));
        }

        InitMessage Message;
        Message.nbUnits = m_UnitIDs.size();

        for (unsigned int id : m_UnitIDs) {
            Message.parameters[id].xtinc =
                (events.exist("xtinc@U" + std::to_string(id))) ?
                vv::toDouble(events.get("xtinc@U" + std::to_string(id))) : 0.7;
            Message.parameters[id].Rue1 =
                (events.exist("Rue1@U" + std::to_string(id))) ?
                vv::toDouble(events.get("Rue1@U" + std::to_string(id))) : 2.8;
            Message.parameters[id].Rue2 =
                (events.exist("Rue2@U" + std::to_string(id))) ?
                vv::toDouble(events.get("Rue2@U" + std::to_string(id))) : 1.5;
            Message.parameters[id].r1Rue =
                (events.exist("r1Rue@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r1Rue@U" + std::to_string(id))) : 1.0;
            Message.parameters[id].r2Rue =
                (events.exist("r2Rue@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r2Rue@U" + std::to_string(id))) : 1.0;
            Message.parameters[id].hiMax =
                (events.exist("hiMax@U" + std::to_string(id))) ?
                vv::toDouble(events.get("hiMax@U" + std::to_string(id))) : 0.55;
            Message.parameters[id].r1hi =
                (events.exist("r1hi@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r1hi@U" + std::to_string(id))) : 0.6;
            Message.parameters[id].r2hi =
                (events.exist("r2hi@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r2hi@U" + std::to_string(id))) : 0.8;
            Message.parameters[id].rateHI =
                (events.exist("rateHI@U" + std::to_string(id))) ?
                vv::toDouble(events.get("rateHI@U" + std::to_string(id))) : 0.015;
            Message.parameters[id].dens =
                (events.exist("dens@U" + std::to_string(id))) ?
                vv::toDouble(events.get("dens@U" + std::to_string(id))) : 10.0;
            Message.parameters[id].p1logi =
                (events.exist("p1logi@U" + std::to_string(id))) ?
                vv::toDouble(events.get("p1logi@U" + std::to_string(id))) : 0.6847;
            Message.parameters[id].p2logi =
                (events.exist("p2logi@U" + std::to_string(id))) ?
                vv::toDouble(events.get("p2logi@U" + std::to_string(id))) :  0.01;
            Message.parameters[id].lai0 =
                (events.exist("lai0@U" + std::to_string(id))) ?
                vv::toDouble(events.get("lai0@U" + std::to_string(id))) : 0.0016;
            Message.parameters[id].r1sf =
                (events.exist("r1sf@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r1sf@U" + std::to_string(id))) : 0.6;
            Message.parameters[id].r2sf =
                (events.exist("r2sf@U" + std::to_string(id))) ?
                vv::toDouble(events.get("r2sf@U" + std::to_string(id))) : 0.8;
            Message.parameters[id].p1sen =
                (events.exist("p1sen@U" + std::to_string(id))) ?
                vv::toDouble(events.get("p1sen@U" + std::to_string(id))) : 0.00161;
            Message.parameters[id].p2sen =
                (events.exist("p2sen@U" + std::to_string(id))) ?
                vv::toDouble(events.get("p2sen@U" + std::to_string(id))) : 6.0;
            Message.parameters[id].soilDepth =
                (events.exist("soilDepth@U" + std::to_string(id))) ?
                vv::toDouble(events.get("soilDepth@U" + std::to_string(id))) : 1500.0;
            Message.parameters[id].maxDepth =
                (events.exist("maxDepth@U" + std::to_string(id))) ?
                vv::toDouble(events.get("maxDepth@U" + std::to_string(id))) : 1300.0;
            Message.parameters[id].rateDepth =
                (events.exist("rateDepth@U" + std::to_string(id))) ?
                vv::toDouble(events.get("rateDepth@U" + std::to_string(id))) : 1.63;
            Message.parameters[id].iniDepth = (
                events.exist("iniDepth@U" + std::to_string(id))) ?
                vv::toDouble(events.get("iniDepth@U" + std::to_string(id))) : 100;
            Message.parameters[id].tt_Sowing_Emergence =
                (events.exist("tt_Sowing_Emergence@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_Sowing_Emergence@U" + std::to_string(id))) : 80.0;
            Message.parameters[id].tt_Emergence_MaxLAI =
                (events.exist("tt_Emergence_MaxLAI@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_Emergence_MaxLAI@U" + std::to_string(id))) : 820.0;
            Message.parameters[id].tt_MaxLAI_Flowering =
                (events.exist("tt_MaxLAI_Flowering@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_MaxLAI_Flowering@U" + std::to_string(id))) : 90.0;
            Message.parameters[id].tt_Flowering_GrainAbort =
                (events.exist("tt_Flowering_GrainAbort@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_Flowering_GrainAbort@U" + std::to_string(id))) : 250.0;
            Message.parameters[id].tt_GrainAbort_LeafSen =
                (events.exist("tt_GrainAbort_LeafSen@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_GrainAbort_LeafSen@U" + std::to_string(id))) : 245.0;
            Message.parameters[id].tt_LeafSen_Maturity =
                (events.exist("tt_LeafSen_Maturity@U" + std::to_string(id))) ?
                vv::toDouble(events.get("tt_LeafSen_Maturity@U" + std::to_string(id))) : 355.0;
        }

        send(Message);

        std::string Reply;
        receive(Reply);

        for (unsigned int id : m_UnitIDs) {
            std::string modelName = "U" + std::to_string(id);
            const vz::BaseModel *model = createModelFromClass("class2CVWithoutCrop", modelName);

            //Linking everything and creating Variables
            for (auto in : model->getOutputPortList())
            {
                std::string inputPort = in.first + "@U" + std::to_string(id);
                addInputPort(getModelName(), inputPort);
                addConnection(modelName, in.first, getModelName(), inputPort);

                Var* v = new Var();
                v->init(this, inputPort, events);
                inputs.push_back(v);
            }

            for (auto out : model->getInputPortList())
            {
                std::string outputPort = out.first + "@U" + std::to_string(id);
                addOutputPort(getModelName(), outputPort);
                addConnection(getModelName(), outputPort, modelName, out.first);

                Var* v = new Var();
                v->init(this, outputPort, events);
                outputs.push_back(v);
            }
        }
        toto.init(this, "toto", events);
    }

    virtual ~multiClientINOUTCropFull() {};

    virtual void compute(const vd::Time& time)
    {
        for (unsigned int id : m_UnitIDs) {
            std::string variableNameValue;
            Variables::const_iterator itv;
            VarMono* v;

            variableNameValue = "ATPT@U" + std::to_string(id);

            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].ATPT = v->getVal(time, 0.0);

            variableNameValue = "Harvesting@U" + std::to_string(id);
            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].Harvesting = v->getVal(time, 0.0);

            variableNameValue = "Rad@U" + std::to_string(id);
            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].Rad = v->getVal(time, 0.0);

            variableNameValue = "Sowing@U" + std::to_string(id);
            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].Sowing = v->getVal(time, 0.0);

            variableNameValue = "Tmax@U" + std::to_string(id);
            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].Tmax = v->getVal(time, 0.0);

            variableNameValue = "Tmin@U" + std::to_string(id);
            itv = getVariables().find(variableNameValue);
            v = (VarMono*) itv->second;
            m_MessageCropFull.inputs[id].Tmin = v->getVal(time, 0.0);
        }

        send(m_MessageCropFull);

        receive(m_MessageCropFull);

        for (const std::pair<unsigned int, Output_t>& out : m_MessageCropFull.outputs) {
            std::string variableName;
            Variables::const_iterator itv;
            VarMono* v;

            variableName = "ALAI@U" + std::to_string(out.first);
            itv = getVariables().find(variableName);
            v = (VarMono*) itv->second;
            v->update(time, out.second.ALAI);

            variableName = "RootDepth@U" + std::to_string(out.first);
            itv = getVariables().find(variableName);
            v = (VarMono*) itv->second;
            v->update(time, out.second.RootDepth);
        }
        //toto = new vv::String("toto");
        //value::toStringValue(msgaction);
        toto = vv::reference(vv::String::create("toto"));
    }

private:
    std::vector<Var*> inputs;
    std::vector<Var*> outputs;

    std::vector<unsigned int> m_UnitIDs;

    template<typename T>
    void send(const T& message)
    {
        msgpack::sbuffer buffer;
        msgpack::pack(&buffer, message);

        zmq::message_t zmqMessage(buffer.size());

        memcpy(zmqMessage.data(), buffer.data(), buffer.size());
        m_Socket.send(zmqMessage);
    }

    template<typename T>
    void receive(T& message)
    {
      zmq::message_t zmqMessage;
      m_Socket.recv(&zmqMessage);

      msgpack::sbuffer buffer;
      buffer.write(static_cast<const char*>(zmqMessage.data()), zmqMessage.size());

      // deserialize it
      msgpack::unpacked unpackedMessage;
      msgpack::unpack(unpackedMessage, buffer.data(), buffer.size());
      unpackedMessage.get().convert(message);
    }

    RunMessage m_MessageCropFull;

    zmq::context_t m_Context;
    zmq::socket_t m_Socket;

    ValueVle toto;
};
}
DECLARE_EXECUTIVE(zmq_test::multiClientINOUTCropFull);
