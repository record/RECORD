#ifndef __COMMUNICATION_INTERFACE_CROPFULL_HPP__
#define __COMMUNICATION_INTERFACE_CROPFULL_HPP__

#include <map>
#include <msgpack.hpp>
#include <chrono>
#include <ctime>

const std::string BindPath = "/tmp/MIPP/vle";
const std::string EndPoint = "ipc://" + BindPath + "/vle_socket";

struct Param_t
{
    /* Parameters AGB*/
    double xtinc;
    double Rue1;
    double Rue2;
    double r1Rue;
    double r2Rue;
    double hiMax;
    double r1hi;
    double r2hi;
    double rateHI;

    /* Parameters LAI*/
    double dens;
    double p1logi;
    double p2logi;
    double lai0;
    double r1sf;
    double r2sf;
    double p1sen;
    double p2sen;

    /* Parameters Root*/
    double soilDepth;
    double maxDepth;
    double rateDepth;
    double iniDepth;

    /* Parameters TT*/
    double tt_Sowing_Emergence;
    double tt_Emergence_MaxLAI;
    double tt_MaxLAI_Flowering;
    double tt_Flowering_GrainAbort;
    double tt_GrainAbort_LeafSen;
    double tt_LeafSen_Maturity;

    MSGPACK_DEFINE(xtinc, Rue1, Rue2, r1Rue, r2Rue, hiMax, r1hi,
                   r2hi, rateHI, dens, p1logi, p2logi, lai0, r1sf, r2sf, p1sen,
                   p2sen, soilDepth, maxDepth, rateDepth, iniDepth,
                   tt_Sowing_Emergence,tt_Emergence_MaxLAI, tt_MaxLAI_Flowering,
                   tt_Flowering_GrainAbort, tt_GrainAbort_LeafSen,
                   tt_LeafSen_Maturity);
};

struct InitMessage
{
  unsigned int nbUnits;

  bool verbose;
  std::string refTime;
  long int refTime_ms;

  std::map<unsigned int, Param_t> parameters;

  MSGPACK_DEFINE(nbUnits, verbose, refTime, refTime_ms, parameters);
};


struct Input_t
{
  double ATPT;
  double Harvesting;
  double Rad;
  double Sowing;
  double Tmax;
  double Tmin;

  MSGPACK_DEFINE(ATPT, Harvesting, Rad, Sowing, Tmax, Tmin);
};

struct Output_t
{
  double ALAI;
  double RootDepth;

    MSGPACK_DEFINE(ALAI, RootDepth);
};

struct RunMessage
{
  //double timestep;
  //int duration;

  std::map<unsigned int, Input_t> inputs;
  std::map<unsigned int, Output_t> outputs;

  MSGPACK_DEFINE(inputs, outputs);
};


// =====================================================================
// =====================================================================

std::chrono::system_clock::time_point getRefTime(int hour, int min, int sec)
{

  std::time_t tt;
  std::time(&tt);

  std::time_t rawtime;
  struct std::tm * timeinfo;
  std::time (&rawtime);
  timeinfo = localtime (&rawtime);

  timeinfo->tm_hour = hour;
  timeinfo->tm_min  = min;
  timeinfo->tm_sec  = sec;
  tt                = std::mktime (timeinfo);

  return std::chrono::high_resolution_clock::from_time_t(tt);

}


// =====================================================================
// =====================================================================


long int delaySinceRef(const std::chrono::system_clock::time_point& refTime,const long int& refTime_ms)
{

  return (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now()-refTime).count() - 1000*refTime_ms);
}

// =====================================================================
// =====================================================================


std::string delaySinceRefToString(const std::chrono::system_clock::time_point& refTime,const long int& refTime_ms)
{

  std::stringstream ssMicrosec;
  long int microsec = delaySinceRef(refTime,refTime_ms);
  ssMicrosec << "\n- time : \33[32;1m";
  if ( (microsec > 999999999) || (microsec < 0) )
    ssMicrosec <<  "xxxxxxxxx µs";
  else
    ssMicrosec << std::setw(9) << microsec;
   ssMicrosec << "\33[0m (" << refTime_ms << ")\t";

  return ssMicrosec.str();
}

// =====================================================================
// =====================================================================


#endif /* __COMMUNICATION_INTERFACE_HPP__ */
