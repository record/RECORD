/**
 * @file src/Simple1.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2014-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time, UseCppLib_dep @@endtagdepends


#include <vle/DiscreteTime.hpp>// Correspond a l'extension DEVS utilise ici
#include <FnLib.hpp>// le fichier header des fonctions getLAI et getLAIref

// Raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

// Definition du namespace de la classe du modele
namespace examples {

class Simple1 : public DiscreteTimeDyn
{
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    Simple1(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events)
    {
        // Variables d'etat gerees par ce composant
        Lai.init(this,"Lai", events);

        // Lecture des valeurs de parametres dans les conditions du vpz (valeur par défaut si non fournies dans le vpz)
        mRUE = (events.exist("RUE")) ? vv::toDouble(events.get("RUE")) : 0.5;
        mRG = (events.exist("RG")) ? vv::toDouble(events.get("RG")) : 30;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~Simple1() { }

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& time)
    {
        std::cout << getModel().getName() << "\ttime:" << time << std::endl;
        Lai = FnLib::getLAI(Lai(-1), mRUE, mRG); // appel de la fonction getLAI définie dans FnLib.cpp
        std::cout  << "\t\tLai=f(" << Lai(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai() << std::endl << std::endl;
    }

    /**
     * @brief Methode appelée apres le dernier pas de temps de simulation
     */
    virtual void finish() { }

private:

    double mRUE;
    double mRG;

    Var Lai;


};

} // namespace vle example

DECLARE_DYNAMICS(examples::Simple1)
