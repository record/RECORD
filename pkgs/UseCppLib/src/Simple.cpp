/**
 * @file src/Simple.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2014-2017 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time, UseCppLib_dep @@endtagdepends


#include <vle/DiscreteTime.hpp>// Correspond a l'extension DEVS utilise ici
#include <FnLib.hpp>// le fichier header des fonctions getLAI et getLAIref
#include <ClassLib.hpp>// le fichier header de la classe maClasse

// Raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace vv = vle::value;

using namespace vle::discrete_time;

// Definition du namespace de la classe du modele
namespace examples {

class Simple : public DiscreteTimeDyn
{
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    Simple(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        DiscreteTimeDyn(atom, events), mC()
    {
        // Variables d'etat gerees par ce composant
        Lai.init(this,"Lai", events);
        Lai2.init(this,"Lai2", events);
        Lai3.init(this,"Lai3", events);
        Lai4.init(this,"Lai4", events);
        Lai5.init(this,"Lai5", events);

        // Lecture des valeurs de parametres dans les conditions du vpz (valeur par défaut si non fournies dans le vpz)
        mRUE = (events.exist("RUE")) ? vv::toDouble(events.get("RUE")) : 0.5;
        mRG = (events.exist("RG")) ? vv::toDouble(events.get("RG")) : 30;

        mC.setParam(mRUE, mRG); // initialisation de la classe maClasse (fixe paramètres)
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~Simple() { }

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& time)
    {
        std::cout << getModel().getName() << "\ttime:" << time << std::endl;

        Lai = FnLib::getLAI(Lai(-1), mRUE, mRG); // appel de la fonction getLAI définie dans FnLib.cpp
        std::cout  << "\t\tLai=f(" << Lai(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai() << std::endl;

        Lai2 = FnLib::getLAIref(Lai2(-1), mRUE, mRG); // appel de la fonction getLAIref définie dans FnLib.cpp (passage de RUE et RG par référence, ne copie pas les valeurs)
        std::cout  << "\t\tLai2=f(" << Lai2(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai2() << std::endl;

        Lai3 = FnLib::getLAIpoin(Lai3(-1), &mRUE, &mRG); // appel de la fonction getLAIpoin définie dans FnLib.cpp (passage de RUE et RG par pointeur, ne copie pas les valeurs)
        std::cout  << "\t\tLai3=f(" << Lai3(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai3() << std::endl;

        double tmp_Lai4 = Lai4(-1);
        FnLib::updateLAIref(tmp_Lai4, mRUE, mRG); // appel de la fonction updateLAIref définie dans FnLib.cpp (passage de RUE et RG par référence, ne copie pas les valeurs)
        Lai4 = tmp_Lai4;
        std::cout  << "\t\tLai4=f(" << Lai4(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai4() << std::endl;

        Lai5 = mC.getLai(Lai5(-1)); // appel de la methode getLAI définie dans la classe maClasse
        std::cout  << "\t\tLai5=f(" << Lai5(-1) << ", " << mRUE << ", " << mRG << ")=" << Lai5() << std::endl;

        std::cout << std::endl;
    }

    /**
     * @brief Methode appelée apres le dernier pas de temps de simulation
     */
    virtual void finish() { }

private:

    double mRUE;
    double mRG;
    ClassLib::maClasse mC;

    Var Lai;
    Var Lai2;
    Var Lai3;
    Var Lai4;
    Var Lai5;


};

} // namespace vle example

DECLARE_DYNAMICS(examples::Simple)
