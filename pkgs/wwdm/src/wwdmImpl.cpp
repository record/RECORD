/**
 * @file wwdmImpl.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// REFERENCES
// Makowski, D., Jeuffroy, M.-H., Gu�rif, M., 2004. Bayseian methods for
//  updating crop model predictions, applications for predicting biomass and
//  grain protein content. In: Bayseian Statistics and Quality Modelling in
//  the Agro-Food Production Chain (van Boeakel et al. eds), pp. 57-68.
//  Kluwer, Dordrecht.
// Monod, H., Naud, C., Makowski, D., 2006. Uncertainty and sensitivity
//  analysis for crop models. In: Working with Dynamic Crop Models (Wallach
//  D., Makowski D. and Jones J. eds), pp. 55-100. Elsevier, Amsterdam.

#include <wwdm.hpp>

wwdm::wwdm(const vle::devs::DynamicsInit& init,
               const vle::devs::InitEventList& events) :
    DiscreteTimeDyn(init, events)
{
    Eb = events.getDouble("Eb");
    Eimax = events.getDouble("Eimax");
    K = events.getDouble("K");
    Lmax = events.getDouble("Lmax");
    A = events.getDouble("A");
    B = events.getDouble("B");
    TI = events.getDouble("TI");

    Tmin.init(this, "Tmin", events);
    Tmax.init(this, "Tmax", events);
    RG.init(this, "RG", events);

    ST.init(this, "ST", events);
    LAI.init(this, "LAI", events);
    U.init(this, "U", events);

    initValue(0.);
}

wwdm::~wwdm() { }

void wwdm::compute(const vle::devs::Time& /* time */)
{
    PAR = 0.5 * 0.01 * RG();

    Tmean = std::max(0.0, (Tmin() + Tmax()) / 2);

    ST = ST(-1) + Tmean;

    LAI = std::max(0.0, Lmax * ((1 / (1 + std::exp(-A * (ST() - TI)))) -
                                std::exp(B * (ST() - Tr))));

    U = U(-1) + Eb * Eimax * (1 - std::exp(-K * LAI())) * PAR;
}

void wwdm::initValue(const vle::devs::Time& /*time*/)
{
    ST = 0;
    LAI = 0;
    U = 0;

    Tr = (1 / B) * std::log(1 + std::exp(A * TI));
}
