/**
 * @file wwdm.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// REFERENCES
// Makowski, D., Jeuffroy, M.-H., Gu�rif, M., 2004. Bayseian methods for
//  updating crop model predictions, applications for predicting biomass and
//  grain protein content. In: Bayseian Statistics and Quality Modelling in
//  the Agro-Food Production Chain (van Boeakel et al. eds), pp. 57-68.
//  Kluwer, Dordrecht.
// Monod, H., Naud, C., Makowski, D., 2006. Uncertainty and sensitivity
//  analysis for crop models. In: Working with Dynamic Crop Models (Wallach
//  D., Makowski D. and Jones J. eds), pp. 55-100. Elsevier, Amsterdam.

#include <vle/DiscreteTime.hpp>

class wwdm : public vle::discrete_time::DiscreteTimeDyn
{
public:
    wwdm(const vle::devs::DynamicsInit& init,
        const vle::devs::InitEventList& events);

    virtual ~wwdm();


    virtual void compute(const vle::devs::Time& /* time */);

    virtual void initValue(const vle::devs::Time& /*time*/);

private:
    double Eb;
    double Eimax;
    double K;
    double Lmax;
    double A;
    double B;
    double TI;

    double PAR;
    double Tmean;
    double Tr;

    vle::discrete_time::Var Tmin;
    vle::discrete_time::Var Tmax;
    vle::discrete_time::Var RG;

    vle::discrete_time::Var ST;
    vle::discrete_time::Var LAI;
    vle::discrete_time::Var U;
};
