/* INRA
 * H.Raynal -  raynal@toulouse.inra.fr
*/


/*
 * Copyright (C) 2008 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "MeteoW.hpp"

#include <vle/utils.hpp>
#include <iostream>
#include <fstream>


#define IsLeapYear(vYear)  ((!(vYear & 3) && (vYear % 100)) || !(vYear % 400)) //ft de test annee bissextile

using namespace vle;
using namespace devs;
using namespace std;

namespace tuto {
    
    MeteoW::MeteoW(const graph::AtomicModel& model,
		 const devs::InitEventList& lst) : 
	devs::Dynamics(model, lst)
    {
	file_path = value::toString(lst.get("meteo_file")); // nom et chemin d'acc�s au fichier de la s�rie climatique
    }
    
    MeteoW::~MeteoW()
   {
   }

    devs::Time  MeteoW::timeAdvance() const
    {
	switch (state) {
	case ETAT2:
	    return 1.0;
    case ETAT1:
	    return 0.0;
	default:
	    Throw(utils::InternalError, "MeteoW::timeAdvance()");
	}
    }

    devs::Time MeteoW::init(const devs::Time& /*time*/)
    {
        state=ETAT1;
	meteo_file.open(file_path.c_str());
	// Pour sauter la 1�re ligne du fichier meteo 
	if (meteo_file.good()) {
	   getline (meteo_file,line);
	}
        mois =0;
        jour =0;
        Tmin=0;
        Tmax=0;
        Rg =0;
        Tmoy=0;
	return 0.0;
    }
    
    void MeteoW::internalTransition(const devs::Time& /*time*/)
    {
	switch (state) {
	case ETAT2 :
	    updateState();
            state=ETAT1;
	    break;
	case ETAT1:
            state=ETAT2;
	    break;
	default:
	    Throw(utils::InternalError, "MeteoW::internalTransition");
	}
    }
    
    void  MeteoW::output(const devs::Time& currentTime, 
			devs::ExternalEventList& output) const
    {
	if (state == ETAT1) {
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("jour");
        ee << vle::devs::attribute("name", buildString("jour"));
        ee << vle::devs::attribute("value", buildDouble(jour));
        output.push_back(ee);
        }
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("mois");
        ee << vle::devs::attribute("name", buildString("mois"));
        ee << vle::devs::attribute("value", buildDouble(mois));
        output.push_back(ee);
        }
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("Tmoy");
        ee << vle::devs::attribute("name", buildString("Tmoy"));
        ee << vle::devs::attribute("value", buildDouble(Tmoy));
        output.push_back(ee);
        }
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("Rg");
        ee << vle::devs::attribute("name", buildString("Rg"));
        ee << vle::devs::attribute("value", buildDouble(Rg));
        output.push_back(ee);
        }
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("Tmin");
        ee << vle::devs::attribute("name", buildString("Tmin"));
        ee << vle::devs::attribute("value", buildDouble(Tmin));
        output.push_back(ee);
        }
        {
        vle::devs::ExternalEvent* ee = new vle::devs::ExternalEvent("Tmax");
        ee << vle::devs::attribute("name", buildString("Tmax"));
        ee << vle::devs::attribute("value", buildDouble(Tmax));
        output.push_back(ee);
        }
     
    }
}
    
    
    value::Value* MeteoW::observation(const devs::ObservationEvent& event) const
    {
/* Pour afficher une ou plusieurs variables, d�commenter ci-dessous ET d�commenter "event" dans l'en t�te de la fonction observation ci-dessus. On obtient:
    value::Value meteo::observation(const devs::ObservationEvent& event) const */

        if (event.onPort("obs_m_jour"))
	    return buildInteger(jour);

        if (event.onPort("obs_m_mois"))
	    return buildInteger(mois);

	if (event.onPort("obs_m_Tx"))
	    return buildDouble(Tmax);

	if (event.onPort("obs_m_Tn"))
	    return buildDouble(Tmin);
	
	if (event.onPort("obs_m_Tmoy"))
	    return buildDouble(Tmoy);
	
	if (event.onPort("obs_m_Rg"))
	    return buildDouble(Rg);
	
    }
    
    void MeteoW::updateState()
    {
	if (meteo_file.good()) getline (meteo_file,line); 
	stringstream ss(line);
	ss >> mois;
	ss >> jour;
	ss >> Tmin;
	ss >> Tmax;
	ss >> Rg;
	Tmoy = ((Tmax + Tmin ) / 2.0) ; 
    }
    
} // namespace tuto 

    

