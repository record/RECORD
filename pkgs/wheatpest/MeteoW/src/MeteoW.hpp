/* INRA
 * H.Raynal - raynal@toulouse.inra.fr
*/

/*
 * Copyright (C) 2008 - INRA
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef METEOW_HPP
#define METEOW_HPP

#include <iostream>
#include <fstream>

#include <vle/devs/Dynamics.hpp>

using namespace std;
using namespace vle;

namespace tuto {

    class MeteoW : public devs::Dynamics
    {
    public:
	
	MeteoW(const graph::AtomicModel& model,
	      const devs::InitEventList& lst);
	
	virtual ~MeteoW();
	
	virtual devs::Time timeAdvance() const;
	
	virtual devs::Time init(const devs::Time& time);

	virtual void internalTransition(const devs::Time& time);
	
	virtual void output(const devs::Time& currentTime, 
			    devs::ExternalEventList& output) const;

	virtual value::Value* observation(const devs::ObservationEvent& event) const;
	
	virtual void updateState();
	
    private:

	enum State {ETAT1, ETAT2};
	
	// Phase du mod�le
	State state;
	
	// Variables de travail
	ifstream meteo_file;
	string line;
	
	//! Parametres
	string file_path;
        int annee_sim;
	int annee_preced;
	
	//! Variables d'�tat du mod�le
	int mois;
	int jour;
	double Tmin;
	double Tmax;   
	double Rg;      
	double Tmoy;
	
    };
} // namespace  
 DECLARE_NAMED_DYNAMICS(MeteoW, tuto::MeteoW);
#endif


 
