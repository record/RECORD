/**
 * @file src/WHEATPEST.hpp
 * @author J.Soudais - INRA
 */

/*
 * Copyright (C) 2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef WHEATPEST_HPP  
#define WHEATPEST_HPP

#include <vle/extension/DifferenceEquation.hpp>
using namespace std;
using namespace vle;

namespace wheatpest { 

class WHEATPEST: public vle::extension::DifferenceEquation::Multiple
{
	
public:
    WHEATPEST(const graph::AtomicModel& model,
      const devs::InitEventList& events) :
     vle::extension::DifferenceEquation::Multiple(model, events)
  {
    // Variables d'etat gerees par ce composant "WHEATPEST"
    STEMP=createVar("STEMP");
    DVS=createVar("DVS");
    LAI=createVar("LAI");
    LEAFW=createVar("LEAFW");
    STEMW=createVar("STEMW");
    MAXST=createVar("MAXST");
    EARW=createVar("EARW");
    ROOTW=createVar("ROOTW");
    YIELD=createVar("YIELD");
    RGATT=createVar("RGATT");
    // A compl�ter
    
    // Variables  gerees par un autre  composant que "WHEATPEST" 
    // "METEO"
    Tmin = createSync("Tmin"); // Temperature minimale du jour (�C), cette variable est synchrone
    Tmax = createSync("Tmax"); // Temperature maximale du jour (�C), cette variable est synchrone
    Rg = createSync("Rg"); // Rayonnement gloabal du jour (MJ.m-2), cette variable est synchrone
    
    // "PESTS"
    RFAPH = createNosync("RFAPH"); // cette variable est synchrone
    RSAP = createNosync("RSAP"); // cette variable est synchrone
    
    // Lecture des parametres
    K = vle::value::toDouble(events.get("K"));
    FRDIST = vle::value::toDouble(events.get("FRDIST"));
    TBASE = vle::value::toDouble(events.get("TBASE"));
    AYIELD = vle::value::toDouble(events.get("AYIELD"));
    SLAT0 = vle::value::toDouble(events.get("SLAT0"));
    SLAT1 = vle::value::toDouble(events.get("SLAT1"));
    SLAT2 = vle::value::toDouble(events.get("SLAT2"));
    DVSSLAT0 = vle::value::toDouble(events.get("DVSSLAT0"));    
    DVSSLAT1 = vle::value::toDouble(events.get("DVSSLAT1"));    
    DVSSLAT2 = vle::value::toDouble(events.get("DVSSLAT2"));                 
    RRSENT0 = vle::value::toDouble(events.get("RRSENT0")); 
    RRSENT1 = vle::value::toDouble(events.get("RRSENT1")); 
    RRSENT2 = vle::value::toDouble(events.get("RRSENT2"));
    RRSENT3 = vle::value::toDouble(events.get("RRSENT3"));
    RRSENT4 = vle::value::toDouble(events.get("RRSENT4"));
    RRSENT5 = vle::value::toDouble(events.get("RRSENT5"));
    DVSRRSENT0 = vle::value::toDouble(events.get("DVSRRSENT0"));
    DVSRRSENT1 = vle::value::toDouble(events.get("DVSRRSENT1"));    
    DVSRRSENT2 = vle::value::toDouble(events.get("DVSRRSENT2"));    
    DVSRRSENT3 = vle::value::toDouble(events.get("DVSRRSENT3"));    
    DVSRRSENT4 = vle::value::toDouble(events.get("DVSRRSENT4"));    
    DVSRRSENT5 = vle::value::toDouble(events.get("DVSRRSENT5"));    
    CPST0 = vle::value::toDouble(events.get("CPST0"));
    CPST1 = vle::value::toDouble(events.get("CPST1")); 
    CPST2 = vle::value::toDouble(events.get("CPST2"));
    CPST3 = vle::value::toDouble(events.get("CPST3"));
    CPST4 = vle::value::toDouble(events.get("CPST4"));
    CPST5 = vle::value::toDouble(events.get("CPST5"));
    CPST6 = vle::value::toDouble(events.get("CPST6"));
    CPST7 = vle::value::toDouble(events.get("CPST7"));
    DVSCPST0 = vle::value::toDouble(events.get("DVSCPST0"));
    DVSCPST1 = vle::value::toDouble(events.get("DVSCPST1"));    
    DVSCPST2 = vle::value::toDouble(events.get("DVSCPST2"));    
    DVSCPST3 = vle::value::toDouble(events.get("DVSCPST3"));    
    DVSCPST4 = vle::value::toDouble(events.get("DVSCPST4"));    
    DVSCPST5 = vle::value::toDouble(events.get("DVSCPST5"));    
    DVSCPST6 = vle::value::toDouble(events.get("DVSCPST6"));    
    DVSCPST7 = vle::value::toDouble(events.get("DVSCPST7"));               
    CPLT0 = vle::value::toDouble(events.get("CPLT0"));
    DVSCPLT0 = vle::value::toDouble(events.get("DVSCPLT0"));        
    CPRT0 = vle::value::toDouble(events.get("CPRT0"));
    CPRT1 = vle::value::toDouble(events.get("CPRT1"));
    CPRT2 = vle::value::toDouble(events.get("CPRT2"));
    CPRT3 = vle::value::toDouble(events.get("CPRT3"));
    CPRT4 = vle::value::toDouble(events.get("CPRT4"));
    CPRT5 = vle::value::toDouble(events.get("CPRT5"));
    CPRT6 = vle::value::toDouble(events.get("CPRT6"));
    CPRT7 = vle::value::toDouble(events.get("CPRT7"));
    CPRT8 = vle::value::toDouble(events.get("CPRT8"));
    CPRT9 = vle::value::toDouble(events.get("CPRT9"));
    CPRT10 = vle::value::toDouble(events.get("CPRT10"));
    CPRT11 = vle::value::toDouble(events.get("CPRT11"));
    CPRT12 = vle::value::toDouble(events.get("CPRT12"));
    DVSCPRT0 = vle::value::toDouble(events.get("DVSCPRT0"));      
    DVSCPRT1 = vle::value::toDouble(events.get("DVSCPRT1"));      
    DVSCPRT2 = vle::value::toDouble(events.get("DVSCPRT2"));      
    DVSCPRT3 = vle::value::toDouble(events.get("DVSCPRT3"));      
    DVSCPRT4 = vle::value::toDouble(events.get("DVSCPRT4"));      
    DVSCPRT5 = vle::value::toDouble(events.get("DVSCPRT5"));      
    DVSCPRT6 = vle::value::toDouble(events.get("DVSCPRT6"));      
    DVSCPRT7 = vle::value::toDouble(events.get("DVSCPRT7"));      
    DVSCPRT8 = vle::value::toDouble(events.get("DVSCPRT8"));      
    DVSCPRT9 = vle::value::toDouble(events.get("DVSCPRT9"));      
    DVSCPRT10 = vle::value::toDouble(events.get("DVSCPRT10"));    
    DVSCPRT11 = vle::value::toDouble(events.get("DVSCPRT11"));                    
    DVSCPRT12 = vle::value::toDouble(events.get("DVSCPRT12")); 
                   
    // Lecture des variables d'entree
    RDIST0 = vle::value::toDouble(events.get("RDIST0")); 
    RDEV2 = vle::value::toDouble(events.get("RDEV2")); 
    //STTIME = vle::value::toDouble(events.get("STTIME")); 
    //FINTIM = vle::value::toDouble(events.get("FINTIM")); 
    TTFLOW = vle::value::toDouble(events.get("TTFLOW")); 
    TTMAT = vle::value::toDouble(events.get("TTMAT")); 
    TTEND = vle::value::toDouble(events.get("TTEND")); 
    DVS0 = vle::value::toDouble(events.get("DVS0")); 
    DVS1 = vle::value::toDouble(events.get("DVS1")); 
    DVS2 = vle::value::toDouble(events.get("DVS2")); 
    DVS3 = vle::value::toDouble(events.get("DVS3"));
    RUET1 = vle::value::toDouble(events.get("RUET1"));      
    RUET2 = vle::value::toDouble(events.get("RUET2"));      
    DVSRUET1 = vle::value::toDouble(events.get("DVSRUET1"));
    DVSRUET2 = vle::value::toDouble(events.get("DVSRUET2"));
    
    
  }       
    virtual ~WHEATPEST() {}; 
    
    virtual void compute(const vle::devs::Time&  time ) ;
    virtual void initValue(const vle::devs::Time&  time);

private:
    //Variables d'etat
    Var STEMP; /**< State variable Thermal time (�C.d)*/ 
    Var DVS; /**< State variable DeVelopment Stage related to thermal time (-)*/
    Var LAI; /**< State variable Leaf Area Index (m�.m-�)*/
    Var LEAFW; /**< State variable Leaves dry mass (g.m-�)*/
    Var STEMW; /**< State variable Stems dry mass (g.m-�)*/
    Var MAXST; /**< State variable Dry mass of stems for maximum STEMW variable (g.m-�)*/
    Var EARW; /**< State variable Ears dry mass (g.m-�)*/
    Var ROOTW; /**< State variable Roots dry mass (g.m-�)*/
    Var YIELD; /**< State variable Yield (g.m-�)*/
    Var RGATT; /**< State variable Potential biomass accumulation (g.m-2)*/ 
    
    //Variables  gerees par un autre  composant que "WHEATPEST"
    Sync Tmin; /**< Synchron variable Daily minimum temperature (�C)*/
    Sync Tmax; /**< Synchron variable Daily maximum temperature (�C)*/
    Sync Rg; /**< Synchron variable Daily global radiation (MJ.m-�)*/ 

    Nosync RSAP; /**< Synchron variable Quantity of assimilates diverted by Sitobion avenae aphid (g.m-�.d-1)*/
    Nosync RFAPH; /**< Synchron variable Reduction factor of RUE due to Sitobion Avenae aphid (-)*/
    //Sync RFBYDV; /**< Synchron variable Reduction factor of RUE due to Barley Yellow Dwarf Viruses (-)*/
    //Sync RFWEED; /**< Synchron variable Reduction factor of RUE due to weeds (-)*/
    //Sync RFTA; /**< Synchron variable Reduction factor of RUE due to Take All (-)*/
    //Sync RFSTDV; /**< Synchron variable Reduction factor of RUE due to Septoria tritici blotch (-)*/
    //Sync RFSNDV; /**< Synchron variable Reduction factor of RUE due to Septoria nodorum  (-)*/
    //Sync RFES; /**< Synchron variable Reduction factor of RUE due to Eyespot (-)*/
    //Sync RES3; /**< Synchron variable LODG*Fraction of tillers with severe Eyespot Symptoms (-)*/
    //Sync RFSES; /**< Synchron variable Reduction factor of RUE due to Sharp Eyespot (-)*/ 
    //Sync RFBFR; /**< Synchron variable Reduction factor of RUE due to Brown foot rot (-)*/ 
    //Sync RSTB; /**< Synchron variable Severity of Septoria tritici blotch (- ; 0-1) */
    //Sync RLR; /**< Synchron variable Severity of Leaf rust (- ; 0-1) */
    //Sync RSNB; /**< Synchron variable Severity of Septoria nodorum (- ; 0-1) */
    //Sync RSR; /**< Synchron variable Severity of Strip rust (- ; 0-1) */
    //Sync RPM; /**< Synchron variable Severity of Powdery Mildew (- ; 0-1) */
    //Sync RFFHB; /**< Synchron variable Fraction of kernels with fusarium head blight symptoms (- ; 0-1) */
    //Sync MRFSNDV; /**< Synchron variable Maximum value between 0 and RFSENDV Reduction factor of RUE due to Septoria nodorum (- ; 0-1) */
    //Sync MRFSTDV; /**< Synchron variable Maximum value between 0 and RFSTDV Reduction factor of RUE due to Septoria tritici (- ; 0-1) */
    //Sync MRFSR; /**< Synchron variable Maximum value between 0 and RFSR Reduction factor of RUE due to Stripe rust (- ; 0-1) */
    //Sync MRFLR; /**< Synchron variable Maximum value between 0 and RFLR Reduction factor of RUE due to Leaf rust (- ; 0-1) */


    //Parametres du modele
    double K; /**< Parametre: Coefficient of light extinction,  (-)*/ 
    double FRDIST; /**< Parametre: Fraction of stem dry weight translocated to ear between DVS1 and DVS2, (-)*/
    double TBASE; /**< Parametre: Temperature treshold for wheat growth, (�C)*/
    double AYIELD; /**< Parametre: Rate of ear biomass corresponding to yield (-, 0-1)*/
	double SLAT0; /**< Input variable: SLAT value at emergence, (m�.g-1)*/
    double SLAT1; /**< Input variable: SLAT value at flowering, (m�.g-1)*/
    double SLAT2; /**< Input variable: SLAT value at maturity, (m�.g-1)*/
	double DVSSLAT0; /**< Input variable: DVS value for SLAT0, (0-1)*/   
    double DVSSLAT1; /**< Input variable: DVS value for SLAT1, (0-1)*/    
    double DVSSLAT2; /**< Input variable: DVS value for SLAT0, (0-1)*/     
    double RRSENT0; /**< Input variable: RRSENT value at DVSRRSENT0, (-)*/
	double RRSENT1; /**< Input variable: RRSENT value at DVSRRSENT1, (-)*/
	double RRSENT2; /**< Input variable: RRSENT value at DVSRRSENT2, (-)*/
	double RRSENT3; /**< Input variable: RRSENT value at DVSRRSENT3, (-)*/
	double RRSENT4; /**< Input variable: RRSENT value at DVSRRSENT4, (-)*/
	double RRSENT5; /**< Input variable: RRSENT value at DVSRRSENT5, (-)*/
    double DVSRRSENT0; /**< Input variable: DVS value for RRSENT0, (-)*/
	double DVSRRSENT1; /**< Input variable: DVS value for RRSENT1, (-)*/   	
	double DVSRRSENT2; /**< Input variable: DVS value for RRSENT2, (-)*/      	
	double DVSRRSENT3; /**< Input variable: DVS value for RRSENT3, (-)*/      	
	double DVSRRSENT4; /**< Input variable: DVS value for RRSENT4, (-)*/      	
	double DVSRRSENT5; /**< Input variable: DVS value for RRSENT5, (-)*/
	double CPST0; /**< Coefficient of partitionning in stems within shoots at DVS0, (-, 0-1)*/
	double CPST1; /**< Coefficient of partitionning in stems within shoots at DVS1, (-, 0-1)*/
	double CPST2; /**< Coefficient of partitionning in stems within shoots at DVS2, (-, 0-1)*/
	double CPST3; /**< Coefficient of partitionning in stems within shoots at DVS3, (-, 0-1)*/
	double CPST4; /**< Coefficient of partitionning in stems within shoots at DVS4, (-, 0-1)*/
	double CPST5; /**< Coefficient of partitionning in stems within shoots at DVS5, (-, 0-1)*/
	double CPST6; /**< Coefficient of partitionning in stems within shoots at DVS6, (-, 0-1)*/
	double CPST7; /**< Coefficient of partitionning in stems within shoots at DVS7, (-, 0-1)*/
	double DVSCPST0; /**< DVS for CPST0, (0-2.1)*/
	double DVSCPST1; /**< DVS for CPST1, (0-2.1)*/
	double DVSCPST2; /**< DVS for CPST2, (0-2.1)*/
	double DVSCPST3; /**< DVS for CPST3, (0-2.1)*/
	double DVSCPST4; /**< DVS for CPST4, (0-2.1)*/
	double DVSCPST5; /**< DVS for CPST5, (0-2.1)*/
	double DVSCPST6; /**< DVS for CPST6, (0-2.1)*/
	double DVSCPST7; /**< DVS for CPST7, (0-2.1)*/
	double CPLT0; /**< Coefficient of partitionning in leaves within shoots at DVS > 0.95, (-, 0-1)*/
	double DVSCPLT0; /**< DVS for CPLT0, (0-2.1)*/	
    double CPRT0; /**< Coefficient of partitionning in roots at DVS0, (-, 0-1)*/
	double CPRT1; /**< Coefficient of partitionning in roots at DVS 0.1, (-, 0-1)*/
	double CPRT2; /**< Coefficient of partitionning in roots at DVS 0.2, (-, 0-1)*/
	double CPRT3; /**< Coefficient of partitionning in roots at DVS 0.35, (-, 0-1)*/
	double CPRT4; /**< Coefficient of partitionning in roots at DVS 0.4, (-, 0-1)*/
	double CPRT5; /**< Coefficient of partitionning in roots at DVS 0.5, (-, 0-1)*/
	double CPRT6; /**< Coefficient of partitionning in roots at DVS 0.6, (-, 0-1)*/
	double CPRT7; /**< Coefficient of partitionning in roots at DVS 0.7, (-, 0-1)*/
	double CPRT8; /**< Coefficient of partitionning in roots at DVS 0.8, (-, 0-1)*/
	double CPRT9; /**< Coefficient of partitionning in roots at DVS 0.9, (-, 0-1)*/
	double CPRT10; /**< Coefficient of partitionning in roots at DVS 1., (-, 0-1)*/
	double CPRT11; /**< Coefficient of partitionning in roots at DVS 1.1, (-, 0-1)*/
	double CPRT12; /**< Coefficient of partitionning in roots at DVS 1.2, (-, 0-1)*/
    double DVSCPRT0; /**< DVS for CPRT0, (0-2.1)*/ 
	double DVSCPRT1; /**< DVS for CPRT1, (0-2.1)*/ 	
	double DVSCPRT2; /**< DVS for CPRT2, (0-2.1)*/ 	
	double DVSCPRT3; /**< DVS for CPRT3, (0-2.1)*/	
	double DVSCPRT4; /**< DVS for CPRT4, (0-2.1)*/ 	
	double DVSCPRT5; /**< DVS for CPRT5, (0-2.1)*/ 	
	double DVSCPRT6; /**< DVS for CPRT6, (0-2.1)*/ 	
	double DVSCPRT7; /**< DVS for CPRT7, (0-2.1)*/ 	
	double DVSCPRT8; /**< DVS for CPRT8, (0-2.1)*/ 	
	double DVSCPRT9; /**< DVS for CPRT9, (0-2.1)*/ 	
	double DVSCPRT10; /**< DVS for CPRT10, (0-2.1)*/ 	
	double DVSCPRT11; /**< DVS for CPRT11, (0-2.1)*/	
	double DVSCPRT12; /**< DVS for CPRT12, (0-2.1)*/	
	
	//Variables d'entr�e du mod�le
    double RDIST0; /**< Input variable: Value of RDIST from DVS0 to DVS1, (-)*/
	double RDEV2; /**< Input variable: Rate of development per degree before and after anthesis, RDEV2=1/thermal time between DVS1 and DVS2, (-)*/ 
    //double STTIME; /**< Input variable: Julian day of begining of simulation, (-)*/ 
    //double FINTIM; /**< Input variable: Julian day of end of simulation, (-)*/ 
    double TTFLOW; /**< Input variable: Thermal time from emergence (DVS0) to flowering (DVS1), (�C.d)*/
    double TTMAT; /**< Input variable: Thermal time from emergence (DVS0) to maturity (DVS2), (�C.d)*/
    double TTEND; /**< Input variable: Thermal time from emergence (DVS0) to post-maturity (DVS2.1), (�C.d)*/
    double DVS0; /**< Input variable: DVS value at emergence, (0-2.1)*/  
    double DVS1; /**< Input variable: DVS value at flowering, (0-2.1)*/  
    double DVS2; /**< Input variable: DVS value at maturity, (0-2.1)*/   
    double DVS3; /**< Input variable: DVS value after maturity, (0-2.1)*/
    double RUET1; /**< Input variable: Coefficient of RUE from emergence to flowering, (g.MJ-1)*/
    double RUET2; /**< Input variable: Coefficient of RUE from flowering to end of simulation, (g.MJ-1)*/
    double DVSRUET1; /**< Input variable: DVS value for RUET1, (0-2.1)*/     
    double DVSRUET2; /**< Input variable: DVS value for RUET2, (0-2.1)*/                       

};
	
}  // fin du namespace

DECLARE_NAMED_DYNAMICS(WHEATPEST, wheatpest::WHEATPEST); // balise specifique VLE

#endif
