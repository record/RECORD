/**
 * @file src/RUN.cpp
 * @author  J.Soudais -INRA
 */

/*
 * Copyright (C)  2009 INRA 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <RUN.hpp>

using namespace vle;
using namespace std;

namespace run {

void RUN::compute(const vle::devs::Time&  time ) 
{
   // Variables d'etat
   PAPH = PAPHR; 
   PBYDV = PBYDVR; 
   PWEED = PWEEDR; 
   PTA = PTAR; 
   PSTB = PSTBR; 
   PLR = PLRR; 
   PFHB = PFHBR; 
   PPM = PPMR; 
   PES = PESR; 
   PSR = PSRR; 
   PSNB = PSNBR; 
   PSES = PSESR; 
   PBFR = PBFRR;    

}
void RUN::initValue(const vle::devs::Time& time )
{

	init(PAPH,0.0); 
    init(PBYDV,0.0);
    init(PWEED,0.0);
    init(PTA,0.0);
    init(PSTB,0.0);
    init(PLR,0.0);
    init(PFHB,0.0); 
	init(PPM,0.0);
	init(PES,0.0);
	init(PSR,0.0);
	init(PSNB,0.0); 
	init(PSES,0.0); 
	init(PBFR,0.0); 	
} 

}
