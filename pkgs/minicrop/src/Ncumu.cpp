/**
 * @file Ncumu.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class Ncumu : public DiscreteTimeDyn
{
public:
    Ncumu(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        M = evts.getDouble("M");
        C = evts.getDouble("C");
        Rsh = evts.getDouble("Rsh");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));

        //Var
        _Ncumu.init(this,"Ncumu", evts);
        //Nosync
        T.init(this,"T", evts);
        Dose.init(this,"Dose", evts);
    }

    virtual ~Ncumu() { }

    virtual void compute(const vd::Time& time)
    {
        if (time+begin_time < Joursh) {
            _Ncumu = 0.;
        } else {
            if (time+begin_time == Joursh) {
                _Ncumu = Rsh;
                // NCUM[j]<-NCUM[j-1]+M*(max(0,TMOY[j-1]))+C*DOSE[j-1]
            } else {
                _Ncumu = _Ncumu(-1) + M * (std::max(0., T(-1))) + C * Dose(-1);
            }
        }
    }

private:
    double M;
    double C;
    double Rsh;
    double Joursh;
    double begin_time;

    Var _Ncumu;
    Var T;
    Var Dose;
};

};

DECLARE_DYNAMICS(minicrop::Ncumu);
