/**
 * @file ft.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class ft : public DiscreteTimeDyn
{
public:
    ft(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        Tmin = evts.getDouble("Tmin");
        Tmax = evts.getDouble("Tmax");
        Topt = evts.getDouble("Topt");

        //Var
        _ft.init(this,"ft", evts);
        //Sync
        T.init(this,"T", evts);
        getOptions().syncs.insert(std::make_pair("T", 1));
    }

    virtual ~ft() { }

    virtual void compute(const vd::Time& /* time */)
    {
        if (T() <= Tmin) {
            _ft = 0.;
            //FT[TMOY>Tmin & TMOY<Topt]<-1-((TMOY[TMOY>Tmin & TMOY<Topt]-Topt)/(Tmin-Topt))^2
        } else if (T() < Topt) {
            double t1 = (T() - Topt)/(Tmin - Topt);

            _ft = 1. - t1 * t1;
//FT[TMOY>=Topt & TMOY<Tmax]<-1-((TMOY[TMOY>=Topt & TMOY<Tmax]-Topt)/(Tmax-Topt))^2
        } else if (T() < Tmax) {
            double t2 = (T() - Topt)/(Tmax - Topt);

            _ft = 1. - t2 * t2;
        } else {
            _ft = 0.;
        }
    }

private:
    double Tmin;
    double Tmax;
    double Topt;

    Var _ft;
    Var T;
};

};

DECLARE_DYNAMICS(minicrop::ft);
