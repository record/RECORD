/**
 * @file System.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class System : public DiscreteTimeDyn
{

public:
    System(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        Tmin = evts.getDouble("Tmin");
        Tmax = evts.getDouble("Tmax");
        Topt = evts.getDouble("Topt");
        Vmax = evts.getDouble("Vmax");
        Nnotav = evts.getDouble("Nnotav");
        R = evts.getDouble("R");
        Beta1 = evts.getDouble("Beta1");
        Beta2 = evts.getDouble("Beta2");
        Beta3 = evts.getDouble("Beta3");
        Beta4 = evts.getDouble("Beta4");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));
        K = evts.getDouble("K");
        H = evts.getDouble("H");
        Ebmax = evts.getDouble("Ebmax");
        Eimax = evts.getDouble("Eimax");
        Theta1 = evts.getDouble("Theta1");
        Theta2 = evts.getDouble("Theta2");
        Theta3 = evts.getDouble("Theta3");
        A = evts.getDouble("A");
        B2 = evts.getDouble("B");
        T1 = evts.getDouble("T1");
        LMAX = evts.getDouble("LMAX");
        ALPHA1 = evts.getDouble("ALPHA1");
        ALPHA2 = evts.getDouble("ALPHA2");
        ALPHA3 = evts.getDouble("ALPHA3");
        M = evts.getDouble("M");
        C = evts.getDouble("C");
        Rsh = evts.getDouble("Rsh");
        T2 = (1. / B2) * log(1. + exp(A * T1));
        ST = 0.;
        NUsh = 0.;

        //Var
        ft.init(this,"ft", evts);
        B.init(this,"B", evts);
        NU.init(this,"NU", evts);
        NUc.init(this,"NUc", evts);
        INN.init(this,"INN", evts);
        LAI.init(this,"LAI", evts);
        Ncumu.init(this,"Ncumu", evts);
        //Sync
        T.init(this,"T", evts);
        getOptions().syncs.insert(std::make_pair("T", 1));
        RG.init(this,"RG", evts);
        getOptions().syncs.insert(std::make_pair("RG", 1));
        Dose.init(this,"Dose", evts);
        getOptions().syncs.insert(std::make_pair("Dose", 1));
    }

    virtual ~System() { }

    virtual void compute(const vd::Time& time)
    {
        // ft
        if (T() <= Tmin) {
            ft = 0.;
            //FT[TMOY>Tmin & TMOY<Topt]<-1-((TMOY[TMOY>Tmin & TMOY<Topt]-Topt)/(Tmin-Topt))^2
        } else if (T() < Topt) {
            double t1 = (T() - Topt)/(Tmin - Topt);

            ft = 1. - t1 * t1;
//FT[TMOY>=Topt & TMOY<Tmax]<-1-((TMOY[TMOY>=Topt & TMOY<Tmax]-Topt)/(Tmax-Topt))^2
        } else if (T() < Tmax) {
            double t2 = (T() - Topt)/(Tmax - Topt);

            ft = 1. - t2 * t2;
        } else {
            ft = 0.;
        }

        // B
        double Eb;
        if (time+begin_time <= Joursh) {
            // EB.jm1<-EBMAX*FT[j-1]
            // BIOM[j]<-BIOM[j-1] + EB.jm1*EIMAX*(1-exp(-K*LAI[j-1]))*PAR[j-1]
            Eb = Ebmax * ft(-1);
        } else {
            // EB.jm1<-min(EBMAX,EBMAX*THET1*(1-THET2*exp(-THET3*INN[j-1])))
            // BIOM[j]<-BIOM[j-1] + EB.jm1*FT[j-1]*EIMAX*(1-exp(-K*LAI[j-1]))*PAR[j-1]
            Eb = std::min(Ebmax, Ebmax * Theta1 * (1. - Theta2 * exp(-Theta3 * INN(-1)))) * ft(-1);
        }
        B = B(-1) + 10000. * Eb * Eimax * (1. - exp(-K * LAI(-1))) * RG(-1) * H;

        // NU
        if (time+begin_time < Joursh) {
            NU = 0.;
        } else {
            // NU[NumSh]<-BIOM[NumSh]*BETH1/100
            // if ((BIOM[NumSh]/1000)>=BETH2) {NU[NumSh]<-(BIOM[NumSh]/100)*BETH3*(BIOM[NumSh]/1000)^BETH4}
            // NU[NumSh]<-NU[NumSh]*R
            if (time+begin_time == Joursh) {
                double value;

                if (B() / 1000. < Beta2) {
                    value = B() * Beta1 / 100;
                } else {
                    value = (B() / 100) * Beta3 * std::pow((B() / 1000), Beta4);
                }
                NUsh = value * R;
                NU =  value;
                // DELTA.NU<-min(VMAX*(max(0,TMOY[j-1])),max(0,NCUM[j-1]-NU[j-1]+NU[NumSh]-NNOTAV))
                // NU[j]<-NU[j-1]+DELTA.NU
            } else {
                double delta = std::min(Vmax * (std::max(0., T(-1))),
                                        std::max(0., Ncumu(-1)
                                                 - NU(-1) + NUsh
                                                 - Nnotav));

                NU = NU(-1) + delta;
            }
        }


        // NUc
        if (time+begin_time <= Joursh) {
            NUc = 0.;
            // NUc[j]<-BIOM[j]*BETH1/100
            // if ((BIOM[j]/1000)>=BETH2) {NUc[j]<-(BIOM[j]/100)*BETH3*(BIOM[j]/1000)^BETH4}
        } else {
            if (B() / 1000. < Beta2) {
                NUc = B() * Beta1 / 100.;
            } else {
                NUc = (B() / 100.) * Beta3 * std::pow((B() / 1000.), Beta4);
            }
        }

        // INN
        if (time+begin_time < Joursh) {
            INN = 0.;
        // INN[NumSh]<-1
        } else if (time+begin_time == Joursh) {
            INN = 1.;
        // INN[j]<-(NU[j]/R)/NUc[j]
        } else {
            INN = (NU() / R) / NUc();
        }


        // LAI

        ST += std::max(0., T());

        double LAImax = LMAX * ((1. / (1. + exp(-A * (ST - T1)))) - exp(B2 * (ST - T2)));

        if (time+begin_time <= Joursh) {
            LAI = LAImax;
        } else {
            LAI = std::min(LAImax, LAImax * ALPHA1 * (1. - ALPHA2 * exp(-ALPHA3 * INN())));
        }

        // Ncumu
        if (time+begin_time < Joursh) {
            Ncumu = 0.;
        } else {
            if (time+begin_time == Joursh) {
                Ncumu = Rsh;
                // NCUM[j]<-NCUM[j-1]+M*(max(0,TMOY[j-1]))+C*DOSE[j-1]
            } else {
                Ncumu = Ncumu(-1) + M * (std::max(0., T(-1))) + C * Dose(-1);
            }
        }
    }

private:

    double Tmin;
    double Tmax;
    double Topt;
    double A;
    double B2;
    double T1;
    double T2;
    double LMAX;
    double ST;
    double ALPHA1;
    double ALPHA2;
    double ALPHA3;
    double Joursh;
    double R;
    double Vmax;
    double Nnotav;
    double Beta1;
    double Beta2;
    double Beta3;
    double Beta4;
    double NUsh;
    double K;
    double H;
    double Ebmax;
    double Eimax;
    double Theta1;
    double Theta2;
    double Theta3;
    double M;
    double C;
    double Rsh;
    double begin_time;
    
    Var ft;
    Var INN;
    Var LAI;
    Var B;
    Var Ncumu;
    Var NU;
    Var NUc;

    Var T;
    Var RG;
    Var Dose;

};

};

DECLARE_DYNAMICS(minicrop::System);
