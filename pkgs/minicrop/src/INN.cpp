/**
 * @file INN.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class INN : public DiscreteTimeDyn
{
public:
    INN(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        R = evts.getDouble("R");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));

        //Var
        _INN.init(this,"INN", evts);
        //Sync
        NU.init(this,"NU", evts);
        getOptions().syncs.insert(std::make_pair("NU", 1));
        NUc.init(this,"NUc", evts);
        getOptions().syncs.insert(std::make_pair("NUc", 1));
    }

    virtual ~INN() { }

    virtual void compute(const vd::Time& time)
    {
        if (time+begin_time < Joursh) {
            _INN = 0.;
        // INN[NumSh]<-1
        } else if (time+begin_time == Joursh) {
            _INN = 1.;
        // INN[j]<-(NU[j]/R)/NUc[j]
        } else {
            _INN = (NU() / R) / NUc();
        }
    }

private:
    double R;
    double Joursh;
    double begin_time;

    Var _INN;
    Var NU;
    Var NUc;
};

};

DECLARE_DYNAMICS(minicrop::INN);
