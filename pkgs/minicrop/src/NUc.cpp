/**
 * @file NUc.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class NUc : public DiscreteTimeDyn
{
public:
    NUc(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        Beta1 = evts.getDouble("Beta1");
        Beta2 = evts.getDouble("Beta2");
        Beta3 = evts.getDouble("Beta3");
        Beta4 = evts.getDouble("Beta4");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));

        //Var
        _NUc.init(this,"NUc", evts);
        //Sync
        B.init(this,"B", evts);
        getOptions().syncs.insert(std::make_pair("B", 1));
    }

    virtual ~NUc() { }

    virtual void compute(const vd::Time& time)
    {
        if (time+begin_time <= Joursh) {
            _NUc = 0.;
            // NUc[j]<-BIOM[j]*BETH1/100
            // if ((BIOM[j]/1000)>=BETH2) {NUc[j]<-(BIOM[j]/100)*BETH3*(BIOM[j]/1000)^BETH4}
        } else {
            if (B() / 1000. < Beta2) {
                _NUc = B() * Beta1 / 100.;
            } else {
                _NUc = (B() / 100.) * Beta3 * std::pow((B() / 1000.), Beta4);
            }
        }
    }

private:
    double Beta1;
    double Beta2;
    double Beta3;
    double Beta4;
    double Joursh;
    double begin_time;

    Var _NUc;
    Var B;
};

};

DECLARE_DYNAMICS(minicrop::NUc);
