/**
 * @file LAI.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class LAI : public DiscreteTimeDyn
{
public:
    LAI(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        A = evts.getDouble("A");
        B = evts.getDouble("B");
        T1 = evts.getDouble("T1");
        LMAX = evts.getDouble("LMAX");
        ALPHA1 = evts.getDouble("ALPHA1");
        ALPHA2 = evts.getDouble("ALPHA2");
        ALPHA3 = evts.getDouble("ALPHA3");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));

        T2 = (1. / B) * log(1. + exp(A * T1));
        ST = 0.;

        //Var
        _LAI.init(this,"LAI", evts);
        //Sync
        INN.init(this,"INN", evts);
        getOptions().syncs.insert(std::make_pair("INN", 1));
        T.init(this,"T", evts);
        getOptions().syncs.insert(std::make_pair("T", 1));
    }

    virtual ~LAI() { }

    virtual void compute(const vd::Time& time)
    {
        ST += std::max(0., T());

        double LAImax = LMAX * ((1. / (1. + exp(-A * (ST - T1)))) - exp(B * (ST - T2)));

        if (time+begin_time <= Joursh) {
            _LAI = LAImax;
        } else {
            _LAI = std::min(LAImax, LAImax * ALPHA1 * (1. - ALPHA2 * exp(-ALPHA3 * INN())));
        }
    }

private:
    double A;
    double B;
    double T1;
    double T2;
    double LMAX;
    double ST;
    double ALPHA1;
    double ALPHA2;
    double ALPHA3;
    double Joursh;
    double begin_time;

    Var _LAI;
    Var INN;
    Var T;
};

};

DECLARE_DYNAMICS(minicrop::LAI);
