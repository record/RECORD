/**
 * @file B.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2008-2017 INRA
 * Copyright (C) 2008-2017 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// @@tagdynamic@@
// @@tagdepends: vle.discrete-time @@endtagdepends

#include <vle/DiscreteTime.hpp>
#include <vle/utils/DateTime.hpp>

namespace vd = vle::devs;
namespace vv = vle::value;
namespace vu = vle::utils;
using namespace vle::discrete_time;

namespace minicrop {

class B : public DiscreteTimeDyn
{
public:
    B(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
            : DiscreteTimeDyn(atom, evts)
    {
        std::string  begin_date;
        if (evts.get("begin_date")->isDouble()) {
            begin_time = evts.getDouble("begin_date");
            begin_date = vu::DateTime::toJulianDay(begin_time);
        } else if (evts.get("begin_date")->isString()) {
            begin_date = evts.getString("begin_date");
            begin_time = vu::DateTime::toJulianDay(begin_date);
        }
        K = evts.getDouble("K");
        H = evts.getDouble("H");
        Ebmax = evts.getDouble("Ebmax");
        Eimax = evts.getDouble("Eimax");
        Theta1 = evts.getDouble("Theta1");
        Theta2 = evts.getDouble("Theta2");
        Theta3 = evts.getDouble("Theta3");
        Joursh = vu::DateTime::toJulianDayNumber(evts.getString("Joursh"));

        //Var
        _B.init(this,"B", evts);
        //Nosync
        LAI.init(this,"LAI", evts);
        RG.init(this,"RG", evts);
        INN.init(this,"INN", evts);
        ft.init(this,"ft", evts);
    }

    virtual ~B() { }

    virtual void compute(const vd::Time& time)
    {
        double Eb;
        if (time+begin_time <= Joursh) {
            // EB.jm1<-EBMAX*FT[j-1]
            // BIOM[j]<-BIOM[j-1] + EB.jm1*EIMAX*(1-exp(-K*LAI[j-1]))*PAR[j-1]
            Eb = Ebmax * ft(-1);
        } else {
            // EB.jm1<-min(EBMAX,EBMAX*THET1*(1-THET2*exp(-THET3*INN[j-1])))
            // BIOM[j]<-BIOM[j-1] + EB.jm1*FT[j-1]*EIMAX*(1-exp(-K*LAI[j-1]))*PAR[j-1]
            Eb = std::min(Ebmax, Ebmax * Theta1 * (1. - Theta2 * exp(-Theta3 * INN(-1)))) * ft(-1);
        }
        _B = _B(-1) + 10000. * Eb * Eimax * (1. - exp(-K * LAI(-1))) * RG(-1) * H;
    }

private:
    double K;
    double H;
    double Ebmax;
    double Eimax;
    double Theta1;
    double Theta2;
    double Theta3;
    double Joursh;
    double begin_time;


    Var _B;
    Var LAI;
    Var RG;
    Var INN;
    Var ft;
};
};

DECLARE_DYNAMICS(minicrop::B);
