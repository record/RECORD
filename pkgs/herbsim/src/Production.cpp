/**
 * @file src/Production.cpp
 * @author (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/extension/difference-equation/Multiple.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs/DynamicsDbg.hpp>


// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

    class Production: public ve::DifferenceEquation::Multiple
    {
	public:
	    Production(const vd::DynamicsInit& atom,
		   const vd::InitEventList& events) :
	ve::DifferenceEquation::Multiple(atom, events)
	    {
			Final_biomasseB = createVar("Final_biomasseB"); 
			Final_biomasseV_SPart = createVar("Final_biomasseV_SPart"); 
			Final_biomasseV_STot = createVar("Final_biomasseV_STot"); 
			Final_digestibiliteInVitro = createVar("Final_digestibiliteInVitro"); 			
			Final_digestibilite = createVar("Final_digestibilite"); 			
			Final_ValeurEnergetique = createVar("Final_ValeurEnergetique"); 			
			Final_Matieres_Azotees = createVar("Final_Matieres_Azotees");
			Final_CroissanceB = createVar("Final_CroissanceB");
			Final_CroissanceN_SPart = createVar("Final_CroissanceN_SPart");
			Final_CroissanceN_STot = createVar("Final_CroissanceN_STot");

			CroissanceB_GA = createSync("croissanceB_GA"); 	
			CroissanceB_GB = createSync("croissanceB_GB"); 
			CroissanceB_GC = createSync("croissanceB_GC"); 
			CroissanceB_GD = createSync("croissanceB_GD");
			CroissanceB_GA_dicot = createSync("croissanceB_GA_dicot"); 	
			CroissanceB_GB_dicot = createSync("croissanceB_GB_dicot"); 
			CroissanceB_GC_dicot = createSync("croissanceB_GC_dicot"); 
			CroissanceB_GD_dicot = createSync("croissanceB_GD_dicot");
			CroissanceB_L = createSync("croissanceB_L");
			CroissanceB_TB = createSync("croissanceB_TB");
			CroissanceN_SPart_GA = createSync("croissanceN_SPart_GA"); 	
			CroissanceN_SPart_GB = createSync("croissanceN_SPart_GB"); 
			CroissanceN_SPart_GC = createSync("croissanceN_SPart_GC"); 
			CroissanceN_SPart_GD = createSync("croissanceN_SPart_GD");
			CroissanceN_SPart_GA_dicot = createSync("croissanceN_SPart_GA_dicot"); 	
			CroissanceN_SPart_GB_dicot = createSync("croissanceN_SPart_GB_dicot"); 
			CroissanceN_SPart_GC_dicot = createSync("croissanceN_SPart_GC_dicot"); 
			CroissanceN_SPart_GD_dicot = createSync("croissanceN_SPart_GD_dicot");
			CroissanceN_SPart_L = createSync("croissanceN_SPart_L");
			CroissanceN_SPart_TB = createSync("croissanceN_SPart_TB");
			CroissanceN_STot_GA = createSync("croissanceN_STot_GA"); 	
			CroissanceN_STot_GB = createSync("croissanceN_STot_GB"); 
			CroissanceN_STot_GC = createSync("croissanceN_STot_GC"); 
			CroissanceN_STot_GD = createSync("croissanceN_STot_GD");
			CroissanceN_STot_GA_dicot = createSync("croissanceN_STot_GA_dicot"); 	
			CroissanceN_STot_GB_dicot = createSync("croissanceN_STot_GB_dicot"); 
			CroissanceN_STot_GC_dicot = createSync("croissanceN_STot_GC_dicot"); 
			CroissanceN_STot_GD_dicot = createSync("croissanceN_STot_GD_dicot");
			biomasseB_GA = createSync("biomasseB_GA"); 	
			biomasseB_GB = createSync("biomasseB_GB"); 
			biomasseB_GC = createSync("biomasseB_GC"); 
			biomasseB_GD = createSync("biomasseB_GD");
			biomasseB_GA_dicot = createSync("biomasseB_GA_dicot"); 	
			biomasseB_GB_dicot = createSync("biomasseB_GB_dicot"); 
			biomasseB_GC_dicot = createSync("biomasseB_GC_dicot"); 
			biomasseB_GD_dicot = createSync("biomasseB_GD_dicot"); 
			biomasseB_L = createSync("biomasseB_L"); 	
			biomasseB_TB = createSync("biomasseB_TB"); 	
			biomasseV_SPart_GA = createSync("biomasseV_SPart_GA"); 	
			biomasseV_SPart_GB = createSync("biomasseV_SPart_GB"); 
			biomasseV_SPart_GC = createSync("biomasseV_SPart_GC"); 
			biomasseV_SPart_GD = createSync("biomasseV_SPart_GD");
			biomasseV_SPart_GA_dicot = createSync("biomasseV_SPart_GA_dicot"); 	
			biomasseV_SPart_GB_dicot = createSync("biomasseV_SPart_GB_dicot"); 
			biomasseV_SPart_GC_dicot = createSync("biomasseV_SPart_GC_dicot"); 
			biomasseV_SPart_GD_dicot = createSync("biomasseV_SPart_GD_dicot"); 
			biomasseV_SPart_L = createSync("biomasseV_SPart_L"); 	
			biomasseV_SPart_TB = createSync("biomasseV_SPart_TB"); 				 
			biomasseV_STot_GA = createSync("biomasseV_STot_GA"); 	
			biomasseV_STot_GB = createSync("biomasseV_STot_GB"); 
			biomasseV_STot_GC = createSync("biomasseV_STot_GC"); 
			biomasseV_STot_GD = createSync("biomasseV_STot_GD"); 
			biomasseV_STot_GA_dicot = createSync("biomasseV_STot_GA_dicot"); 	
			biomasseV_STot_GB_dicot = createSync("biomasseV_STot_GB_dicot"); 
			biomasseV_STot_GC_dicot = createSync("biomasseV_STot_GC_dicot"); 
			biomasseV_STot_GD_dicot = createSync("biomasseV_STot_GD_dicot");			
			digestibilite_GA = createSync("digestibilite_GA"); 	
			digestibilite_GB = createSync("digestibilite_GB"); 
			digestibilite_GC = createSync("digestibilite_GC"); 
			digestibilite_GD = createSync("digestibilite_GD");
			digestibilite_GA_dicot = createSync("digestibilite_GA_dicot"); 	
			digestibilite_GB_dicot = createSync("digestibilite_GB_dicot"); 
			digestibilite_GC_dicot = createSync("digestibilite_GC_dicot"); 
			digestibilite_GD_dicot = createSync("digestibilite_GD_dicot"); 
			digestibilite_L = createSync("digestibilite_L"); 	
			digestibilite_TB = createSync("digestibilite_TB"); 	
			MAT_A = createSync("MAT_A");
			MAT_A_dicot = createSync("MAT_A_dicot");
			MAT_B = createSync("MAT_B");
			MAT_B_dicot = createSync("MAT_B_dicot");
			MAT_C = createSync("MAT_C");
			MAT_C_dicot = createSync("MAT_C_dicot");
			MAT_D = createSync("MAT_D");
			MAT_D_dicot = createSync("MAT_D_dicot");
			MAT_L = createSync("MAT_L");
			MAT_TB = createSync("MAT_TB");

			pctA = (events.exist("pctA")) ? vv::toDouble(events.get("pctA")) : 100.0; 
			pctB = (events.exist("pctB")) ? vv::toDouble(events.get("pctB")) : 0.0; 
			pctC = (events.exist("pctC")) ? vv::toDouble(events.get("pctC")) : 0.0; 
			pctD = (events.exist("pctD")) ? vv::toDouble(events.get("pctD")) : 0.0; 
			dicot = (events.exist("dicot")) ? vv::toDouble(events.get("dicot")) : 0.0; 
			pctL = (events.exist("pctL")) ? vv::toDouble(events.get("pctL")) : 0.0; 
			pctTB = (events.exist("pctTB")) ? vv::toDouble(events.get("pctTB")) : 0.0; 
			
			a_digestibilite = (events.exist("a_digestibilite")) ? vv::toDouble(events.get("a_digestibilite")) : 0.63; 
			b_digestibilite = (events.exist("b_digestibilite")) ? vv::toDouble(events.get("b_digestibilite")) : 297.0; 
			a_ValeurEnergetique = (events.exist("a_ValeurEnergetique")) ? vv::toDouble(events.get("a_ValeurEnergetique")) : 1.0; 
			b_ValeurEnergetique = (events.exist("b_ValeurEnergetique")) ? vv::toDouble(events.get("b_ValeurEnergetique")) : 0.0; 

			pctA = pctA / 100;
			pctB = pctB / 100;
			pctC = pctC / 100;
			pctD = pctD / 100;
			pctL = pctL / 100;
			pctTB = pctTB / 100;
			dicot = dicot / 100;
			
			pctA_dicot = dicot * pctA;						
			pctB_dicot = dicot * pctB;
			pctC_dicot = dicot * pctC;
			pctD_dicot = dicot * pctD;
			
			pctA = (1 - dicot) * pctA;
			pctB = (1 - dicot) * pctB;
			pctC = (1 - dicot) * pctC;
			pctD = (1 - dicot) * pctD;
			
			double pctTot = pctA + pctA_dicot + pctB + pctB_dicot + pctC + pctC_dicot + pctD + pctD_dicot + pctL + pctTB;
			if (pctTot != 1.0 && pctTot != 0.0)
			{
				pctA = pctA / pctTot;
				pctA_dicot = pctA_dicot / pctTot;
				pctB = pctB / pctTot;
				pctB_dicot = pctB_dicot / pctTot;
				pctC = pctC / pctTot;
				pctC_dicot = pctC_dicot / pctTot;
				pctD = pctD / pctTot;
				pctD_dicot = pctD_dicot / pctTot;
				pctL = pctL / pctTot;
				pctTB = pctTB / pctTot;
			}
	    }


	    virtual ~Production() {};


    	virtual void compute(const vd::Time& /*time*/)
	    {
			calcul_Final_biomasseB();	
			calcul_Final_biomasseV_SPart();	
			calcul_Final_biomasseV_STot();
			calcul_Final_digestibiliteInVitro();
			calcul_Final_digestibilite();
			calcul_Final_ValeurEnergetique();
			calcul_Final_Matieres_Azotees();
			calcul_Final_CroissanceB();
			calcul_Final_CroissanceN_SPart();
			calcul_Final_CroissanceN_STot();
	   	}


	    virtual void initValue(const vd::Time& /* time */)
	    {
			Final_biomasseB = 0.0;
			Final_biomasseV_SPart = 0.0;
			Final_biomasseV_STot = 0.0;
			Final_digestibiliteInVitro = 0.0;
			Final_digestibilite = 0.0;
			Final_ValeurEnergetique = 0.0;
			Final_Matieres_Azotees = 0.0;
			Final_CroissanceB = 0.0;
			Final_CroissanceN_SPart = 0.0;
			Final_CroissanceN_STot = 0.0;
	    }


	private:
    	//Variables d'état
		Var Final_biomasseB;		//Biomasse brute moyenne moyenne
		Var Final_biomasseV_SPart;	//Biomasse verte partielle moyenne
		Var Final_biomasseV_STot;	//Biomasse verte totale moyenne
		Var Final_digestibilite;	//Digestibilité in "vivo" finale	
		Var Final_digestibiliteInVitro;	//Digestibilité théorique finale	
		Var Final_ValeurEnergetique;	//Digestibilité finale	
		Var Final_Matieres_Azotees;      //MAT finale	
		Var Final_CroissanceB;
		Var Final_CroissanceN_SPart;
		Var Final_CroissanceN_STot;	
		//Variables d'état issues d'autres modèles
		//Modèles graminées
		Sync biomasseB_GA;		//Biomasse brute issue du modèle relatif aux graminées de type A
		Sync biomasseB_GB; 		//Biomasse brute issue du modèle relatif aux graminées de type B
		Sync biomasseB_GC; 		//Biomasse brute issue du modèle relatif aux graminées de type C
		Sync biomasseB_GD;  		//Biomasse brute issue du modèle relatif aux graminées de type D
		Sync biomasseV_SPart_GA;	//Biomasse verte partielle issue du modèle relatif aux graminées de type A
		Sync biomasseV_SPart_GB; 	//Biomasse verte partielle issue du modèle relatif aux graminées de type B
		Sync biomasseV_SPart_GC; 	//Biomasse verte partielle issue du modèle relatif aux graminées de type C
		Sync biomasseV_SPart_GD;  	//Biomasse verte partielle issue du modèle relatif aux graminées de type D
		Sync biomasseV_STot_GA;		//Biomasse verte totale issue du modèle relatif aux graminées de type A
		Sync biomasseV_STot_GB; 	//Biomasse verte totale issue du modèle relatif aux graminées de type B
		Sync biomasseV_STot_GC; 	//Biomasse verte totale issue du modèle relatif aux graminées de type C
		Sync biomasseV_STot_GD; 	//Biomasse verte totale issue du modèle relatif aux graminées de type D
		Sync CroissanceB_GA; 	
		Sync CroissanceB_GB; 
		Sync CroissanceB_GC; 
		Sync CroissanceB_GD;
		Sync CroissanceB_GA_dicot; 	
		Sync CroissanceB_GB_dicot; 
		Sync CroissanceB_GC_dicot; 
		Sync CroissanceB_GD_dicot;
		Sync CroissanceB_L;
		Sync CroissanceB_TB;
		Sync CroissanceN_SPart_GA; 	
		Sync CroissanceN_SPart_GB; 
		Sync CroissanceN_SPart_GC; 
		Sync CroissanceN_SPart_GD;
		Sync CroissanceN_SPart_GA_dicot; 	
		Sync CroissanceN_SPart_GB_dicot; 
		Sync CroissanceN_SPart_GC_dicot; 
		Sync CroissanceN_SPart_GD_dicot;
		Sync CroissanceN_SPart_L;
		Sync CroissanceN_SPart_TB;
		Sync CroissanceN_STot_GA; 	
		Sync CroissanceN_STot_GB; 
		Sync CroissanceN_STot_GC; 
		Sync CroissanceN_STot_GD;
		Sync CroissanceN_STot_GA_dicot; 	
		Sync CroissanceN_STot_GB_dicot; 
		Sync CroissanceN_STot_GC_dicot; 
		Sync CroissanceN_STot_GD_dicot;
		Sync digestibilite_GA;		//Digestibilite issue du modèle relatif aux graminées de type A	
		Sync digestibilite_GB;		//Digestibilite issue du modèle relatif aux graminées de type A	
		Sync digestibilite_GC;		//Digestibilite issue du modèle relatif aux graminées de type A	
		Sync digestibilite_GD;		//Digestibilite issue du modèle relatif aux graminées de type A		
		Sync MAT_A;
		Sync MAT_B;
		Sync MAT_C;
		Sync MAT_D;
		//Modèles graminées avec dicotylédones
		Sync biomasseB_GA_dicot;	//Biomasse brute issue du modèle relatif aux graminées de type A (avec dicotylédones)
		Sync biomasseB_GB_dicot; 	//Biomasse brute issue du modèle relatif aux graminées de type B (avec dicotylédones)
		Sync biomasseB_GC_dicot; 	//Biomasse brute issue du modèle relatif aux graminées de type C (avec dicotylédones)
		Sync biomasseB_GD_dicot;  	//Biomasse brute issue du modèle relatif aux graminées de type D (avec dicotylédones)
		Sync biomasseV_SPart_GA_dicot;	//Biomasse verte partielle issue du modèle relatif aux graminées de type A (avec dicotylédones)
		Sync biomasseV_SPart_GB_dicot; 	//Biomasse verte partielle issue du modèle relatif aux graminées de type B (avec dicotylédones)
		Sync biomasseV_SPart_GC_dicot; 	//Biomasse verte partielle issue du modèle relatif aux graminées de type C (avec dicotylédones)
		Sync biomasseV_SPart_GD_dicot;  //Biomasse verte partielle issue du modèle relatif aux graminées de type D (avec dicotylédones)
		Sync biomasseV_STot_GA_dicot;	//Biomasse verte totale issue du modèle relatif aux graminées de type A (avec dicotylédones)
		Sync biomasseV_STot_GB_dicot; 	//Biomasse verte totale issue du modèle relatif aux graminées de type B (avec dicotylédones)
		Sync biomasseV_STot_GC_dicot; 	//Biomasse verte totale issue du modèle relatif aux graminées de type C (avec dicotylédones)
		Sync biomasseV_STot_GD_dicot;  	//Biomasse verte totale issue du modèle relatif aux graminées de type D (avec dicotylédones)
		Sync digestibilite_GA_dicot;	//Digestibilite issue du modèle relatif aux graminées de type A (avec dicotylédones)
		Sync digestibilite_GB_dicot; 	//Digestibilite issue du modèle relatif aux graminées de type B (avec dicotylédones)
		Sync digestibilite_GC_dicot; 	//Digestibilite issue du modèle relatif aux graminées de type C (avec dicotylédones)
		Sync digestibilite_GD_dicot;  	//Digestibilite issue du modèle relatif aux graminées de type D (avec dicotylédones)
		Sync MAT_A_dicot;
		Sync MAT_B_dicot;
		Sync MAT_C_dicot;
		Sync MAT_D_dicot;
		//Modèle luzerne
		Sync biomasseB_L;		//Biomasse brute issue du modèle relatif à la luzerne
		Sync biomasseV_SPart_L;		//Biomasse verte partielle issue du modèle relatif à la luzerne
		Sync digestibilite_L;		//Digestibilité issue du modèle relatif à la luzerne
		Sync MAT_L;
		//Modèle trèfle blanc
		Sync biomasseB_TB;		//Biomasse brute issue du modèle relatif aux trèfles blanc
		Sync biomasseV_SPart_TB; 	//Biomasse verte partielle issue du modèle relatif aux trèfles blanc
		Sync digestibilite_TB;		//Digestibilité issue du modèle relatif au trèfle blanc

		Sync MAT_TB;

		//Paramètres issus du VPZ
		double pctA;	//Pourcentage de graminées de type A
		double pctB;	//Pourcentage de graminées de type B
		double pctC; 	//Pourcentage de graminées de type C
		double pctD;	//Pourcentage de graminées de type D
		double dicot;	//Pourcentage de dicotylédones
		double pctL;	//Pourcentage de luzernes
		double pctTB;	//Pourcentage de trèfles blancs
		double a_digestibilite;
		double b_digestibilite;
		double a_ValeurEnergetique;
		double b_ValeurEnergetique;
		

		//Utilisés si présence de dicotylédone (voir formules)
		double pctA_dicot;	
		double pctB_dicot;	
		double pctC_dicot; 
		double pctD_dicot;	


		//Procédures de calcul

		void calcul_Final_biomasseB()	
		{
			Final_biomasseB =  ((biomasseB_GA()*pctA) + (biomasseB_GA_dicot()*pctA_dicot) + (biomasseB_GB()*pctB) + (biomasseB_GB_dicot()*pctB_dicot) + (biomasseB_GC()*pctC) + (biomasseB_GC_dicot()*pctC_dicot) + (biomasseB_GD()*pctD) + (biomasseB_GD_dicot()*pctD_dicot) + (biomasseB_L()*pctL) + (biomasseB_TB()*pctTB))*10;
		}
	
	
		void calcul_Final_biomasseV_SPart()		
		{
			Final_biomasseV_SPart = ((biomasseV_SPart_GA()*pctA) + (biomasseV_SPart_GA_dicot()*pctA_dicot) + (biomasseV_SPart_GB()*pctB) + (biomasseV_SPart_GB_dicot()*pctB_dicot) + (biomasseV_SPart_GC()*pctC) + (biomasseV_SPart_GC_dicot()*pctC_dicot) + (biomasseV_SPart_GD()*pctD) + (biomasseV_SPart_GD_dicot()*pctD_dicot) + (biomasseV_SPart_L()*pctL) + (biomasseV_SPart_TB()*pctTB))*10;
		}


		void calcul_Final_biomasseV_STot()	
		{
			Final_biomasseV_STot = ((biomasseV_STot_GA()*pctA) + (biomasseV_STot_GA_dicot()*pctA_dicot) + (biomasseV_STot_GB()*pctB) + (biomasseV_STot_GB_dicot()*pctB_dicot) + (biomasseV_STot_GC()*pctC) + (biomasseV_STot_GC_dicot()*pctC_dicot) + (biomasseV_STot_GD()*pctD) + (biomasseV_STot_GD_dicot()*pctD_dicot) + (biomasseV_SPart_L()*pctL) + (biomasseV_SPart_TB()*pctTB))*10;
		}


		void calcul_Final_digestibiliteInVitro()	 
		{
			Final_digestibiliteInVitro = (digestibilite_GA()*pctA) + (digestibilite_GA_dicot()*pctA_dicot) + (digestibilite_GB()*pctB) + (digestibilite_GB_dicot()*pctB_dicot) + (digestibilite_GC()*pctC) + (digestibilite_GC_dicot()*pctC_dicot) + (digestibilite_GD()*pctD) + (digestibilite_GD_dicot()*pctD_dicot) + (digestibilite_L()*pctL) + (digestibilite_TB()*pctTB);
		}

		void calcul_Final_Matieres_Azotees()	 
		{
			Final_Matieres_Azotees = (MAT_A()*pctA) + (MAT_A_dicot()*pctA_dicot) + (MAT_B()*pctB) + (MAT_B_dicot()*pctB_dicot) + (MAT_C()*pctC) + (MAT_C_dicot()*pctC_dicot) + (MAT_D()*pctD) + (MAT_D_dicot()*pctD_dicot) + (MAT_L()*pctL) + (MAT_TB()*pctTB);
		}



		void calcul_Final_CroissanceB()	 
		{
			Final_CroissanceB = (CroissanceB_GA()*pctA) + (CroissanceB_GA_dicot()*pctA_dicot) + (CroissanceB_GB()*pctB) + (CroissanceB_GB_dicot()*pctB_dicot) + (CroissanceB_GC()*pctC) + (CroissanceB_GC_dicot()*pctC_dicot) + (CroissanceB_GD()*pctD) + (CroissanceB_GD_dicot()*pctD_dicot) + (CroissanceB_L()*pctL) + (CroissanceB_TB()*pctTB);
		}

		void calcul_Final_CroissanceN_SPart()	 
		{
			Final_CroissanceN_SPart = (CroissanceN_SPart_GA()*pctA) + (CroissanceN_SPart_GA_dicot()*pctA_dicot) + (CroissanceN_SPart_GB()*pctB) + (CroissanceN_SPart_GB_dicot()*pctB_dicot) + (CroissanceN_SPart_GC()*pctC) + (CroissanceN_SPart_GC_dicot()*pctC_dicot) + (CroissanceN_SPart_GD()*pctD) + (CroissanceN_SPart_GD_dicot()*pctD_dicot) + (CroissanceN_SPart_L()*pctL) + (CroissanceN_SPart_TB()*pctTB);
		}

		void calcul_Final_CroissanceN_STot()	 
		{
			Final_CroissanceN_STot = (CroissanceN_STot_GA()*pctA) + (CroissanceN_STot_GA_dicot()*pctA_dicot) + (CroissanceN_STot_GB()*pctB) + (CroissanceN_STot_GB_dicot()*pctB_dicot) + (CroissanceN_STot_GC()*pctC) + (CroissanceN_STot_GC_dicot()*pctC_dicot) + (CroissanceN_STot_GD()*pctD) + (CroissanceN_STot_GD_dicot()*pctD_dicot);
		}

		void calcul_Final_digestibilite()	 
		{
			Final_digestibilite = a_digestibilite * Final_digestibiliteInVitro() + b_digestibilite;
		}

		void calcul_Final_ValeurEnergetique()	 
		{
			Final_ValeurEnergetique = a_ValeurEnergetique * Final_digestibiliteInVitro() + b_ValeurEnergetique;
		}


	};
}
DECLARE_DYNAMICS_DBG(herbsim::Production); // balise specifique VLE

