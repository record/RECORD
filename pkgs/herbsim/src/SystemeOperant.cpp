/**
 * @file SystemeOperant.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/devs/DynamicsDbg.hpp>

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

namespace vd = vd;
namespace vede = ve::DifferenceEquation;

enum SOState { DEC_INIT, DEC_COUPE, DEC_WAIT };

	class SystemeOperant: public ve::fsa::Statechart
	{
	public:
		SystemeOperant(const vd::DynamicsInit& atom,
		    const vd::InitEventList& events) :
		    Statechart(atom, events), Coupe(0), biomasseB(0), Tmoy(0)
		{
		    states(this) << DEC_INIT << DEC_COUPE << DEC_WAIT;
		    
		    
		    transition(this, DEC_COUPE, DEC_WAIT)
		        << action(&SystemeOperant::reset);
		        
			transition(this, DEC_WAIT, DEC_COUPE)
		        << guard(&SystemeOperant::coupe)
		        << action(&SystemeOperant::actionCoupe)
		        << send(&SystemeOperant::out_coupe);
	 
			transition(this, DEC_INIT, DEC_COUPE)
		        << guard(&SystemeOperant::coupe)
		        << send(&SystemeOperant::out_init);
	 
	 
		    initialState(DEC_INIT);
		    
		    
		    laiRes = (events.exist("laiRes")) ? vv::toDouble(events.get("laiRes")) : 1.0; 
		    hauteurInit = (events.exist("hauteurInit")) ? vv::toDouble(events.get("hauteurInit")) : 2.0; 
		    hauteur1 = (events.exist("hauteur1")) ? vv::toDouble(events.get("hauteur1")) : hauteurInit; 
		    hauteur2 = (events.exist("hauteur2")) ? vv::toDouble(events.get("hauteur2")) : hauteurInit; 
		    hauteur3 = (events.exist("hauteur3")) ? vv::toDouble(events.get("hauteur3")) : hauteurInit; 
		    hauteur4 = (events.exist("hauteur4")) ? vv::toDouble(events.get("hauteur4")) : hauteurInit; 
		    hauteur5 = (events.exist("hauteur5")) ? vv::toDouble(events.get("hauteur5")) : hauteurInit; 
		    hauteur6 = (events.exist("hauteur6")) ? vv::toDouble(events.get("hauteur6")) : hauteurInit; 
		    densiteInit = (events.exist("densiteInit")) ? vv::toDouble(events.get("densiteInit")) : 150.0; 
		    densite1 = (events.exist("densite1")) ? vv::toDouble(events.get("densite1")) : densiteInit; 
		    densite2 = (events.exist("densite2")) ? vv::toDouble(events.get("densite2")) : densiteInit; 
		    densite3 = (events.exist("densite3")) ? vv::toDouble(events.get("densite3")) : densiteInit; 
		    densite4 = (events.exist("densite4")) ? vv::toDouble(events.get("densite4")) : densiteInit; 
		    densite5 = (events.exist("densite5")) ? vv::toDouble(events.get("densite5")) : densiteInit; 
		    densite6 = (events.exist("densite6")) ? vv::toDouble(events.get("densite6")) : densiteInit; 
		 	
		    a_laiN = (events.exist("a_laiN")) ? vv::toDouble(events.get("a_laiN")) : 1.9; 
		    b_laiN = (events.exist("b_laiN")) ? vv::toDouble(events.get("b_laiN")) : 0.73; 
		    
		 	NbrCoupe = 0;
		 	hauteurCourante = hauteurInit;
		 	densiteCourante = densiteInit;
		 	bioresid = ResidusHauteur(hauteurCourante, densiteCourante);
		 	
		 	
		 	eventInState(this, "Coupe", &SystemeOperant::inCoupe)
				>> DEC_INIT >> DEC_WAIT;
		 	eventInState(this, "biomasseB", &SystemeOperant::inbiomasseB)
				>> DEC_WAIT;
		 	eventInState(this, "Tmoy", &SystemeOperant::inTmoy)
				>> DEC_INIT >> DEC_WAIT;
		}
		
		
		virtual ~SystemeOperant() {}
		
				
		//condition de Coupe 
		bool coupe(const vd::Time& /* julianDay */)
		{ return (Coupe == 1.0); }
		
		//perturbation lors de la coupe d'initialisation
		void out_init(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
		{
		    output << (vede::Var("hauteur") = hauteurCourante);
		    
		    output << (vede::Var("bioresid") = bioresid);
		    output << (vede::Var("biomasseB") = bioresid);
		    output << (vede::Var("biomasseV_SPart") = bioresid);
		    output << (vede::Var("biomasseV_STot") = bioresid);
		   
		    output << (vede::Var("laiN") = laiRes);
		    
		    output << (vede::Var("SomTmoyV") = Tmoy);  
		    output << (vede::Var("SomTmoyF") = Tmoy);  
		}
	 
		//perturbation lors des coupes
		void out_coupe(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
		{
		    output << (vede::Var("hauteur") = hauteurCourante);
		    
		    output << (vede::Var("bioresid") = bioresid);
		    output << (vede::Var("biomasseB") = bioresid);
		    output << (vede::Var("biomasseV_SPart") = bioresid);
		    output << (vede::Var("biomasseV_STot") = bioresid);
		   
		    output << (vede::Var("laiN") = a_laiN * pow( bioresid / 100, b_laiN) * bioresid / biomasseBmemory);
		    
		    output << (vede::Var("SomTmoyV") = Tmoy);  
		    
		    output << (vede::Var("CoupeApresTmont") = 1.0);  
		    
		    output << (vede::Var("cptDvf") = 1.0);  
		    output << (vede::Var("cptDvfmin") = 1.0);  
		    output << (vede::Var("cptDvfmax") = 1.0);  

		    output << (vede::Var("ack") = 1.0);  
		}
	 
		//action interne lors des coupes
	 	void actionCoupe(const vd::Time& /* time */)
	 	{
	 		NbrCoupe = NbrCoupe + 1;		
	 		hauteurCourante = HauteurCoupe(NbrCoupe);	 			
	 		densiteCourante = DensiteCoupe(NbrCoupe);	 			
	 		bioresid = ResidusHauteur(hauteurCourante, densiteCourante);
	 	}
	 	
	 	//remise a 0 de la variable d'entrée déclenchant la coupe (pour eviter une boucle de changement d'etat infini)
	 	void reset(const vd::Time& /* time */)
	 	{ Coupe = 0.0; }
	 
		virtual vv::Value* observation(const vd::ObservationEvent& /*event*/) const
		{ return vv::Integer::create(currentState()); }


		
	private :
		//ecoute du port d'entrée Coupe
		void inCoupe(const vd::Time& /* time */, const vd::ExternalEvent* event)
		{ Coupe << vede::Var("Coupe", event); }
	   
	   //ecoute du port d'entrée biomasseB
	   void inbiomasseB(const vd::Time& /* time */, const vd::ExternalEvent* event)
	   {
	     biomasseBmemory = biomasseB;
		 biomasseB << vede::Var("biomasseB", event); 
	   }
	   
	   //ecoute du port d'entrée Tmoy
	   void inTmoy(const vd::Time& /* time */, const vd::ExternalEvent* event)
	   { Tmoy << vede::Var("Tmoy", event); }

		//paramètres lu dans le vpz
		double hauteurInit;
		double hauteur1;
		double hauteur2;
		double hauteur3;
		double hauteur4;
		double hauteur5;
		double hauteur6;
		double laiRes;
		double dpot;
		double densiteInit;
		double densite1;
		double densite2;
		double densite3;
		double densite4;
		double densite5;
		double densite6;
	
		double a_laiN;
		double b_laiN;
		
		//paramèrtes locaux
		double bioresid;
		int NbrCoupe;
		double hauteurCourante;
		double densiteCourante;
		
		//inputs
		double Coupe;
		double biomasseB;
		double Tmoy;
		
		//variables locales
		double biomasseBmemory;
		
		//estimation de la biomasse residuelle en fonction de la hauteur de coupe
		double ResidusHauteur(double h, double d)
		{ return h * d; }
		
		//hauteur de la coupe i
		double HauteurCoupe(int i)
		{
	 		if (i == 1)
	 			return hauteur1;
	 		else if (i == 2)
	 			return hauteur2;
	 		else if (i == 3)
	 			return hauteur3;
	 		else if (i == 4)
	 			return hauteur4;
	 		else if (i == 5)
	 			return hauteur5;
	 		else
	 			return hauteur6;
		}

		//densite de la coupe i
		double DensiteCoupe(int i)
		{
	 		if (i == 1)
	 			return densite1;
	 		else if (i == 2)
	 			return densite2;
	 		else if (i == 3)
	 			return densite3;
	 		else if (i == 4)
	 			return densite4;
	 		else if (i == 5)
	 			return densite5;
	 		else
	 			return densite6;
		}

	};

}

DECLARE_DYNAMICS_DBG(herbsim::SystemeOperant);

