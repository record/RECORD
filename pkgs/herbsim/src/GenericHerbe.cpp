/**
 * @file src/GenericHerbe.cpp
 * @author (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/extension/difference-equation/Multiple.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs/DynamicsDbg.hpp>
#include <EspecesHerbe.hpp>


// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

    class GenericHerbe: public ve::DifferenceEquation::Multiple
    {
	public:
	    GenericHerbe(const vd::DynamicsInit& atom,
		   const vd::InitEventList& events) :
	ve::DifferenceEquation::Multiple(atom, events)
	    {
			//Variables d'etat
 			digestibilite = createVar("digestibilite"); 
 			MAT = createVar("MAT"); 
			hauteur = createVar("hauteur"); 
			PAR = createVar("PAR");
			croissanceB = createVar("croissanceB"); 
			biomasseB = createVar("biomasseB"); 
			croissanceN_STot = createVar("croissanceN_STot");
			croissanceN_SPart = createVar("croissanceN_SPart");
			biomasseV_SPart = createVar("biomasseV_SPart"); 
			biomasseV_STot = createVar("biomasseV_STot"); 
			senesTot = createVar("senesTot"); 
			senesPart = createVar("senesPart"); 
			bioresid = createVar("bioresid"); 
			cPhoto = createVar("cPhoto"); 
			cTemp = createVar("cTemp"); 
			cEau = createVar("cEau"); 
			cAzote = createVar("cAzote"); 
			coefRue = createVar("coefRue"); 
			rue = createVar("rue"); 
			ePhoto = createVar("ePhoto"); 
			eTemp = createVar("eTemp"); 
			eEau = createVar("eEau"); 
			eAzote = createVar("eAzote");  
			laiB = createVar("laiB"); 
			laiS = createVar("laiS");  
			laiN = createVar("laiN");  
			ef = createVar("ef"); 
			sEau = createVar("sEau"); 
			ETR = createVar("ETR"); 
			Hi = createVar("Hi"); 
			Tmoy = createVar("Tmoy"); 
			SomTmoyV = createVar("SomTmoyV"); 
			SomTmoyF = createVar("SomTmoyF"); 
			CoupeApresTmont = createVar("CoupeApresTmont"); 
			cptDvf = createVar("cptDvf"); 
			cptDvfmin = createVar("cptDvfmin"); 
			cptDvfmax = createVar("cptDvfmax"); 
			//inputs
			Ni = createSync("Ni");
			Rg = createSync("Rg");
			Pi = createSync("Pi");
			pluie = createSync("pluie");
			ru = createSync("ru");
			TmoyMeteo = createSync("TmoyMeteo"); 
			ETP = createSync("ETP"); 
			//paramètres
			tmont = (events.exist("tmont")) ? vv::toDouble(events.get("tmont")) : 500.0; 
			dpot = (events.exist("dpot")) ? vv::toDouble(events.get("dpot")) : 840.0; 
			pctA = (events.exist("pctA")) ? vv::toDouble(events.get("pctA")) : 100.0; 
			pctB = (events.exist("pctB")) ? vv::toDouble(events.get("pctB")) : 0.0; 
			ratioParRg = (events.exist("ratioParRg")) ? vv::toDouble(events.get("ratioParRg")) : 0.48;  			
			lambda = (events.exist("lambda")) ? vv::toDouble(events.get("lambda")) : 0.3;  			
			dvf = (events.exist("dvf")) ? vv::toDouble(events.get("dvf")) : 1000.0;
			dvfmin = (events.exist("dvfmin")) ? vv::toDouble(events.get("dvfmin")) : 500.0;
			dvfmax = (events.exist("dvfmax")) ? vv::toDouble(events.get("dvfmax")) : 750.0;
			tflor = (events.exist("tflor")) ? vv::toDouble(events.get("tflor")) : 1200.0;
			cfrue = (events.exist("cfrue")) ? vv::toDouble(events.get("cfrue")) : 1.0; 
			laiRes = (events.exist("laiRes")) ? vv::toDouble(events.get("laiRes")) : 1.0; 
			coefLai =  (events.exist("coefLai")) ? vv::toDouble(events.get("coefLai")) : 1.0; 
			tempBorneMin = (events.exist("tempBorneMin")) ? vv::toDouble(events.get("tempBorneMin")) : 0.0;
			tempBorneMax = (events.exist("tempBorneMax")) ? vv::toDouble(events.get("tempBorneMax")) : 18.0;
			//paramètres d'espece (graminee, luzerne ou trefle)
			espece = (events.exist("espece")) ? vv::toString(events.get("espece")) : "graminee";  		
			a_eTemp = (events.exist("a_eTemp")) ? vv::toDouble(events.get("a_eTemp")) : 0.000647;  			
			b_eTemp = (events.exist("b_eTemp")) ? vv::toDouble(events.get("b_eTemp")) : 0.0;  			
			c_eTemp = (events.exist("c_eTemp")) ? vv::toDouble(events.get("c_eTemp")) : 0.0;  			
			a_cPhoto = (events.exist("a_cPhoto")) ? vv::toDouble(events.get("a_cPhoto")) : -0.00085;  			
			b_cPhoto = (events.exist("b_cPhoto")) ? vv::toDouble(events.get("b_cPhoto")) : 1.026;  			
			a_cTemp = (events.exist("a_cTemp")) ? vv::toDouble(events.get("a_cTemp")) : -0.0022;  			
			b_cTemp = (events.exist("b_cTemp")) ? vv::toDouble(events.get("b_cTemp")) : 0.09;  			
			c_cTemp = (events.exist("c_cTemp")) ? vv::toDouble(events.get("c_cTemp")) : 0.037;  	

			a_ePhoto = (events.exist("a_ePhoto")) ? vv::toDouble(events.get("a_ePhoto")) : -0.00215;  			
			b_ePhoto = (events.exist("b_ePhoto")) ? vv::toDouble(events.get("b_ePhoto")) : 1.0625;  			
			xtinc = (events.exist("xtinc")) ? vv::toDouble(events.get("xtinc")) : -0.73;
			a_cAzote = (events.exist("a_cAzote")) ? vv::toDouble(events.get("a_cAzote")) : 0.8;  			
			b_cAzote = (events.exist("b_cAzote")) ? vv::toDouble(events.get("b_cAzote")) : 0.2;  			
		
			tableEspeces["graminee"] = GRAMINEE;		
			tableEspeces["luzerne"] = LUZERNE;		
			tableEspeces["trefle"] = TREFLE_BLANC;					
	    }


	    virtual ~GenericHerbe() {};


    	virtual void compute(const vd::Time& time)
	    {
//Temperature
			Tmoy = calcul_Tmoy();
			SomTmoyV = calcul_SomTmoyV();
			SomTmoyF = calcul_SomTmoyF();
//StressEau
			sEau = calcul_sEau();
			ETR = calcul_ETR();
			Hi = calcul_Hi();
//InterceptionLumiere
			ePhoto = calcul_ePhoto(time);
			eTemp = calcul_eTemp();
			eEau = calcul_eEau();
			eAzote = calcul_eAzote();
			laiB = calcul_laiB();
			laiS = calcul_laiS();
			laiN = calcul_laiN();
			ef = calcul_ef();
//ConversionLumiere
			cEau = calcul_cEau();
			cPhoto = calcul_cPhoto(time);
			cTemp = calcul_cTemp();
			cAzote = calcul_cAzote();
			
			switch (tableEspeces[espece]) {
				case GRAMINEE :
					calcul_coefRue_G();
					break;
				default :
					calcul_coefRue_TL();
					break;
			}
			
			rue = calcul_rue();
//Senescence
	    	bioresid = bioresid(-1);
			switch (tableEspeces[espece]) {
				case GRAMINEE :
					calcul_senes_G();
					break;
				default :
					calcul_senes_TL();
					break;
			}
//ConversionBiomasse
			PAR = calcul_PAR();
			croissanceB = calcul_croissanceB();
			biomasseB = calcul_biomasseB();
			croissanceN_SPart = calcul_croissanceN_SPart();
			croissanceN_STot = calcul_croissanceN_STot();
			biomasseV_SPart = calcul_biomasseV_SPart();
			biomasseV_STot = calcul_biomasseV_STot();
//Qualite
	    	hauteur = hauteur(-1);

			switch (tableEspeces[espece]) {
				case GRAMINEE :
					if (coefRue() == 1.0)
					{
						a_DigestRate = 0.0;
						b_DigestRate = -0.2;
					}
					else if (coefRue() > 1.0)
					{
						a_DigestRate = 0.47;
						b_DigestRate = -0.62 * (pct + 1);
					}
					else
					{
						a_DigestRate = 0.47 / 3;
						b_DigestRate = -0.62 * (pct + 1) / 3;
					}
					break;
				case LUZERNE :
					a_DigestRate = -0.1855;
					b_DigestRate = 0.0;
					break;
				case TREFLE_BLANC :
					a_DigestRate = -0.07;
					b_DigestRate = 0.0;
					break;
			}

			if (SomTmoyV()<SomTmoyV(-1))//detecte les coupe pour reinitialiser la digestibilite (devrais plutot etre dans le SystemeOperant, mais plus facile/rapide a mettre ici)
			{
				DPOT = dpot + a_DPOT + b_DPOT * Ni_local + c_DPOT * hauteur();
				digestibilite = DPOT;
			}
			else
				digestibilite = digestibilite(-1) + calcul_digestibilite();
			
			MAT = calcul_MAT();
    	}


	    virtual void initValue(const vd::Time& /* time */)
	    {
			//Initialisation des paramètres de sortie
			Tmoy = std::max(std::min(TmoyMeteo(), tempBorneMax), tempBorneMin); 
			SomTmoyV =  Tmoy();
			SomTmoyF = Tmoy();
//
			//Initialisation des paramètres de sortie
			sEau = ru(); 
			ETR = std::min(std::max(0.0, ETP()), sEau());
			if (ETP() <= 0.0)
				Hi = 1.0;
			else
				Hi = (ETR() / ETP());  
//
			ePhoto = 1.0; 
			eTemp = 1.0;
			eEau = 1.0; 
			eAzote = 1.0; 
			laiB = 0.0; 
			laiS = 0.0; 
			laiN = laiRes; 
			ef = 0.95 * (1.0 - exp(laiRes * -0.73)); 

//
			//Initialisation des paramètres de sortie
			cPhoto = 1.0;
			cTemp = 1.0;
			cEau = 1.0;
			cAzote = 1.0;
			coefRue = 1.0;
			rue = 0.0;
			CoupeApresTmont = 0.0;
			
//
			//Initialisation des paramètres de sortie
			bioresid = 1.0;
			senesTot = 0.0;
			senesPart = 0.0;
			cptDvf = 1.0;
			cptDvfmin = 1.0;
			cptDvfmax = 1.0;


			//Initialisation des paramètres locaux
			seuilDvfMinMontaison = 0;	//Variable de stockage du seuil dans lequel se situe tmont (sénéscence partielle)
			seuilDvfMaxMontaison = 0;	//Variable de stockage du seuil dans lequel se situe tmont (sénéscence totale)
			trouveTmontSPart = false;	//Au début de la simulation, on n'a bien sur pas trouvé tmont (sénéscence partielle)
			trouveTmontSTot = false; 	//Au début de la simulation, on n'a bien sur pas trouvé tmont (sénéscence totale)
			msdvfmin = 0;			//Variable de stockage de la biomasse verte partielle de la veille du seuil de dvf
			msdvfmax = 0;			//Variable de stockage de la biomasse verte totale de la veille du seuil de dvf
			msdvf = 0;			//Variable de stockage de la biomasse verte totale de la veille du seuil de dvf
//
			//Initialisation des paramètres de sortie
			PAR = Rg() * ratioParRg;
			croissanceB = 0.0;
			biomasseB = 0.0;
			croissanceN_STot = 0.0;
			croissanceN_SPart = 0.0;
			biomasseV_SPart = 0.0;
			biomasseV_STot = 0.0;
//
			//Initialisation des variables de sortie
			
			MAT = 0.0;
			hauteur = 2.0;
			//Initialisation des paramètres locaux
			pct = (pctA + pctB) / 100;

			
			switch (tableEspeces[espece]) {
				case GRAMINEE :
					DVF = dvfmax;
					Ni_local = Ni(); //attention si Ni pas constant a mettre dans le compute
					a_DPOT = -30.0;
					b_DPOT = 190.0;
					c_DPOT = -25.5;
					break;
				case LUZERNE :
					DVF = dvf;
					Ni_local = 1.0;
					a_DPOT = 0.0;
					b_DPOT = 0.0;
					c_DPOT = 0.0;
					break;	
				case TREFLE_BLANC :
					DVF = dvf;
					Ni_local = 1.0;
					a_DPOT = 0.0;
					b_DPOT = 0.0;
					c_DPOT = 0.0;
					break;	
			}
			DPOT = dpot + a_DPOT + b_DPOT * Ni_local + c_DPOT * hauteur();
			digestibilite = DPOT;


			//Initialisation des paramètres locaux
			//Variables permettant de calculer le coefRue
			moyenneMontflor = (tmont + tflor) / 2;
			npi = Ni_local * ((0.3 * Pi()) + 0.7); //ATTENTION si Ni et Pi ne sont pas constant a mettre dans le compute
			y3 = (npi * 1.19) + 0.59;
			ruePostFlo = std::max(0.0, 2 - y3);
			a = ((y3 - 1) * (tflor - tmont)) / ((pow(tflor, 2)-pow(tmont, 2)) * (tflor - moyenneMontflor) - (pow(tflor, 2) - pow(moyenneMontflor, 2)) *(tflor - tmont));
			b = (1 - y3 - a * (pow(tflor, 2) - pow(moyenneMontflor, 2))) / (tflor - moyenneMontflor);
			c = y3 - a * pow(moyenneMontflor, 2) - b * moyenneMontflor;
	    }


	private:
    	//Variables d'état
		Var digestibilite;	//Digestibilité
		Var MAT;		//matières azotées
		Var hauteur;		//Hauteur de coupe
		Var PAR; 		//Rayonnement photosynthétiquement actif
		Var croissanceB; 	//Croissance brute
		Var croissanceN_SPart; 	//Croissance brute moins la scénéscence (dvfmax)
		Var croissanceN_STot; 	//Croissance brute moins la scénéscence (dvfmin)
		Var biomasseB;  	//Production permise : biomasse totale brute
		Var biomasseV_SPart; 	//Biomasse brute moins la scénéscence appliquée à la totalité de la biomasse moins la proportion de tiges (dvfmax)
		Var biomasseV_STot; 	//Biomasse brute moins la scénéscence appliquée à la totalité de la biomasse (dvfmin)
    	Var bioresid;		//biomasse residuelle
    	Var senesTot;		//Sénescence totale
    	Var senesPart;		//Sénescence partielle
		Var cPhoto;		//Conversion de la photopériode
		Var cTemp; 		//Conversion de la température
		Var cEau; 		//Conversion de l'eau
		Var cAzote;		//Conversion de l'azote
		Var coefRue;		//Coefficient d'efficience d'utilisation du rayonnement
		Var rue;		//Efficience d'utilisation du rayonnement
		Var ePhoto; 	//Effets de la photopériode
		Var eTemp; 		//Effets de la température
		Var eEau; 		//Effets de l'eau
		Var eAzote; 	//Effets de l'azote
		Var laiB; 		//Surface foliaire brute
		Var laiS; 		//Surface foliaire senescente
		Var laiN; 		//Surface foliaire nette
		Var ef; 		//Efficience
		Var sEau; 		//Stock d'eau dans le sol
		Var ETR; 		//Evapotranspiration réèlle
		Var Hi; 		//Heat Index
		Var Tmoy;		//Température moyenne bornée
		Var SomTmoyV;		//Somme des températures (variables)
		Var SomTmoyF;		//Somme des températures (fixes)
		Var CoupeApresTmont;		//Indique si la coupe a eu lieu apres le debut de la montaison
		Var cptDvf;		//Indice de changement de la valeur de la biomasse de référence pour le calcul de la sénéscence (proc : calcul_senes())
		Var cptDvfmin;		//Indice de changement de la valeur de la biomasse de référence pour le calcul de la sénéscence (proc : calcul_senes())
		Var cptDvfmax;		//Indice de changement de la valeur de la biomasse de référence pour le calcul de la sénéscence (proc : calcul_senes())
		
		//Variables d'état issues d'autres modèles
		Sync Ni; 	//Température moyenne non bornée
		Sync Rg;		//Indice de rayonnement
		Sync Pi;		//Indice du phosphore
		Sync ru; 		//Réserve utile en eau
		Sync pluie;		//Indice de pluie 
 		Sync TmoyMeteo; 	//Température moyenne non bornée
		Sync ETP; 		//Evapotranspiration potentielle
		
		//Paramètres issus du VPZ
		double tmont;		//Valeur de la somme des températures à la montaison
		double dpot;		//Digestibilité potentielle
		double pctA;	//Pourcentage de graminées de type A
		double pctB;	//Pourcentage de graminées de type B
		double ratioParRg;
		double dvf;		//Durée de vie des feuilles (pour luzerne ou trefle)
		double dvfmin;		//Durée de vie minimale des feuilles (pour graminees)
		double dvfmax;		//Durée de vie maximale des feuilles (pour graminees)
		double lambda; 		//Coefficient de remobilisation de la matière organique au cours de la senescence
		double tflor;		//Valeur de la somme des températures à la floraison
		double cfrue; 		//Coefficient de conversion de l'énergie en biomasse 
		double laiRes; 		//LAI résiduel
		double coefLai; 	//Coefficient d'interception de la lumière
		double tempBorneMin;	//Minorant de la température
		double tempBorneMax;	//Majorant de la température
		std::string espece;	//Espece simulée (graminee ou luzerne ou trefle)
		double a_eTemp;
		double b_eTemp;
		double c_eTemp;
		double a_cPhoto;
		double b_cPhoto;
		double a_cTemp;
		double b_cTemp;
		double c_cTemp;
		double a_ePhoto;
		double b_ePhoto;
		double xtinc;
		double a_cAzote;
		double b_cAzote;
		
		
		//paramètre local
		double pct;		//Pourcentage de graminée dans la prairie
		double seuilDvfMinMontaison;	//Utilisé pour le calcul de la senescence : intervalle ou se situe la montaison pour le calcul avec dvfmin
		double seuilDvfMaxMontaison;	//Utilisé pour le calcul de la senescence : intervalle ou se situe la montaison pour le calcul avec dvfmax
		double msdvf;		//Utilisé pour le calcul de la senescence avec dvf : valeur de la biomasse
		double msdvfmin;		//Utilisé pour le calcul de la senescence avec dfvmin : valeur de la biomasse 
		double msdvfmax;		//Utilisé pour le calcul de la senescence avec dvfmax : valeur de la biomasse
		bool trouveTmontSTot;		//Utilisé pour le calcul de la senescence : TRUE si on a dépassé tmont
		bool trouveTmontSPart;		//Utilisé pour le calcul de la senescence : TRUE si on a dépassé tmont
		double npi;			//Indice pondéré de l'indice de nutrition azoté en fonction du niveau de nutrition du phosphore
		double y3;			//Utilisé pour le calcul du coefRue
		double moyenneMontflor;		//Utilisé pour le calcul du coefRue
		double ruePostFlo;		//Utilisé pour le calcul du coefRue
		double a;			//Utilisé pour le calcul du coefRue
		double b;			//Utilisé pour le calcul du coefRue
		double c;			//Utilisé pour le calcul du coefRue

		std::map<std::string, Especes> tableEspeces;
		double DVF; //durée de vie des feuilles
		double Ni_local; // pour les legumineuses (luzerne et trefle blanc) on garde Ni=1

		double DPOT;
		double a_DPOT;
		double b_DPOT;
		double c_DPOT;
		double a_DigestRate;
		double b_DigestRate;

		double calcul_Tmoy()
		{ return std::max(std::min(TmoyMeteo(), tempBorneMax), tempBorneMin); /*On borne la température*/}

		double calcul_SomTmoyV()
		{ return SomTmoyV(-1) + Tmoy(); }

		double calcul_SomTmoyF()
		{ return SomTmoyF(-1) + Tmoy(); }

		double calcul_sEau()
		{
			double tmp = sEau(-1) + pluie(-1) - std::max(0.0, ETP(-1)); 
			if(tmp > ru()) 
				return ru(); 
			else if(ETP(-1) > sEau(-1)) 
				return pluie(-1); 
			else 
				return std::max(0.0, tmp); 
		}

		double calcul_ETR()
		{ return std::min(std::max(0.0, ETP()), sEau()); }

		double calcul_Hi()
		{
			double A = (ETR() / ETP()); 
				if (A <= 1.0 && A >= 0.7)
					return 1.0;
				else if (A <= 0.2)
					return 0.0;
				else if (A > 0.2 && A < 0.7)
					return 2*A-0.4;
				else
					return A;  
		}

		double calcul_ePhoto(const vd::Time& time)
		{ return std::max(0.0, std::min(1.0,a_ePhoto * vle::utils::DateTime::dayOfYear(time) + b_ePhoto)); }

		double calcul_eTemp()
		{ return std::max(0.0, std::min(1.0, a_eTemp * pow(Tmoy(), 2) + b_eTemp * Tmoy() + c_eTemp)); }

		double calcul_eEau()
		{ return std::max(0.0, std::min(1.0,1.0 * Hi())); }

		double calcul_eAzote()
		{ return std::max(0.0, std::min(1.0,1.0 * Ni_local)); }
		
		double calcul_laiB()
		{ return std::max(0.0, ePhoto() * eTemp() * eEau() * eAzote() * coefLai ); }
		
		double calcul_laiS()
		{ return laiRes * (Tmoy() / DVF); }

		double calcul_laiN()
		{ return std::max(0.0, laiN(-1) + laiB() - laiS()); }
		
		double calcul_ef()
		{ return std::max(0.0, std::min(1.0,0.95 * (1.0 - exp(laiN() * xtinc)))); }
		
		double calcul_cEau()
		{ return std::max(0.0, std::min(1.0,Hi() * 1.0)); }
		
		double calcul_cPhoto(const vd::Time& time)
		{ return std::max(0.0, std::min(1.0,a_cPhoto * vle::utils::DateTime::dayOfYear(time) + b_cPhoto)); }
		
		double calcul_cTemp()
		{ return std::max(0.0, std::min(1.0, a_cTemp * pow(Tmoy(), 2) + b_cTemp * Tmoy() + c_cTemp)); }
		
		double calcul_cAzote()
		{ return std::max(0.0, std::min(1.0,npi * a_cAzote + b_cAzote)); }
		
		void calcul_coefRue_G()
		{
			if(SomTmoyF() <= tmont)
			{
				coefRue = 1.0;
				CoupeApresTmont = 0.0;
			}
			else if ( SomTmoyF() < tflor )
			{
				CoupeApresTmont = CoupeApresTmont(-1);
				if (CoupeApresTmont() == 0.0)
					coefRue = std::max(0.0, a * (pow(SomTmoyF(), 2)) + b * SomTmoyF() + c);
				else
					coefRue = 1.0;
			}
			else
			{
				CoupeApresTmont = CoupeApresTmont(-1);
				if (CoupeApresTmont() == 0.0)
					coefRue = ruePostFlo;
				else
					coefRue = 1.0;
			}
		}
		
		void calcul_coefRue_TL()
		{ 
			coefRue = 1.3; 
			CoupeApresTmont = 0.0;
		}
		
		double calcul_rue()
		{ return 2.0 * cPhoto() * cTemp() * cAzote() *  cEau() * coefRue() * cfrue; }
		
		void calcul_senes_G()
		{
			double BioM;
			
			//Vérification de la présence de tmont dans l'intervalle
			if(!trouveTmontSPart && tmont <= dvfmin * cptDvfmin(-1)) //Si on a pas encore trouvé tmont dans l'intervalle de dvfmin actuel //Si tmont se trouve avant le prochain dvfmin
			{
				trouveTmontSPart = true; //Alors tmont se situe dans l'intervalle [position actuelle, dvfminCourant]
				seuilDvfMinMontaison = dvfmin * cptDvfmin(-1); //On sauvegarde le premier dvfmin se situant aprés la montaison
			}
			if(SomTmoyV() >= dvfmin * cptDvfmin(-1))
			{
				msdvfmin = biomasseV_SPart(-1); //On récupère la valeur de la biomasse de la veille
				cptDvfmin = cptDvfmin(-1) + 1.0;
			}
			else
				cptDvfmin = cptDvfmin(-1);

			if(SomTmoyV() < dvfmin)//Cas ou on se situe avant dvfmin
				BioM = bioresid();
			else
			{
				if(!trouveTmontSPart) //Si on n'a pas trouvé tmont
					BioM = msdvfmin;
				else //Si on a déjà trouve tmont
				{
					if(SomTmoyV() < seuilDvfMinMontaison) //Si la valeur de la somme des températures variables courante est inférieure au seuil de montaison
						BioM = msdvfmin;
					else
						BioM = 0.824 * pow((msdvfmin / 100), -0.42) * msdvfmin;
				}
			}
			senesPart = std::max((1 - lambda) * BioM * (Tmoy()/ dvfmin), 0.0);
			
			
			//Vérification de la présence de tmont dans l'intervalle
			if(!trouveTmontSTot && tmont <= dvfmax * cptDvfmax(-1)) //Si on a pas encore trouvé tmont dans l'intervalle de dvfmax actuel //Si tmont se trouve avant le prochain dvfmin
			{
				trouveTmontSTot = true; //Alors tmont se situe dans l'intervalle [position actuelle, dvfminCourant]
				seuilDvfMaxMontaison = dvfmax * cptDvfmax(-1); //On sauvegarde le premier dvfmin se situant aprés la montaison
			}
			if(SomTmoyV() >= dvfmax * cptDvfmax(-1))
			{
				msdvfmax = biomasseV_STot(-1); //On récupère la valeur de la biomasse de la veille
				cptDvfmax = cptDvfmax(-1) + 1.0;
			}
			else
				cptDvfmax = cptDvfmax(-1);

			if(SomTmoyV() < dvfmax)//Cas ou on se situe avant dvfmin
				BioM = bioresid();
			else
			{
				if(!trouveTmontSTot) //Si on n'a pas trouvé tmont
					BioM = msdvfmax;
				else //Si on a déjà trouve tmont
				{
					if(SomTmoyV() < seuilDvfMaxMontaison) //Si la valeur de la somme des températures variables courante est inférieure au seuil de montaison
						BioM = msdvfmax;
					else
						BioM = 0.824 * pow((msdvfmax / 100), -0.42) * msdvfmax;
				}
			}
			senesTot = std::max((1 - lambda) * BioM * (Tmoy()/ dvfmax), 0.0);
			
		}
		
		void calcul_senes_TL()
		{
			if(SomTmoyV() < dvf)
				msdvf = bioresid();
			
			if(SomTmoyV() >= dvf * cptDvf(-1)) //Prise en compte du changement de dvf et de biomasse de référence 
			{
				msdvf = biomasseV_SPart(-1);
				cptDvf = cptDvf(-1) + 1.0;				
			}
			else
				cptDvf = cptDvf(-1);
			
			senesPart = (1 - lambda) * 0.824 * pow(msdvf / 100, -0.42) * msdvf * Tmoy() / dvf;
			senesTot = (1 - lambda) * 0.824 * pow(msdvf / 100, -0.42) * msdvf * Tmoy() / dvf;
		}
		
		double calcul_PAR()
		{ return std::max(0.0, Rg() * ratioParRg); }
		
		double calcul_croissanceB()
		{ return std::max(0.0, PAR() * rue() * ef()); }
		
		double calcul_biomasseB()
		{ return std::max(0.0, biomasseB(-1) + croissanceB()); }
		
		double calcul_croissanceN_SPart()
		{ return std::max(-biomasseV_SPart(-1), croissanceB() - senesPart()); }
		
		double calcul_croissanceN_STot()
		{ return std::max(-biomasseV_STot(-1), croissanceB() - senesTot()); }
		
		double calcul_biomasseV_SPart()
		{ return std::max(0.0, biomasseV_SPart(-1) + croissanceN_SPart()); }
		
		double calcul_biomasseV_STot()
		{ return std::max(0.0, biomasseV_STot(-1) + croissanceN_STot()); }
		
		double calcul_digestibilite()
		{ return a_DigestRate * Tmoy() + b_DigestRate * Tmoy() * Ni_local; }
				
		double calcul_MAT()
		{ return ( Ni_local * 4.8 * pow(biomasseB()/100, -0.32)) * 62.5 ; }
	//	{ return (10 * Ni_local * 4.8 * pow(biomasseB(), -0.32)) * 62.5 ; } ancienne formul
	};
}
DECLARE_DYNAMICS_DBG(herbsim::GenericHerbe); // balise specifique VLE

