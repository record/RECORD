/**
 * @file DecFSA.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/fsa/Statechart.hpp>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/Trace.hpp>
#include <vle/utils/i18n.hpp>
#include <vle/devs/DynamicsDbg.hpp>

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

enum DecState { DEC_INIT, DEC_COUPE_1, DEC_COUPE_2, DEC_COUPE_3, DEC_COUPE_4, DEC_COUPE_5, DEC_COUPE_6, DEC_END};

class DecFSA: public ve::fsa::Statechart
{
	public:
		DecFSA(const vd::DynamicsInit& atom,
		    const vd::InitEventList& events) :
		    Statechart(atom, events), SomTmoyV(0)
		{
		    states(this) << DEC_INIT << DEC_COUPE_1 << DEC_COUPE_2 
		    << DEC_COUPE_3 << DEC_COUPE_4 << DEC_COUPE_5 
		    << DEC_COUPE_6 << DEC_END;
		    
		    initialState(DEC_INIT);
		  
		    eventInState(this, "SomTmoyV", &DecFSA::inSomTmoyV)
				>> DEC_INIT >> DEC_COUPE_1 >> DEC_COUPE_2 
		    >> DEC_COUPE_3 >> DEC_COUPE_4 >> DEC_COUPE_5 
		    >> DEC_COUPE_6 >> DEC_END;
		  
		  
		    Date1 = (events.exist("Date1")) ? vv::toInteger(events.get("Date1")) : 0;  
		    Date2 = (events.exist("Date2")) ? vv::toInteger(events.get("Date2")) : 0;  
		    Date3 = (events.exist("Date3")) ? vv::toInteger(events.get("Date3")) : 0;  
		    Date4 = (events.exist("Date4")) ? vv::toInteger(events.get("Date4")) : 0;  
		    Date5 = (events.exist("Date5")) ? vv::toInteger(events.get("Date5")) : 0;  
		    Date6 = (events.exist("Date6")) ? vv::toInteger(events.get("Date6")) : 0;  
		  
		    SeuilSomTmoy1 = (events.exist("SeuilSomTmoy1")) ? vv::toDouble(events.get("SeuilSomTmoy1")) : 0.0;  
		    SeuilSomTmoy2 = (events.exist("SeuilSomTmoy2")) ? vv::toDouble(events.get("SeuilSomTmoy2")) : 0.0;  
		    SeuilSomTmoy3 = (events.exist("SeuilSomTmoy3")) ? vv::toDouble(events.get("SeuilSomTmoy3")) : 0.0;  
		    SeuilSomTmoy4 = (events.exist("SeuilSomTmoy4")) ? vv::toDouble(events.get("SeuilSomTmoy4")) : 0.0;  
		    SeuilSomTmoy5 = (events.exist("SeuilSomTmoy5")) ? vv::toDouble(events.get("SeuilSomTmoy5")) : 0.0;  
		    SeuilSomTmoy6 = (events.exist("SeuilSomTmoy6")) ? vv::toDouble(events.get("SeuilSomTmoy6")) : 0.0;  

		    TypeScenario = (events.exist("TypeScenario")) ? vv::toString(events.get("TypeScenario")) : "DatesFixes";  
		  	
		  	
		  	if (TypeScenario == "DatesFixes" && Date1 == 0)
		  		TypeScenario = "NoCoupe";
		  	if (TypeScenario == "SeuilsSomTmoy" && SeuilSomTmoy1 == 0.0)
		  		TypeScenario = "NoCoupe";
		  	if (TypeScenario == "DatesFixes" && Date1 == 0 && SeuilSomTmoy1 > 0.0)
		  		TypeScenario = "SeuilsSomTmoy";
		  		 
		  
		  	if (TypeScenario == "DatesFixes")
		  	{//Type Dates fixes
				transition(this, DEC_INIT, DEC_COUPE_1)
				    << guard(&DecFSA::initial)
				    << send(&DecFSA::out_coupe);
				    
				transition(this, DEC_COUPE_1, DEC_COUPE_2)
				    << guard(&DecFSA::coupe1_DF)
				    << send(&DecFSA::out_coupe);
				if ( Date2 > Date1 )
				{ 
					transition(this, DEC_COUPE_2, DEC_COUPE_3)
						<< guard(&DecFSA::coupe2_DF)
						<< send(&DecFSA::out_coupe);
				}
				if ( Date3 > Date2 )
				{ 
				transition(this, DEC_COUPE_3, DEC_COUPE_4)
				    << guard(&DecFSA::coupe3_DF)
				    << send(&DecFSA::out_coupe);  
				}
				if ( Date4 > Date3 )
				{ 
       
				transition(this, DEC_COUPE_4, DEC_COUPE_5)
				    << guard(&DecFSA::coupe4_DF)
				    << send(&DecFSA::out_coupe);
				}
				if ( Date5 > Date4 )
				{ 

				transition(this, DEC_COUPE_5, DEC_COUPE_6)
				    << guard(&DecFSA::coupe5_DF)
				    << send(&DecFSA::out_coupe);
				}
				if ( Date6 > Date5 )
				{ 

				transition(this, DEC_COUPE_6, DEC_END)
				    << guard(&DecFSA::coupe6_DF)
				    << send(&DecFSA::out_coupe);  
				}
		    }
		  	if (TypeScenario == "SeuilsSomTmoy")
		  	{//Type pheno (precoce/tardif)
				transition(this, DEC_INIT, DEC_COUPE_1)
				    << guard(&DecFSA::initial)
				    << send(&DecFSA::out_coupe);
				    
				transition(this, DEC_COUPE_1, DEC_COUPE_2)
				    << guard(&DecFSA::coupe1_P)
				    << send(&DecFSA::out_coupe)
		        	<< action(&DecFSA::reset);
				if ( SeuilSomTmoy2 > 0.0 )
				{ 
					transition(this, DEC_COUPE_2, DEC_COUPE_3)
						<< guard(&DecFSA::coupe2_P)
						<< send(&DecFSA::out_coupe)
			        	<< action(&DecFSA::reset);
				}
				if ( SeuilSomTmoy3 > 0.0 )
				{ 
					transition(this, DEC_COUPE_3, DEC_COUPE_4)
						<< guard(&DecFSA::coupe3_P)
						<< send(&DecFSA::out_coupe) 
			        	<< action(&DecFSA::reset);
				}
				if ( SeuilSomTmoy4 > 0.0 )
				{       
					transition(this, DEC_COUPE_4, DEC_COUPE_5)
						<< guard(&DecFSA::coupe4_P)
						<< send(&DecFSA::out_coupe)
			        	<< action(&DecFSA::reset);
				}
				if ( SeuilSomTmoy5 > 0.0 )
				{ 
					transition(this, DEC_COUPE_5, DEC_COUPE_6)
						<< guard(&DecFSA::coupe5_P)
						<< send(&DecFSA::out_coupe)
			        	<< action(&DecFSA::reset);
				}
				if ( SeuilSomTmoy6 > 0.0 )
				{ 
					transition(this, DEC_COUPE_6, DEC_END)
						<< guard(&DecFSA::coupe6_P)
						<< send(&DecFSA::out_coupe)
			        	<< action(&DecFSA::reset);
				}
		    }
		  	if (TypeScenario == "NoCoupe")
		  	{		    
				transition(this, DEC_INIT, DEC_COUPE_1)
				    << guard(&DecFSA::initial)
				    << send(&DecFSA::out_coupe);
			}		    
		    
		}
		
		
		virtual ~DecFSA() {}
			
			
	private :
			
			//regles de decision
		//initialisation au 1er Fev
		bool initial(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == 32);
		}
		//Coupe 1
		bool coupe1_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date1);
		}
		bool coupe1_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy1);
		}
		//Coupe 2
		bool coupe2_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date2);
		}
		bool coupe2_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy2);
		}
		//Coupe 3
		bool coupe3_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date3);
		}
		bool coupe3_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy3);
		}
		//Coupe 4
		bool coupe4_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date4);
		}
		bool coupe4_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy4);
		}
		//Coupe 5
		bool coupe5_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date5);
		}
		bool coupe5_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy5);
		}
		//Coupe 6
		bool coupe6_DF(const vd::Time& julianDay)
		{
		    return (vle::utils::DateTime::dayOfYear(julianDay) == Date6);
		}
		bool coupe6_P(const vd::Time& /*julianDay*/)
		{
			return (SomTmoyV >= SeuilSomTmoy6);
		}
		
		
			//evenement lors des coupes
		void out_coupe(const vd::Time& /*julianDay*/, vd::ExternalEventList& output) const
		{
		    output << (ve::DifferenceEquation::Var("Coupe") = 1.0);
		}

			//ecoutes des ports inputs
		void inSomTmoyV(const vd::Time& /* time */, const vd::ExternalEvent* event)
		{ 
			SomTmoyV << ve::DifferenceEquation::Var("SomTmoyV", event); 
		}

			//actions
		void reset(const vd::Time& /* time */)
	 	{
	 		SomTmoyV = 0.0;
	 	}


		virtual vv::Value* observation(const vd::ObservationEvent& /*event*/) const
		{
		   return vv::Integer::create(currentState());
		}
		

		//parametres
		unsigned int Date1;
		unsigned int Date2;
		unsigned int Date3;
		unsigned int Date4;
		unsigned int Date5;
		unsigned int Date6;
		
		double SeuilSomTmoy1;
		double SeuilSomTmoy2;
		double SeuilSomTmoy3;
		double SeuilSomTmoy4;
		double SeuilSomTmoy5;
		double SeuilSomTmoy6;
		
		std::string TypeScenario;
		
		//inputs
		double SomTmoyV;

	};

}

DECLARE_DYNAMICS_DBG(herbsim::DecFSA);
