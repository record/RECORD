/**
 * @file src/MeteoAjustement.cpp
 * @author (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/extension/difference-equation/Multiple.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs/DynamicsDbg.hpp>


// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

    class MeteoAjustement: public ve::DifferenceEquation::Multiple
    {
	public:
	    MeteoAjustement(const vd::DynamicsInit& atom,
		   const vd::InitEventList& events) :
	ve::DifferenceEquation::Multiple(atom, events)
	    {
 			TmoyMeteo = createVar("TmoyMeteo"); 
 			Rg = createVar("Rg"); 

			TmoyMeteo_Original = createSync("TmoyMeteo_Original");
			Rg_Original = createSync("Rg_Original");
			
			Altitude = (events.exist("Altitude")) ? vv::toDouble(events.get("Altitude")) : 0.0;
			RefAltitude = (events.exist("RefAltitude")) ? vv::toDouble(events.get("RefAltitude")) : 0.0;
			TAltCoef = (events.exist("TAltCoef")) ? vv::toDouble(events.get("TAltCoef")) : 0.006;
			IncRadAltCoef = (events.exist("IncRadAltCoef")) ? vv::toDouble(events.get("IncRadAltCoef")) : 0.0001;
			orientation = (events.exist("orientation")) ? vv::toString(events.get("orientation")) : "plat";
			
			if ( orientation == "sud" )
				IncRadAspectCoef = 1.18;
			else if ( orientation == "nord" )
				IncRadAspectCoef = 0.82;
			else
				IncRadAspectCoef = 1.0;	
	    }


	    virtual ~MeteoAjustement() {};


    	virtual void compute(const vd::Time& /*time*/)
	    {
	    	TmoyMeteo = TmoyMeteo_Original() + (RefAltitude - Altitude) * TAltCoef;
	    	
	    	Rg = Rg_Original() * ( IncRadAspectCoef - (RefAltitude - Altitude) * IncRadAltCoef );
    	}


	    virtual void initValue(const vd::Time& /* time */)
	    {
			//Initialisation des paramètres de sortie
			TmoyMeteo = 0.0;
			Rg = 0.0;
	    }


	private:
    	//Variables d'état
		Var TmoyMeteo;	//Température moyenne
		Var Rg;		//Radiation solaire globale
		
		//Variables d'état issues d'autres modèles
        Sync TmoyMeteo_Original;	//Température moyenne
		Sync Rg_Original; 	//Radiation solaire globale
		
		//Paramètres issus du VPZ
		double Altitude;		//Altitude voulu pour la correction
		double RefAltitude;		//Altitude du fichier meteo de référence
		double TAltCoef;	//
		double IncRadAltCoef;	//
		std::string orientation;
		//paramètre local
		double IncRadAspectCoef;
		
	};
}
DECLARE_DYNAMICS_DBG(herbsim::MeteoAjustement); // balise specifique VLE

