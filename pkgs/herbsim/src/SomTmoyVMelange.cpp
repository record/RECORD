/**
 * @file src/SomTmoyVMelange.cpp
 * @author (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/extension/difference-equation/Multiple.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/devs/DynamicsDbg.hpp>


// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

    class SomTmoyVMelange: public ve::DifferenceEquation::Multiple
    {
	public:
	    SomTmoyVMelange(const vd::DynamicsInit& atom,
		   const vd::InitEventList& events) :
	ve::DifferenceEquation::Multiple(atom, events)
	    {
	    	SomTmoyV = createVar("SomTmoyV"); 
	    	SomTmoyF = createVar("SomTmoyF"); 

			SomTmoyV_GA = createSync("SomTmoyV_GA");
			SomTmoyV_GB = createSync("SomTmoyV_GB");
			SomTmoyV_GC = createSync("SomTmoyV_GC");
			SomTmoyV_GD = createSync("SomTmoyV_GD");
			SomTmoyV_GADicot = createSync("SomTmoyV_GADicot");
			SomTmoyV_GBDicot = createSync("SomTmoyV_GBDicot");
			SomTmoyV_GCDicot = createSync("SomTmoyV_GCDicot");
			SomTmoyV_GDDicot = createSync("SomTmoyV_GDDicot");
			
			pctA = (events.exist("pctA")) ? vv::toDouble(events.get("pctA")) : 100.0; 
			pctB = (events.exist("pctB")) ? vv::toDouble(events.get("pctB")) : 0.0; 
			pctC = (events.exist("pctC")) ? vv::toDouble(events.get("pctC")) : 0.0; 
			pctD = (events.exist("pctD")) ? vv::toDouble(events.get("pctD")) : 0.0; 
			dicot = (events.exist("dicot")) ? vv::toDouble(events.get("dicot")) : 0.0; 

			pctA = pctA / 100;
			pctB = pctB / 100;
			pctC = pctC / 100;
			pctD = pctD / 100;
			dicot = dicot / 100;
			
			pctA_dicot = dicot * pctA;						
			pctB_dicot = dicot * pctB;
			pctC_dicot = dicot * pctC;
			pctD_dicot = dicot * pctD;
			
			pctA = (1 - dicot) * pctA;
			pctB = (1 - dicot) * pctB;
			pctC = (1 - dicot) * pctC;
			pctD = (1 - dicot) * pctD;
	    }


	    virtual ~SomTmoyVMelange() {};


    	virtual void compute(const vd::Time& /*time*/)
	    {
	    	double SomTmoyVMelange_temp = -1.0;
	    	
	    	if ( pctD >= pctA && pctD >= pctA_dicot && pctD >= pctB && pctD >= pctB_dicot && pctD >= pctC && pctD >= pctC_dicot && pctD >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GD();	    
	    	if ( pctD_dicot >= pctA && pctD_dicot >= pctA_dicot && pctD_dicot >= pctB && pctD_dicot >= pctB_dicot && pctD_dicot >= pctC && pctD_dicot >= pctC_dicot && pctD_dicot >= pctD )
				SomTmoyVMelange_temp = SomTmoyV_GDDicot();	    
	    	if ( pctC >= pctA && pctC >= pctA_dicot && pctC >= pctB && pctC >= pctB_dicot && pctC >= pctC_dicot && pctC >= pctD && pctC >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GC();	    
	    	if ( pctB >= pctA && pctB >= pctA_dicot && pctB >= pctB_dicot && pctB >= pctC && pctB >= pctC_dicot && pctB >= pctD && pctB >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GB();	    
	    	if ( pctC_dicot >= pctA && pctC_dicot >= pctA_dicot && pctC_dicot >= pctB && pctC_dicot >= pctB_dicot && pctC_dicot >= pctC && pctC_dicot >= pctD && pctC_dicot >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GCDicot();	    
	    	if ( pctA >= pctA_dicot && pctA >= pctB && pctA >= pctB_dicot && pctA >= pctC && pctA >= pctC_dicot && pctA >= pctD && pctA >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GA();	    
	    	if ( pctB_dicot >= pctA && pctB_dicot >= pctA_dicot && pctB_dicot >= pctB && pctB_dicot >= pctC && pctB_dicot >= pctC_dicot && pctB_dicot >= pctD && pctB_dicot >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GBDicot();	    
	    	if ( pctA_dicot >= pctA && pctA_dicot >= pctB && pctA_dicot >= pctB_dicot && pctA_dicot >= pctC && pctA_dicot >= pctC_dicot && pctA_dicot >= pctD && pctA_dicot >= pctD_dicot )
				SomTmoyVMelange_temp = SomTmoyV_GADicot();	    
	    	
	    	SomTmoyV = SomTmoyVMelange_temp;
	    	
	    	if ( SomTmoyV()<SomTmoyV(-1) )
	    		SomTmoyF = SomTmoyF(-1) + SomTmoyV();
	    	else
	    		SomTmoyF = SomTmoyF(-1) + (SomTmoyV() - SomTmoyV(-1));
    	}


	    virtual void initValue(const vd::Time& /* time */)
	    {
			//Initialisation des paramètres de sortie
			SomTmoyV = 0.0;
			SomTmoyF = 0.0;
	    }


	private:
    	//Variables d'état
		Var SomTmoyV;	//Somme de température du melange
		Var SomTmoyF;	//Somme de température du melange
		
		//Variables d'état issues d'autres modèles
        Sync SomTmoyV_GA;	//Somme de température des graminées A
        Sync SomTmoyV_GADicot;	//Somme de température des graminées A (dicotyledones)
        Sync SomTmoyV_GB;	//Somme de température des graminées B
        Sync SomTmoyV_GBDicot;	//Somme de température des graminées B (dicotyledones)
        Sync SomTmoyV_GC;	//Somme de température des graminées C
        Sync SomTmoyV_GCDicot;	//Somme de température des graminées C (dicotyledones)
        Sync SomTmoyV_GD;	//Somme de température des graminées D
        Sync SomTmoyV_GDDicot;	//Somme de température des graminées D (dicotyledones)
		
		//Paramètres issus du VPZ
		double pctA;	//Pourcentage de graminées de type A
		double pctB;	//Pourcentage de graminées de type B
		double pctC; 	//Pourcentage de graminées de type C
		double pctD;	//Pourcentage de graminées de type D
		double dicot;	//Pourcentage de dicotylédones

		//Utilisés si présence de dicotylédone (voir formules)
		double pctA_dicot;	
		double pctB_dicot;	
		double pctC_dicot; 
		double pctD_dicot;	
		
	};
}
DECLARE_DYNAMICS_DBG(herbsim::SomTmoyVMelange); // balise specifique VLE

