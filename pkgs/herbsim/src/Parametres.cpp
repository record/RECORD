/**
 * @file src/Parametres.cpp
 * @author (The RECORD team -INRA )
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <vle/extension/difference-equation/Multiple.hpp>
#include <vle/devs/DynamicsDbg.hpp>

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

namespace herbsim {

    class Parametres: public ve::DifferenceEquation::Multiple
    {
	public:
	    Parametres(const vd::DynamicsInit& atom,
		   const vd::InitEventList& events) :
	ve::DifferenceEquation::Multiple(atom, events)
	    {
			//jourAnnee = createVar("jourAnnee"); 
			ru = createVar("ru"); 
			Ni = createVar("Ni"); 
			Pi = createVar("Pi"); 

			VPZ_ru = (events.exist("VPZ_ru")) ? vv::toDouble(events.get("VPZ_ru")) : 100.0;  			
			VPZ_Ni = (events.exist("VPZ_Ni")) ? vv::toDouble(events.get("VPZ_Ni")) : 1.0;  			
			VPZ_Pi = (events.exist("VPZ_Pi")) ? vv::toDouble(events.get("VPZ_Pi")) : 1.0;  			
	    }


	    virtual ~Parametres() {};


    	virtual void compute(const vd::Time& /*time*/)
	    {
			ru = ru(-1);
			Ni = Ni(-1);
			Pi = Pi(-1); 	
		}


	    virtual void initValue(const vd::Time& /* time */)
	    {
			ru = VPZ_ru;
			Ni = VPZ_Ni;
			Pi = VPZ_Pi; 	
	    }


	private:
    	//Variables d'état		
		Var ru; 	//Réserve utile en eau 
		Var Ni;		//Indice d'azote
		Var Pi;		//Indice du phosphore

		//Paramètres issus du VPZ	
		double VPZ_ru; 		//Réserve utile en eau 
		double VPZ_Ni;		//Indice d'azote
		double VPZ_Pi;		//Indice du phosphore

	};
}
DECLARE_DYNAMICS_DBG(herbsim::Parametres); // balise specifique VLE

